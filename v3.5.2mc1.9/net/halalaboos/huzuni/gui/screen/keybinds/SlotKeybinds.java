package net.halalaboos.huzuni.gui.screen.keybinds;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.gui.render.Theme;
import net.halalaboos.huzuni.api.mod.Keybind;
import net.halalaboos.huzuni.gui.clickable.slot.SlotComponent;
import net.halalaboos.huzuni.util.Timer;

public class SlotKeybinds extends SlotComponent<Keybind> {

	private int selectedItem = -1;
	private Timer timer = new Timer();
	
	public SlotKeybinds() {
		super(0, 0);
		
	}
	
	@Override
	protected void render(Theme theme, int index, Keybind keybind, float[] area, boolean highlight, boolean mouseOver,
			boolean mouseDown) {
		
		theme.renderSlot(this.getOffsetX(), this.getOffsetY(), index, area, selectedItem == index, mouseOver, mouseDown);
		Huzuni.drawStringWithShadow(keybind.getName(), area[0] + 2, area[1] + 2, 0xFFFFFF);
		Huzuni.drawStringWithShadow("\2477" + keybind.getDescription(), area[0] + 2, area[1] + 12, 0xFFFFFF);
		Huzuni.drawStringWithShadow("\2477Key:", area[0] + 2, area[1] + 22, 0xFFFFFF);
		Huzuni.drawStringWithShadow(keybind.getKeyName(), area[0] + Huzuni.getStringWidth("Key:") + 2, area[1] + 22, Huzuni.getColorTheme().getRGB());
	}

	@Override
	public int getComponentHeight(Keybind keybind) {
		return 32;
	}
	
	@Override
	protected void onReleased(int index, Keybind keybind, int mouseX,
			int mouseY) {
	}

	@Override
	protected void onClicked(int index, Keybind keybind, float[] area,
			int mouseX, int mouseY, int buttonId) {
		if (selectedItem == index) {
			if (timer.getTimePassed() < 500) {
				selectedItem = -1;
			}
		} else
			selectedItem = index;
		
		timer.reset();
	}

	@Override
	public void keyTyped(int keyCode, char c) {
		super.keyTyped(keyCode, c);
		if (selectedItem != -1) {
			Keybind keybind = getComponents().get(selectedItem);
			keybind.setKeybind(keyCode);
			selectedItem = -1;
		}
	}
	
	public boolean isSelected() {
		return selectedItem >= 0 && selectedItem < getComponents().size();
	}

	public boolean isKeybindableSelected() {
		return isSelected() && getComponents().get(selectedItem) instanceof Keybind;
	}
	
	public void setKeybind(int keyCode) {
		((Keybind) getComponents().get(selectedItem)).setKeybind(keyCode);
		selectedItem = -1;
	}

}
