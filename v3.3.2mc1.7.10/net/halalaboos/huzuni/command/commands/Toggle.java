package net.halalaboos.huzuni.command.commands;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.console.Command;
import net.halalaboos.huzuni.api.plugin.Plugin;
import net.halalaboos.huzuni.plugin.PluginManager;
import net.halalaboos.huzuni.util.StringUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.event.ClickEvent;
import net.minecraft.event.HoverEvent;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;

/**
 * @author brudin
 * @version 1.0
 * @since 3/26/14
 */
public class Toggle implements Command {
	@Override
	public String[] getAliases() {
		return new String[] { "toggle", "t"};
	}

	@Override
	public String[] getHelp() {
		return new String[] { "toggle <mod name>"};
	}

	@Override
	public String getDescription() {
		return "Toggles the specified plugin.";
	}

	@Override
	public void run(String input, String[] args) {
		String pluginName = StringUtils.getAfter(input, 1);
		Plugin plugin = PluginManager.getPlugin(pluginName);
		plugin.toggle();
		Minecraft.getMinecraft().ingameGUI.getChatGUI().func_146227_a(getToggleMessage(plugin, Huzuni.COMMAND_PREFIX, pluginName));
	}

	private ChatComponentText getToggleMessage(Plugin plugin, String prefix, String name) {
		ChatComponentText output = new ChatComponentText(EnumChatFormatting.DARK_GREEN + "[H] " + EnumChatFormatting.RESET + plugin.getName() + " toggled " + (plugin.isEnabled() ? EnumChatFormatting.DARK_GREEN + "on" : EnumChatFormatting.RED + "off") + EnumChatFormatting.RESET + ".");
		output.getChatStyle().setChatHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ChatComponentText("\247oClick to toggle again.")));
		output.getChatStyle().setChatClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, prefix + "toggle " + name));
		return output;
	}
}
