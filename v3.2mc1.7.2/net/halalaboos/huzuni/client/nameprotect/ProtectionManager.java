package net.halalaboos.huzuni.client.nameprotect;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.client.Settings;
import net.halalaboos.huzuni.events.game.EventGameEnd;
import net.halalaboos.huzuni.events.game.EventGameStartup;
import net.halalaboos.huzuni.events.game.EventSendMessage;
import net.halalaboos.huzuni.events.render.EventPreRenderNamePlate;
import net.halalaboos.lib.io.FileUtils;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.StringUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public final class ProtectionManager implements Listener {
    private final File saveFile = new File(Huzuni.getSaveDirectory(), "Protection.txt");
    private final Map<String, String> protectionMap = new ConcurrentHashMap<String, String>();

    public ProtectionManager() {
        super();
        registry.registerListener(this);
    }

    public void add(String username, String alias) {
        username = StringUtils.stripControlCodes(username);
        for (Entry<String, String> entry : protectionMap.entrySet())
            if (entry.getValue().equalsIgnoreCase(alias))
                remove(entry.getKey());

        protectionMap.put(username, alias);
    }

    public String remove(String username) {
        username = StringUtils.stripControlCodes(username);
        return protectionMap.remove(username);
    }

    public boolean contains(String username) {
        username = StringUtils.stripControlCodes(username);
        for (String user : protectionMap.keySet())
            if (user.equalsIgnoreCase(username))
                return true;
        return protectionMap.containsKey(username);
    }

    public void clear() {
        protectionMap.clear();
    }

    /**
     * @return input with regex replaced; ignoring all casing.
     */
    public String replaceIgnoreCase(String input, String regex, String replacement) {
        return input.replaceAll("(?i)" + regex, replacement);
    }
    
    /**
     * 
     * */
    public String replaceAliases(String text) {
    	if (Settings.isHuzuniEnabled()) {
	    	// Loop through each key (username)
	        for (String username : getProtectionKeySet()) {
	            // Grab the alias
	            final String alias = get(username);
	            // Replace all usernames with the alias.
	            text = replaceIgnoreCase(text, username, EnumChatFormatting.BLUE + alias + EnumChatFormatting.WHITE);
	        }
    	}
        return text;
    }
    
    @EventTarget
    public void onRenderNamePlate(EventPreRenderNamePlate event) {
        // Loop through each key (username)
        for (String username : getProtectionKeySet()) {
            // Grab the alias
            final String alias = get(username);
            // Replace all usernames with the alias.
            event.setTitleRendered(replaceIgnoreCase(event.getTitleRendered(), username, EnumChatFormatting.BLUE + alias + EnumChatFormatting.WHITE));
        }
    }

    @EventTarget
    public void onSendMessage(EventSendMessage event) {
        // Loop through each key (username)
        for (String username : getProtectionKeySet()) {
            // Grab the alias
            final String alias = get(username);
            // Replace all alias's with the username. This is to
            event.setMessage(replaceIgnoreCase(event.getMessage(), "-" + alias, username));
        }
    }
    
    /**
     * @return alias assigned to this username.
     */
    public String get(String username) {
        return protectionMap.get(username);
    }

    /**
     * @return keys. (usernames within the event map)
     */
    public Set<String> getProtectionKeySet() {
        return protectionMap.keySet();
    }

    public Map<String, String> getProtectionMap() {
        return protectionMap;
    }

    @EventTarget
    public void onGameStartup(EventGameStartup event) {
    	load();
    }
    
    @EventTarget
    public void onGameEnd(EventGameEnd event) {
    	save();
    }
    
    public void load() {
        if (!saveFile.exists()) {
            try {
                saveFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return;
        }
        clear();
        for (String line : FileUtils.readFile(saveFile)) {
            try {
                String username = line.split(":")[0],
                        alias = line.split(username + ":")[1];
                add(username, alias);
            } catch (Exception e) {/* Bad line */}
        }
    }
    
	public void save() {
		if (!saveFile.exists())
			try {
				saveFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		List<String> lines = new ArrayList<String>();
		// For each key
		for (String username : protectionMap.keySet()) {
			String alias = protectionMap.get(username);
			lines.add(username + ":" + alias);
		}
		FileUtils.writeFile(saveFile, lines);
	}
}
