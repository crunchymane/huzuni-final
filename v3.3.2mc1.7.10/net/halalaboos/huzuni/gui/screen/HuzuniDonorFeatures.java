/**
 * 
 */
package net.halalaboos.huzuni.gui.screen;

import pw.brudin.huzuni.util.screen.PanoramaRenderer;
import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.util.GLUtils;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;

/**
 * @author Halalaboos
 *
 */
public class HuzuniDonorFeatures extends GuiScreen {
	
	private final GuiScreen lastScreen;
    private PanoramaRenderer panoramaRenderer;
	
	/**
	 * @param lastScreen
	 */
	public HuzuniDonorFeatures(GuiScreen lastScreen) {
		this.lastScreen = lastScreen;
	}

	@Override
    public void initGui() {
        panoramaRenderer = new PanoramaRenderer(width, height);
        panoramaRenderer.init();
        buttonList.add(new GuiButton(0, this.width / 2 - 100, this.height - 28, 200, 20, "Done"));
	}
	
	@Override
    protected void actionPerformed(GuiButton button) {
        switch (button.id) {
            case 0:
            	mc.displayGuiScreen(lastScreen);
            	break;
            default:
            	break;
        }
	}

    @Override
    public void updateScreen(){
        panoramaRenderer.panoramaTick();
    }
	
	@Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        panoramaRenderer.renderSkybox(mouseX, mouseY, partialTicks);
        GLUtils.drawRect(0, 0, width, 30, 0x4F000000);
        Huzuni.drawString("THANK YOU", 2, 2, 0xFFFFFF);
        Huzuni.drawString("Thank you very much for donating. I appreciate it greatly.", 2, 20, 0xFFFFFF);
        int incrementFeature = 13, incrementCategory = 16;
        
        int y = 37;
        int x = width / 2 - 140;

        GLUtils.drawRect(width / 2 - 145, y - 4, width / 2 + 145, height - 40, 0x4F000000);
        Huzuni.drawStringWithShadow("Donor Features", x, y, 0xFFFFFF);
        y += incrementCategory + 5;
        
        String[] donorFeatures = new String[] {
    			"A wicked cape, for all your friends to see."
        };
        for (String feature : donorFeatures) {
            Huzuni.drawStringWithShadow(feature, x, y, 0xFFFFFF);
            y += incrementFeature;
        }
        
        y += incrementCategory;
        Huzuni.drawStringWithShadow("VIP Features", x, y, 0xFFFFFF);
        y += incrementCategory + 5;
        
        String[] vipFeatures = new String[] {
    			"Enhance Command: A fun way to crash others in creative!",
        		"Auto Build Mod: Auto-Builds in a pre-defined template.",
    			"Chest Stealer Mod: Because shift clicking is just too difficult.",
        		"Anti Censor Mod: Because \247mfuck\247r shrek you.",
        		"Dolan Speak Mod: maeks yu sund lik dolun",
        		"Fast Ladder Mod: Climb ladders faster"
        };
        for (String feature : vipFeatures) {
            Huzuni.drawStringWithShadow(feature, x, y, 0xFFFFFF);
            y += incrementFeature;
        }
        super.drawScreen(mouseX, mouseY, partialTicks);
        panoramaRenderer.renderFade();
	}
	
}
