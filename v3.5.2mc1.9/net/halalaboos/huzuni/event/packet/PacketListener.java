package net.halalaboos.huzuni.event.packet;

import net.halalaboos.huzuni.api.event.Listener;
import net.halalaboos.huzuni.event.packet.PacketEvent.PacketType;

public interface PacketListener extends Listener {

	void onPacket(PacketEvent event, PacketType type);
	
}
