package net.halalaboos.huzuni.plugin.plugins.player;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.plugin.Category;
import net.halalaboos.huzuni.event.packet.PacketEvent;
import net.halalaboos.huzuni.event.packet.PacketListener;
import net.halalaboos.huzuni.plugin.DefaultPlugin;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import net.minecraft.network.play.client.C0APacketAnimation;
import net.minecraft.network.play.server.S0EPacketSpawnObject;
import net.minecraft.network.play.server.S12PacketEntityVelocity;

/**
 * @author brudin
 * @version 1.0
 * @since 3/31/14
 */
public class AutoFish extends DefaultPlugin implements PacketListener {

	private int idFish;

	public AutoFish() {
		super("Auto Fish");
		setDescription("Automatically fishes for you.");
		setAuthor("brudin");
		setCategory(Category.PLAYER);
	}

	@Override
	public void onPacket(PacketEvent event, PacketEvent.PacketType type) {
		if(type == PacketEvent.PacketType.READ) {
			if(event.getPacket() instanceof S0EPacketSpawnObject) {
				S0EPacketSpawnObject packet = (S0EPacketSpawnObject)event.getPacket();
				if(packet.func_148993_l() == 90 && packet.func_149009_m() == mc.thePlayer.getEntityId()) {
					idFish = packet.func_149001_c();
				}
			} else if(event.getPacket() instanceof S12PacketEntityVelocity) {
				S12PacketEntityVelocity packetEntityVelocity = (S12PacketEntityVelocity)event.getPacket();
				if(packetEntityVelocity.func_149412_c() == idFish && packetEntityVelocity.func_149410_e() != 0 && packetEntityVelocity.func_149411_d() == 0 && packetEntityVelocity.func_149409_f() == 0) {
					recastRod();
					idFish = -420;
				}
			}
		}
	}

	private void recastRod() {
		C0APacketAnimation packetAnimation = new C0APacketAnimation(mc.thePlayer, 1);
		C08PacketPlayerBlockPlacement packetPlace = new C08PacketPlayerBlockPlacement(-1, -1, -1, 255, mc.thePlayer.getCurrentEquippedItem(), 0F, 0F, 0F);

		mc.thePlayer.sendQueue.addToSendQueue(packetAnimation);
		mc.thePlayer.sendQueue.addToSendQueue(packetPlace);

		mc.thePlayer.sendQueue.addToSendQueue(packetAnimation);
		mc.thePlayer.sendQueue.addToSendQueue(packetPlace);
	}

	@Override
	protected void onEnable() {
		Huzuni.registerPacketListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterPacketListener(this);
	}
}
