package net.halalaboos.huzuni.utils;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import javax.swing.SwingUtilities;

import net.minecraft.client.Minecraft;
import net.minecraft.util.HttpUtil;
import net.minecraft.util.Session;

public final class LoginUtils {

    private LoginUtils() {
    }

    public static String[] loginToMinecraft(String username, String password) {
        final HashMap parameters = new HashMap();
        parameters.put("user", username);
        parameters.put("password", password);
        parameters.put("version", Integer.valueOf(13));
        String response;

        try {
            response = HttpUtil.func_151226_a(new URL(
                    "http://login.minecraft.net/"), parameters, true);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return new String[] { "We've fucked up." };
        }

        if (response != null && response.length() > 0) {
        	return response.split(":");
        } else
            return new String[] { "Can't connect to 'minecraft.net'." };
    }
    
    public static void login(final Minecraft mc, final String username, final String password) {
    	SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    String[] login = LoginUtils.loginToMinecraft(username, password);
                    // ONLY if we got a good login status from the server.
                    if (login.length >= 5) {
                    	mc.setSession(new Session(login[2].trim(), login[4].trim(), login[3].trim()));
                    }
                } catch (Exception e) {
                	e.printStackTrace();
                }
            }
        });
    }
}
