package net.halalaboos.huzuni.plugin.plugins.player;

import org.lwjgl.input.Keyboard;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.plugin.Category;
import net.halalaboos.huzuni.api.plugin.ValueOption;
import net.halalaboos.huzuni.event.packet.PacketEvent;
import net.halalaboos.huzuni.event.packet.PacketEvent.PacketType;
import net.halalaboos.huzuni.event.packet.PacketListener;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent;
import net.halalaboos.huzuni.event.tick.UpdateListener;
import net.halalaboos.huzuni.plugin.DefaultPlugin;
import net.halalaboos.huzuni.plugin.value.Value;
import net.halalaboos.huzuni.plugin.value.ValueManager;
import net.minecraft.block.Block;
import net.minecraft.network.play.client.C07PacketPlayerDigging;

public class Speedmine extends DefaultPlugin implements PacketListener, UpdateListener {

	private boolean digging = false;
	
	private float curBlockDamage = 0;
	
	private Block block;
	
	private int x, y, z, side;

	private final Value speed = new Value("Mine Speed", 1F, 1.3F, 2F);

	public Speedmine() {
		super("Speedmine", Keyboard.KEY_V);
		setCategory(Category.PLAYER);
		setAuthor("Halalaboos");
		setDescription("Mines blocks faster.");
		ValueManager.add(speed);
		this.setOptions(new ValueOption(speed));
	}

	@Override
	public void onPacket(PacketEvent event, PacketType type) {
		if (type == PacketType.SENT && !mc.playerController.isInCreativeMode()) {
			if (event.getPacket() instanceof C07PacketPlayerDigging) {
				C07PacketPlayerDigging packet = (C07PacketPlayerDigging) event.getPacket();
				int id = packet.func_149506_g();
				if (id == 0) {
					digging = true;
					block = mc.theWorld.getBlock(packet.func_149505_c(), packet.func_149503_d(), packet.func_149502_e());
					this.x = packet.func_149505_c(); 
					this.y = packet.func_149503_d();
					this.z = packet.func_149502_e();
					this.side = packet.func_149501_f();
					this.curBlockDamage = 0;
				} else if (id == 1 || id == 2) {
					digging = false;
					this.x = 0;
					this.y = 0;
					this.z = 0;
					this.side = 0;
					block = null;
				}
			}
		}
	}

	@Override
	protected void onEnable() {
		Huzuni.registerPacketListener(this);
		Huzuni.registerUpdateListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterPacketListener(this);
		Huzuni.unregisterUpdateListener(this);
	}

	@Override
	public void onPreUpdate(MotionUpdateEvent event) {
		if (digging) {
			curBlockDamage += block.getPlayerRelativeBlockHardness(this.mc.thePlayer, this.mc.thePlayer.worldObj, x, y, z) * (speed.getValue());
			if (curBlockDamage >= 1.0F) {
				mc.playerController.onPlayerDestroyBlock(x, y, z, side);
				mc.getNetHandler().addToSendQueue(new C07PacketPlayerDigging(2, x, y, z, side));
				curBlockDamage = 0F;
				digging = false;
			}
		}
	}

	@Override
	public void onPostUpdate(MotionUpdateEvent event) {
	}

}
