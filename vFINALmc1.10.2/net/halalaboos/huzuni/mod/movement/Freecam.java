package net.halalaboos.huzuni.mod.movement;

import org.lwjgl.input.Keyboard;

import com.mojang.authlib.GameProfile;

import net.halalaboos.huzuni.api.event.EventPacket;
import net.halalaboos.huzuni.api.event.EventPlayerMove;
import net.halalaboos.huzuni.api.event.EventManager.EventMethod;
import net.halalaboos.huzuni.api.mod.BasicMod;
import net.halalaboos.huzuni.api.mod.Category;
import net.halalaboos.huzuni.api.settings.Value;
import net.minecraft.client.entity.EntityOtherPlayerMP;
import net.minecraft.network.play.client.CPacketEntityAction;
import net.minecraft.network.play.client.CPacketPlayer;

/**
 * Allows the player to fly freely from their body and explore the world.
 * */
public class Freecam extends BasicMod {
	
	public static final Freecam INSTANCE = new Freecam();
	
	public final Value speed = new Value("Speed", "", 0.1F, 1F, 10F, "movement speed");
	
    private EntityOtherPlayerMP fakePlayer;
	
	private Freecam() {
		super("Freecam", "Allows an individual to fly FROM THEIR BODY?", Keyboard.KEY_U);
		this.setCategory(Category.MOVEMENT);
		addChildren(speed);
	}
	
	@Override
	public void toggle() {
		super.toggle();
		if (mc.thePlayer != null && mc.theWorld != null) {
	        if (isEnabled()) {
	            fakePlayer = new EntityOtherPlayerMP(mc.theWorld, new GameProfile(mc.thePlayer.getUniqueID(), mc.thePlayer.getName()));
	            fakePlayer.copyLocationAndAnglesFrom(mc.thePlayer);
				fakePlayer.inventory = mc.thePlayer.inventory;
	            fakePlayer.setPositionAndRotation(mc.thePlayer.posX, mc.thePlayer.posY, mc.thePlayer.posZ, mc.thePlayer.rotationYaw, mc.thePlayer.rotationPitch);
	            fakePlayer.rotationYawHead = mc.thePlayer.rotationYawHead;
				mc.theWorld.addEntityToWorld(-69, fakePlayer);
				mc.thePlayer.capabilities.isFlying = true;
	        } else {
	        	if (fakePlayer != null && mc.thePlayer != null) {
	        		mc.thePlayer.setPositionAndRotation(fakePlayer.posX, fakePlayer.posY, fakePlayer.posZ, fakePlayer.rotationYaw, fakePlayer.rotationPitch);
	            	mc.theWorld.removeEntityFromWorld(-69);
	            	mc.thePlayer.capabilities.isFlying = false;
	        	}
	        	 if (mc.thePlayer != null)
	                 mc.thePlayer.capabilities.isFlying = false;
	        }
		}
    }
	
	@Override
	public void onEnable() {
		huzuni.eventManager.addListener(this);
	}
	
	@Override
	public void onDisable() {
		huzuni.eventManager.removeListener(this);
	}

	@EventMethod
	public void onPacket(EventPacket event) {
		if (event.type == EventPacket.Type.SENT) {
			if (event.getPacket() instanceof CPacketPlayer)
				event.setCancelled(true);
			if (event.getPacket() instanceof CPacketEntityAction) {
				CPacketEntityAction packet = (CPacketEntityAction) event.getPacket();
				if (packet.getAction() != CPacketEntityAction.Action.OPEN_INVENTORY)
					event.setCancelled(true);
			}
			mc.thePlayer.setSprinting(false);
			mc.thePlayer.renderArmPitch += 200;
			mc.thePlayer.renderArmYaw += 180;
			mc.thePlayer.capabilities.isFlying = true;
			if (fakePlayer != null)
				fakePlayer.setHealth(mc.thePlayer.getHealth());
		}
	}

	@EventMethod
	public void onPlayerMove(EventPlayerMove event) {
		event.setMotionX(event.getMotionX() * speed.getValue());
		event.setMotionY(event.getMotionY() * speed.getValue());
		event.setMotionZ(event.getMotionZ() * speed.getValue());
	}

}
