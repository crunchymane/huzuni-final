package net.halalaboos.huzuni.gui.clickable;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

import net.halalaboos.huzuni.api.gui.Component;
import net.halalaboos.huzuni.api.gui.Container;
import net.halalaboos.huzuni.api.gui.layout.Layout;
import net.halalaboos.huzuni.api.gui.render.Theme;
import net.halalaboos.huzuni.util.GLUtils;

public abstract class DefaultContainer implements Container {

	private List<Component> components = new ArrayList<Component>();

	private Layout layout;
	
	private float x, y, width, height;

	private boolean mouseOver, mouseDown, active = true;

	private Component mousedComponent, downComponent, selectedComponent;
	
	private int[] offset = new int[] { 0, 0 };
	
	private boolean layering = true;
		
	public DefaultContainer(Layout layout) {
		this.layout = layout;
	}

	@Override
	public boolean mouseClicked(int x, int y, int buttonId) {
		boolean inside = x > this.x && x < this.x + this.width && y > this.y && y < this.y + this.height;
		for (int i = components.size() - 1; i >= 0; i--) {
			Component component = components.get(i);
			if (component.mouseClicked(x, y, buttonId)) {
				inside = true;
				selectedComponent = component;
				if (downComponent == null) {
					if (layering) {
						components.remove(component);
						components.add(components.size(), component);
					}
					downComponent = component;
				}
				return true;
			}
		}
		return inside;
	}

	@Override
	public void mouseReleased(int x, int y, int buttonId) {
		if (downComponent != null) {
			downComponent.mouseReleased(x, y, buttonId);
			downComponent = null;
		}
	}

	@Override
	public void keyTyped(int keyCode, char c) {
		if (selectedComponent != null && downComponent == null) {
			selectedComponent.keyTyped(keyCode, c);
		}
	}

	@Override
	public void renderComponents(Theme theme) {
		for (int i = 0; i < components.size(); i++) {
			Component component = components.get(i);
			component.setMouseOver(this.mouseOver && (downComponent == null ? mousedComponent == component : downComponent == component && mousedComponent == component));
			component.setMouseDown(downComponent == component);
			component.update();
			component.updateContainerOffset(x + offset[0], y + offset[1]);
			theme.renderComponent(x + offset[0], y + offset[1], component);
		}
	}
	
	@Override
	public void layout() {
		Rectangle size = layout.layout(this, components);
		this.width = (float) size.getWidth();
		this.height = (float) size.getHeight();
	}

	@Override
	public boolean isPointInside(int x, int y) {
		boolean inside = x > this.x && x < this.x + this.width && y > this.y && y < this.y + this.height;
		for (int i = components.size() - 1; i >= 0; i--) {
			Component component = components.get(i);
			if (component.isPointInside(x, y)) {
				inside = true;
			}
		}
		return inside;
	}

	@Override
	public void update() {
		calculateMousedComponent();
	}
	
	private void calculateMousedComponent() {
		int mouseX = GLUtils.getMouseX(), mouseY = GLUtils.getMouseY();
		for (int i = components.size() - 1; i >= 0; i--) {
			Component component = components.get(i);
			if (component.isPointInside(mouseX, mouseY)) {
				this.mousedComponent = component;
				return;
			}
		}
		this.mousedComponent = null;
	}
	
	public void center() {
		this.setX(GLUtils.getScreenWidth() / 2 - this.getWidth() / 2);
		this.setY(GLUtils.getScreenHeight() / 2 - this.getHeight() / 2);
	}
	
	protected int[] getOffset() {
		return offset;
	}
	
	protected void setOffset(int[] offset) {
		this.offset = offset;
	}

	@Override
	public float getX() {
		return x;
	}

	@Override
	public void setX(float x) {
		this.x = x;
	}

	@Override
	public float getY() {
		return y;
	}

	@Override
	public void setY(float y) {
		this.y = y;
	}

	@Override
	public float getWidth() {
		return width;
	}

	@Override
	public void setWidth(float width) {
		this.width = width;
	}

	@Override
	public float getHeight() {
		return height;
	}

	@Override
	public void setHeight(float height) {
		this.height = height;
	}

	@Override
	public boolean isMouseOver() {
		return mouseOver;
	}

	@Override
	public void setMouseOver(boolean mouseOver) {
		this.mouseOver = mouseOver;
	}

	@Override
	public boolean isMouseDown() {
		return mouseDown;
	}

	@Override
	public void setMouseDown(boolean mouseDown) {
		this.mouseDown = mouseDown;
	}

	@Override
	public Layout getLayout() {
		return layout;
	}

	@Override
	public void setLayout(Layout layout) {
		this.layout = layout;
	}

	@Override
	public List<Component> getComponents() {
		return components;
	}
	
	@Override
	public void setComponents(List<Component> components) {
		this.components = components;
	}
	
	@Override
	public boolean isActive() {
		return active;
	}

	@Override
	public void setActive(boolean active) {
		this.active = active;
	}

	@Override
	public void add(Component component) {
		this.components.add(component);
		component.setParent(this);
	}

	@Override
	public void remove(Component component) {
		this.components.remove(component);
		component.setParent(null);
	}
	
	@Override
	public String getTooltip() {
		return this.mousedComponent != null ? mousedComponent.getTooltip() : null;
	}
	
	public void clear() {
		for (Component component : components)
			component.setParent(null);
		this.components.clear();
	}
	
	protected Component getMousedComponent() {
		return mousedComponent;
	}

	protected void setMousedComponent(Component mousedComponent) {
		this.mousedComponent = mousedComponent;
	}

	protected Component getDownComponent() {
		return downComponent;
	}

	protected void setDownComponent(Component downComponent) {
		this.downComponent = downComponent;
	}

	protected Component getSelectedComponent() {
		return selectedComponent;
	}

	protected void setSelectedComponent(Component selectedComponent) {
		this.selectedComponent = selectedComponent;
	}

}
