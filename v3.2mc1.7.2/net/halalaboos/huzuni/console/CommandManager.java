package net.halalaboos.huzuni.console;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.console.commands.*;
import net.halalaboos.lib.manager.Manager;
import net.minecraft.util.EnumChatFormatting;

/**
 * Manager to hold all command instances. Use this to run all commands.
 */
public final class CommandManager extends Manager<Command> {

    public void loadCommands() {
        Huzuni.logger.log("Loading commands...");
        add(new Help());
        add(new Add()/*LOLOLOLOL HOW IRONIC IM FUNNY*/);
        add(new Remove());
        add(new RockPaperScissors());
        add(new Toggle());
        add(new Font());
        add(new AllOff());
        add(new Vclip());
        add(new Say());
        add(new Enchant());
        add(new Protect());
        add(new Tp());
        add(new MassMessage());
        add(new Check());
        add(new SanjPakalou());
        add(new RemoteView());
        add(new GetIP());
        add(new Drop());
        add(new Macro());
        add(new OpAlts());
        add(new WaypointCommand());
        add(new RoxerDoxer());
        add(new Clear());
        add(new Kill());
        add(new ItemEnhancer());
        add(new SpawnTp());
        add(new Find());
        Huzuni.logger.log("Loaded " + this.size() + " commands.");
    }

    /**
     * Invoked to process commands.
     *
     * @return True if the command was goooood.
     */
    public boolean processCommand(String input) {
        // We'll parse the command name if the input had spaces in it.
        String commandName = input.contains(" ") ? input.split(" ")[0] : input;
        for (Command command : getList()) {
            // We'll try to find an alias and invoke the command.
            for (String alias : command.getAliases()) {
                if (alias.toLowerCase().equals(commandName.toLowerCase())) {
                	if (input.contains(" ")) {
                		if (input.split(" ")[1].equalsIgnoreCase("aliases")) {
                			listAliases(command);
                			return true;
                		}
                	}
                	tryCommand(command, input);
                    return true;
                }
            }
        }
        addMessage("'" + input + "' is not recognized as a command.");
        Huzuni.logger.log("Failed to execute command: ." + input);
        return false;
    }

    /**
	 * @param command
	 */
	private void listAliases(Command command) {
		String aliasList = "Available aliases: ";
		String[] aliases = command.getAliases();
		for (int i = 0; i < aliases.length; i++) {
			String alias = aliases[i];
			aliasList+= EnumChatFormatting.GOLD + alias + EnumChatFormatting.RESET + (i != aliases.length ? ", " : "");
		 }
		addMessage(aliasList);	
	}

	/**
     * Attempts to process the command instance with the input.
     */
    public void tryCommand(Command command, String input) {
        try {
            //lettuce run the command.
            String[] args = input.contains(" ") ? input.substring(input.indexOf(" ") + 1).split(" ") : null;
            command.run(input, args);
        	Huzuni.logger.log("Executed command: '" + command.getAliases()[0] + "' with input: ." + input);
        } catch (Exception e) {
            // lists all of the help for the command.
        	listHelp(command);
        }
    }

    private void listHelp(String reason, Command command) {
    	addMessage(reason);
    	for (String helpMessage : command.getHelp())
            addMessage(helpMessage);
    }
    
    private void listHelp(Command command) {
    	for (String helpMessage : command.getHelp())
            addMessage(helpMessage);
    }
    
    /**
     * Adds the messages outputted by the command.
     */
    private void addMessage(String message) {
        Huzuni.instance.addChatMessage(message);
    }

}
