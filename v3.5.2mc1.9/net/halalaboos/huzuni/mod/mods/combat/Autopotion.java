package net.halalaboos.huzuni.mod.mods.combat;


import java.util.List;
import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.mod.Category;
import net.halalaboos.huzuni.api.mod.ValueOption;
import net.halalaboos.huzuni.api.request.SimpleApplicant;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent;
import net.halalaboos.huzuni.event.tick.UpdateListener;
import net.halalaboos.huzuni.mod.DefaultMod;
import net.halalaboos.huzuni.queue.ClickQueue;
import net.halalaboos.huzuni.queue.rotation.RotationData;
import net.halalaboos.huzuni.util.Timer;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemPotion;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.potion.PotionHealth;

public class Autopotion extends DefaultMod implements UpdateListener {
	
	private final Item itemPotion, itemBottle;
		
	private int oldItem = -1, potionItem = -1;
	
    private boolean usingItem = false, splash = true;
    
    private final ValueOption health = new ValueOption("Health", 1F, 14F, 20F, 1F, "Health level you want to start throwing the potion");
    private final ValueOption useSpeed = new ValueOption("Use Speed", 1F, 140F, 200F, 1F, "Speed you want to use potions at");

    private Timer useTimer = new Timer();
        
    private final SimpleApplicant<Object> application = new SimpleApplicant<Object>(this);
    
	public Autopotion() {
		super("Auto Potion");
		setAuthor("Halalaboos");
		setDescription("Automatically heals you with a potion. Pretty stable but has some bugs that need to be fixed.");
		setCategory(Category.UNFINISHED);
		this.setOptions(health, useSpeed);
		itemPotion = Items.potionitem;
		itemBottle = Items.glass_bottle;
		Huzuni.rotationQueue.registerApplicant(application);
	}

	@Override
	public void onPreUpdate(MotionUpdateEvent event) {
		if (mc.thePlayer.isDead)
			return;
		if (usingItem) {
			if (mc.thePlayer.getHealth() > health.getValue()) {
				usingItem = false;
				potionItem = -1;
				mc.thePlayer.inventory.currentItem = oldItem;
				oldItem = -1;
				mc.playerController.updateController();
				return;
			}
			if (hasEmptyHand()) {
				usingItem = false;
				potionItem = -1;
				return;
			}
			if (mc.thePlayer.inventory.currentItem != potionItem) {
				mc.thePlayer.inventory.currentItem = potionItem;
				mc.playerController.updateController();
			}
			return;
		}
		if (mc.thePlayer.getHealth() <= health.getValue() && Huzuni.rotationQueue.hasPriority(application)) {
			int potion = findHotbarPotion(splash);
			if (potion == -1) {
				int goodPotion = findPotion(splash), replacementSlot = findEmptyBottle();
				if (goodPotion != -1 && replacementSlot != -1) {
					if (isShiftable(mc.thePlayer.inventoryContainer.getSlot(replacementSlot).getStack())) {
						ClickQueue.add(replacementSlot, 0, 1);
						ClickQueue.add(goodPotion, 0, 1);
					} else {
						ClickQueue.add(goodPotion, 0, 0);
						ClickQueue.add(replacementSlot, 0, 0);
						ClickQueue.add(goodPotion, 0, 0);
					}
				}
			} else {
				if (oldItem == -1)
					oldItem = mc.thePlayer.inventory.currentItem;
				mc.thePlayer.inventory.currentItem = potion;
				potionItem = potion;
				usingItem = true;
				Huzuni.rotationQueue.request(application, new RotationData(mc.thePlayer.rotationYaw, 90, true));
			}
		}
	}

	private boolean hasEmptyHand() {
		return false; //fuck if i kno
//		return mc.thePlayer.getCurrentEquippedItem() == null || mc.thePlayer.getCurrentEquippedItem().getItem() != this.itemPotion;
	}

	@Override
	public void onPostUpdate(MotionUpdateEvent event) {
		if (usingItem && application.hasRequest() && useTimer.hasReach((int) (useSpeed.getMaxValue() - useSpeed.getValue()))) {
			useItem(potionItem, mc.thePlayer.inventory.getStackInSlot(potionItem));
			useTimer.reset();
		}
	}

	@Override
	protected void onEnable() {
		Huzuni.registerUpdateListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterUpdateListener(this);
		this.oldItem = -1;
		this.potionItem = -1;
	}
	
	private int findPotion(boolean splash) {
//        for (int o = 9; o < 36; o++) {
//            if (mc.thePlayer.inventoryContainer.getSlot(o).getHasStack()) {
//                ItemStack item = mc.thePlayer.inventoryContainer.getSlot(o).getStack();
//                if (item != null) {
//                	if (item.getItem() == itemPotion) {
//                		if (splash ? ItemPotion.isSplash(item.getMetadata()) : true) {
//    	            		List<PotionEffect> effects = ((ItemPotion) item.getItem()).getEffects(item);
//    	            		for (PotionEffect effect : effects) {
//    	            			Potion potion = Potion.potionTypes[effect.getPotionID()];
//    	            			if (potion instanceof PotionHealth)
//								{
//    	            				return o;
//    	            			}
//    	            		}
//                		}
//                	}
//                }
//            }
//        }
        return -1;
    }
	
	private int findEmptyBottle() {
        for (int o = 36; o < 45; o++) {
            ItemStack item = mc.thePlayer.inventoryContainer.getSlot(o).getStack();
            if (item == null)
            	return o;
            else
            	if (item.getItem() == itemBottle)
            		return o;
        }
        return -1;
	}
	
	private int findHotbarPotion(boolean splash) {
        for (int o = 0; o < 9; o++) {
            ItemStack item = mc.thePlayer.inventory.getStackInSlot(o);
            if (item != null) {
            	if (item.getItem() == itemPotion) {
//            		if (splash ? ItemPotion.isSplash(item.getMetadata()) : true) {
//	            		List<PotionEffect> effects = ((ItemPotion) item.getItem()).getEffects(item);
//	            		for (PotionEffect effect : effects) {
//	            			Potion potion = Potion.potionTypes[effect.getPotionID()];
//	            			if (potion instanceof PotionHealth) {
//	            				return o;
//	            			}
//	            		}
//            		}
            	}
            }
        }
        return -1;
    }

	 /**
     * @return True if the item is shift clickable.
     */
    private boolean isShiftable(ItemStack preferedItem) {
        if (preferedItem == null)
            return true;
        for (int o = 9; o < 36; o++) {
            if (mc.thePlayer.inventoryContainer.getSlot(o).getHasStack()) {
                ItemStack item = mc.thePlayer.inventoryContainer.getSlot(o).getStack();
                if (item == null)
                    return true;
                else if (Item.getIdFromItem(item.getItem()) == Item.getIdFromItem(preferedItem.getItem())) {
                    if (item.stackSize + preferedItem.stackSize <= preferedItem.getMaxStackSize())
                        return true;
                }
            } else
                return true;
        }
        return false;
    }
    
    private void useItem(int index, ItemStack item) {
        mc.thePlayer.inventory.currentItem = index;
//        mc.playerController.sendUseItem(mc.thePlayer, mc.theWorld, item);
    }

	private int getCount(int itemID) {
		int count = 0;
		for (int o = 0; o < 9; o++) {
			ItemStack item = mc.thePlayer.inventory.getStackInSlot(o);
			if (item != null) {
				if (Item.getIdFromItem(item.getItem()) == itemID)
					count += item.stackSize;
			}
		}
		for (int o = 9; o < 36; o++) {
			if (mc.thePlayer.inventoryContainer.getSlot(o).getHasStack()) {
				ItemStack item = mc.thePlayer.inventoryContainer.getSlot(o).getStack();
				if (item != null) {
					if (Item.getIdFromItem(item.getItem()) == itemID)
						count += item.stackSize;
				}
			}
		}
		return count;
	}
	
}
