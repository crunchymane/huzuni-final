package net.halalaboos.huzuni.command.commands;

import java.io.File;
import java.util.Collection;
import java.util.List;

import com.mojang.authlib.GameProfile;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.console.Command;
import net.halalaboos.huzuni.util.FileUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.client.network.NetworkPlayerInfo;

public class AccountCheck implements Command {

	@Override
	public String[] getAliases() {
		return new String[] { "check", "czech" };
	}

	@Override
	public String[] getHelp() {
		return new String[] { "check" };
	}

	@Override
	public String getDescription() {
		return "Checks if any accounts on your account list are online.";
	}

	@Override
	public void run(String input, String[] args) {
		Huzuni.addChatMessage("Checking..");
        List<String> alts = FileUtils.readFile(new File(Huzuni.SAVE_DIRECTORY, "Accounts.txt"));
        for (NetworkPlayerInfo playerInfo : (Collection<NetworkPlayerInfo>) Minecraft.getMinecraft().getNetHandler().getPlayerInfoMap()) {
            GameProfile profile = playerInfo.getGameProfile();
            if (profile.getName().equals(Minecraft.getMinecraft().thePlayer.getName()))
                continue;
            for (String alt : alts) {
                String username = alt.split(":")[0];
                if (profile.getName().equalsIgnoreCase(username))
                	Huzuni.addChatMessage("Account '" + username + "' is on this server!");
            }
        }
        Huzuni.addChatMessage("Done!");
	}

}
