package net.halalaboos.huzuni.gui.screen.builder;

import net.halalaboos.huzuni.gui.GuiManager;
import net.halalaboos.huzuni.gui.clickable.menu.Menu;
import net.halalaboos.huzuni.gui.clickable.window.Window;
import net.halalaboos.huzuni.gui.screen.MenuScreen;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiYesNo;
import net.minecraft.client.gui.GuiYesNoCallback;

public class WindowBuilder extends MenuScreen implements GuiYesNoCallback {
	
	private final WindowSlot slotWindows = new WindowSlot();
	
	private final GuiScreen lastScreen;
	
	private GuiButton edit;
    private GuiButton remove;
    
	public WindowBuilder(GuiScreen lastScreen) {
		super("Window Builder");
		this.lastScreen = lastScreen;
	}

	@Override
    protected void actionPerformed(GuiButton button) {
        switch (button.id) {
            case 0:
                mc.displayGuiScreen(lastScreen);
                break;
            case 1:
                mc.displayGuiScreen(new WindowEditor(new Window("New window"), this));
                break;
            case 2:
            	if (slotWindows.hasSelectedComponent())
                    mc.displayGuiScreen(new GuiYesNo(this, "Remove window '" + slotWindows.getSelectedComponentO().getTitle() + "'", "Are you sure?", "Yes", "No", 0));
                break;
            case 3:
            	if (slotWindows.hasSelectedComponent())
            		mc.displayGuiScreen(new WindowEditor(slotWindows.getSelectedComponentO(), this));
                break;
        }
	}

	@Override
	protected void setup(Menu menu) {
		menu.clear();
		menu.add(slotWindows);
		buttonList.add(new GuiButton(0, this.width / 2 - 100, this.height - 28, "Done"));
        buttonList.add(new GuiButton(1, this.width / 2 - 154, this.height - 52, 100, 20, "Add"));
        
        edit = new GuiButton(3, this.width / 2 + 50, this.height - 52, 100, 20, "Edit");
        buttonList.add(edit);
        edit.enabled = slotWindows.hasSelectedComponent();
        remove = new GuiButton(2, this.width / 2 - 52, this.height - 52, 101, 20, "Remove");
        buttonList.add(remove);
        remove.enabled = slotWindows.hasSelectedComponent();
        slotWindows.setComponents(GuiManager.getUserWindows());
	}

	@Override
    public void drawScreen(int mouseX, int mouseY, float delta) {
		super.drawScreen(mouseX, mouseY, delta);
		edit.enabled = slotWindows.hasSelectedComponent();
        remove.enabled = slotWindows.hasSelectedComponent();
	}
	
	@Override
    public void confirmClicked(boolean confirm, int id) {
        if (confirm && id == 0) {
        	GuiManager.getWindowManager().remove(slotWindows.getSelectedComponentO());
        	GuiManager.saveWindows();
        	slotWindows.setComponents(GuiManager.getUserWindows());
        }
        mc.displayGuiScreen(this);
	}
}
