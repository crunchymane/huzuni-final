package net.halalaboos.huzuni.gui.xray;

import java.awt.Rectangle;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import net.halalaboos.huzuni.api.gui.render.Theme;
import net.halalaboos.huzuni.gui.clickable.slot.SlotComponent;
import net.halalaboos.huzuni.plugin.plugins.world.Xray;
import net.halalaboos.huzuni.util.GLUtils;
import net.halalaboos.huzuni.util.MathUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class SlotBlocks extends SlotComponent<ItemStack[]> {
	
	private final RenderItem itemRenderer = new RenderItem();
    private ItemStack mousedDownItemStack = null;
    
	public SlotBlocks(float width, float height) {
		super(width, height);
		
	}

	@Override
	protected void onReleased(int index, ItemStack[] itemStacks, int mouseX,
			int mouseY) {
		
	}

	@Override
	protected void onClicked(int index, ItemStack[] itemStacks, float[] area,
			int mouseX, int mouseY, int buttonId) {
		int xPos = 0;
		for (ItemStack itemStack : itemStacks) {
			if (itemStack == null)
				continue;
			Rectangle bounds = new Rectangle((int) area[0] + xPos, (int) area[1], 20, 20);
			if (bounds.contains(mouseX, mouseY)) {
				mousedDownItemStack = itemStack;
				checkBlock(itemStack);
				Xray.instance.save();
				break;
			}
			xPos += 21;
		}
	}
	
	private void checkBlock(ItemStack itemStack) {
		int itemId = Item.getIdFromItem(itemStack.getItem());
		Xray.instance.toggleBlock(itemId);
		if (Xray.instance.isEnabled())
			Minecraft.getMinecraft().renderGlobal.loadRenderers();
	}
	
	@Override
	protected void render(Theme theme, int index, ItemStack[] itemStacks, float[] area, boolean highlight, boolean mouseOver,
			boolean mouseDown) {
		Minecraft mc = Minecraft.getMinecraft();
		int xPos = 0;
		for (ItemStack itemStack : itemStacks) {
			if (itemStack == null)
				continue;
			float[] bounds = new float[] { area[0] + xPos, area[1], 20, 20 };
			boolean mouseOverItemStack = mouseOver && MathUtils.inside(GLUtils.getMouseX(), GLUtils.getMouseY(), bounds) && (mousedDownItemStack == null ? true : mousedDownItemStack == itemStack),
			mouseDownItemStack = mouseDown && mousedDownItemStack == itemStack;
			theme.renderSlot(this.getOffsetX(), this.getOffsetY(), index, bounds, Xray.instance.isBlockEnabled(Item.getIdFromItem(itemStack.getItem())), mouseOverItemStack, mouseDownItemStack);
            GL11.glPushMatrix();
            GL11.glDisable(GL11.GL_BLEND);
            GL11.glEnable(GL12.GL_RESCALE_NORMAL);
            RenderHelper.enableGUIStandardItemLighting();
            GL11.glDepthMask(true);
            GL11.glDisable(GL11.GL_DEPTH_TEST);
            try {
            	itemRenderer.renderItemIntoGUI(mc.fontRenderer, mc.getTextureManager(), itemStack, (int) bounds[0] + 2, (int) bounds[1] + 2);
            	itemRenderer.renderItemOverlayIntoGUI(mc.fontRenderer, mc.getTextureManager(), itemStack, (int) bounds[0] + 2, (int) bounds[1] + 2);
            } catch (Exception e) {
            	e.printStackTrace();
            }
            RenderHelper.disableStandardItemLighting();
            GL11.glDisable(GL12.GL_RESCALE_NORMAL);
            GL11.glEnable(GL11.GL_DEPTH_TEST);
            GL11.glDepthMask(true);
            GL11.glPopMatrix();
			xPos += 21;
		}
		if (!Mouse.isButtonDown(0))
			mousedDownItemStack = null;
	}

	@Override
	protected int getComponentHeight() {
		return 20;
	}

}
