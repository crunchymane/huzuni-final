package net.halalaboos.huzuni.command.commands;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.console.Command;
import net.minecraft.client.Minecraft;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.StatCollector;

import java.util.ArrayList;

/**
 * @author brudin
 * @version 1.0
 * @since 4/14/14
 */
public class Enchant implements Command {

	@Override
	public String[] getAliases() {
		return new String[] { "enchant", "enc"};
	}

	@Override
	public String[] getHelp() {
		return new String[] { "enchant" };
	}

	@Override
	public String getDescription() {
		return "Applies every enchant to your held item.";
	}

	@Override
	public void run(String input, String[] args) {
		if(Minecraft.getMinecraft().playerController.isInCreativeMode()) {
			if(Minecraft.getMinecraft().thePlayer.getCurrentEquippedItem() != null) {
				if(args == null) {
					for (int i = 0; i < Enchantment.enchantmentsBookList.length; i++) {
						Enchantment enchantment = Enchantment.enchantmentsBookList[i];
						Minecraft.getMinecraft().thePlayer.getCurrentEquippedItem().addEnchantment(enchantment, 127);
						Minecraft.getMinecraft().playerController.updateController();
					}
					Huzuni.addChatMessage("Enchantments added.");
				} else {
					String enchantName = args[0];
					int enchantLevel = Integer.parseInt(args[1]);
					for(Enchantment enchantment : Enchantment.enchantmentsBookList) {
						if(StatCollector.translateToLocal(enchantment.getName()).equalsIgnoreCase(enchantName.replaceAll("_", " "))) {
							String translateID = "enchantment.level.";
							Minecraft.getMinecraft().thePlayer.getCurrentEquippedItem().addEnchantment(enchantment, enchantLevel);
							Minecraft.getMinecraft().playerController.updateController();
							Huzuni.addChatMessage(String.format("Enchantment %s added.", enchantment.getTranslatedName(enchantLevel).replaceAll(translateID, "")));
						}
					}
				}
			}
		} else {
			Huzuni.addChatMessage("Must be in creative.");
		}
	}

	/**
	 * Going to use this later.
	 */
	private ArrayList<Enchantment> getEnchants(ItemStack itemStack) {
		ArrayList<Enchantment> localEnchants = new ArrayList<Enchantment>();
		NBTTagList enchantmentTags = itemStack.getEnchantmentTagList();
		if (enchantmentTags != null) {
			for (int i = 0; i < enchantmentTags.tagCount(); i++) {
				short enchantmentID = enchantmentTags.getCompoundTagAt(i).getShort("id");
				if (Enchantment.enchantmentsList[enchantmentID] != null) {
					localEnchants.add(Enchantment.enchantmentsList[enchantmentID]);
				}
			}
		}
		return localEnchants;
	}
}
