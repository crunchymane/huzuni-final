package net.halalaboos.huzuni.api.plugin;

import java.util.ArrayList;
import java.util.List;



public abstract class Plugin {

	private boolean enabled = false;
	
	private String name, author, description = "No description available.";

	private Category category = null;
	
	private final List<Option> options = new ArrayList<Option>();
	
	public Plugin(String name, String author) {
		this.name = name;
		this.author = author;
	}
	
	protected abstract void onEnable();
	
	protected abstract void onDisable();
	
	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		if (enabled != this.enabled) {
			this.enabled = enabled;
			if (enabled)
				onEnable();
			else
				onDisable();
		}
	}
	
	public void toggle() {
		setEnabled(!isEnabled());
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}
	
	public void setOptions(Option... options) {
		for (Option option : options)
			this.options.add(option);
	}

	public int length() {
		return options.size();
	}

	public Option get(int index) {
		return this.options.get(index);
	}

}
