/**
 *
 */
package net.halalaboos.huzuni.events.game;

import com.darkmagician6.eventapi.events.Event;
import net.minecraft.entity.Entity;

/**
 * @author Halalaboos
 * @since Jul 25, 2013
 */
public class EventKillEntity implements Event {

    public final Entity entity;

    public EventKillEntity(Entity entity) {
        this.entity = entity;
    }
}
