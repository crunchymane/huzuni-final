package net.halalaboos.huzuni.utils;

import static org.lwjgl.opengl.GL11.*;

public final class TextureUtils {

    private TextureUtils() {
    }

    public static void renderTexture(int texID, double x, double y, double width, double height) {
        glBindTexture(GL_TEXTURE_2D, texID);
        renderTexture(x, y, width, height);
    }

    public static void renderTexture(int textureWidth, int textureHeight, float x, float y, float width, float height, float srcX, float srcY, float srcWidth, float srcHeight) {
        float renderSRCX = srcX / textureWidth,
                renderSRCY = srcY / textureHeight,
                renderSRCWidth = (srcWidth) / textureWidth,
                renderSRCHeight = (srcHeight) / textureHeight;
        boolean tex2D = glGetBoolean(GL_TEXTURE_2D);
        boolean blend = glGetBoolean(GL_BLEND);
        glPushMatrix();
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glEnable(GL_TEXTURE_2D);
        glBegin(GL_TRIANGLES);
        glTexCoord2f(renderSRCX + renderSRCWidth, renderSRCY);
        glVertex2d(x + width, y);
        glTexCoord2f(renderSRCX, renderSRCY);
        glVertex2d(x, y);
        glTexCoord2f(renderSRCX, renderSRCY + renderSRCHeight);
        glVertex2d(x, y + height);
        glTexCoord2f(renderSRCX, renderSRCY + renderSRCHeight);
        glVertex2d(x, y + height);
        glTexCoord2f(renderSRCX + renderSRCWidth, renderSRCY + renderSRCHeight);
        glVertex2d(x + width, y + height);
        glTexCoord2f(renderSRCX + renderSRCWidth, renderSRCY);
        glVertex2d(x + width, y);
        glEnd();
        if (!tex2D)
            glDisable(GL_TEXTURE_2D);
        if (!blend)
            glDisable(GL_BLEND);
        glPopMatrix();

    }

    public static void renderTexture(double x, double y, double width, double height) {
        boolean tex2D = glGetBoolean(GL_TEXTURE_2D);
        boolean blend = glGetBoolean(GL_BLEND);
        glPushMatrix();
        glEnable(GL_BLEND);
        glEnable(GL_TEXTURE_2D);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glScalef(0.5F, 0.5F, 0.5F);
        x *= 2;
        y *= 2;
        width *= 2;
        height *= 2;
        glBegin(GL_TRIANGLES);
        glTexCoord2f(1, 0);
        glVertex2d(x + width, y);
        glTexCoord2f(0, 0);
        glVertex2d(x, y);
        glTexCoord2f(0, 1);
        glVertex2d(x, y + height);
        glTexCoord2f(0, 1);
        glVertex2d(x, y + height);
        glTexCoord2f(1, 1);
        glVertex2d(x + width, y + height);
        glTexCoord2f(1, 0);
        glVertex2d(x + width, y);
        glEnd();
        if (!tex2D)
            glDisable(GL_TEXTURE_2D);
        if (!blend)
            glDisable(GL_BLEND);
        glPopMatrix();
    }
}
