package net.halalaboos.huzuni.mods.player;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;
import com.darkmagician6.eventapi.types.Priority;
import com.mojang.authlib.GameProfile;

import net.halalaboos.huzuni.client.Settings;
import net.halalaboos.huzuni.events.game.EventMovement;
import net.halalaboos.huzuni.events.game.EventPreMotionUpdate;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.DefaultModule;
import net.halalaboos.lib.io.config.DefaultConfigListener;
import net.minecraft.client.entity.EntityOtherPlayerMP;

import org.lwjgl.input.Keyboard;

public class Freecam extends DefaultModule implements Listener {
    private EntityOtherPlayerMP fakePlayer;
	
    private DefaultConfigListener<Float> speed = new DefaultConfigListener<Float>("Flight Speed", 2F);

    public Freecam() {
        super("Freecam", Keyboard.KEY_U);
        setCategory(Category.PLAYER);
        setDescription("Allows you to temporarily fly out of your body.");
        Settings.addListener(speed);
    }

    @Override
    protected void onEnable() {
        registry.registerListener(this);
    }

    @Override
    protected void onDisable() {
        registry.unregisterListener(this);
    }

    @Override
    public void onToggle() {
        if (isEnabled()) {
            fakePlayer = new EntityOtherPlayerMP(mc.theWorld, new GameProfile("your mother", "\2479" + mc.thePlayer.getCommandSenderName()));
            fakePlayer.copyDataFrom(mc.thePlayer, true);
            fakePlayer.setPositionAndRotation(mc.thePlayer.posX, mc.thePlayer.posY - 1.5, mc.thePlayer.posZ, mc.thePlayer.rotationYaw, mc.thePlayer.rotationPitch);
            fakePlayer.rotationYawHead = mc.thePlayer.rotationYawHead;
            mc.theWorld.addEntityToWorld(-69, fakePlayer);
            mc.thePlayer.capabilities.isFlying = true;
            mc.thePlayer.noClip = true;
        } else {
            mc.thePlayer.setPositionAndRotation(fakePlayer.posX, fakePlayer.posY + 1.5, fakePlayer.posZ, fakePlayer.rotationYaw, fakePlayer.rotationPitch);
            mc.thePlayer.noClip = false;
            mc.theWorld.removeEntityFromWorld(-69);
            mc.thePlayer.capabilities.isFlying = false;
        }
    }

    @EventTarget(Priority.LOWEST)
    public void onPreMotionUpdate(EventPreMotionUpdate event) {
        mc.thePlayer.capabilities.isFlying = true;
        mc.thePlayer.noClip = true;
        mc.thePlayer.renderArmPitch += 200;
        mc.thePlayer.renderArmYaw += 180;
        event.setCancelled(true);
    }

    @EventTarget
    public void onPlayerMove(EventMovement event) {
        int multiplier = Keyboard.isKeyDown(Keyboard.KEY_LCONTROL) ? 2 : 1;
    	event.motionX *= speed.getValue() * multiplier;
        event.motionY *= speed.getValue() * multiplier;
        event.motionZ *= speed.getValue() * multiplier;
    }
    
}