package net.halalaboos.huzuni.command.commands;

import java.util.Collection;

import com.mojang.authlib.GameProfile;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.console.Command;
import net.halalaboos.huzuni.util.StringUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.client.network.NetworkPlayerInfo;

/**
 * @author Halalaboos
 * @since Aug 4, 2013
 */
public class MassMessage implements Command {

    @Override
    public String[] getAliases() {
        return new String[] {"massmessage", "mass", "mm"};
    }

    @Override
    public String[] getHelp() {
        return new String[] {".massmessage <message>"};
    }

    @Override
    public String getDescription() {
        return "Attempts sending that message to all players.";
    }

    @Override
    public void run(String input, String[] args) {
        String message = StringUtils.getAfter(input, 1);
        if (message.contains("$username$") || message.contains("$u$")) {
        	sendMessage(message);
        } else {
        	Huzuni.addChatMessage("No username specified!");
        	Huzuni.addChatMessage("Use $username$ to designate the player's username in the message. Ex: /msg $username$ howdy!");
        }
    }

    private void sendMessage(final String message) {
        new Thread() {
            @Override
            public void run() {
            	Collection<NetworkPlayerInfo> list = (Collection<NetworkPlayerInfo>) Minecraft.getMinecraft().getNetHandler().getPlayerInfoMap();
            	for (NetworkPlayerInfo info : list) {
                    GameProfile profile = info.getGameProfile();
                    Minecraft.getMinecraft().thePlayer.sendChatMessage(message.replaceAll("$username$", profile.getName()).replaceAll("$u$", profile.getName()));
                    try {
                        Thread.sleep(1000L);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (NullPointerException ex) {
                        ex.printStackTrace();
                    }
        		}
            }
        }.start();
    }
}
