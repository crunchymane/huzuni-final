package net.halalaboos.huzuni.plugin.value;

public class Value {

	private final String name;
	
	private float minValue, value, maxValue, incrementValue;
	
	public Value(String name, float minValue, float startValue, float maxValue, float incrementValue) {
		this.name = name;
		this.minValue = minValue;
		this.value = startValue;
		this.maxValue = maxValue;
		this.incrementValue = incrementValue;
	}
	
	public Value(String name, float minValue, float startValue, float maxValue) {
		this(name, minValue, startValue, maxValue, -1);
	}

	public float getValue() {
		return value;
	}

	public void setValue(float value) {
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public float getMinValue() {
		return minValue;
	}

	public float getMaxValue() {
		return maxValue;
	}

	public float getIncrementValue() {
		return incrementValue;
	}
}
