package net.halalaboos.huzuni.api.action;

public abstract class Action {
	
	public abstract void run();
}