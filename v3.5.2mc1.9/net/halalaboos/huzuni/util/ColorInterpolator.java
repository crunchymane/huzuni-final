package net.halalaboos.huzuni.util;

import java.awt.Color;
import java.util.Random;


public class ColorInterpolator {
	
	protected float red = 1F, green = 1F, blue = 1F, alpha = 1F;
	
	protected final Random random;
	
	public ColorInterpolator(Random random) {
		this.random = random;
		interpolate(1F);
	}
	
	public ColorInterpolator(long seed) {
		this(new Random(seed));
	}
	
	public ColorInterpolator() {
		this(new Random());
	}
	
	public void interpolate(float r, float g, float b, float a, float coefficient) {
		this.red += coefficient * (r - red);
		this.green += coefficient * (g - green);
		this.blue += coefficient * (b - blue);
		this.alpha += coefficient * (a - alpha);
		clamp();
	}
	
	public void clamp() {
		if (red < 0F)
			red = 0F;
		else if (red > 1F) red = 1F;

		if (green < 0F)
			green = 0F;
		else if (green > 1F) green = 1F;

		if (blue < 0F)
			blue = 0F;
		else if (blue > 1F) blue = 1F;

		if (alpha < 0F)
			alpha = 0F;
		else if (alpha > 1F) alpha = 1F;
	}
	
	public void interpolate(float coefficient) {
		interpolate(random.nextFloat(), random.nextFloat(), random.nextFloat(), random.nextFloat(), coefficient);
	}
	
	public void interpolateKeepAlpha(float coefficient) {
		interpolate(random.nextFloat(), random.nextFloat(), random.nextFloat(), alpha, coefficient);
	}

	public int getColor() {
		return ((int) (alpha * 255F) << 24) | (int) (red * 255F) | ((int) (green * 255F) << 8) | ((int) (blue * 255F) << 16) ;
	}
	
	public static Color getRandomColor(Random random) {
        return getRandomColor(random, 1000, 0.6F);
    }
	
	public static Color getRandomColor(Random random, int saturationRandom, float luminance) {
        final float hue = random.nextFloat();
        final float saturation = (random.nextInt(saturationRandom) + 1000) / (float) saturationRandom + 1000F;
        return Color.getHSBColor(hue, saturation, luminance);
    }

	public float getRed() {
		return red;
	}

	public void setRed(float red) {
		this.red = red;
	}

	public float getGreen() {
		return green;
	}

	public void setGreen(float green) {
		this.green = green;
	}

	public float getBlue() {
		return blue;
	}

	public void setBlue(float blue) {
		this.blue = blue;
	}

	public float getAlpha() {
		return alpha;
	}

	public void setAlpha(float alpha) {
		this.alpha = alpha;
	}
	
}
