package net.halalaboos.huzuni.gui.clickable.theme;

import java.awt.Color;
import java.text.DecimalFormat;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.gui.Component;
import net.halalaboos.huzuni.api.gui.Container;
import net.halalaboos.huzuni.api.gui.render.Theme;
import net.halalaboos.huzuni.gui.clickable.button.Button;
import net.halalaboos.huzuni.gui.clickable.button.PluginButton;
import net.halalaboos.huzuni.gui.clickable.dropdown.Dropdown;
import net.halalaboos.huzuni.gui.clickable.label.Label;
import net.halalaboos.huzuni.gui.clickable.menu.Menu;
import net.halalaboos.huzuni.gui.clickable.slider.Slider;
import net.halalaboos.huzuni.gui.clickable.slot.SlotComponent;
import net.halalaboos.huzuni.gui.clickable.textfield.TextField;
import net.halalaboos.huzuni.gui.clickable.window.Window;
import net.halalaboos.huzuni.util.GLUtils;
import net.halalaboos.huzuni.util.MathUtils;
import net.minecraft.util.EnumChatFormatting;

public abstract class DefaultTheme implements Theme {
	
    protected DecimalFormat formatter = new DecimalFormat("#.#");

	@Override
	public void renderComponent(float xOffset, float yOffset,
			Component component) {
		if (component instanceof PluginButton) {
			PluginButton button = (PluginButton) component;
			renderButtonRect(button.getX() + xOffset, button.getY() + yOffset, button.getX() + button.getWidth() + xOffset, button.getY() + button.getHeight() + yOffset, button.isHighlight(), button.isMouseOver() && !button.isDown(), button.isMouseDown());
			Huzuni.drawString(button.getTitle(), button.getX() + (button.getWidth() / 2) - (Huzuni.getStringWidth(button.getTitle()) / 2) + xOffset, button.getY() + (button.getHeight() / 2) - (Huzuni.getStringHeight(button.getTitle()) / 2) + yOffset + 2, getButtonTextColor(button));
			
			if (button.hasDropdown())
				Huzuni.drawString(getPluginDropdownText(), button.getX() + button.getWidth() - PluginButton.DROPDOWN_WIDTH + 1 + xOffset, button.getY() + (button.getHeight() / 2) - (Huzuni.getStringHeight(button.getTitle()) / 2) + yOffset + 2, button.isMouseOver() && button.isPointInsideDropdown(GLUtils.getMouseX(), GLUtils.getMouseY()) ? 0xFFFFFF : 0x40FFFFFF);
			
			if (button.isDown()) {    			
				renderButtonRect(button.getX() + xOffset, button.getY() + yOffset + button.getHeight() + 1, button.getX() + button.getWidth() + xOffset, button.getY() + yOffset + button.getHeight() + button.getDownHeight() + 3, false, false, false);
    			button.renderComponents(this, xOffset, yOffset, button.isMouseOver(), button.isMouseDown());
    			if (button.hasEnoughToScroll()) {
    				float[] sliderPoint = button.getSliderPoint();
    				GLUtils.glColor(GLUtils.getColorWithAffects(Huzuni.getColorTheme(), button.isMouseOver() && MathUtils.inside(GLUtils.getMouseX(), GLUtils.getMouseY(), sliderPoint), false));
    				renderButtonRect(sliderPoint[0], sliderPoint[1], sliderPoint[0] + sliderPoint[2], sliderPoint[1] + sliderPoint[3], true, button.isMouseOver() && MathUtils.inside(GLUtils.getMouseX(), GLUtils.getMouseY(), sliderPoint), false);
    			}
			}
		} else if (component instanceof Button) {
			Button button = (Button) component;
			renderButtonRect(button.getX() + xOffset, button.getY() + yOffset, button.getX() + button.getWidth() + xOffset, button.getY() + button.getHeight() + yOffset, button.isHighlight(), button.isMouseOver(), button.isMouseDown());
			Huzuni.drawString(button.getTitle(), button.getX() + (button.getWidth() / 2) - (Huzuni.getStringWidth(button.getTitle()) / 2) + xOffset, button.getY() + (button.getHeight() / 2) - (Huzuni.getStringHeight(button.getTitle()) / 2) + yOffset + 2, getButtonTextColor(button));
		} else if (component instanceof Slider) {
			Slider slider = (Slider) component;
			renderButtonRect(slider.getX() + xOffset, slider.getY() + yOffset, slider.getX() + slider.getWidth() + xOffset, slider.getY() + slider.getHeight() + yOffset, false, false, false);
			
			Huzuni.drawString(slider.getLabel(), slider.getX() + 2 + xOffset, slider.getY() + (slider.getHeight() / 2) - (Huzuni.getStringHeight(slider.getLabel()) / 2) + yOffset + 2, 0xFFFFFF);
			String value = formatter.format(slider.getValue()) + slider.getValueWatermark();
			Huzuni.drawString(value, slider.getX() + (slider.getWidth()) - (Huzuni.getStringWidth(value)) - 2 + xOffset, slider.getY() + (slider.getHeight() / 2) - (Huzuni.getStringHeight(value) / 2) + yOffset + 2, 0xFFFFFF);
			
			float[] sliderPoint = slider.getSliderPoint();
			renderButtonRect(sliderPoint[0] + xOffset, sliderPoint[1] + yOffset, sliderPoint[0] + sliderPoint[2] + xOffset, sliderPoint[1] + sliderPoint[3] + yOffset, true, slider.isMouseOver(), false);
		} else if (component instanceof SlotComponent) {
			SlotComponent slotComponent = (SlotComponent) component;
			float[] sliderPoint = slotComponent.getSliderPoint();
			renderButtonRect(slotComponent.getX() + xOffset, slotComponent.getY() + yOffset, slotComponent.getX() + slotComponent.getWidth() + xOffset, slotComponent.getY() + slotComponent.getHeight() + yOffset, false, false, false);
			renderButtonRect(sliderPoint[0], sliderPoint[1], sliderPoint[0] + sliderPoint[2], sliderPoint[1] + sliderPoint[3], true, slotComponent.isMouseOver() && MathUtils.inside(GLUtils.getMouseX(), GLUtils.getMouseY(), sliderPoint), slotComponent.isDragging());
			slotComponent.renderComponents(this, xOffset, yOffset, slotComponent.isMouseOver(), slotComponent.isMouseDown());
		} else if (component instanceof Dropdown) {
			Dropdown dropdown = (Dropdown) component;
			
			renderButtonRect(dropdown.getX() + xOffset, dropdown.getY() + yOffset, dropdown.getX() + dropdown.getWidth() + xOffset, dropdown.getY() + dropdown.getHeight() + yOffset, false, dropdown.isDown() ? true : dropdown.isMouseOver(), dropdown.isDown() ? false : dropdown.isMouseDown());

            Huzuni.drawStringWithShadow(getDropdownTitle(dropdown), dropdown.getX() + xOffset + 2, dropdown.getY() + yOffset + dropdown.getHeight() / 2 - Huzuni.getStringHeight(dropdown.getTitle()) / 2 + 2, Color.WHITE.getRGB());
            
			if (dropdown.isDown()) {    			
				renderButtonRect(dropdown.getX() + xOffset, dropdown.getY() + yOffset + dropdown.getHeight() + 1, dropdown.getX() + dropdown.getWidth() + xOffset, dropdown.getY() + yOffset + dropdown.getHeight() + dropdown.getDownHeight() + 3,  false, false, false);
    			dropdown.renderComponents(this, xOffset, yOffset, dropdown.isMouseOver(), dropdown.isMouseDown());
    			if (dropdown.hasEnoughToScroll()) {
    				float[] sliderPoint = dropdown.getSliderPoint();
    				renderButtonRect(sliderPoint[0], sliderPoint[1], sliderPoint[0] + sliderPoint[2], sliderPoint[1] + sliderPoint[3], true, dropdown.isMouseOver() && MathUtils.inside(GLUtils.getMouseX(), GLUtils.getMouseY(), sliderPoint), false);
    			}
			}
		} else if (component instanceof TextField) {
			TextField textField = (TextField) component;
			String text = textField.getTextForRender(textField.getWidth()) + textField.getCarot();
			renderButtonRect(textField.getX() + xOffset, textField.getY() + yOffset, textField.getX() + textField.getWidth() + xOffset, textField.getY() + textField.getHeight() + yOffset, false, textField.isMouseOver(), textField.isMouseDown());
			Huzuni.drawString(text, textField.getX() + xOffset, textField.getY() + (textField.getHeight() / 2) - (Huzuni.getStringHeight(text) / 2) + yOffset + 2, 0xFFFFFF);
		} else if (component instanceof Label) {
			Label label = (Label) component;
			Huzuni.drawString(label.getText(), label.getX() + xOffset, label.getY() + yOffset + 2, label.getColor().getRGB());
		}
	}

	@Override
	public void renderContainer(Container container) {
		if (container instanceof Window) {
			Window window = (Window) container;
			this.renderWindowRect(window.getX(), window.getY(), window.getX() + window.getWidth(), window.getY() + window.getHeight() + window.getTabHeight(), window.getTabHeight(), 2F);
			Huzuni.drawStringWithShadow(EnumChatFormatting.BOLD + window.getTitle(), window.getX() + 2, window.getY() + 2 + window.getTabHeight() / 2 - Huzuni.getStringHeight(EnumChatFormatting.BOLD + window.getTitle()) / 2, getWindowTextColor());
		} else if (container instanceof Menu) {
			Menu menu = (Menu) container;
			this.renderWindowRect(menu.getX(), menu.getY(), menu.getX() + menu.getWidth(), menu.getY() + menu.getHeight(), 0F, 2F);
			Huzuni.drawStringWithShadow(EnumChatFormatting.BOLD + menu.getTitle(), menu.getX() + 2, menu.getY() + 2, getWindowTextColor());
		}
	}

	@Override
	public void renderSlot(float xOffset, float yOffset, int index, float[] area, boolean highlight,
			boolean mouseOver, boolean mouseDown) {
		renderButtonRect(area[0], area[1], area[0] + area[2], area[1] + area[3], highlight, mouseOver, mouseDown);
	}
	
	@Override
	public void renderTooltip(String tooltip) {
		float mouseX = GLUtils.getMouseX(), mouseY = GLUtils.getMouseY() - 8, width = Huzuni.getStringWidth(tooltip);
		renderButtonRect(mouseX, mouseY, mouseX + width + 4, mouseY + 12, false, false, false);
		Huzuni.drawStringWithShadow(tooltip, mouseX + 2, mouseY + 2, getTooltipTextColor());
	}
	
	protected String getDropdownTitle(Dropdown dropdown) {
		return dropdown.getTitle() + " (" + dropdown.getSelectedComponentName() + ")";
	}

	protected String getPluginDropdownText() {
		return "..";
	}
	
	protected abstract int getButtonTextColor(Button button);
	
	protected abstract int getWindowTextColor();
	
	protected abstract int getTooltipTextColor();

	protected abstract void renderButtonRect(float x, float y, float x1, float y1, boolean highlight, boolean mouseOver, boolean mouseDown);
	
	protected abstract void renderWindowRect(float x, float y, float x1, float y1, float tabHeight, float lineWidth);

}
