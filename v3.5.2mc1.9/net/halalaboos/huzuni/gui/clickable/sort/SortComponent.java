package net.halalaboos.huzuni.gui.clickable.sort;

import net.halalaboos.huzuni.api.gui.render.Theme;
import net.halalaboos.huzuni.gui.clickable.slot.SlotComponent;
import net.halalaboos.huzuni.util.GLUtils;

public abstract class SortComponent <T> extends SlotComponent <T> {
	
	private float lastMouseY, lastComponentY;
	
	private float[] clickedArea;
	
	private int clickedIndex;
		
	public SortComponent(float width, float height) {
		super(width, height);
	}

	@Override
	public void mouseReleased(int x, int y, int buttonId) {
		if (clickedComponent != null) {
			float[] sliderPoint = getSliderPoint();
			int yPos = 2 - (int) getScrollHeight();
			if (!components.isEmpty()) {
				for (int i = 0; i < components.size() && i >= 0; i++) {
					float[] componentArea = new float[] { (int) (this.getOffsetX() + this.getX() + 2), (int) (this.getOffsetY() + this.getY() + yPos), (int) (getWidth() - (hasEnoughToScroll() && hasSlider ? (sliderPoint[2]) : 0) - 4), getComponentHeight(components.get(i)) };
					float intersection = componentArea[1] + componentArea[3] - getDraggedHeight();
					if (shouldRender(componentArea)) {
						if (intersection > (componentArea[3] / 2)) {
							components.remove(this.clickedComponent);
							components.add(i, clickedComponent);
							clickedComponent = null;
							return;
						}
					}
					yPos += componentArea[3] + componentPadding;
				}
			}
		clickedComponent = null;
		} else
			super.mouseReleased(x, y, buttonId);
	}
	
	@Override
	protected void onReleased(int index, T component, int mouseX, int mouseY) {}

	@Override
	protected void onClicked(int index, T component, float[] area, int mouseX, int mouseY, int buttonId) {
		lastMouseY = mouseY;
		lastComponentY = area[1];
	}
		
	@Override
	protected void render(Theme theme, int index, T component, float[] area, boolean highlight, boolean mouseOver, boolean mouseDown) {
		if (component == clickedComponent) {
			area[1] = getDraggedHeight();
			clickedIndex = index;
			clickedArea = area;
			return;
		}
		highlight = false;
		renderComponent(theme, index, component, area, highlight, mouseOver, mouseDown);
	}
	
	@Override
	public void renderComponents(Theme theme, float offsetX, float offsetY, boolean mouseOver, boolean mouseDown) {
		super.renderComponents(theme, offsetX, offsetY, mouseOver, mouseDown);
		if (this.clickedComponent != null) {
			this.renderComponent(theme, clickedIndex, clickedComponent, clickedArea, false, true, false);
		}
	}
	
	private float getDraggedHeight() {
		float height = GLUtils.getMouseY() - (lastMouseY - lastComponentY);
		if (height + this.getComponentHeight(this.clickedComponent) > this.getY() + this.getHeight() + this.getOffsetY() - 2) {
			height = this.getY() + this.getHeight() + this.getOffsetY() - this.getComponentHeight(this.clickedComponent) - 2;
		}
		if (height < this.getY() + this.getOffsetY() + 2) {
			height = this.getY() + this.getOffsetY() + 2;
		}
		return height;
	}
	
	protected abstract void renderComponent(Theme theme, int index, T component, float[] area, boolean highlight, boolean mouseOver, boolean mouseDown);
}
