package net.halalaboos.huzuni.mod.mods.world;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.mod.Category;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent;
import net.halalaboos.huzuni.event.tick.UpdateListener;
import net.halalaboos.huzuni.mod.DefaultMod;
import net.halalaboos.huzuni.queue.ClickQueue;
import net.halalaboos.huzuni.util.Timer;
import net.minecraft.client.gui.inventory.GuiChest;
import net.minecraft.client.player.inventory.ContainerLocalMenu;
import net.minecraft.item.ItemStack;

public class ChestXploder extends DefaultMod implements UpdateListener {
	
	private final Timer timer = new Timer();
	
	private GuiChest guiChest;
	
	private ContainerLocalMenu chest;
	
	private int windowId, index;

	public ChestXploder() {
		super("Chest Xploder");
		setCategory(Category.WORLD);
		setDescription("Automatically drops all items in a chest.");
	}

	@Override
	protected void onEnable() {
		Huzuni.registerUpdateListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterUpdateListener(this);
	}

	@Override
	public void onPreUpdate(MotionUpdateEvent event) {
		if (guiChest != null && chest != null) {
			if (timer.hasReach(125)) {
				for (; index < chest.getSizeInventory(); index++) {
					ItemStack item = chest.getStackInSlot(index);
					if (item == null)
						continue;
					ClickQueue.add(windowId, index, 0, 0);
					ClickQueue.add(windowId, -999, 0, 0);
					timer.reset();
					index++;
					return;
				}
				mc.displayGuiScreen(null);
				chest = null;
				guiChest = null;
			}
		} else {
			if (mc.currentScreen instanceof GuiChest) {
				guiChest = (GuiChest) mc.currentScreen;
				chest = ((ContainerLocalMenu) guiChest.getLowerChestInventory());
				index = 0;
				windowId = guiChest.inventorySlots.windowId;
			}
		}
	}

	@Override
	public void onPostUpdate(MotionUpdateEvent event) {
	}
	
}
