/**
 * 
 */
package net.halalaboos.huzuni.client.keybinds;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.events.game.EventGameEnd;
import net.halalaboos.huzuni.events.game.EventGameStartup;
import net.halalaboos.huzuni.events.game.EventKeyPressed;
import net.halalaboos.huzuni.helpers.Helper;
import net.halalaboos.lib.io.FileUtils;

import org.lwjgl.input.Keyboard;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;

/**
 * @author Halalaboos
 *
 * @since Oct 17, 2013
 */
public final class KeybindManager implements Listener {
    private final File saveFile = new File(Huzuni.getSaveDirectory(), "Macros.txt");

	private final Keybind[] keybinds = new Keybind[Keyboard.KEYBOARD_SIZE];
	
	public KeybindManager() {
        registry.registerListener(this);
	}

	@EventTarget
	public void onGameStartup(EventGameStartup event) {
		load();
	}
	
	@EventTarget
	public void onGameEnd(EventGameEnd event) {
		save();
	}

	@EventTarget
	public void onKeypress(EventKeyPressed event) {
		if (keybinds[event.keyCode] != null) {
			keybinds[event.keyCode].onKeyPressed();
		}
	}
	
	public void save() {
		List<String> lines = new ArrayList<String>();
		for (int i = 0; i < keybinds.length; i++) {
			Keybind keybind = keybinds[i];
			if (keybind != null) {
				lines.add(i + " " + keybind.getCommand());
			}
		}
		FileUtils.writeFile(saveFile, lines);
	}
	
	public void load() {
		for (String line : FileUtils.readFile(saveFile)) {
			String key = Helper.stringHelper.getBefore(line, 1);
			if (Helper.stringHelper.isInteger(key)) {
				int keyCode = Integer.parseInt(key);
				String command = Helper.stringHelper.getAfter(line, 1);
				keybinds[keyCode] = new CommandKeybind(command);
			}
		}
	}
	
	public boolean contains(int keyCode) {
		return keybinds[keyCode] != null;
	}
	
	public void add(int keyCode, Keybind keybind) {
		keybinds[keyCode] = keybind;
	}
	
	public void remove(int keyCode) {
		keybinds[keyCode] = null;
	}
	
}
