package net.halalaboos.huzuni.api.gui;

import net.halalaboos.huzuni.api.gui.event.ActionListener;


public interface Component {

	/**
	 * Invoked when the containers layout layed it.. out
	 * */
	void layout(float x, float y, float width, float height);

	/**
	 * Invoked when the containers layout layed it.. out
	 * */
	void layout(float x, float y);
	
	/**
	 * An update method.
	 * */
	void update();
	
	/**
	 * Updates the offset the component will need to be inside of the container.
	 * */
	void updateContainerOffset(float containerX, float containerY);
	
	/**
	 * Invoked when you press a mouse button.
	 * @return If you clicked inside of the component.
	 * */
	boolean mouseClicked(int x, int y, int buttonId);
	
	/**
	 * Invoked when the mouse button is released after holding it on this component.
	 * */
	void mouseReleased(int x, int y, int buttonId);
	
	/**
	 * Invoked when the component is selected an you type a key.
	 * */
	void keyTyped(int keyCode, char c);
	
	/**
	 * @return if the point is inside of the component.
	 * */
	boolean isPointInside(int x, int y);
	
	
	/* GETTERS / SETTERS */
	
	float getX();
	
	void setX(float x);
	
	float getY();
	
	void setY(float y);
	
	float getWidth();
	
	void setWidth(float width);
	
	float getHeight();
	
	void setHeight(float height);
		
	boolean isMouseOver();
	
	void setMouseOver(boolean mouseOver);
	
	boolean isMouseDown();
	
	void setMouseDown(boolean mouseDown);

	void addActionListener(ActionListener listener);
	
	void removeActionListener(ActionListener listener);
	
	Container getParent();
	
	void setParent(Container parent);
	
	String getTooltip();
	
	void setTooltip(String tooltip);
	
}
