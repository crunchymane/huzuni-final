/**
 * 
 */
package net.halalaboos.lib.ui;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;

/**
 * @author Halalaboos
 *
 */
public interface Component {
	
    /**
     * Invoked to update the component with the interactive area given by the container.
     * */
    public void update(Point mouse);

    /**
     * Invoked when a mouse button is pressed. 
     * @return True if your component was triggered by this event, cancelling all other invocations of other components / containers. 
     * */
    public boolean onMousePressed(int x, int y, int buttonId);

    /**
     * Invoked when the mouse is released off this component.
     * */
    public void onMouseReleased(int x, int y, int buttonId);

    /**
     * Invoked when the keyboard is typed onto.
     * */
    public void onKeyTyped(int keyCode, char c);

    /**
     * @return the state of the component. If inactive, no events will occur, along with no rendering.
     * */
    public boolean isActive();

    /**
     * Area of the component. For rendering and magic.
     * */
    public Rectangle getArea();

    public Rectangle getRenderableArea();

    /**
     * Setter for the area.
     * */
    public void setArea(Rectangle area);

    /**
     * Smallest (preferred) dimensions of the component. Used for laying out by the container.
     * */
    public Dimension getDimensions();
    
    /**
     * Tooltip hover string when you hover over the component.
     * */
    public String getTooltip();
    
    /**
     * Setter for the mouseover boolean.
     * */
    public void setMouseOver(boolean mouseOver);
    
    /**
     * Getter for the mouseover boolean.
     * */
    public boolean isMouseOver();
    
    /**
     * Setter for the mousedown boolean.
     * */
    public void setMouseDown(boolean mouseDown);
    
    /**
     * Getter for mousedown boolean.
     * */
    public boolean isMouseDown();
    
    /**
     * @return True if the point is inside the renderable area.
     * */
    public boolean isPointInside(Point point);
    
    /**
     * Setter for the offset on the component when rendering.
     * */
    public void setOffset(Point offset);
    
}
