package net.halalaboos.huzuni.mod.mods.player;

import net.halalaboos.huzuni.event.packet.PacketEvent;
import net.halalaboos.huzuni.event.packet.PacketListener;
import net.minecraft.network.play.client.*;

import net.minecraft.potion.PotionEffect;
import org.lwjgl.input.Keyboard;

import com.mojang.authlib.GameProfile;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.mod.Category;
import net.halalaboos.huzuni.api.mod.ValueOption;
import net.halalaboos.huzuni.mod.DefaultMod;
import net.minecraft.client.entity.EntityOtherPlayerMP;

public class Freecam extends DefaultMod implements PacketListener {

	public static final Freecam instance = new Freecam();
	
    private EntityOtherPlayerMP fakePlayer;

	private final ValueOption speed = new ValueOption("Speed", 1F, 1F, 10F, 0.5F, "Speed you will fly at");
    
	private Freecam() {
		super("Freecam", Keyboard.KEY_U);
		setCategory(Category.PLAYER);
		setAliases("oob", "fc");
        setDescription("Allows you to temporarily fly out of your body.");
		this.setOptions(speed);
	}

	@Override
	protected void onEnable() {
		Huzuni.registerPacketListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterPacketListener(this);
	}
	
	public void toggle() {
		super.toggle();
        if (isEnabled()) {
            fakePlayer = new EntityOtherPlayerMP(mc.theWorld, new GameProfile(mc.thePlayer.getUniqueID(), mc.thePlayer.getName()));
            fakePlayer.copyLocationAndAnglesFrom(mc.thePlayer);
			fakePlayer.inventory = mc.thePlayer.inventory;
            fakePlayer.setPositionAndRotation(mc.thePlayer.posX, mc.thePlayer.posY, mc.thePlayer.posZ, mc.thePlayer.rotationYaw, mc.thePlayer.rotationPitch);
            fakePlayer.rotationYawHead = mc.thePlayer.rotationYawHead;
			mc.theWorld.addEntityToWorld(-69, fakePlayer);
			 mc.thePlayer.capabilities.isFlying = true;
            mc.thePlayer.noClip = true;
        } else {
        	if (fakePlayer != null && mc.thePlayer != null) {
        		mc.thePlayer.setPositionAndRotation(fakePlayer.posX, fakePlayer.posY, fakePlayer.posZ, fakePlayer.rotationYaw, fakePlayer.rotationPitch);
            	mc.thePlayer.noClip = false;
            	mc.theWorld.removeEntityFromWorld(-69);
            	mc.thePlayer.capabilities.isFlying = false;
        	}
        	 if (mc.thePlayer != null)
                 mc.thePlayer.capabilities.isFlying = false;
        }
    }

	/**
	 * Decided to try and see if this would work well using only a packet event.
	 *
	 * I did this mainly because things like Retard mode, Kill Aura, etc. were glitching out when using Freecam.  While
	 * this could've been fixed in the mod(s)' classes, I felt it would be fine to just use a packet event since all we
	 * are using the update event for is to cancel sending motion updates.
	 *
	 * If you encounter any bugs with this, feel free to revert to what we were using before.  It's working fine for
	 * me currently but I haven't really used it much.
	 * 
	 * packet event is gay as shit
	 */
	@Override
	public void onPacket(PacketEvent event, PacketEvent.PacketType type) {
		if (type == PacketEvent.PacketType.SENT) {
			if (event.getPacket() instanceof CPacketPlayer) {
				event.setCancelled(true);
			}
			if (event.getPacket() instanceof CPacketEntityAction) {
				CPacketEntityAction packet = (CPacketEntityAction) event.getPacket();
				if (packet.getAction() != CPacketEntityAction.Action.OPEN_INVENTORY) {
					event.setCancelled(true);
				}
			}
			mc.thePlayer.setSprinting(false);
			mc.thePlayer.noClip = true;
			mc.thePlayer.renderArmPitch += 200;
			mc.thePlayer.renderArmYaw += 180;
			mc.thePlayer.capabilities.isFlying = true;
			/*double motionX = Math.cos(Math.toRadians(this.mc.thePlayer.rotationYaw + 90.0F));
			double motionZ = Math.sin(Math.toRadians(this.mc.thePlayer.rotationYaw + 90.0F));
			float multiplier = speed.getValue() / 5;
			mc.thePlayer.motionX = (mc.thePlayer.moveForward * motionX + mc.thePlayer.moveStrafing * motionZ) * multiplier;
			mc.thePlayer.motionZ = (mc.thePlayer.moveForward * motionZ - mc.thePlayer.moveStrafing * motionX) * multiplier;
			mc.thePlayer.motionY = mc.thePlayer.movementInput.jump ? multiplier : mc.thePlayer.movementInput.sneak ? -multiplier : 0;*/
			if (fakePlayer != null) {
				fakePlayer.setHealth(mc.thePlayer.getHealth());
			}
		}
	}

	public float getSpeed() {
    	return speed.getValue();
    }
}
