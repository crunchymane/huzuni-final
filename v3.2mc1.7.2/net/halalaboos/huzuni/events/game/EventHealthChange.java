/**
 *
 */
package net.halalaboos.huzuni.events.game;

import com.darkmagician6.eventapi.events.Event;

/**
 * @author Halalaboos
 * @since Jul 19, 2013
 */
public class EventHealthChange implements Event {

    public final float health;

    public EventHealthChange(float health) {
        this.health = health;
    }

}
