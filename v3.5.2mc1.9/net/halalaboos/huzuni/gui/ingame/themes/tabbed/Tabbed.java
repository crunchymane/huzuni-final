package net.halalaboos.huzuni.gui.ingame.themes.tabbed;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import net.halalaboos.huzuni.mod.DefaultMod;
import org.lwjgl.input.Keyboard;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.mod.Category;
import net.halalaboos.huzuni.api.mod.Mod;
import net.halalaboos.huzuni.gui.ingame.IngameTheme;
import net.halalaboos.huzuni.mod.ModManager;
import net.halalaboos.huzuni.util.GLUtils;
import net.minecraft.client.Minecraft;

public class Tabbed implements IngameTheme {

	private int tabSelection = 0;
	
	private boolean open = false;
	
	private Tab[] tabs = genTabs();
	
	private int width;
	
	@Override
	public void render(Minecraft mc, int screenWidth, int screenHeight) {
		genWidth();
		int x = 2, y = 2, fontHeight = 12;
		Color color = Huzuni.getColorTheme();
		int highlight = color.getRed() << 16 | color.getGreen() << 8 | color.getBlue();
		Tab selectedTab = tabs[tabSelection];
		GLUtils.drawBorderedRect(x, y, x + width + 5, y + tabs.length * fontHeight, 0.5F, 0x80000000, 0xFF000000);
		for (int i = 0; i < tabs.length; i++) {
			Tab tab = tabs[i];
			if (tab == selectedTab) {
				GLUtils.drawRect(x + 1, y + i * fontHeight + 1, x + 4 + width, y + i * fontHeight + 11, highlight + (150 << 24));
				mc.fontRenderer.drawStringWithShadow(tab.getCategory().formalName, x + 4, y + 2 + i * fontHeight, 0xFFFFFF);
			} else {
				mc.fontRenderer.drawString(tab.getCategory().formalName, x + 2, y + 2 + i * fontHeight, 0xFFFFFF);
			}
		}
		if (open) {
			selectedTab.calculateWidth();
			GLUtils.drawBorderedRect(x + width + 6, y, x + width + 6 + selectedTab.getWidth(), y + selectedTab.getMods().length * 12, 0.5F, 0x80000000, 0xFF000000);
			for (int i = 0; i < selectedTab.getMods().length; i++) {
				Mod mod = selectedTab.getMods()[i];
				if (mod == selectedTab.getSelectedMod()) {
					GLUtils.drawRect(x + width + 7, y + i * fontHeight + 1, x + width + 5 + selectedTab.getWidth(), y + i * fontHeight + 11, highlight + (150 << 24));
					mc.fontRenderer.drawStringWithShadow(mod.getName(), x + width + 10, y + 2 + i * fontHeight, mod.isEnabled() ? highlight : 0xFFFFFF);
				} else
					mc.fontRenderer.drawStringWithShadow(mod.getName(), x + width + 8, y + 2 + i * fontHeight, mod.isEnabled() ? highlight : 0xFFFFFF);
			}
		}
		renderEnabledMods(screenWidth);
	}

	private void renderEnabledMods(int screenWidth) {
		int yPos = 2;
		for (DefaultMod mod : ModManager.getDefaultMods()) {
			if (mod.getVisible() && mod.isEnabled()) {
				String name = mod.getRenderName();
				int color = mod.getColor().getRGB();
				int width = mc.fontRenderer.getStringWidth(mod.getRenderName());
				mc.fontRenderer.drawStringWithShadow(name, screenWidth - width - 2, yPos, color);
				yPos += 10;
			}
		}
	}
	
	private void genWidth() {
		width = 0;
		for (int i = 0; i < tabs.length; i++) {
			Tab tab = tabs[i];
			if (mc.fontRenderer.getStringWidth(tab.getCategory().formalName) + 6 > width)
				width = mc.fontRenderer.getStringWidth(tab.getCategory().formalName) + 6;
		}
	}

	private Tab[] genTabs() {
		Tab[] tabs = new Tab[Category.values().length];
		List<Mod> mods = new ArrayList<Mod>();
		for (int i = 0; i < tabs.length; i++) {
			mods.clear();
			Category category = Category.values()[i];
			if (mc.fontRenderer.getStringWidth(category.formalName) > width)
				width = mc.fontRenderer.getStringWidth(category.formalName) + 8;
			tabs[i] = new Tab(category);
			for (Mod mod : ModManager.getMods()) {
				if (mod.getCategory() == category) {
					mods.add(mod);
				}
			}
			tabs[i].setMods(mods.toArray(new Mod[mods.size()]));
		}
		return tabs;
	}

	@Override
	public String getName() {
		return "Tabbed";
	}

	@Override
	public void onKeyTyped(int keyCode) {
		keepSafe();
		if (open) {
			Tab tab = tabs[tabSelection];
			if (keyCode == Keyboard.KEY_UP) {
				tab.up();
			} else if (keyCode == Keyboard.KEY_DOWN) {
				tab.down();
			} else if (keyCode == Keyboard.KEY_LEFT) {
				open = false;
			} else if (keyCode == Keyboard.KEY_RETURN) {
				tab.getSelectedMod().toggle();
			}
		} else {
			if (keyCode == Keyboard.KEY_UP) {
				up();
			} else if (keyCode == Keyboard.KEY_DOWN) {
				down();
			} else if (keyCode == Keyboard.KEY_RIGHT) {
				open = true;
			}
		}
	}
	
	public void keepSafe() {
		if (tabSelection >= tabs.length)
			tabSelection = tabs.length - 1;
		if (tabSelection < 0)
			tabSelection = 0;
	}
	
	public void up() {
		tabSelection--;
		if (tabSelection < 0)
			tabSelection = tabs.length - 1;
	}
	public void down() {
		tabSelection++;
		if (tabSelection >= tabs.length)
			tabSelection = 0;
	}

}
