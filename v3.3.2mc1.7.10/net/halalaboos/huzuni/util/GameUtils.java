package net.halalaboos.huzuni.util;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.ShapedRecipes;
import net.minecraft.item.crafting.ShapelessRecipes;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.StatCollector;
import net.minecraft.util.Vec3;

public final class GameUtils {

	private static final Minecraft mc = Minecraft.getMinecraft();
	
	private GameUtils() {
		
	}
	
	public static void face(double posX, double posY, double posZ) {
        double diffX = (posX) - mc.thePlayer.posX;
        double diffZ = (posZ) - mc.thePlayer.posZ;
        double diffY = (posY) - (mc.thePlayer.posY + (double) mc.thePlayer.getEyeHeight());


        double dist = (double) MathHelper.sqrt_double(diffX * diffX + diffZ * diffZ);
        float yaw = (float) (Math.atan2(diffZ, diffX) * 180.0D / Math.PI) - 90.0F;
        float pitch = (float) (-(Math.atan2(diffY, dist) * 180.0D / Math.PI));
        mc.thePlayer.rotationPitch = mc.thePlayer.rotationPitch + MathHelper.wrapAngleTo180_float(pitch - mc.thePlayer.rotationPitch);
        mc.thePlayer.rotationYaw = mc.thePlayer.rotationYaw + MathHelper.wrapAngleTo180_float(yaw - mc.thePlayer.rotationYaw);
    }
	
	public static float getDistance(float x, float y, float z) {
        float xDiff = (float) (mc.thePlayer.posX - x);
        float yDiff = (float) (mc.thePlayer.posY - y);
        float zDiff = (float) (mc.thePlayer.posZ - z);
        return MathHelper.sqrt_float(xDiff * xDiff + yDiff * yDiff + zDiff * zDiff);
	}
	
	public static boolean rayTraceBlock(int x, int y, int z) {
		MovingObjectPosition position = rayTrace(x + 0.5F, y + 0.5F, z + 0.5F);
		return position != null && position.typeOfHit == MovingObjectPosition.MovingObjectType.BLOCK && position.blockX == x && position.blockY == y && position.blockZ == z;
	}
	
	public static MovingObjectPosition rayTrace(double x, double y, double z) {
        Vec3 player = Vec3.createVectorHelper(mc.thePlayer.posX, mc.thePlayer.posY + mc.thePlayer.getEyeHeight(), mc.thePlayer.posZ),
                position = Vec3.createVectorHelper(x, y, z);
        return mc.theWorld.rayTraceBlocks(player, position);
	}
	
	public static void breakBlock(int x, int y, int z) {
		mc.theWorld.setBlockToAir(x, y, z);
	}
	
	public static String getBlockName(Block block) {
		String name = Item.getItemFromBlock(block).getUnlocalizedName();
		return name == null ? "null" : StatCollector.translateToLocal(name + ".name").trim();
	}
	
	public static int findHotbarItemById(int itemId) {
        for (int o = 0; o < 9; o++) {
            ItemStack item = mc.thePlayer.inventory.getStackInSlot(o);
            if (item != null) {
            	if (Item.getItemById(itemId) == item.getItem())
                	return o;
            }
        }
        return -1;
    }
	
	public static int findHotbarItem(Item item) {
        for (int o = 0; o < 9; o++) {
            ItemStack item2 = mc.thePlayer.inventory.getStackInSlot(o);
            if (item2 != null) {
            	if (item == item2.getItem())
                	return o;
            }
        }
        return -1;
    }
	
	public static Item getItemFromName(String name) {
		for (Object o : Item.itemRegistry) {
			Item item = (Item) o;
			String name2 = item.getUnlocalizedName() == null ? "" : StatCollector.translateToLocal(item.getUnlocalizedName() + ".name").trim();
			if (name.replaceAll(" ", "").equalsIgnoreCase(name2.replaceAll(" ", "")))
				return item;
		}
		return null;
	}
	
	public static IRecipe findRecipe(ItemStack itemStack) {
		for (IRecipe recipe : (List<IRecipe>) CraftingManager.getInstance().getRecipeList()) {
			if (compare(itemStack, recipe.getRecipeOutput()))
				return recipe;
		}
		return null;
	}
	
	public static boolean isCraftable(IRecipe recipe) {
		if (recipe instanceof ShapedRecipes)
			return isCraftable((ShapedRecipes) recipe);
		else if (recipe instanceof ShapelessRecipes)
			return isCraftable((ShapelessRecipes) recipe);
		else
			return false;
	}
	
	public static boolean isCraftable(ShapedRecipes shapedRecipe) {
		ItemStack[] items = shapedRecipe.getRecipeItems().clone();
		boolean failed = false;
		for (ItemStack item : (List<ItemStack>) mc.thePlayer.inventoryContainer.getInventory()) {
			if (item != null) {
				ItemStack inventoryItem = item.copy();
				for (int i = 0; i < items.length; i++) {
					ItemStack neededItem = items[i];
					if (neededItem != null && inventoryItem != null) {
						if (compare(inventoryItem, neededItem))
							items[i] = null;
						inventoryItem.stackSize--;
						if (inventoryItem.stackSize <= 0)
							inventoryItem = null;
					}
				}
			}
		}
		for (ItemStack neededItem : items)
			if (neededItem != null)
				failed = true;
		return !failed;
	}
	
	public static boolean isCraftable(ShapelessRecipes shapelessRecipe) {
		List<ItemStack> items = new ArrayList(shapelessRecipe.getRecipeItems()); 
		boolean failed = false;
		for (ItemStack item : (List<ItemStack>) mc.thePlayer.inventoryContainer.getInventory()) {
			if (item != null) {
				ItemStack inventoryItem = item.copy();
				for (int i = 0; i < items.size(); i++) {
					ItemStack neededItem = items.get(i);
					if (neededItem != null && inventoryItem != null) {
						if (compare(inventoryItem, neededItem))
							items.remove(i);
						inventoryItem.stackSize--;
						if (inventoryItem.stackSize <= 0)
							inventoryItem = null;
					}
				}
			}
		}
		for (ItemStack neededItem : items)
			if (neededItem != null)
				failed = true;
		return !failed;
	}
	
	public static boolean compare(ItemStack item1, ItemStack item2) {
		if (item1 != null || item2 != null) {
			if (item1 == null && item2 != null || item1 != null && item2 == null) {
				return false;
			}

			if (item2.getItem() != item1.getItem()) {
				return false;
			}

			if (item2.getItemDamage() != 32767 && item2.getItemDamage() != item1.getItemDamage()) {
				return false;
			}
		}
		return true;
	}
}
