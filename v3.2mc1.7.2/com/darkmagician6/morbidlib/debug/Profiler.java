package com.darkmagician6.morbidlib.debug;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public final class Profiler {
	private final Map<String, Long> sectionMap = new HashMap<String, Long>();
	
	private final Logger logger;
	
	private final String name;
	
	public Profiler(String profilerName) {
		logger = new Logger();
		name = profilerName;
	}
	
	public Profiler(String profilerName, final File logFile, boolean saveEveryCall) {
		logger = new Logger(logFile, saveEveryCall);
		name = profilerName;
	}
	
	public void startSection(String section) {
		logger.log(String.format("[%s] Started section: %s", name, section));
		sectionMap.put(section, System.nanoTime() / 1000000);
	}
	
	public void endSection(String section) {
		if (sectionMap.containsKey(section)) {
			long startTime = sectionMap.get(section);
			
			logger.log(String.format("[%s] Section '%s' took %s milliseconds to complete.", name, section, System.nanoTime() / 1000000 - startTime));
			sectionMap.remove(section);
		} else {
			logger.log(String.format("[%s] Section '%s' doesn't exist.", name, section));
		}
	}

}
