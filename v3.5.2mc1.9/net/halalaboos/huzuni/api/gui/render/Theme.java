package net.halalaboos.huzuni.api.gui.render;

import net.halalaboos.huzuni.api.gui.Component;
import net.halalaboos.huzuni.api.gui.Container;

public interface Theme {

	void renderComponent(float xOffset, float yOffset, Component component);
	
	void renderContainer(Container container);
	
	void renderTooltip(String tooltip);
	
	void renderSlot(float xOffset, float yOffset, int index, float[] area, boolean highlight, boolean mouseOver, boolean mouseDown);
	
    String getName();
    
	void renderBackground(int screenWidth, int screenHeight);
	
	void renderForeground(int screenWidth, int screenHeight);

}
