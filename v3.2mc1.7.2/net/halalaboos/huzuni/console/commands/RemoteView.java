/**
 *
 */
package net.halalaboos.huzuni.console.commands;

import net.halalaboos.huzuni.console.Command;
import net.halalaboos.huzuni.helpers.Helper;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.StringUtils;

/**
 * @author Halalaboos
 * @since Aug 31, 2013
 */
public class RemoteView implements Command, Helper {
    private EntityLivingBase oldPlayer = null;

    /**
     * @see net.halalaboos.huzuni.console.Command#getAliases()
     */
    @Override
    public String[] getAliases() {
        return new String[] {"remoteview", "remote", "rm"};
    }

    /**
     * @see net.halalaboos.huzuni.console.Command#getHelp()
     */
    @Override
    public String[] getHelp() {
        return new String[] {"remoteview <player>"};
    }

    /**
     * @see net.halalaboos.huzuni.console.Command#getDescription()
     */
    @Override
    public String getDescription() {
        return "Locks onto players and shows their screen.";
    }

    /**
     * @see net.halalaboos.huzuni.console.Command#run(java.lang.String, java.lang.String[])
     */
    @Override
    public void run(String input, String[] args) {
        if (oldPlayer != null) {
            huzuni.addChatMessage("No longer viewing '\2474" + mc.renderViewEntity.getCommandSenderName() + "\247f'.");
            mc.renderViewEntity = oldPlayer;
            oldPlayer = null;
        } else {
            String playerName = args[0];
            for (EntityPlayer entityPlayer : entityHelper.getPlayers()) {
                if (StringUtils.stripControlCodes(entityPlayer.getCommandSenderName()).equalsIgnoreCase(playerName)) {
                    oldPlayer = mc.renderViewEntity;
                    mc.renderViewEntity = entityPlayer;
                    huzuni.addChatMessage("Now viewing '\2472" + entityPlayer.getCommandSenderName() + "\247f'.");
                    return;
                }
            }
        }
    }

}
