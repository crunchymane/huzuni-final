package net.halalaboos.huzuni.mod.mods.render;

import net.halalaboos.huzuni.api.mod.Category;
import net.halalaboos.huzuni.api.mod.ValueOption;
import net.halalaboos.huzuni.mod.DefaultMod;
import net.halalaboos.huzuni.util.GLUtils;
import net.minecraft.client.resources.I18n;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import java.util.Collection;

import static org.lwjgl.opengl.GL11.*;

public class StatusHUD extends DefaultMod {

	//This is somewhat in 1.9 already, should we keep it?
	
	public static final StatusHUD instance = new StatusHUD();
	
	private ResourceLocation inventory = new ResourceLocation("textures/gui/container/inventory.png");
		
	private final ValueOption hudScale = new ValueOption("Scale", 0.5F, 1F, 2F, 0.5F, "Value you want to scale the hud");
		
	private StatusHUD() {
		super("Status HUD");
		setAuthor("Halalaboos");
		setAliases("hud");
		setDescription("Renders active potion effects and armor durability.");
		setCategory(Category.RENDER);
		setVisible(false);
		setOptions(hudScale);
	}

	@Override
	protected void onEnable() { }

	@Override
	protected void onDisable() { }

	public void renderHUD(int screenWidth, int screenHeight) {
//		int left = screenWidth / 2 - 110;
//		int right = left + 9 * 20 + 20;
//		int top = screenHeight - 39;
//		int bottom = top + 20;
//		renderItemStack(getWearingArmor(0).getStack(), left, top);
//		renderItemStack(getWearingArmor(1).getStack(), left, bottom);
//		renderItemStack(getWearingArmor(2).getStack(), right, top);
//		renderItemStack(getWearingArmor(3).getStack(), right, bottom);
//        float scale = hudScale.getValue(), inverse = 1F / scale, iconSize = 20 * scale;
//        int renderY = (int) (screenHeight - iconSize / 2 - (2 * scale)) - 1, ySeparator = (int) (Math.ceil(4 * scale));
//        for (PotionEffect potionEffect : (Collection<PotionEffect>) mc.thePlayer.getActivePotionEffects()) {
//            Potion potion = Potion.potionTypes[potionEffect.getPotionID()];
//            String amplifier = "";
//            if (potionEffect.getAmplifier() == 1)
//                amplifier = " II";
//            else if (potionEffect.getAmplifier() == 2)
//                amplifier = " III";
//            else if (potionEffect.getAmplifier() == 3)
//                amplifier = " IV";
//
//            String renderText = I18n.format(potionEffect.getEffectName()) + " " + amplifier;
//
//            glScalef(scale, scale, scale);
//            if (potion.hasStatusIcon()) {
//                int iconIndex = potion.getStatusIconIndex();
//                mc.getTextureManager().bindTexture(inventory);
//                GLUtils.glColor(1F, 1F, 1F, 1F);
//                renderTexture(256, 256, (screenWidth - iconSize) * inverse, (renderY - ySeparator - (2 * scale)) * inverse, 18, 18, iconIndex % 8 * 18, 198 + iconIndex / 8 * 18, 18, 18);
//            }
//
//            mc.fontRenderer.drawStringWithShadow(renderText, (int) ((screenWidth - (mc.fontRenderer.getStringWidth(renderText) * scale) - (potion.hasStatusIcon() ? iconSize : 2)) * inverse), (int) ((renderY - ySeparator) * inverse), 0xFFFFFF);
//            mc.fontRenderer.drawStringWithShadow(Potion.getDurationString(potionEffect), (int) ((screenWidth - (mc.fontRenderer.getStringWidth(Potion.getDurationString(potionEffect)) * scale) - (potion.hasStatusIcon() ? iconSize : 2)) * inverse), (int) ((renderY + ySeparator) * inverse), 0xFFFFFF);
//            renderY -= iconSize;
//            glScalef(inverse, inverse, inverse);
//        }
//        glEnable(GL_DEPTH_TEST);
	}

	private void renderItemStack(ItemStack item, int x, int y) {
		GLUtils.draw2dItem(item, x + 2, y + 2, 0F);
	}

	private Slot getWearingArmor(int armorType) {
		return mc.thePlayer.inventoryContainer.getSlot(5 + armorType);
	}

	private void renderTexture(int textureWidth, int textureHeight, float x, float y, float width, float height, float srcX, float srcY, float srcWidth, float srcHeight) {
		float renderSRCX = srcX / textureWidth, renderSRCY = srcY / textureHeight, renderSRCWidth = (srcWidth) / textureWidth, renderSRCHeight = (srcHeight) / textureHeight;
		boolean tex2D = glGetBoolean(GL_TEXTURE_2D);
		boolean blend = glGetBoolean(GL_BLEND);
		glPushMatrix();
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_TEXTURE_2D);
		glBegin(GL_TRIANGLES);
		glTexCoord2f(renderSRCX + renderSRCWidth, renderSRCY);
		glVertex2d(x + width, y);
		glTexCoord2f(renderSRCX, renderSRCY);
		glVertex2d(x, y);
		glTexCoord2f(renderSRCX, renderSRCY + renderSRCHeight);
		glVertex2d(x, y + height);
		glTexCoord2f(renderSRCX, renderSRCY + renderSRCHeight);
		glVertex2d(x, y + height);
		glTexCoord2f(renderSRCX + renderSRCWidth, renderSRCY + renderSRCHeight);
		glVertex2d(x + width, y + height);
		glTexCoord2f(renderSRCX + renderSRCWidth, renderSRCY);
		glVertex2d(x + width, y);
		glEnd();
		if (!tex2D)
			glDisable(GL_TEXTURE_2D);
		if (!blend)
			glDisable(GL_BLEND);
		glPopMatrix();
	}
}
