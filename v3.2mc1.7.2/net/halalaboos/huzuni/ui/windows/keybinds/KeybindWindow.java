/**
 * 
 */
package net.halalaboos.huzuni.ui.windows.keybinds;

import java.awt.Color;
import java.awt.Dimension;
import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.client.Settings;
import net.halalaboos.huzuni.client.waypoints.Waypoint;
import net.halalaboos.huzuni.mods.Keybindable;
import net.halalaboos.huzuni.mods.Module;
import net.halalaboos.huzuni.ui.clickable.containers.HuzuniWindow;
import net.halalaboos.huzuni.ui.clickable.theme.BmcTheme;
import net.halalaboos.huzuni.ui.clickable.theme.FalconTheme;
import net.halalaboos.huzuni.ui.clickable.theme.HuzuniTheme;
import net.halalaboos.huzuni.ui.clickable.theme.ProximityTheme;
import net.halalaboos.huzuni.ui.windows.waypoints.SlotWaypoints;
import net.halalaboos.lib.ui.Component;
import net.halalaboos.lib.ui.components.Button;
import net.halalaboos.lib.ui.components.Dropdown;
import net.halalaboos.lib.ui.components.SlotComponent;
import net.halalaboos.lib.ui.components.TextArea;
import net.halalaboos.lib.ui.containers.Window;
import net.halalaboos.lib.ui.listeners.ActionListener;
import net.halalaboos.lib.ui.theme.Theme;

/**
 * @author Halalaboos
 *
 * @since Oct 6, 2013
 */
public class KeybindWindow extends HuzuniWindow implements ActionListener {
	
	private final SlotKeybinds slotKeybinds;
	
	private final Button clearButton;
	
	/**
	 * @param title
	 */
	public KeybindWindow() {
		super("Keybinds");
		slotKeybinds = new SlotKeybinds(new Dimension(100, 54));
		add(slotKeybinds);
		slotKeybinds.setComponents(Huzuni.modManager.getKeybindableModules());
		clearButton = new Button("Clear");
		clearButton.addActionListener(this);
		add(clearButton);
	}

	/**
	 * @see net.halalaboos.lib.ui.listeners.ActionListener#onAction(net.halalaboos.lib.ui.Component)
	 */
	@Override
	public void onAction(Component component) {
		slotKeybinds.clearKeybind();
	}
	
}
