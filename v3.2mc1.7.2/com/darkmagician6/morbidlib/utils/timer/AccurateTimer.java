package com.darkmagician6.morbidlib.utils.timer;

public final class AccurateTimer extends Timer {
	
	@Override
	public long getCurrentTime() {
		return System.nanoTime() / 1000000;
	}

}
