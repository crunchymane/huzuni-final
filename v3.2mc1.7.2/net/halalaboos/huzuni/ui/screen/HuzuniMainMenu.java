package net.halalaboos.huzuni.ui.screen;

import com.darkmagician6.morbidlib.utils.timer.AccurateTimer;
import com.darkmagician6.morbidlib.utils.timer.Timer;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.client.DonatorLoader;
import net.halalaboos.huzuni.client.Settings;
import net.halalaboos.huzuni.helpers.Helper;
import net.halalaboos.huzuni.rendering.ColorFader;
import net.halalaboos.huzuni.rendering.Texture;
import net.halalaboos.huzuni.ui.particle.ParticleEngine;
import net.halalaboos.huzuni.ui.screen.alts.AltManager;
import net.halalaboos.huzuni.utils.RenderUtils;
import net.halalaboos.lib.ui.BackgroundContainer;
import net.halalaboos.lib.ui.DefaultContainer;
import net.halalaboos.lib.ui.components.SlotComponent;
import net.halalaboos.lib.ui.theme.DefaultTheme;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiMultiplayer;
import net.minecraft.client.gui.GuiOptions;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiSelectWorld;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.MathHelper;

import org.lwjgl.Sys;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class HuzuniMainMenu extends GuiScreen implements Helper {
	private static final Texture backgroundImage = new Texture("huzuni/background.jpg");
	private static final Texture title = new Texture("huzuni/title.png");
    private static final Random random = new Random();
    private static final ParticleEngine particleEngine = new ParticleEngine(false);
    private static final Timer timer = new AccurateTimer();
    
    // Secret BS
    private static final String[] secretURLs = new String[] {
            "http://goo.gl/NTXjm",
            "http://goo.gl/7WRg2",
            "http://goo.gl/FSFotD"
    };

    // Update BS
    private BackgroundContainer container;

    public HuzuniMainMenu() {
    }

    @Override
    public boolean doesGuiPauseGame() {
        return false;
    }

    @Override
    public void initGui() {
        container = new BackgroundContainer(new DefaultTheme());
        SlotComponent<String> textArea = new SlotComponent<String>(new Dimension(100, height - 2)) {

            @Override
            public void render(int index, String text, Rectangle boundaries,
                               boolean mouseOver, boolean mouseDown) {
                Huzuni.display.fontRenderer.drawString(text, boundaries.x, boundaries.y, 0xFFFFFF);
            }

            @Override
            public void onClicked(int index, String text,
                                  Rectangle boundaries, Point mouse, int buttonID) {
            }

            @Override
            public void onReleased(int index, String text, Point mouse) {
            }

            @Override
            public int getElementHeight() {
                return Huzuni.display.fontRenderer.getHeight();
            }

        };
        textArea.setArea(new Rectangle(width - 102, 1, 100, height - 2));
        textArea.setBackgroundColor(new Color(0.3F, 0.3F, 0.3F, 0.75F));
        container.add(textArea);
        for (String updateLine : huzuniHelper.getUpdates()) {
            for (String formattedLine : Huzuni.display.fontRenderer.wrapWords(updateLine, 90))
                textArea.add(formattedLine);
        }

        int var4 = this.height / 4 + 48;
        buttonList.clear();
        buttonList.add(new HuzuniButton(1, 0, this.height / 2 - 5, 90, 20, I18n.getStringParams("menu.singleplayer")));
        buttonList.add(new HuzuniButton(2, 0, this.height / 2 + 15, 90, 20, I18n.getStringParams("menu.multiplayer")));
        buttonList.add(new HuzuniButton(3, 0, this.height / 2 + 35, 90, 20, "Accounts"));
        buttonList.add(new HuzuniButton(7, 0, this.height / 2 + 55, 90, 20, DonatorLoader.isDonator() ? "Donator Perks" : "VIP/Donate"));
        buttonList.add(new HuzuniButton(0, 0, this.height / 2 + 75, 90, 20, "Options"));
        buttonList.add(new HuzuniButton(4, 0, this.height / 2 + 95, 90, 20, I18n.getStringParams("menu.quit")));
        if (huzuniHelper.getUpdateCode() == 1)
            buttonList.add(new HuzuniButton(6, 213 - 45, this.height / 2 + 30, 45, 20, "Update"));
    }

    @Override
    protected void actionPerformed(GuiButton button) {
        switch (button.id) {
            case 0:
                this.mc.displayGuiScreen(new GuiOptions(this, this.mc.gameSettings));
                break;

            case 1:
                this.mc.displayGuiScreen(new GuiSelectWorld(this));
                break;

            case 2:
                if ((new Random().nextInt(1000) + 1) == 1) {
                    Sys.openURL(secretURLs[random.nextInt(secretURLs.length)]);
                }
                this.mc.displayGuiScreen(new GuiMultiplayer(this));
                break;

            case 3:
                this.mc.displayGuiScreen(new AltManager(this));
                break;

            case 4:
                this.mc.shutdown();
                break;

            case 5:
                this.mc.displayGuiScreen(new HuzuniOptions(this));
                break;

            case 6:
            	Sys.openURL("http://halalaboos.net/huzuni.html");
            	break;
                
            case 7:
            	if (DonatorLoader.isDonator()) {
            		mc.displayGuiScreen(new HuzuniDonorFeatures(this));
            	} else
            		Sys.openURL("http://halalaboos.net/donate.html");
                break;
        }
    }

    @Override
    protected void mouseClicked(int x, int y, int buttonID) {
        super.mouseClicked(x, y, buttonID);
        container.onMousePressed(x, y, buttonID);
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {

    	GL11.glColor3f(1, 1, 1);
    	super.drawDefaultBackground();
    	float titleX = 0, 
    	titleY = height / 2 - 75,
    	titleWidth = 213,
    	titleHeight = 76;

    	drawClientBG();
    	title.renderTexture(titleX, titleY, 256, 128, Color.WHITE);

    	Huzuni.display.fontRenderer.drawString("v" + Huzuni.VERSION, titleWidth - Huzuni.display.fontRenderer.getStringWidth("v" + Huzuni.VERSION), titleY + titleHeight - 10, 0x9fdddddd);
    	
    	this.drawRect(0, 0, width, height, 0x4f000000);
    	super.drawScreen(mouseX, mouseY, partialTicks);
    	container.render(mouseX, mouseY);
    	String versionStatus = getReturnCodeStatus(huzuniHelper.getUpdateCode());
	    GL11.glPushMatrix();
	    GL11.glTranslated(titleWidth - Huzuni.display.fontRenderer.getStringWidth(versionStatus), titleY + titleHeight + 12, 0);
	    GL11.glRotatef(-20F, 0F, 0F, 1F);
	    float scale = 1.8F - MathHelper.abs(MathHelper.sin((float)(Minecraft.getSystemTime() % 1000L) / 1000.0F * (float)Math.PI * 2.0F) * 0.1F);
	    scale = scale * 100.0F / (float)(this.fontRenderer.getStringWidth(versionStatus) + 32);
	    GL11.glScalef(scale, scale, scale);
	    mc.fontRenderer.drawStringWithShadow(versionStatus, 0,  0, 0xFFFFFF);
	    GL11.glPopMatrix();
	    particleEngine.render();
	    if (timer.hasReach(100)) {
	    	particleEngine.spawnParticles(0, 0, width, height, 2.5F, 0.75F);
	    	timer.reset();
	    }
	    if (Settings.isVip()) {
		    mc.fontRenderer.drawStringWithShadow("VIP", 2,  2, 0xFF0000);
	    }
    }

    /**
     * @return String representing what the version return code means.
     */
    private String getReturnCodeStatus(int versionReturnCode) {
        switch (versionReturnCode) {
            case -1:
                return "\247oUnable to connect.";
            case 0:
                return Settings.isVip() ? "Thank you :)" : "Donate!!";
            case 1:
                return "\2474Out of date!";
            case -2:
            	return "Retrieving...";
        }
        return "" + versionReturnCode;
    }

    @Override
    public void updateScreen() {
        super.updateScreen();
        particleEngine.updateParticles();
    }
    

    private static double offsetX = 0, offsetY = 0;
    private static int factorX = 1, factorY = 1;

    /**
     * Stul's method to render our background image.
     */
    public static void drawClientBG() {
        offsetX += 0.006D * factorX;
        offsetY += 0.0025D * factorY;
        if (factorY == 1 && offsetY >= gameHelper.getScaledResolution().getScaledHeight() / 3) {
            factorY = -1;
        }
        if (factorX == 1 && offsetX >= gameHelper.getScaledResolution().getScaledWidth() / 3) {
            factorX = -1;
        }
        if (factorX == -1 && offsetX <= 0) {
            factorX = 1;
        }
        if (factorY == -1 && offsetY <= 0) {
            factorY = 1;
        }
        GL11.glPushMatrix();
        GL11.glTranslated(offsetX, offsetY, 0);
        GL11.glScaled(1.5D, 1.5D, 1.5D);
       backgroundImage.renderTexture(-(gameHelper.getScreenWidth() / 3), -(gameHelper.getScreenHeight() / 3), gameHelper.getScreenWidth(),
                gameHelper.getScreenHeight(), Color.WHITE);
        GL11.glPopMatrix();
    }
}