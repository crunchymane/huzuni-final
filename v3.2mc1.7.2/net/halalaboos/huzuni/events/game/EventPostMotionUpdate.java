package net.halalaboos.huzuni.events.game;

import net.minecraft.client.entity.EntityClientPlayerMP;

import com.darkmagician6.eventapi.events.Event;

public class EventPostMotionUpdate implements Event {

    public final EntityClientPlayerMP player;

    public EventPostMotionUpdate(EntityClientPlayerMP player) {
        this.player = player;
    }
}
