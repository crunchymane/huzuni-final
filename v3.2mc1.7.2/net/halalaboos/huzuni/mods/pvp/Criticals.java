package net.halalaboos.huzuni.mods.pvp;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;

import net.halalaboos.huzuni.events.game.EventAttackEntity;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.DefaultModule;
import net.minecraft.block.material.Material;

public class Criticals extends DefaultModule implements Listener {

    public Criticals() {
        super("Criticals", -1);
        setCategory(Category.PVP);
        setDescription("Trys to land criticals.");
    }

    @Override
    protected void onEnable() {
        registry.registerListener(this);
    }

    @Override
    protected void onDisable() {
        registry.unregisterListener(this);
    }

    @Override
    protected void onToggle() {
    }

    @EventTarget
    public void onClick(EventAttackEntity event) {
        if (shouldCritical()) {
            mc.thePlayer.motionY = 0.1F;
            mc.thePlayer.fallDistance = 0.1F;
            mc.thePlayer.onGround = false;
        }
    }

    private boolean shouldCritical() {
        return !mc.thePlayer.isInWater() && !mc.thePlayer.isInsideOfMaterial(Material.lava) && !mc.thePlayer.isInsideOfMaterial(Material.web) && mc.thePlayer.onGround;
    }

}
