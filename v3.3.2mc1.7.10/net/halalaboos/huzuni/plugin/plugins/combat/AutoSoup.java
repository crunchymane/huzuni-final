package net.halalaboos.huzuni.plugin.plugins.combat;


import net.halalaboos.huzuni.ClickQueue;
import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.plugin.Category;
import net.halalaboos.huzuni.api.plugin.ValueOption;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent;
import net.halalaboos.huzuni.event.tick.UpdateListener;
import net.halalaboos.huzuni.plugin.DefaultPlugin;
import net.halalaboos.huzuni.plugin.value.Value;
import net.halalaboos.huzuni.plugin.value.ValueManager;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class AutoSoup extends DefaultPlugin implements UpdateListener {
	
	private static final int BOWL_SOUP = 282, BOWL_SOUP_EMPTY = 281;
		
	private int oldItem = -1, soupItem = -1;
	
    private boolean eating = false;
    
    private final Value health = new Value("Health", 1F, 14F, 20F, 1F);
    
	public AutoSoup() {
		super("Auto Soup");
		setAuthor("Halalaboos");
		setDescription("Instantly eats soup for you.");
		setCategory(Category.COMBAT);
		ValueManager.add(health);
		this.setOptions(new ValueOption(health));
	}

	@Override
	public void onPreUpdate(MotionUpdateEvent event) {
		if (mc.thePlayer.isDead)
			return;
		if (eating) {
			if (mc.thePlayer.getHealth() > health.getValue()) {
				mc.playerController.onStoppedUsingItem(mc.thePlayer);
				eating = false;
				soupItem = -1;
				mc.thePlayer.inventory.currentItem = oldItem;
				oldItem = -1;
				mc.playerController.updateController();
				return;
			}
			if (hasEmptySoup()) {
				mc.playerController.onStoppedUsingItem(mc.thePlayer);
				eating = false;
				soupItem = -1;
				if (mc.thePlayer.getHealth() > health.getValue()) {
					mc.thePlayer.inventory.currentItem = oldItem;
					oldItem = -1;
					mc.playerController.updateController();
				}
				return;
			}
			if (mc.thePlayer.inventory.currentItem != soupItem) {
				mc.thePlayer.inventory.currentItem = soupItem;
				mc.playerController.updateController();
			}
			return;
		}
		if (mc.thePlayer.getHealth() <= health.getValue()) {
			int soup = findHotbarSoup();
			if (soup == -1) {
				int goodSoup = findGoodSoup(), replacementSlot = findEmptySoup();
				if (goodSoup != -1 && replacementSlot != -1) {
					if (isShiftable(mc.thePlayer.inventoryContainer.getSlot(replacementSlot).getStack())) {
						ClickQueue.add(replacementSlot, 0, 1);
						ClickQueue.add(goodSoup, 0, 1);
					} else {
						ClickQueue.add(goodSoup, 0, 0);
						ClickQueue.add(replacementSlot, 0, 0);
						ClickQueue.add(goodSoup, 0, 0);
					}
				}
			} else {
				if (oldItem == -1)
					oldItem = mc.thePlayer.inventory.currentItem;
				mc.thePlayer.inventory.currentItem = soup;
				soupItem = soup;
				useItem(soup, mc.thePlayer.inventory.getStackInSlot(soup));
				eating = true;
			}
		}
	}

	private boolean hasEmptySoup() {
		return mc.thePlayer.getCurrentEquippedItem() == null || Item.getIdFromItem(mc.thePlayer.getCurrentEquippedItem().getItem()) != BOWL_SOUP;
	}

	@Override
	public void onPostUpdate(MotionUpdateEvent event) {
	}

	@Override
	protected void onEnable() {
		Huzuni.registerUpdateListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterUpdateListener(this);
	}
	
	private int findGoodSoup() {
        for (int o = 9; o < 36; o++) {
            if (mc.thePlayer.inventoryContainer.getSlot(o).getHasStack()) {
                ItemStack item = mc.thePlayer.inventoryContainer.getSlot(o).getStack();
                if (item != null) {
                    if (Item.getIdFromItem(item.getItem()) == BOWL_SOUP)
                        return o;
                }
            }
        }
        return -1;
    }
	
	private int findEmptySoup() {
        for (int o = 36; o < 45; o++) {
            ItemStack item = mc.thePlayer.inventoryContainer.getSlot(o).getStack();
            if (item == null)
            	return o;
            else
            	if (Item.getIdFromItem(item.getItem()) == BOWL_SOUP_EMPTY)
            		return o;
        }
        return -1;
	}
	
	private int findHotbarSoup() {
        for (int o = 0; o < 9; o++) {
            ItemStack item = mc.thePlayer.inventory.getStackInSlot(o);
            if (item != null)
            	if (Item.getIdFromItem(item.getItem()) == BOWL_SOUP)
            		return o;
        }
        return -1;
    }

	 /**
     * @return True if the item is shift clickable.
     */
    private boolean isShiftable(ItemStack preferedItem) {
        if (preferedItem == null)
            return true;
        for (int o = 9; o < 36; o++) {
            if (mc.thePlayer.inventoryContainer.getSlot(o).getHasStack()) {
                ItemStack item = mc.thePlayer.inventoryContainer.getSlot(o).getStack();
                if (item == null)
                    return true;
                else if (Item.getIdFromItem(item.getItem()) == Item.getIdFromItem(preferedItem.getItem())) {
                    if (item.stackSize + preferedItem.stackSize <= preferedItem.getMaxStackSize())
                        return true;
                }
            } else
                return true;
        }
        return false;
    }
    
    private void useItem(int index, ItemStack item) {
        mc.thePlayer.inventory.currentItem = index;
        mc.playerController.sendUseItem(mc.thePlayer, mc.theWorld, item);
    }
	
}
