/**
 *
 */
package net.halalaboos.huzuni.ui.windows;

import in.brud.huzuni.ui.window.ChatWindow;
import in.brud.huzuni.ui.window.theme.HuzuniThemeRedux;
import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.client.Settings;
import net.halalaboos.huzuni.client.waypoints.Waypoint;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.Module;
import net.halalaboos.huzuni.ui.clickable.containers.HuzuniModuleWindow;
import net.halalaboos.huzuni.ui.clickable.containers.HuzuniWindow;
import net.halalaboos.huzuni.ui.clickable.theme.BmcTheme;
import net.halalaboos.huzuni.ui.clickable.theme.FalconTheme;
import net.halalaboos.huzuni.ui.clickable.theme.HuzuniTheme;
import net.halalaboos.huzuni.ui.clickable.theme.PringlesTheme;
import net.halalaboos.huzuni.ui.clickable.theme.ProximityTheme;
import net.halalaboos.huzuni.ui.windows.friends.FriendWindow;
import net.halalaboos.huzuni.ui.windows.hub.HubWindow;
import net.halalaboos.huzuni.ui.windows.hub.SlotHub;
import net.halalaboos.huzuni.ui.windows.keybinds.KeybindWindow;
import net.halalaboos.huzuni.ui.windows.waypoints.WaypointWindow;
import net.halalaboos.huzuni.ui.windows.xray.XrayWindow;
import net.halalaboos.lib.io.XMLFileHandler;
import net.halalaboos.lib.ui.Component;
import net.halalaboos.lib.ui.Container;
import net.halalaboos.lib.ui.DefaultContainer;
import net.halalaboos.lib.ui.GlobalContainer;
import net.halalaboos.lib.ui.components.Button;
import net.halalaboos.lib.ui.components.Slider;
import net.halalaboos.lib.ui.containers.Window;
import net.halalaboos.lib.ui.listeners.ActionListener;
import net.halalaboos.lib.ui.theme.Theme;

import java.awt.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * @author Halalaboos
 * @since Aug 19, 2013
 */
public final class WindowManager extends GlobalContainer {
	private final XMLFileHandler fileHandler = new XMLFileHandler(new File(Huzuni.getSaveDirectory(), "Windows.xml"));
	
	private final String[] themeNames = new String[] {
			"Huzuni",
			"Proximity",
			"BMC",
            "Secret",
	};
	
	private final Theme[] themes = new Theme[] {
			new HuzuniTheme(),
			new ProximityTheme(),
			new BmcTheme(),
            new HuzuniThemeRedux(),
	};
	
	private final GlobalContainer ingameManager = new GlobalContainer();
	
	/**
	 * Held in a field so we can invoke it's on message recieved function. <br>
	 * I feel that making it a listener and waiting for a chat event is kindof unnecessary.
	 * */
	private ChatWindow chatWindow;
	
    /**
     *
     */
    public void initWindows() {
        ActionListener buttonActionListener = getButtonListener();

        for (Category category : Category.values()) {
        	HuzuniWindow window = new HuzuniWindow(category.formalName);
            for (Module module : Huzuni.modManager.getList()) {
                if (module.getCategory() != category)
                    continue;
                Button button = new Button(module.getName());
                button.setTooltip(module.getDescription());
                button.setHighlight(module.isEnabled());
                button.addActionListener(buttonActionListener);
                window.add(button);
            }
            window.layout();
            super.add(window);
        }

        Window friendsWindow = new FriendWindow();
        friendsWindow.layout();
        super.add(friendsWindow);

        Window xrayWindow = new XrayWindow();
        xrayWindow.layout();
        super.add(xrayWindow);
        
        Window waypointWindow = new WaypointWindow(this);
        super.add(waypointWindow);
        
        Window sliderWindow = new ValuesWindow();
        sliderWindow.layout();
        super.add(sliderWindow);
        
        Window hubWindow = new HubWindow(this);
        hubWindow.layout();
        super.add(hubWindow);
        
        chatWindow = new ChatWindow();
        chatWindow.layout();
        chatWindow.setMinimized(false);
        ingameManager.add(chatWindow);
        
        updateThemes();
        
        layoutWindows(getWindows(), 2, 2, 2, 5);
        
        load();
    }

    public void layoutWindows(List<HuzuniWindow> windows, int startX, int startY, int padding, int rowSize) {
        int xPos = 0, rowCount = 0;
        int[] heights = new int[rowSize];
        for (Window window : windows) {
        	if (window.isActive()) {
        		if (rowCount >= rowSize) {
        			rowCount = 0;
        			xPos = 0;
        		}        
        		window.setPosition(startX + xPos, startY + heights[rowCount]);
        		int windowWidth = window.getArea().width + window.getBorderPadding(),
        		windowHeight = window.getArea().y + window.getArea().height;
        		heights[rowCount] = windowHeight + padding;
        		xPos += windowWidth + 1 + padding;
        		rowCount++;
        	}
        }
    }

    protected ActionListener getButtonListener() {
        return new ActionListener() {

            @Override
            public void onAction(Component component) {
                if (component instanceof Button) {
                    Button button = (Button) component;
                    Module mod = Huzuni.modManager.getMod(button.getTitle());
                    if (mod != null) {
                        mod.toggle();
                        button.setHighlight(mod.isEnabled());
                    }
                }
            }
        };
    }

    public void addModuleToWindow(Module module) {
        if (module.getCategory() != null) {
            for (HuzuniWindow window : getWindows()) {
        		if (window.getTitle().equals(module.getCategory().formalName)) {
        			Button button = new Button(module.getName());
                    button.setTooltip(module.getDescription());
                    button.setHighlight(module.isEnabled());
                    button.addActionListener(getButtonListener());
                    window.add(button);
                    window.layout();
        			return;	
        		}
            }
        }
    }

    public List<HuzuniWindow> getWindows() {
        List<HuzuniWindow> windows = new ArrayList<HuzuniWindow>();
        for (Container container : getContainers())
            if (container instanceof HuzuniWindow)
                windows.add((HuzuniWindow) container);
        return windows;
    }
    
    public HuzuniWindow getWindow(String title) {
    	for (HuzuniWindow window : getWindows()) {
    		if (window.getTitle().equalsIgnoreCase(title)) {
    			return window;
    		}
    	}
    	return null;
    }
    
    public void load() {
        try {
        	// Read the document.
            Document doc = fileHandler.read();
            NodeList elementList = doc.getElementsByTagName("Window");
            for (int i = 0; i < elementList.getLength(); i++) {
                Node node = elementList.item(i);
                // If it's an element.
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    String title = element.getAttribute("Title");
                    boolean active = Boolean.parseBoolean(element.getAttribute("Active"));
                    boolean minimized = Boolean.parseBoolean(element.getAttribute("Minimized"));
                    
                    int x = Integer.parseInt(element.getElementsByTagName("X").item(0).getTextContent()),
                    y = Integer.parseInt(element.getElementsByTagName("Y").item(0).getTextContent());
                    loadAttributes(title, x, y, active, minimized);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

	public void save() {
        fileHandler.createDocument();
        
        Element windowsRoot = fileHandler.createElement("Windows");
        fileHandler.addElement(windowsRoot);
        
        for (HuzuniWindow window : getWindows()) {
            Element windowElement = fileHandler.createElement("Window");
            windowsRoot.appendChild(windowElement);
            // Give window attributes.
            windowElement.setAttribute("Title", window.getTitle());
            windowElement.setAttribute("Active", "" + window.isActive());
            windowElement.setAttribute("Minimized", "" + window.isMinimized());
            windowElement.appendChild(fileHandler.createElement("X", "" + window.getArea().x));
            windowElement.appendChild(fileHandler.createElement("Y", "" + window.getArea().y));
        }
        try {
            fileHandler.write();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
	}
	
	/**
	 * @param windowTitle
	 * @param x
	 * @param y
	 * @param active
	 */
	private void loadAttributes(String windowTitle, int x, int y, boolean active, boolean minimized) {
        for (HuzuniWindow window : getWindows()) {
        	if (window.getTitle().equals(windowTitle)) {
        		window.setPosition(x, y);
        		window.setActive(active);
        		window.setMinimized(minimized);
        		break;
        	}
        }
	}
	
	public ChatWindow getChatWindow() {
		return chatWindow;
	}

	public GlobalContainer getIngameManager() {
		return ingameManager;
	}

	@Override
	public void add(Container container) {
		super.add(container);
		updateThemes();
	}
	
    public void updateThemes() {
    	updateThemes(getSelectedThemeIndex());
    }
    
    public void updateThemes(int index) {
    	if (index < 0 || index >= themes.length)
    		index = 0;
    	Theme theme = themes[index];
		Settings.put("GUI Theme", index);
    	setTheme(theme);
    	ingameManager.setTheme(theme);
    }
    
	/**
	 * @return
	 */
	public String[] getThemeNames() {
		return themeNames;
	}

	public Theme[] getThemes() {
		return themes;
	}
	
	public Theme getTheme(int index) {
		return themes[index];
	}
	
	public Theme getSelectedTheme() {
		return themes[Settings.config.getInt("GUI Theme")];
	}
	
	public int getSelectedThemeIndex() {
		return Settings.config.getInt("GUI Theme");
	}

	/**
	 * 
	 */
	public void resetWindows() {
        layoutWindows(getWindows(), 2, 2, 2, 5);
        save();
	}
}
