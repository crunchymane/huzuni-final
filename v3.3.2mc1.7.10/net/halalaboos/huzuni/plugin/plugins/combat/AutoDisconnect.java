package net.halalaboos.huzuni.plugin.plugins.combat;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.plugin.Category;
import net.halalaboos.huzuni.event.packet.PacketEvent;
import net.halalaboos.huzuni.event.packet.PacketListener;
import net.halalaboos.huzuni.plugin.DefaultPlugin;
import net.minecraft.network.play.server.S06PacketUpdateHealth;

/**
 * @author brudin
 * @version 1.0
 * @since 4/10/14
 */
public class AutoDisconnect extends DefaultPlugin implements PacketListener {

	public AutoDisconnect() {
		super("Auto Disconnect");
		setDescription("Automatically disconnects when your health is lower than 3 hearts.");
		setAuthor("brudin");
		setCategory(Category.COMBAT);
		setVisible(false);
	}

	@Override
	public void onPacket(PacketEvent event, PacketEvent.PacketType type) {
		if(type == PacketEvent.PacketType.READ) {
			if(event.getPacket() instanceof S06PacketUpdateHealth) {
				S06PacketUpdateHealth packetUpdateHealth = (S06PacketUpdateHealth)event.getPacket();
				if(packetUpdateHealth.func_149332_c() <= 6.0F) {
					mc.thePlayer.boundingBox.offset(0, 42000, 0);
					setEnabled(false);
				}
			}
		}
	}

	@Override
	protected void onEnable() {
		Huzuni.registerPacketListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterPacketListener(this);
	}
}
