package net.halalaboos.huzuni.mod.mods.movement;

import org.lwjgl.input.Keyboard;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.mod.Category;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent;
import net.halalaboos.huzuni.event.tick.UpdateListener;
import net.halalaboos.huzuni.mod.DefaultMod;
import net.minecraft.block.material.Material;

/**
 * @author brudin
 * @version 1.0
 * @since 4/10/14
 */
public class Dolphin extends DefaultMod implements UpdateListener {

	public Dolphin() {
		super("Dolphin", Keyboard.KEY_K);
		setDescription("Automatically swims for you.");
		setAuthor("brudin");
		setCategory(Category.MOVEMENT);
	}

	@Override
	protected void onEnable() {
		Huzuni.registerUpdateListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterUpdateListener(this);
	}

	@Override
	public void onPreUpdate(MotionUpdateEvent event) {
		if (!mc.gameSettings.keyBindSneak.isKeyDown() && !mc.gameSettings.keyBindJump.isKeyDown()) {
			if (mc.thePlayer.isInLava()) {
				mc.thePlayer.motionY += 0.03999999910593033;
			} else if (mc.thePlayer.isInWater()) {
				mc.thePlayer.motionY += 0.03999999910593033;
			}
		}
	}

	@Override
	public void onPostUpdate(MotionUpdateEvent event) {

	}
}
