/**
 * 
 */
package net.halalaboos.huzuni.ui.clickable.containers;

import java.awt.Dimension;

import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.Module;
import net.halalaboos.huzuni.ui.clickable.components.SlotModules;

/**
 * @author Halalaboos
 *
 * @since Oct 5, 2013
 */
public class HuzuniModuleWindow extends HuzuniWindow {
	
	private final Category category;
	
	private final SlotModules slotModules = new SlotModules(new Dimension(110, 59));

	/**
	 * @param title
	 * @param slot
	 */
	public HuzuniModuleWindow(Category category) {
		super(category.formalName);
		this.category = category;
		add(slotModules);
	}
	
	public SlotModules getSlotModules() {
		return slotModules;
	}

	public void add(Module module) {
		slotModules.add(module);
	}
	
	public void remove(Module module) {
		slotModules.remove(module);
	}

	public Category getCategory() {
		return category;
	}
}
