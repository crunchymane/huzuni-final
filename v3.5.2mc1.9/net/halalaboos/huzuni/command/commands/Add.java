package net.halalaboos.huzuni.command.commands;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.NameProtect;
import net.halalaboos.huzuni.api.console.Command;
import net.halalaboos.huzuni.util.StringUtils;

public class Add implements Command {

    @Override
    public String[] getAliases() {
        return new String[] {"add"};
    }

    @Override
    public String[] getHelp() {
        return new String[] {"add <username> <(optional) alias>"};
    }

    @Override
    public String getDescription() {
        return "Adds a user to the friends list. (Protect them from kill aura, distinguishable from other players)";
    }

    @Override
    public void run(String input, String[] args) {
    	String name = args[0];
		boolean sucessful = !Huzuni.isFriend(name);
        Huzuni.addChatMessage("Friend '" + name + "' " + (sucessful ? "added!" : "already in your friends!"));
        Huzuni.addFriend(name);
		Huzuni.saveFriends();
        String alias = StringUtils.getAfter(input, 2);
        if (alias != null) {
        	NameProtect.add(name, alias);
        	NameProtect.save();
            Huzuni.addChatMessage("'" + name + "' now referred to as '" + alias + "'.");
        }
    }

}
