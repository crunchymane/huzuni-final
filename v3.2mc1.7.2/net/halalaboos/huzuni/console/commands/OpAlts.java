package net.halalaboos.huzuni.console.commands;

import com.darkmagician6.morbidlib.io.file.FileHelper;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.console.Command;

import java.io.File;
import java.util.List;

public class OpAlts implements Command {

    @Override
    public String[] getAliases() {
        return new String[] {"opalts"};
    }

    @Override
    public String[] getHelp() {
        return new String[] {".opalts"};
    }

    @Override
    public String getDescription() {
        return "Op's all your alts.";
    }

    @Override
    public void run(String input, String[] args) {
        File altFile = new File(Huzuni.getSaveDirectory(), "Accounts.txt");
        final List<String> altList = FileHelper.read(altFile);
        new Thread() {
            @Override
            public void run() {
                for (String line : altList) {
                    String[] splittedLine = line.split(":");
                    String username = splittedLine[0];
                    if (!username.contains("@")) {
                        try {
                            mc.thePlayer.sendChatMessage("/op " + splittedLine[0]);
                            Thread.sleep(300);
                        } catch (InterruptedException e) {
                        } catch (NullPointerException e) {
                        }
                    }
                }
            }
        }.start();
    }

}
