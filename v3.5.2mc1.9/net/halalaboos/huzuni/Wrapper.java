package net.halalaboos.huzuni;

import net.halalaboos.huzuni.command.CommandManager;
import net.halalaboos.huzuni.gui.GuiManager;
import net.halalaboos.huzuni.mod.ModManager;
import net.halalaboos.huzuni.mod.keybinds.WindowOpener;
import net.halalaboos.huzuni.mod.mods.Patcher;
import net.halalaboos.huzuni.mod.mods.render.StatusHUD;
import net.halalaboos.huzuni.mod.mods.MiddleClickFriends;
import net.halalaboos.huzuni.mod.mods.world.Xray;
import net.halalaboos.huzuni.scripting.ScriptManager;
import net.halalaboos.huzuni.scripting.wrapping.ScriptWrapper;
import net.halalaboos.huzuni.util.threads.ThreadCheckVersion;
import net.halalaboos.huzuni.util.threads.ThreadCheckVip;
import net.halalaboos.huzuni.util.threads.ThreadDownloadDonators;
import net.halalaboos.huzuni.util.threads.ThreadReadChangelog;
import net.halalaboos.huzuni.util.threads.ThreadReadSplash;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.ServerData;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.util.Session;

public final class Wrapper {

	private static Patcher patcher = new Patcher();

	private Wrapper() {
		/**
		 *
		 * Packet events
		 * - NetworkManager.processReceivedPackets for READING packets.		[]
		 * - NetHandlerPlayClient.addToSendQueue for SENDING packets.		[]
		 *
		 * Xray
		 * - Block.shouldSideBeRendered										[]
		 * - Block.isFullCube												[]
		 * - TileEntityRendererDispatcher									[]
		 * - BlockRendererDispatcher										[]
		 * 
		 * Nametags
		 * - Render.renderLivingLabel										[]
		 * - RendererLivingEntity.passSpecialRender							[]
		 * - RenderPlayer removed distance check							[]
		 *
		 * Renderables
		 * - EntityRenderer													[]
		 *
		 * Huzuni button
		 * - GuiIngameMenu													[]
		 * 
		 * Inventory helper
		 * - GuiInventory													[]
		 * - GuiCrafting													[]
		 * 
		 * No slowdown
		 * - EntityPlayerSP													[]
		 * - BlockWeb														[]
		 * - BlockSoulSand													[]
		 * 
		 * Freecam
		 * - EntityPlayerSP (aparently you never wrote this DOWN)			[]
		 * 
		 * Huzuni chat messages
		 * - GuiNewChat														[]
		 *
		 * AbstractClientPlayer for cape									[x]
		 * */
	}

	/**
	 * Inside of Minecraft.startGame
	 * */
	public static void onGameStart() {
		Huzuni.setupDefaultConfig();
		Huzuni.getTeam().setup();
		ModManager.setupMods();
		Huzuni.addKeybind(new WindowOpener());
		Huzuni.loadConfig();
		ModManager.loadMods();
		Huzuni.loadRotationQueue();
		CommandManager.loadCommands();
		GuiManager.loadWindows();
		Huzuni.loadFriends();
		Huzuni.loadMacros();
		Huzuni.loadKeybinds();
		NameProtect.load();
		Xray.instance.load();
		new ThreadCheckVip().start();
		new ThreadDownloadDonators().start();
		new ThreadCheckVersion().start();
		new ThreadReadChangelog().start();
		new ThreadReadSplash().start();
		ScriptManager.setupEngine();
		patcher.init();
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				onGameEnd();
			}
		});
	}

	/**
	 * Inside of Minecraft.shutdownMinecraftApplet
	 * */
	public static void onGameEnd() {
		Huzuni.CONFIG.put("First use", false);
		Huzuni.saveConfig();
		ModManager.saveMods();
		GuiManager.saveWindows();
		Huzuni.saveFriends();
		Huzuni.saveMacros();
		Huzuni.saveKeybinds();
		NameProtect.save();
		Xray.instance.save();
		Huzuni.saveRotationQueue();
	}

	/**
	 * Inside of Minecraft.setServerData
	 * */
	public static void onJoinServer(ServerData serverData) {
		if (serverData != null) {
		}
	}
	
	/**
	 * Inside of Minecraft.loadWorld
	 * */
	public static void onLoadWorld(WorldClient world) {
		if (Huzuni.CONFIG.getBoolean("First use")) {
			Huzuni.addChatMessage("Welcome to huzuni!");
			Huzuni.addChatMessage("Press right shift to open the gui.");
			Huzuni.addChatMessage("Type '.help' for commands!");
			Huzuni.CONFIG.put("First use", false);
		}
	}
	
	/**
	 * Inside of Minecraft constructor
	 * Inside of Minecraft.setSession
	 * */
	public static void onChangeSession(Session session) {
	}
	
	/**
	 * Inside of Minecraft.runTick
	 * */
	public static void onKeyTyped(int keyCode, char c) {
		if (Huzuni.isEnabled()) {
			String macro = Huzuni.MACROS[keyCode];
			if (macro != null && !macro.isEmpty())
				CommandManager.processCommand(macro);
			Huzuni.runKeybinds(keyCode);
			GuiManager.getIngameTheme().onKeyTyped(keyCode);
		}
	}

	public static void onMouseClicked(int buttonID) {
		if (Huzuni.isEnabled()) {
			if (MiddleClickFriends.instance.isEnabled()) {
				MiddleClickFriends.instance.onClick(buttonID);
			}
		}
	}

	/**
	 * Inside of GuiIngame.renderGameOverlay
	 * */
	public static void renderIngame(Minecraft mc, int screenWidth, int screenHeight) {
		if (Huzuni.isEnabled()) {
			GuiManager.getIngameTheme().render(mc, screenWidth, screenHeight);
			if (StatusHUD.instance.isEnabled()) {
				StatusHUD.instance.renderHUD(screenWidth, screenHeight);
			}
			ScriptWrapper.instance.renderList();
		}
	}
}
