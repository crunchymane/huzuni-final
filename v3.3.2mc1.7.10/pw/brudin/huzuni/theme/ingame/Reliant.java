package pw.brudin.huzuni.theme.ingame;

import net.halalaboos.huzuni.gui.ingame.IngameTheme;
import net.halalaboos.huzuni.plugin.DefaultPlugin;
import net.halalaboos.huzuni.plugin.PluginManager;
import net.minecraft.client.Minecraft;

/**
 * @author brudin
 * @version 1.0
 * @since 6/8/2014
 */
public class Reliant implements IngameTheme {

	private TagReplacement[] tagReplacements = {
			new TagReplacement("Speedmine", "Speedy Gonzales", 14721857),
			new TagReplacement("Bright", "Fullbright", 15400711),
			new TagReplacement("Nuker", "Nuker", 4751892),
			new TagReplacement("Flight", "Flight", 8370648),
			new TagReplacement("NoFall", "NoFall", 1295394),
			new TagReplacement("Xray", "Wallhack", 8167066),
			new TagReplacement("Kill Aura", "Kill Aura", 14816280),
			new TagReplacement("Sprint", "Speed", 9882681),
			new TagReplacement("Sneak", "Sneak", 36864),
			new TagReplacement("Annoy", "Annoy", 12867327),
			new TagReplacement("Retard", "Retard", 7182592),
	};

	@Override
	public void render(Minecraft mc, int screenWidth, int screenHeight) {
		int yPos = 2;
		for(DefaultPlugin plugin : PluginManager.getDefaultPlugins()) {
			if(plugin.isEnabled()) {
				for (TagReplacement tagReplacement : tagReplacements) {
					if (plugin.getName().equalsIgnoreCase(tagReplacement.originalName)) {
						mc.fontRenderer.drawStringWithShadow(tagReplacement.newName, screenWidth - 2 -
										mc.fontRenderer.getStringWidth(tagReplacement.newName), yPos,
								tagReplacement.newColor);
						yPos += 10;
					}
				}
			}
		}
	}

	@Override
	public String getName() {
		return "Reliant";
	}

	@Override
	public void onKeyTyped(int keyCode) {

	}
}
