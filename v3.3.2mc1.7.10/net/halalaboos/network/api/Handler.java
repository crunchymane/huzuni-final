package net.halalaboos.network.api;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;

public interface Handler {

	public Socket getSocket();
	
	public DataInputStream getInputStream();
	
	public DataOutputStream getOutputStream();
	
	public void onRecievedPacket(Packet packet, Object data);
}
