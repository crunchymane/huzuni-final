package net.halalaboos.huzuni.mod.mods.world;

import net.halalaboos.huzuni.api.mod.Category;
import net.halalaboos.huzuni.mod.DefaultMod;
import net.halalaboos.huzuni.queue.packet.PacketQueue;
import net.minecraft.network.play.client.CPacketUpdateSign;
import net.minecraft.tileentity.TileEntitySign;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;

public class Autosign extends DefaultMod {
	
	public static final Autosign instance = new Autosign();
	
	private ITextComponent[] lines = new ITextComponent[] {
			new TextComponentString("Halalaboos"), new TextComponentString("is"), new TextComponentString("super cool"), new TextComponentString("")
	};
	
	private Autosign() {
		super("Auto Sign");
		setCategory(Category.WORLD);
		setAliases("sign");
		setDescription("Automatically place down a sign with text.");
	}
	
	public void placeSign(final TileEntitySign sign) {
		sign.signText = lines;
		sign.markDirty();
		int time = 0;
		for (ITextComponent component : lines) {
			if (component != null)
				time += component.getUnformattedText().toCharArray().length * 50;
		}
		PacketQueue.add(new CPacketUpdateSign(sign.getPos(), lines), 150 + time);
		sign.setEditable(true);
	}

	@Override
	protected void onEnable() {
	}

	@Override
	protected void onDisable() {
	}
	
	public void setLines(ITextComponent[] lines) {
		this.lines = lines;
	}
	
	
}
