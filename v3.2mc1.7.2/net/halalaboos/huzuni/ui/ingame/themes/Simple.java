package net.halalaboos.huzuni.ui.ingame.themes;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.DefaultModule;
import net.halalaboos.huzuni.ui.ingame.IngameTheme;
import net.minecraft.client.Minecraft;

public class Simple implements IngameTheme {

    @Override
    public void render(Minecraft mc, int screenWidth, int screenHeight) {
        mc.fontRenderer.drawStringWithShadow(Huzuni.TITLE, 2, 2, 0xFFFFFF);

        int yPos = 2;
        for (DefaultModule module : Huzuni.modManager.getDefaultModules()) {
            if (module.shouldRenderWhenEnabled() && module.isEnabled()) {
                mc.fontRenderer.drawStringWithShadow(module.getRenderName(), screenWidth - 2 -
                        mc.fontRenderer.getStringWidth(module.getRenderName()), yPos, getColor(module.getCategory()));
                yPos += 10;
            }
        }
    }

    @Override
    public String getName() {
        return "Simple";
    }

    @Override
    public void onKeyPressed(int keyCode) {
    }

    private int getColor(Category category) {
        int color = -1;
        switch (category) {
            case PLAYER:
                color = 0xFF59C28C;
                break;
            case PVP:
                color = 0xFFDB5858;
                break;
            case WORLD:
                color = 0xFF58B6DB;
                break;
            case RENDER:
                color = 0xFF5BAB6F;
                break;
            case MOVEMENT:
                color = 0xFFDEC1D1;
                break;
            case MISC:
                color = 0xFF54D6BA;
                break;
        }
        return color;
    }
}
