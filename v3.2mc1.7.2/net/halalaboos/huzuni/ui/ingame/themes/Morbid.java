package net.halalaboos.huzuni.ui.ingame.themes;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.mods.DefaultModule;
import net.halalaboos.huzuni.ui.ingame.IngameTheme;
import net.minecraft.client.Minecraft;

public class Morbid implements IngameTheme {

    @Override
    public void render(Minecraft mc, int screenWidth, int screenHeight) {
        mc.fontRenderer.drawStringWithShadow(Huzuni.TITLE + " \2470[\247fv" + Huzuni.VERSION + "\2470]", 2, 2, 0xFF4400);

        int yPos = 2;
        for (DefaultModule module : Huzuni.modManager.getDefaultModules()) {
            if (module.isEnabled()) {
                if (module.shouldRenderWhenEnabled()) {
                    int with = screenWidth - (mc.fontRenderer.getStringWidth(module.getRenderName()) + 10);
                    mc.fontRenderer.drawStringWithShadow("\2470[\247r" + module.getRenderName() + "\2470]", with, yPos, 0xFFFFFF);
                    yPos += 12;
                }
            }
        }
    }

    @Override
    public String getName() {
        return "Morbid";
    }

    @Override
    public void onKeyPressed(int keyCode) {
    }

}
