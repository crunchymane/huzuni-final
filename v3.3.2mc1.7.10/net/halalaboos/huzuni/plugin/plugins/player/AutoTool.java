package net.halalaboos.huzuni.plugin.plugins.player;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.plugin.ModeOption;
import net.halalaboos.huzuni.api.plugin.Category;
import net.halalaboos.huzuni.event.packet.PacketEvent;
import net.halalaboos.huzuni.event.packet.PacketListener;
import net.halalaboos.huzuni.event.packet.PacketEvent.PacketType;
import net.halalaboos.huzuni.plugin.DefaultPlugin;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.client.C07PacketPlayerDigging;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

/**
 * @author brudin
 * @version 1.0
 * @since 3/25/14
 */
public class AutoTool extends DefaultPlugin implements PacketListener {

	private boolean running = false;
	
	private int oldSlot, newSlot;
	
    private final ModeOption mode;
    
	public AutoTool() {
		super("Auto Tool", Keyboard.KEY_J);
		setCategory(Category.PLAYER);
		setAuthor("Halalaboos");
		setDescription("Automatically switches to the best tool when mining.");
		setOptions(mode = new ModeOption("Auto tool mode", "Switch", "Normal"));
	}

	@Override
	protected void onEnable() {
		Huzuni.registerPacketListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterPacketListener(this);
	}

	@Override
	public void onPacket(PacketEvent event, PacketType type) {
		if (type == PacketEvent.PacketType.SENT && !mc.playerController.isInCreativeMode()) {
			if (event.getPacket() instanceof C07PacketPlayerDigging) {
				C07PacketPlayerDigging packet = (C07PacketPlayerDigging) event.getPacket();
				if (packet.func_149506_g() == 0) { //Click block
					int blockX = packet.func_149505_c(), blockY = packet.func_149503_d(), blockZ = packet.func_149502_e();
					int newSlot = bestSlot(Minecraft.getMinecraft().theWorld.getBlock(blockX, blockY, blockZ));
					if (!running) {
						oldSlot = Minecraft.getMinecraft().thePlayer.inventory.currentItem;
						running = true;
					}
					if (newSlot != -420) {
						this.newSlot = newSlot;
						Minecraft.getMinecraft().thePlayer.inventory.currentItem = this.newSlot;
					}
				}
			} else  { //Break block, but probably better just in a tick event
				if (running && !Mouse.isButtonDown(0)) {
					if (mode.getSelected() == 0) {
						Minecraft.getMinecraft().thePlayer.inventory.currentItem = oldSlot;
					}
					running = false;
				}
			}
		}
	}
	
	private int bestSlot(Block block) {
		int slot = -420;
		float str = 1.0F;
		for (int i = 0; i < 9; i++) {
			try {
				ItemStack item = Minecraft.getMinecraft().thePlayer.inventory.getStackInSlot(i);
				if (item != null) {
					if (item.func_150997_a(block) > str) {
						str = item.func_150997_a(block);
						slot = i;
					}
				}
			} catch (Exception e) { }
		}
		return slot;
	}
}
