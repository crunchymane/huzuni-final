/**
 * 
 */
package net.halalaboos.lib.ui;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import net.halalaboos.lib.ui.layout.DefaultLayout;
import net.halalaboos.lib.ui.layout.Layout;
import net.halalaboos.lib.ui.theme.Theme;

/**
 * @author Halalaboos
 *
 */
public class DefaultContainer implements Container {

	protected List<Component> components = new CopyOnWriteArrayList<Component>();
	
	protected Component mousedComponent, downComponent;
	
	protected boolean mouseDown, mouseOver;
	
	protected Layout layout = new DefaultLayout();
	
	protected Point offset = new Point(0, 0);
	
	protected Rectangle area = new Rectangle();
	
	protected boolean active = true;
	
	protected final Point mouse = new Point(0, 0);
	
	public DefaultContainer(Rectangle area) {
		this.area = area;
	}
	
	/**
	 * @see net.halalaboos.lib.ui.Container#update()
	 */
	@Override
	public void update(Point mouse) {
		for (Component component : components) {
			component.update(mouse);
		}
	}
	
	/**
	 * @see net.halalaboos.lib.ui.Container#renderComponents(net.halalaboos.lib.ui.theme.Theme, java.awt.Point)
	 */
	@Override
	public void renderComponents(Theme theme, Point mouse) {
		this.mouse.setLocation(mouse);
		for (Component component : components) {
			boolean mouseDown = downComponent == component && this.mouseDown;
			boolean mouseOver = downComponent == null ? mousedComponent == component && this.mouseOver : mousedComponent == component && this.mouseOver && mouseDown;
			component.setOffset(offset);
			component.setMouseDown(mouseDown);
			component.setMouseOver(mouseOver);
			theme.render(this, offset, component, mouse);
		}
		calculateMousedComponent();
	}

	/**
	 * @see net.halalaboos.lib.ui.Container#getArea()
	 */
	@Override
	public Rectangle getArea() {
		return area;
	}

	/**
	 * @see net.halalaboos.lib.ui.Container#setArea(java.awt.Rectangle)
	 */
	@Override
	public void setArea(Rectangle area) {
		this.area = area;
	}

	/**
	 * @see net.halalaboos.lib.ui.Container#isPointInside(java.awt.Point)
	 */
	@Override
	public boolean isPointInside(Point point) {
		boolean inside = area.contains(point);
		for (Component component : components) {
			if (component.isPointInside(point)) {
				inside = true;
			}
		}
		return inside;
	}

	/**
	 * @see net.halalaboos.lib.ui.Container#isActive()
	 */
	@Override
	public boolean isActive() {
		return active;
	}

	/**
	 * @see net.halalaboos.lib.ui.Container#onMousePressed(int, int, int)
	 */
	@Override
	public boolean onMousePressed(int x, int y, int buttonId) {
		boolean returnVal = false;
		for (int i = components.size() - 1; i >= 0; i--) {
			Component component = components.get(i);
			if (component.onMousePressed(x, y, buttonId) && buttonId == 0) {
				downComponent = component;
				returnVal = true;
			}
		}
		return mouseOver || returnVal;
	}
	
	protected void calculateMousedComponent() {
		mousedComponent = findWithinPoint(mouse);
	}
	
	/**
	 * Finds the container with the point inside of it.
	 * */
	protected Component findWithinPoint(Point point) {
		for (int i = components.size() - 1; i >= 0; i--) {
			Component component = components.get(i);
			if (component.isPointInside(mouse)) {
				return component;
			}
		}
		return null;
	}

	/**
	 * @see net.halalaboos.lib.ui.Container#onKeyTyped(int, char)
	 */
	@Override
	public void onKeyTyped(int keyCode, char c) {
		for (Component component : components) {
			component.onKeyTyped(keyCode, c);
		}
	}

	/**
	 * @see net.halalaboos.lib.ui.Container#onMouseReleased(int, int, int)
	 */
	@Override
	public void onMouseReleased(int x, int y, int buttonId) {
		if (downComponent != null) {
			downComponent.onMouseReleased(x, y, buttonId);
			downComponent = null;
		}
	}

	/**
	 * @see net.halalaboos.lib.ui.Container#getComponents()
	 */
	@Override
	public List<Component> getComponents() {
		return components;
	}

	/**
	 * @see net.halalaboos.lib.ui.Container#add(net.halalaboos.lib.ui.Component)
	 */
	@Override
	public void add(Component component) {
		components.add(component);
	}

	/**
	 * @see net.halalaboos.lib.ui.Container#remove(net.halalaboos.lib.ui.Component)
	 */
	@Override
	public void remove(Component component) {
		components.remove(component);
	}

	/**
	 * @see net.halalaboos.lib.ui.Container#setMouseOver(boolean)
	 */
	@Override
	public void setMouseOver(boolean mouseOver) {
		this.mouseOver = mouseOver;
	}

	/**
	 * @see net.halalaboos.lib.ui.Container#isMouseOver()
	 */
	@Override
	public boolean isMouseOver() {
		return mouseOver;
	}

	/**
	 * @see net.halalaboos.lib.ui.Container#setMouseDown(boolean)
	 */
	@Override
	public void setMouseDown(boolean mouseDown) {
		this.mouseDown = mouseDown;
	}

	/**
	 * @see net.halalaboos.lib.ui.Container#isMouseDown()
	 */
	@Override
	public boolean isMouseDown() {
		return mouseDown;
	}

	/**
	 * Lays out each component inside the container 
	 * @see net.halalaboos.lib.ui.Container#layout()
	 */
	@Override
	public void layout() {
		Rectangle componentArea = layout.layoutComponents(this, components);
        if (area != null) {
            if (componentArea.width > area.width)
                area.width = componentArea.width;
            if (componentArea.height > area.height)
                area.height = componentArea.height;
        }
	}

	/**
	 * @see net.halalaboos.lib.ui.Container#setLayout(net.halalaboos.lib.ui.layout.Layout)
	 */
	@Override
	public void setLayout(Layout layout) {
		this.layout = layout;
	}

	/**
	 * @see net.halalaboos.lib.ui.Container#getLayout()
	 */
	@Override
	public Layout getLayout() {
		return layout;
	}
	
	public void setPosition(int x, int y) {
		area.setLocation(x, y);
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	/**
	 * @see net.halalaboos.lib.ui.Container#getTooltip()
	 */
	@Override
	public String getTooltip() {
		return downComponent == null ? mousedComponent == null ? null : mousedComponent.getTooltip() : null;
	}

}
