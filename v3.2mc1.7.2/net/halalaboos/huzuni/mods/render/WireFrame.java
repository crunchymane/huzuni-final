package net.halalaboos.huzuni.mods.render;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;
import com.darkmagician6.eventapi.types.Priority;

import net.halalaboos.huzuni.events.render.EventPostRenderWorld;
import net.halalaboos.huzuni.events.render.EventPreRenderWorld;
import net.halalaboos.huzuni.events.render.EventRenderGui;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.DefaultModule;
import net.halalaboos.huzuni.utils.TextureUtils;
import org.lwjgl.input.Keyboard;

import static org.lwjgl.opengl.GL11.*;

public class WireFrame extends DefaultModule implements Listener {
			
    public WireFrame() {
        super("WireFrame", Keyboard.KEY_G);
        setCategory(Category.RENDER);
        setRenderWhenEnabled(true);
        setDescription("Renders the world in a wire-frame mode.");
    }

    @Override
    protected void onEnable() {
        registry.registerListener(this);
    }

    @Override
    protected void onDisable() {
        registry.unregisterListener(this);
    }

    @Override
    protected void onToggle() {
    }

    @EventTarget(Priority.HIGHEST)
    public void onPreRenderEvent(EventPreRenderWorld event) {
        // glCullFace(GL_FRONT);
        // glEnable(GL_CULL_FACE);
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        glLineWidth(1F);
	}

    @EventTarget(Priority.LOWEST)
    public void onPostRenderEvent(EventPostRenderWorld event) {
        // glCullFace(GL_BACK);
        // glDisable(GL_CULL_FACE);
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }
    /*
	
    private final Fbo frameBuffer = new Fbo();

    @EventTarget
    public void renderGui(EventRenderGui event) {
    	int oldWidth = mc.displayWidth, oldHeight = mc.displayHeight;
    	mc.thePlayer.rotationYaw += 180;
    	mc.displayWidth = 512;
    	mc.displayHeight = 512;
    	frameBuffer.start();
    	mc.entityRenderer.renderWorld(0F, 0L);
    	frameBuffer.end();
    	mc.displayWidth = oldWidth;
    	mc.displayHeight = oldHeight;
    	mc.thePlayer.rotationYaw -= 180;
    	
    	glViewport(0, 0, this.mc.displayWidth, this.mc.displayHeight);
    	glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
    	mc.entityRenderer.setupOverlayRendering();
    	frameBuffer.render(2, 2, 152, 152);
    	
    }
    */


}