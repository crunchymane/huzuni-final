package net.halalaboos.huzuni.command.commands;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.console.Command;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.*;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.event.HoverEvent;

/**
 * @author brudin
 * @version 1.0
 * @since 6:17 PM on 8/17/2014
 */
public class Invsee implements Command {
	@Override
	public String[] getAliases() {
		return new String[] { "invsee" };
	}

	@Override
	public String[] getHelp() {
		return new String[] { "invsee <playername>" };
	}

	@Override
	public String getDescription() {
		return "Shows the items in the specified player's hotbar, as well as info on their armor.";
	}

	@Override
	public void run(String input, String[] args) {
		EntityPlayer player = findPlayer(args[0]);
		if(player != null) {
			Huzuni.addChatMessage("Inventory info for " + player.getName() + ":");
			ItemStack[] items = player.inventory.mainInventory;
			if(items[0] != null) {
				Minecraft.getMinecraft().ingameGUI.getChatGUI().printChatMessage(itemInfo(TextFormatting.DARK_GREEN + "[H] " + TextFormatting.RESET + "Held item: ", items));
			} else {
				Huzuni.addChatMessage("Held item: None");
			}
			ItemStack[] armor = player.inventory.armorInventory;
			if(armor != null) {
				Minecraft.getMinecraft().ingameGUI.getChatGUI().printChatMessage(itemInfo(TextFormatting.DARK_GREEN + "[H] " + TextFormatting.RESET + "Armor: ", armor));
			} else {
				Huzuni.addChatMessage("Armor: None");
			}
		} else {
			Huzuni.addChatMessage("Player not found or out of range!");
		}
	}

	private ITextComponent itemInfo(String text, ItemStack[] items) {
		TextComponentString outputMessage = new TextComponentString(text);
		for (ItemStack itemStack : items) {
			if (itemStack != null) {
				outputMessage.appendSibling(getItemText(itemStack));
				outputMessage.appendSibling(new TextComponentString(", "));
			}
		}
		return outputMessage;
	}

	private ITextComponent getItemText(ItemStack itemStack) {
		if (itemStack == null)
			return new TextComponentString("None");
		ITextComponent output = new TextComponentString(itemStack.getDisplayName());
		if (itemStack.getItem() != null) {
			NBTTagCompound tag = new NBTTagCompound();
			itemStack.writeToNBT(tag);
			output.getChatStyle().setChatHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_ITEM, new TextComponentString(tag.toString())));
			output.getChatStyle().setColor(itemStack.getRarity().rarityColor);
		}
		return output;
	}

	private EntityPlayer findPlayer(String name) {
		for (Object o : Minecraft.getMinecraft().theWorld.loadedEntityList) {
			if (o instanceof EntityPlayer) {
				EntityPlayer entityPlayer = (EntityPlayer)o;
				if (StringUtils.stripControlCodes(entityPlayer.getName()).equalsIgnoreCase(name)) {
					return entityPlayer;
				}
			}
		}
		return null;
	}
}
