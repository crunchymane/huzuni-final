package net.halalaboos.huzuni.hooks;

import com.darkmagician6.eventapi.Dispatcher;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.events.render.EventRenderGui;
import net.halalaboos.lib.reflection.ReflectionHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiIngame;

public class GuiIngameHook extends GuiIngame {

    public GuiIngameHook(Minecraft mc) {
        super(mc);
        try {
			ReflectionHelper.replaceInstance(GuiIngame.class, this, new GuiNewChatHook(mc));
		} catch (IllegalArgumentException e) {
			Huzuni.logger.log("ERROR HOOKING INTO CHAT:" + e.getMessage());
		} catch (IllegalAccessException e) {
			Huzuni.logger.log("ERROR HOOKING INTO CHAT:" + e.getMessage());
		}
        Huzuni.logger.log("Initialized new " + this.getClass().getSimpleName() + ".");
    }

    public void renderGameOverlay(float partialTicks, boolean dontKnow, int mouseX, int mouseY) {
        super.renderGameOverlay(partialTicks, dontKnow, mouseX, mouseY);
		Huzuni.display.render();
        Dispatcher.call(new EventRenderGui(partialTicks));
    }

}
