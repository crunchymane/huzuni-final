/**
 * 
 */
package net.halalaboos.huzuni.ui.windows.waypoints;

import java.awt.Color;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.client.waypoints.Waypoint;
import net.halalaboos.huzuni.helpers.MinecraftHelper;
import net.halalaboos.huzuni.ui.clickable.components.ColorPicker;
import net.halalaboos.huzuni.ui.clickable.containers.HuzuniWindow;
import net.halalaboos.huzuni.utils.RenderUtils;
import net.halalaboos.lib.ui.Component;
import net.halalaboos.lib.ui.components.Button;
import net.halalaboos.lib.ui.components.Label;
import net.halalaboos.lib.ui.components.Slider;
import net.halalaboos.lib.ui.components.TextField;
import net.halalaboos.lib.ui.listeners.ActionListener;

/**
 * @author Halalaboos
 *
 * @since Oct 7, 2013
 */
public class ModifyWaypointWindow extends HuzuniWindow implements MinecraftHelper, ActionListener {
	private final WaypointWindow waypointWindow;
	
	private final boolean modify;

	private Waypoint waypoint;
	
	private TextField waypointNameField;
	
	private Label color;
	
	private Button addOrModify;
	
	private Slider red, green, blue;
	
	public ModifyWaypointWindow(WaypointWindow waypointWindow) {
		super("New Waypoint");
		this.waypointWindow = waypointWindow;
		this.waypoint = null;
		modify = false;
		this.minimized = false;
		initComponents(false);
	}

	/**
	 * @param title
	 */
	public ModifyWaypointWindow(WaypointWindow waypointWindow, Waypoint waypoint) {
		super("Modify Waypoint");
		this.waypointWindow = waypointWindow;
		this.waypoint = waypoint;
		modify = true;
		this.minimized = false;
		initComponents(true);
	}
	
	public void initComponents(boolean modify) {
		waypointNameField = new TextField();
		waypointNameField.setText(modify ? waypoint.getName() : "Waypoint " + Huzuni.waypointManager.size());
		waypointNameField.addActionListener(this);
		add(waypointNameField);

		Color color = modify ? waypoint.getColor() : RenderUtils.getRandomColor();
		add(this.color = new Label("Color"));
        this.red = new Slider("Red", 0, color.getRed(), 255, 1);
        this.green = new Slider("Green", 0, color.getGreen(), 255, 1);
        this.blue = new Slider("Blue", 0, color.getBlue(), 255, 1);
        this.color.setTextColor(getColor());
        add(this.red);
        red.addActionListener(this);
        add(this.green);
        green.addActionListener(this);
        add(this.blue);
        blue.addActionListener(this);
		addOrModify = new Button(modify ? "Save" : "Add");
		addOrModify.addActionListener(this);
		add(addOrModify);
		layout();
	}

	/**
	 * @see net.halalaboos.lib.ui.listeners.ActionListener#onAction(net.halalaboos.lib.ui.Component)
	 */
	@Override
	public void onAction(Component component) {
		if (component == red || component == green || component == blue) {
			color.setTextColor(getColor());
		} else if (component == addOrModify || component == waypointNameField) {
			if (modify) {
				String name = waypointNameField.getText();
				Color color = getColor();
				waypoint.setName(name);
				waypoint.setColor(color);
				waypointWindow.finishModify(this, waypoint, false);
			} else {
				String name = waypointNameField.getText();
				Color color = getColor();
				int x = (int) mc.thePlayer.posX,
				y = (int) mc.thePlayer.posY,
				z = (int) mc.thePlayer.posZ;
				waypointWindow.finishModify(this, new Waypoint(name, x, y, z, color), true);
			}
		}
	}
	
	private Color getColor() {
    	return new Color((int) red.getValue(), (int) green.getValue(), (int) blue.getValue());
	}

}
