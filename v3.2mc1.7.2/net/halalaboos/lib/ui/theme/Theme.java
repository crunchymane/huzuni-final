/**
 *
 */
package net.halalaboos.lib.ui.theme;

import net.halalaboos.lib.ui.Component;
import net.halalaboos.lib.ui.Container;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import java.awt.Point;
/**
 * @author Halalaboos
 * @since Aug 30, 2013
 */
public abstract class Theme {

    private Map<Class<? extends Component>, ComponentRenderer> rendererMap = new ConcurrentHashMap<Class<? extends Component>, ComponentRenderer>();

    public void render(Container container, Point offset,  Component component, Point mouse) {
    	ComponentRenderer renderer = getRenderer(component);
        if (renderer != null) {
            renderer.render(container, offset, component, mouse);
        }
    }

    public void registerRenderer(Class<? extends Component> renderType, ComponentRenderer renderer) {
        rendererMap.put(renderType, renderer);
    }

    public ComponentRenderer getRenderer(Component component) {
    	ComponentRenderer lastRenderer = null;
        Set<Entry<Class<? extends Component>, ComponentRenderer>> entries = rendererMap.entrySet();
        for (Entry<Class<? extends Component>, ComponentRenderer> entry : entries) {
        	if (entry.getKey().isInstance(component)) {
        		 lastRenderer = entry.getValue();
        	}
        }
        return lastRenderer;
    }
    
	public abstract void renderContainer(Container container, Point mouse);
    
	public abstract void renderToolTip(String tooltip, Point mouse);

}
