package net.halalaboos.huzuni.mods.world;

import java.util.Set;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.client.Settings;
import net.halalaboos.huzuni.console.Command;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.DefaultModule;
import net.minecraft.block.Block;

import org.lwjgl.input.Keyboard;

public class Xray extends DefaultModule implements Command {

    private float oldGamma;

    public Xray() {
        super("Xray", Keyboard.KEY_X);
        setCategory(Category.WORLD);
        setDescription("X-ray for blocks.");
        setRenderWhenEnabled(true);
        Settings.config.put("Xray Opacity", 70F);
        registerCommand(this);
    }

    @Override
    protected void onEnable() {
        oldGamma = mc.gameSettings.gammaSetting;
		mc.gameSettings.gammaSetting = 10;
    }

    @Override
    protected void onDisable() {
		mc.gameSettings.gammaSetting = oldGamma;
    }

    @Override
    protected void onToggle() {
        /*int range = 256;
		int playerX = (int) mc.thePlayer.posX, playerY = (int) mc.thePlayer.posY, playerZ = (int) mc.thePlayer.posZ;
		mc.renderGlobal.markBlocksForUpdate(playerX - range, playerY - range, playerZ - range, playerX + range, playerY + range, playerZ + range);*/
        mc.renderGlobal.loadRenderers();
    }

	/**
	 * @see net.halalaboos.huzuni.console.Command#getAliases()
	 */
	@Override
	public String[] getAliases() {
		return new String[] { "xray", "x" };
	}

	/**
	 * @see net.halalaboos.huzuni.console.Command#getHelp()
	 */
	@Override
	public String[] getHelp() {
		return new String[] { "xray add <name / id>", "xray remove <name / id>" };
	}

	/**
	 * @see net.halalaboos.huzuni.console.Command#run(java.lang.String, java.lang.String[])
	 */
	@Override
	public void run(String input, String[] args) {
		if (args[0].startsWith("add")) {
			String blockName = input.substring((input.indexOf(args[0] + " ")) + (args[0] + " ").length());
			int id = blockHelper.getBlockIDFromInput(blockName);
			add(id, blockName);
		} else if (args[0].startsWith("remove") || args[0].startsWith("del") || args[0].startsWith("delete")) {
			String blockName = input.substring((input.indexOf(args[0] + " ")) + (args[0] + " ").length());
			int id = blockHelper.getBlockIDFromInput(blockName);
			remove(id, blockName);
		} else {
			String blockName = input.substring(input.indexOf(args[0]));
			int id = blockHelper.getBlockIDFromInput(blockName);
			if (id != 0) {
				if (Huzuni.xrayManager.contains(id))
					remove(id, blockName);
				else
					add(id, blockName);
			}
		}
	}
	
	private void remove(int id, String blockName) {
		if (id != 0) {
			if (Huzuni.xrayManager.contains(id)) {
				Huzuni.xrayManager.remove(id);
				Huzuni.instance.addChatMessage("Sucessfully removed '" + blockName + "' from xray.");
			} else 
				Huzuni.instance.addChatMessage("'" + blockName + "' is not on the xray list.");

		} else 
			Huzuni.instance.addChatMessage("Unable to find '" + blockName + "'.");
	}
	
	private void add(int id, String blockName) {
		if (id != 0) {
			if (!Huzuni.xrayManager.contains(id)) {
				Huzuni.xrayManager.add(id);
				Huzuni.instance.addChatMessage("Sucessfully added '" + blockName + "' to xray.");
			} else 
				Huzuni.instance.addChatMessage("'" + blockName + "' is already on the xray list.");
		} else 
			Huzuni.instance.addChatMessage("Unable to find '" + blockName + "'.");
	}

}
