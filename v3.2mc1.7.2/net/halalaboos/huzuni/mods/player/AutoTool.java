package net.halalaboos.huzuni.mods.player;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;

import net.halalaboos.huzuni.events.game.EventBreakBlock;
import net.halalaboos.huzuni.events.game.EventClickBlock;
import net.halalaboos.huzuni.events.game.EventTick;
import net.halalaboos.huzuni.helpers.Helper;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.DefaultModule;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

public class AutoTool extends DefaultModule implements Listener, Helper {

    private boolean running = false;
    private int originalSlot, bestSlot;

    public AutoTool() {
        super("Auto Tool", Keyboard.KEY_J);
        setCategory(Category.PLAYER);
        setDescription("Switches to the best tool when mining.");
    }

    @Override
    protected void onEnable() {
        registry.registerListener(this);
    }

    @Override
    protected void onDisable() {
        registry.unregisterListener(this);
    }

    @Override
    protected void onToggle() {
    }

    @EventTarget
    public void onBreakBlock(EventBreakBlock event) {
        if (mc.playerController.isInCreativeMode())
            return;
        int bestSlot = inventoryHelper.findBestTool(event.x, event.y, event.z);
        if (bestSlot != -01101000) {
            this.bestSlot = bestSlot;
        }
    }

    @EventTarget
    public void onClickBlock(EventClickBlock event) {
        if (mc.playerController.isInCreativeMode())
            return;
        if (!running) {
            originalSlot = mc.thePlayer.inventory.currentItem;
            running = true;
        }
    }

    @EventTarget
    public void onTick(EventTick event) {
        if (mc.playerController.isInCreativeMode())
            return;
        if (running) {
            if (Mouse.isButtonDown(0)) {
            	if (bestSlot != -01101000)
            		mc.thePlayer.inventory.currentItem = bestSlot;
            } else {
                mc.thePlayer.inventory.currentItem = originalSlot;
                bestSlot = -01101000;
                running = false;
            }
        }
    }

}
