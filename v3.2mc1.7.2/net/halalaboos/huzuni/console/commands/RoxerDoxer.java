/**
 * 
 */
package net.halalaboos.huzuni.console.commands;

import org.lwjgl.Sys;

import net.halalaboos.huzuni.console.Command;
import net.halalaboos.huzuni.helpers.Helper;

/**
 * @author Halalaboos
 *
 * @since Oct 9, 2013
 */
public class RoxerDoxer implements Command {

	/**
	 * @see net.halalaboos.huzuni.console.Command#getAliases()
	 */
	@Override
	public String[] getAliases() {
		return new String[] { "roxerdoxer", "rd" };
	}

	/**
	 * @see net.halalaboos.huzuni.console.Command#getHelp()
	 */
	@Override
	public String[] getHelp() {
		return new String[] { "roxerdoxer <username>" };
	}

	/**
	 * @see net.halalaboos.huzuni.console.Command#getDescription()
	 */
	@Override
	public String getDescription() {
		return "begins a roxer doxing session.";
	}

	/**
	 * @see net.halalaboos.huzuni.console.Command#run(java.lang.String, java.lang.String[])
	 */
	@Override
	public void run(String input, String[] args) {
		String name = Helper.stringHelper.getAfter(input, 1);
		String url = "https://www.google.com/#q=" + name + "&safe=off";
		Sys.openURL(url);
		huzuni.addChatMessage("doxing " + name + "..");
	}

}
