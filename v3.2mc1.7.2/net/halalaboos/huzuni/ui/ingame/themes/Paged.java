/**
 *
 */
package net.halalaboos.huzuni.ui.ingame.themes;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.DefaultModule;
import net.halalaboos.huzuni.ui.ingame.IngameTheme;
import net.halalaboos.huzuni.utils.RenderUtils;
import net.minecraft.client.Minecraft;
import org.lwjgl.input.Keyboard;

import java.awt.*;

/**
 * @author Halalaboos
 * @since Aug 3, 2013
 */
public class Paged implements IngameTheme {
    private int page = 0;

    public Paged() {
    }

    /**
     * @see net.halalaboos.huzuni.ui.ingame.IngameTheme#render(net.minecraft.src.Minecraft, int, int)
     */
    @Override
    public void render(Minecraft mc, int screenWidth, int screenHeight) {
        mc.fontRenderer.drawStringWithShadow(Huzuni.TITLE + "\247r v" + Huzuni.VERSION, 2, 2, 0xFFFFFF);

        Category category = Category.values()[page];
        String title = category.formalName + " " + (page + 1) + "/" + Category.values().length;
        int y = 12,
                width = getMaxWidth();

        RenderUtils.drawRect(2, y, 2 + width, y + 11, 0x9F5E5E5E);
        RenderUtils.drawRect(2, y + 10, 2 + width, y + 11, 0x9F00B3FF);
        mc.fontRenderer.drawStringWithShadow(title, 4, y + 2, 0xFFFFFF);

        y += 11;
        for (DefaultModule module : Huzuni.modManager.getDefaultModules()) {
            if (module.getCategory() == category) {
                Rectangle boundaries = new Rectangle(2, y, width, 11);
                RenderUtils.drawRect(boundaries, module.isEnabled() ? 0x9F00B3FF : 0x9F5E5E5E);
                String name = module.getKeyName().equals("-1") ? module.getName() : "[" + module.getKeyName() + "] " + module.getName();
                mc.fontRenderer.drawStringWithShadow(name, 4, y + 2, 0xFFFFFF);
                y += 11;
            }
        }
    }

    /**
     * @return Maximum width of the current page.
     */
    private int getMaxWidth() {
        Category category = Category.values()[page];
        String title = category.formalName + " " + (page + 1) + "/" + Category.values().length;
        int width = mc.fontRenderer.getStringWidth(title) + 4;
        for (DefaultModule module : Huzuni.modManager.getDefaultModules())
            if (module.getCategory() == category) {
                String name = module.getKeyName().equals("-1") ? module.getName() : "[" + module.getKeyName() + "] " + module.getName();
                if (mc.fontRenderer.getStringWidth(name) + 4 > width)
                    width = mc.fontRenderer.getStringWidth(name) + 4;
            }
        return width;
    }

    /**
     * @see net.halalaboos.huzuni.ui.ingame.IngameTheme#getName()
     */
    @Override
    public String getName() {
        return "Paged";
    }

    /**
     * Keeps our page within the page limits.
     */
    private void keepWithinBoundaries() {
        int size = Category.values().length;
        if (page >= size)
            page = 0;
        else if (page < 0)
            page = size - 1;
    }

    /**
     * @see net.halalaboos.huzuni.ui.ingame.IngameTheme#onKeyPressed(int)
     */
    @Override
    public void onKeyPressed(int keyCode) {
        if (keyCode == Keyboard.KEY_RIGHT) {
            page++;
            keepWithinBoundaries();
        } else if (keyCode == Keyboard.KEY_LEFT) {
            page--;
            keepWithinBoundaries();
        }
    }

}
