package net.halalaboos.huzuni.command.commands;

import net.halalaboos.huzuni.api.console.Command;
import net.minecraft.client.Minecraft;
import net.minecraft.network.play.client.C03PacketPlayer;

/**
 * @author brudin
 * @version 1.0
 * @since 1:14 PM on 8/20/2014
 */
public class Damage implements Command {

	@Override
	public String[] getAliases() {
		return new String[] {"damage", "dmg"};
	}

	@Override
	public String[] getHelp() {
		return new String[] { "damage <amount>"};
	}

	@Override
	public String getDescription() {
		return "Inflicts damage on yourself.  Must be on a NoCheat+ server.";
	}

	@Override
	public void run(String input, final String[] args) {
		final int amount = Integer.parseInt(args[0]);
		new Thread() {
			@Override
			public void run() {
				try {
					Minecraft.getMinecraft().thePlayer.sendQueue.addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(Minecraft.getMinecraft().thePlayer.posX, Minecraft.getMinecraft().thePlayer.boundingBox.minY + 1, Minecraft.getMinecraft().thePlayer.posY + 1, Minecraft.getMinecraft().thePlayer.posZ, false));
					Thread.sleep(5L);
					Minecraft.getMinecraft().thePlayer.sendQueue.addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(Minecraft.getMinecraft().thePlayer.posX, Minecraft.getMinecraft().thePlayer.boundingBox.minY - 2 - amount,
							Minecraft.getMinecraft().thePlayer.posY - 2 - amount, Minecraft.getMinecraft().thePlayer.posZ, false));
				} catch (Exception e) {}
			}
		}.start();
	}
}
