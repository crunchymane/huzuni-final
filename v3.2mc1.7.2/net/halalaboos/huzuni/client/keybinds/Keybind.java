package net.halalaboos.huzuni.client.keybinds;

import org.lwjgl.input.Keyboard;

public interface Keybind {

    public void onKeyPressed();

    public String getCommand();
}
