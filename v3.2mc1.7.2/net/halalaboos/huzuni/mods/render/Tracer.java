package net.halalaboos.huzuni.mods.render;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;
import com.darkmagician6.eventapi.types.Priority;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.events.render.EventRenderWorld;
import net.halalaboos.huzuni.helpers.Helper;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.DefaultModule;
import net.halalaboos.huzuni.rendering.Vbo;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.player.EntityPlayer;

import org.lwjgl.input.Keyboard;

import static org.lwjgl.opengl.GL11.*;

public class Tracer extends DefaultModule implements Listener, Helper {
    
	private Vbo vbo;
    
    public Tracer() {
        super("Tracer", Keyboard.KEY_B);
        setCategory(Category.RENDER);
        setRenderWhenEnabled(false);
        setDescription("Shows lines to players and boxes around players.");
        setupVbo();
    }

    @Override
    protected void onEnable() {
        registry.registerListener(this);

    }

    @Override
    protected void onDisable() {
        registry.unregisterListener(this);

    }

    @Override
    protected void onToggle() {
    }

    @EventTarget(Priority.LOW)
    public void onRenderWorld(EventRenderWorld event) {
        for (EntityPlayer player : entityHelper.getPlayers()) {
            if (player == mc.thePlayer || player.isDead)
                continue;
            final float renderX = (float) (event.interpolate(player.prevPosX, player.posX) - RenderManager.renderPosX);
            final float renderY = (float) (event.interpolate(player.prevPosY, player.posY) - RenderManager.renderPosY);
            final float renderZ = (float) (event.interpolate(player.prevPosZ, player.posZ) - RenderManager.renderPosZ);
            final float distance = mc.thePlayer.getDistanceToEntity(player);
            final boolean friend = Huzuni.friendManager.contains(player.getCommandSenderName());

            colorLines(distance, friend);
            glBegin(GL_LINES);
            glVertex3d(0, 0, 0);
            glVertex3d(renderX, renderY, renderZ);
            glEnd();

            colorLines(distance, friend);
            glPushMatrix();
            glTranslatef(renderX, renderY, renderZ);
            glRotatef(-player.rotationYaw, 0F, 1F, 0F);
            vbo.render(GL_LINE_STRIP);
            glPopMatrix();
        }
    }

    private void colorLines(float distance, boolean friend) {
        if (friend) {
            glColor3f(0, 0.5F, 0.75F);
        } else {
            glColor3f(1F, distance / 64F, 0F);
        }
    }
    
	private void setupVbo() {
		vbo = new Vbo();
        float width = 0.6F / 1.5F;
		float minX = -width, minY = 0, minZ = -width, maxX = width, maxY = 1.9F, maxZ = width;

        vbo.addVertex(minX, minY, minZ);
        vbo.addVertex(maxX, minY, minZ);
        vbo.addVertex(maxX, minY, maxZ);
        vbo.addVertex(minX, minY, maxZ);
        vbo.addVertex(minX, minY, minZ);

        vbo.addVertex(minX, maxY, minZ);
        vbo.addVertex(maxX, maxY, minZ);
        vbo.addVertex(maxX, maxY, maxZ);
        vbo.addVertex(minX, maxY, maxZ);
        vbo.addVertex(minX, maxY, minZ);

        vbo.addVertex(minX, minY, minZ);
        vbo.addVertex(minX, maxY, minZ);
        vbo.addVertex(maxX, minY, minZ);
        vbo.addVertex(maxX, maxY, minZ);
        vbo.addVertex(maxX, minY, maxZ);
        vbo.addVertex(maxX, maxY, maxZ);
        vbo.addVertex(minX, minY, maxZ);
        vbo.addVertex(minX, maxY, maxZ);    
		vbo.compile();
	}

}
