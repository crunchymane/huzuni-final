package net.halalaboos.huzuni.scripting.action;

import java.awt.Font;

import net.halalaboos.huzuni.Huzuni;

public class ChangeFont extends Action {

	private final String fontName;
	
	private final int size;
	
	public ChangeFont(String fontName, int size) {
		this.fontName = fontName;
		this.size = size;
	}
	
	@Override
	public void run() {
		Huzuni.fontRenderer.setFont(new Font(fontName, Font.TRUETYPE_FONT, size));
	}

}
