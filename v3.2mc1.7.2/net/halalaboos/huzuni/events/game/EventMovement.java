package net.halalaboos.huzuni.events.game;

import com.darkmagician6.eventapi.events.Event;

public class EventMovement implements Event {

    public double motionX, motionY, motionZ;

    public EventMovement(double motionX, double motionY, double motionZ) {
        this.motionX = motionX;
        this.motionY = motionY;
        this.motionZ = motionZ;
    }

}
