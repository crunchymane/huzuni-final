/**
 *
 */
package net.halalaboos.huzuni.console.commands;

import net.halalaboos.huzuni.console.Command;
import net.halalaboos.huzuni.helpers.Helper;
import net.halalaboos.huzuni.mods.Module;
import net.minecraft.util.EnumChatFormatting;

/**
 * @author Halalaboos
 * @since Jul 14, 2013
 */
public class Toggle implements Command {

    /**
     * @see net.halalaboos.huzuni.console.Command#getAliases()
     */
    @Override
    public String[] getAliases() {
        return new String[] {"toggle", "t"};
    }

    /**
     * @see net.halalaboos.huzuni.console.Command#getHelp()
     */
    @Override
    public String[] getHelp() {
        return new String[] {"toggle <mod name>"};
    }

    /**
     * @see net.halalaboos.huzuni.console.Command#getDescription()
     */
    @Override
    public String getDescription() {
        return "Toggles a mod.";
    }

    /**
     * @see net.halalaboos.huzuni.console.Command#run(java.lang.String, java.lang.String[])
     */
    @Override
    public void run(String input, String[] args) {

        String moduleName = Helper.stringHelper.getAfter(input, 1);
        Module mod = huzuni.modManager.getMod(moduleName);
        mod.toggle();
        huzuni.addChatMessage(mod.getName() + " is " + (mod.isEnabled() ? EnumChatFormatting.GREEN + "enabled" : EnumChatFormatting.RED + "disabled") + EnumChatFormatting.RESET + ".");
    }

}
