package net.halalaboos.huzuni.mod.mods.combat;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.mod.Category;
import net.halalaboos.huzuni.event.packet.PacketEvent;
import net.halalaboos.huzuni.event.packet.PacketListener;
import net.halalaboos.huzuni.mod.DefaultMod;
import net.minecraft.network.play.client.CPacketUseEntity;

import java.util.Random;

/**
 * @author brudin
 * @version 1.0
 * @since 1:33 AM on 8/20/2014
 */
public class Criticals extends DefaultMod implements PacketListener {

	public Criticals() {
		super("Criticals");
		setAliases("crits");
		setAuthor("brudin");
		setCategory(Category.COMBAT);
		setDescription("Attempts to force criticals when attacking.");
	}

	@Override
	protected void onEnable() {
		Huzuni.registerPacketListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterPacketListener(this);
	}

	@Override
	public void onPacket(PacketEvent event, PacketEvent.PacketType type) {
		if (type == PacketEvent.PacketType.SENT) {
			if (event.getPacket() instanceof CPacketUseEntity) {
				CPacketUseEntity packetUseEntity = (CPacketUseEntity)event.getPacket();
				if (packetUseEntity.getAction() == CPacketUseEntity.Action.ATTACK) {
					if (shouldCritical()) {
						doCrit();
					}
				}
			}
		}
	}

	private void doCrit() {
		boolean preGround = mc.thePlayer.onGround;
		mc.thePlayer.onGround = false;
		mc.thePlayer.jump();
		mc.thePlayer.onGround = preGround;
	}

	private boolean shouldCritical() {
		return !mc.thePlayer.isInWater() && mc.thePlayer.onGround && !mc.thePlayer.isOnLadder();
	}
}
