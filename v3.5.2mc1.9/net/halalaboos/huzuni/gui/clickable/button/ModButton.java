package net.halalaboos.huzuni.gui.clickable.button;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import net.halalaboos.huzuni.api.gui.render.Theme;
import net.halalaboos.huzuni.api.mod.Option;
import net.halalaboos.huzuni.api.mod.Mod;
import net.halalaboos.huzuni.util.GLUtils;
import net.halalaboos.huzuni.util.MathUtils;

public class ModButton extends Button {

	public static final int SLIDER_POINT_WIDTH = 6;
	public static final int SLIDER_PADDING = 1;
	public static final int DROPDOWN_WIDTH = 8; 
	
	protected final Mod mod;
	
	protected boolean down = false, dragging = false;
	
	protected int textPadding = 1, totalComponentHeight = 0, dropdownHeight = 60;
	
	protected float scrollPercentage = 0;
	
	protected Option lastClicked = null, mousedOver;
	
	protected float lastMouseY, lastScrollbarY;
	
	public ModButton(Mod mod) {
		super(mod.getName());
		this.mod = mod;
		this.setTooltip(mod.getDescription());
	}

	@Override
	public boolean isHighlight() {
		return mod.isEnabled();
	}

	@Override
	public String getTitle() {
		return mod.getName();
	}
	
	@Override
	public void update() {
		super.update();
		handleScrollWheel();
		handleDragging();
		calculateComponentHeight();
		if (!this.down)
			this.setTooltip(mod.getDescription());
	}
	
	@Override
	protected void invokeActionListeners() {
		super.invokeActionListeners();
		mod.toggle();
	}
	
	@Override
	public boolean mouseClicked(int x, int y, int buttonId) {
		if (hasDropdown() && down) {
			float[] sliderPoint = getSliderPoint();
			lastMouseY = y;
			lastScrollbarY = sliderPoint[1];
			if (MathUtils.inside(x, y, sliderPoint)) {
				dragging = true;
			}
			int yPos = (int) (getHeight() + 2) - (int) getScrollHeight();
			for (int i = 0; i < mod.length(); i++) {
				float[] componentArea = new float[] { this.getX() + this.getOffsetX() + 1, this.getY() + this.getOffsetY() + yPos, this.getWidth() - (this.hasEnoughToScroll() ? sliderPoint[2] + 4 : 4), getComponentHeight(mod.get(i)) };
				if (this.shouldRender(componentArea)) {
					if (MathUtils.inside(x, y, componentArea)) {
						mod.get(i).mouseClicked(x, y, buttonId);
						lastClicked = mod.get(i);
						return true;
					}
				}
				yPos += componentArea[3] + textPadding;
			}
			down = !super.isPointInside(x, y) && this.isPointInside(x, y);
		} else {
			down = this.isPointInsideDropdown(x, y);
			if (down)
				return true;
		}
		return !isPointInsideDropdown(x, y) && super.mouseClicked(x, y, buttonId);
	}
	
	@Override
	public void mouseReleased(int x, int y, int buttonId) {
		dragging = false;
		if (hasDropdown() && down) {
			if (lastClicked != null) {
				lastClicked.mouseReleased(x, y, buttonId);
				lastClicked = null;
			}
			return;
		} else
			super.mouseReleased(x, y, buttonId);
	}
	
	public void renderComponents(Theme theme, float offsetX, float offsetY, boolean mouseOver, boolean mouseDown) {
		int yPos = (int) (getHeight() + 2) - (int) getScrollHeight();
		float[] sliderPoint = getSliderPoint();
		this.mousedOver = null;
		float[] mouse = new float[] { GLUtils.getMouseX(), GLUtils.getMouseY() };
		GL11.glEnable(GL11.GL_SCISSOR_TEST);
		GLUtils.glScissor(getX() + offsetX + 2, getY() + getHeight() + offsetY + 2, getX() + offsetX + getWidth(), getY() + offsetY + getHeight() + getDownHeight() + textPadding);
		for (int i = 0; i < mod.length(); i++) {
			Option option = mod.get(i);
			float[] componentArea = new float[] { getX() + offsetX + 2, getY() + offsetY + yPos, getWidth() - (this.hasEnoughToScroll() ? sliderPoint[2] + 4 : 4), getComponentHeight(option) };
			if (this.shouldRender(componentArea)) {
				option.setArea(componentArea);
				option.render(theme, i, componentArea, (mouseOver && MathUtils.inside(mouse[0], mouse[1], componentArea)), mouseDown);
				if (mousedOver == null && (mouseOver && MathUtils.inside(mouse[0], mouse[1], componentArea))) {
					this.mousedOver = option;
				}
			}
			yPos += componentArea[3] + textPadding;
		}
		GL11.glDisable(GL11.GL_SCISSOR_TEST);
		if (mousedOver == null)
			this.setTooltip(null);
		else
			this.setTooltip(mousedOver.getDescription());
	}
	
	protected void handleScrollWheel() {
		if (this.isMouseOver()) {
			if (Mouse.hasWheel()) {
				int amount = Mouse.getDWheel();
				if (amount > 0)
					amount = -1;
				else if (amount < 0)
					amount = 1;
				else if (amount == 0)
					return;
				if (hasEnoughToScroll()) {
					scrollPercentage += ((float) amount / (float) (mod.length()));
					keepSafe();
				}
				
			}
		}
	}
	
	protected void handleDragging() {
		if (dragging) {
			if (!hasEnoughToScroll()) {
				dragging = false;
				return;
			}
			//get the difference (in pixels) from the cursor and the beginning of the slider boundaries ( (mouse X) / (slider X) ) and subtract the width of the slider.
			float differenceWithMouseAndSliderBase = (float) ((GLUtils.getMouseY() - (lastMouseY - lastScrollbarY) - 1) - (this.getY() + getHeight() + 2 + this.getOffsetY()));

			//converted into 0.0F ~ 1.0F percentage
			scrollPercentage = differenceWithMouseAndSliderBase / getHeightForPoint();
			this.keepSafe();
		}
	}
	
	public boolean hasEnoughToScroll() {
		return totalComponentHeight > this.getDownHeight();
	}
	
	protected boolean shouldRender(float[] area) {
		return area[1] + area[3] > this.getY() + getHeight() + getOffsetY() && area[1] < this.getY() + getOffsetY() + getHeight() + this.getDownHeight();
	}
	
	protected void calculateComponentHeight() {
		this.totalComponentHeight = 0;
		for (int i = 0; i < mod.length(); i++) 
			this.totalComponentHeight += getComponentHeight(mod.get(i)) + textPadding;
	}
	
	private void keepSafe() {
		if (scrollPercentage > 1)
			scrollPercentage = 1;
		if (scrollPercentage < 0)
			scrollPercentage = 0;
	}
	
	protected float getScrollHeight() {
		return (this.totalComponentHeight - this.getDownHeight()) * scrollPercentage;
	}
	
	protected int getComponentHeight(Option option) {
		return (int) option.getHeight();
	}
	
	protected int getScrollBarHeight() {
		float ratio = (float) this.getDownHeight() / (float) this.totalComponentHeight;
		if (ratio * this.getDownHeight() < 8)
			return 8;
		else
			return (int) (ratio * this.getDownHeight());
	}
	
	@Override
	public boolean isPointInside(int x, int y) {
		if (hasDropdown() && down) {
			float x1 = this.getX() + this.getOffsetX();
			float y1 = this.getY() + this.getOffsetY();
			float height = this.getHeight() + this.getDownHeight();
			float width = getWidth() - 2;
			return x1 < x && x1 + width > x && y1 < y && y1 + height > y;
		} else
			return super.isPointInside(x, y);
	}
	
	public float[] getSliderPoint() {
		int scrollbarY = (int) (this.scrollPercentage * getHeightForPoint());
		return new float[] { getOffsetX() + getX() + getWidth() - SLIDER_POINT_WIDTH - SLIDER_PADDING,
				getOffsetY() + getY() + getHeight() + scrollbarY + SLIDER_PADDING + 2,
				SLIDER_POINT_WIDTH,
				getScrollBarHeight() };
	}
	
	protected float getHeightForPoint() {
		float maxPointForRendering = (float) (this.getDownHeight() - getScrollBarHeight() - 1),
				beginPoint = (1);
		return maxPointForRendering - beginPoint;
	}
	
	public float[] getDropdown() {
		float x1 = this.getX() + getWidth() - DROPDOWN_WIDTH + this.getOffsetX();
		float y1 = this.getY() + this.getOffsetY();
		float height = getHeight();
		float width = DROPDOWN_WIDTH;
		return new float[] { x1, y1, width, height };
	}
	
	public boolean isPointInsideDropdown(int x, int y) {
		return hasDropdown() && MathUtils.inside(x, y, getDropdown());
	}

	public Mod getMod() {
		return mod;
	}
	
	@Override
	public String getCommand() {
		return "toggle " + mod.getName();
	}
	
	public boolean hasDropdown() {
		return mod.length() > 0;
	}

	public boolean isDown() {
		return down;
	}

	public void setDown(boolean down) {
		this.down = down;
	}

	public int getTextPadding() {
		return textPadding;
	}

	public void setTextPadding(int textPadding) {
		this.textPadding = textPadding;
	}
	
	public int getDownHeight() {
		return this.dropdownHeight > this.totalComponentHeight ? totalComponentHeight : dropdownHeight;
	}
}
