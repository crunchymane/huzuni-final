package net.halalaboos.huzuni.gui.xray;

import net.halalaboos.huzuni.mod.mods.world.Xray;
import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;

public class BlockInfo {
	
	private ItemStack renderItem;
	
	private Block[] blocks;
	
	public BlockInfo(ItemStack renderItem, Block[] blocks) {
		this.renderItem = renderItem;
		this.blocks = blocks;
	}
	
	public void toggleBlocks() {
		for (Block block : blocks)
			Xray.instance.toggleBlock(Block.getIdFromBlock(block));
	}
	
	public boolean isEnabled() {
		return Xray.instance.isBlockEnabled(Block.getIdFromBlock(blocks[0]));
	}
	
	public String getName() {
		return renderItem.getDisplayName().toLowerCase();
	}
	
	public ItemStack getRenderItem() {
		return renderItem;
	}
	
	public Block[] getBlocks() {
		return blocks;
	}
	
}