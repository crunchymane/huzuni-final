package net.halalaboos.huzuni.plugin.plugins.world;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.w3c.dom.Element;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.console.Command;
import net.halalaboos.huzuni.api.gui.render.Theme;
import net.halalaboos.huzuni.api.plugin.ModeOption;
import net.halalaboos.huzuni.api.plugin.Category;
import net.halalaboos.huzuni.api.plugin.Option;
import net.halalaboos.huzuni.command.CommandManager;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent;
import net.halalaboos.huzuni.event.tick.UpdateListener;
import net.halalaboos.huzuni.plugin.DefaultPlugin;
import net.halalaboos.huzuni.util.GLUtils;
import net.halalaboos.huzuni.util.GameUtils;
import net.halalaboos.huzuni.util.MathUtils;
import net.halalaboos.huzuni.util.StringUtils;
import net.halalaboos.huzuni.util.Timer;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import net.minecraft.util.MovingObjectPosition;

public class AutoFarm extends DefaultPlugin implements Command, UpdateListener {

	private final Timer timer = new Timer();
	
	private float oldYaw = 0F, oldPitch = 0F;
	
	private Block block = null;
	
	private Item replantItem = Items.wheat_seeds;
	
	private int x = -1, y = -1, z = -1, side = -1;
	
	private final ModeOption mode;
	
	public AutoFarm() {
		super("Auto Farm", Keyboard.KEY_H);
		setCategory(Category.WORLD);
		setDescription("Automatically farms / harvests crops.");
		setOptions(mode = new ModeOption("Farm mode", "Harvest", "Plant", "Bonemeal"), new FarmOption());
		CommandManager.registerCommand(this);
	}

	@Override
	public void onPreUpdate(MotionUpdateEvent event) {
		if (hasSelectedBlock()) {
			oldYaw = mc.thePlayer.rotationYaw;
			oldPitch = mc.thePlayer.rotationPitch;
			GameUtils.face(x + 0.5F, y + (mode.getSelected() == 1 ? 1.0F : 0.5F), z + 0.5F);
			if (GameUtils.getDistance(x, y, z) > 4)
				reset();
			return;
		}
		int radius = 4;

		int closestX = -1, closestY = -1, closestZ = -1, closestSide = -1;
		Block closestBlock = null;

		for (int i = radius; i >= -radius; i--) {
			for (int j = -radius; j <= radius; j++) {
				for (int k = radius; k >= -radius; k--) {
					x = (int) (mc.thePlayer.posX + i);
					y = (int) (mc.thePlayer.posY + j);
					z = (int) (mc.thePlayer.posZ + k);
					block = mc.theWorld.getBlock(x, y, z);
					if ((block != null && block.getMaterial() != Material.air) && GameUtils.getDistance(x, y, z) < radius) {
						MovingObjectPosition position = GameUtils.rayTrace(x + 0.5F, y + 0.5F, z + 0.5F);
						if (position != null && position.typeOfHit == MovingObjectPosition.MovingObjectType.BLOCK && position.blockX == x && position.blockY == y && position.blockZ == z)
							side = position.sideHit;
						else
							side = 1;
						
						// bonemeal
						if (mode.getSelected() == 2 && (block == Blocks.carrots || block == Blocks.potatoes || block == Blocks.wheat) && getGrowthLevel(x, y, z) < getMaxGrowthLevel(block)) {								
							if (closestBlock == null) {
								closestBlock = block;
								closestX = x;
								closestY = y;
								closestZ = z;
								closestSide = side;
							} else {
								if (GameUtils.getDistance(x, y, z) < GameUtils.getDistance(closestX, closestY, closestZ)) {
									closestBlock = block;
									closestX = x;
									closestY = y;
									closestZ = z;
									closestSide = side;
								}
							}
							//replant
						} else if (mode.getSelected() == 1 && block == getSoil(replantItem) && isOpenSoil()) {
							if (closestBlock == null) {
								closestBlock = block;
								closestX = x;
								closestY = y;
								closestZ = z;
								closestSide = side;
							} else {
								if (GameUtils.getDistance(x, y, z) < GameUtils.getDistance(closestX, closestY, closestZ)) {
									closestBlock = block;
									closestX = x;
									closestY = y;
									closestZ = z;
									closestSide = side;
								}
							}
							// Harvest
						} else if (mode.getSelected() == 0) {
							if (block == Blocks.reeds) {
								if (mc.theWorld.getBlock(x, y - 1, z) == Blocks.reeds) {
									if (mc.theWorld.getBlock(x, y - 2, z) == Blocks.reeds)
										y--;
									if (closestBlock == null) {
										closestBlock = block;
										closestX = x;
										closestY = y;
										closestZ = z;
										closestSide = side;
									} else {
										if (GameUtils.getDistance(x, y, z) < GameUtils.getDistance(closestX, closestY, closestZ)) {
											closestBlock = block;
											closestX = x;
											closestY = y;
											closestZ = z;
											closestSide = side;
										}
									}
								}
							} else if (block == Blocks.cocoa || block == Blocks.nether_wart || block == Blocks.carrots || block == Blocks.potatoes || block == Blocks.wheat) {
								int growthLevel = getGrowthLevel(x, y, z);
								if (block == Blocks.cocoa)
									growthLevel = (growthLevel & 12) >> 2;
								if (growthLevel >= getMaxGrowthLevel(block)) {
									if (closestBlock == null) {
										closestBlock = block;
										closestX = x;
										closestY = y;
										closestZ = z;
										closestSide = side;
									} else {
										if (GameUtils.getDistance(x, y, z) < GameUtils.getDistance(closestX, closestY, closestZ)) {
											closestBlock = block;
											closestX = x;
											closestY = y;
											closestZ = z;
											closestSide = side;
										}
									}
								}
							}
						}
					}
				}
			}
		}
		if (closestBlock != null) {
			x = closestX;
			y = closestY;
			z = closestZ;
			side = closestSide;
			block = closestBlock;
			oldYaw = mc.thePlayer.rotationYaw;
			oldPitch = mc.thePlayer.rotationPitch;
			GameUtils.face(x + 0.5F, y + (mode.getSelected() == 1 ? 1.0F : 0.5F), z + 0.5F);
		} else {
			x = -1;
			y = -1;
			z = -1;
			block = null;
			side = 0;
		}
	}

	@Override
	public void onPostUpdate(MotionUpdateEvent event) {
		if (hasSelectedBlock()) {
			mc.thePlayer.rotationYaw = oldYaw;
			mc.thePlayer.rotationPitch = oldPitch;
			if (timer.hasReach(100)) {
				if (mode.getSelected() == 0) {
					mc.thePlayer.swingItem();
					sendPacket(0);
					GameUtils.breakBlock(x, y, z);
					reset();
				} else if (mode.getSelected() == 1) {
					int itemSlot = GameUtils.findHotbarItem(replantItem);
					if (itemSlot != -1 && mc.thePlayer.inventory.currentItem != itemSlot) {
						mc.thePlayer.inventory.currentItem = itemSlot;
						mc.playerController.updateController();
					}
					ItemStack item = mc.thePlayer.getCurrentEquippedItem();
					if (item != null && item.getItem() == replantItem) {
						sendPlacePacket();
						mc.thePlayer.getCurrentEquippedItem().tryPlaceItemIntoWorld(mc.thePlayer, mc.theWorld, x, y, z, side, 0, 0, 0);
						mc.thePlayer.swingItem();
						reset();
					}
				} else if (mode.getSelected() == 2) {
					int itemSlot = findBonemeal();
					if (itemSlot != -1 && mc.thePlayer.inventory.currentItem != itemSlot) {
						mc.thePlayer.inventory.currentItem = itemSlot;
						mc.playerController.updateController();
					}
					ItemStack item = mc.thePlayer.getCurrentEquippedItem();
					if (item != null && item.getItem() == Items.dye && item.getItemDamage() == 15) {
						sendPlacePacket();
						mc.thePlayer.getCurrentEquippedItem().tryPlaceItemIntoWorld(mc.thePlayer, mc.theWorld, x, y, z, side, 0, 0, 0);
						mc.thePlayer.swingItem();
						reset();
					}
				}
			}
		}
	}
	
	private int findBonemeal() {
        for (int o = 0; o < 9; o++) {
            ItemStack item = mc.thePlayer.inventory.getStackInSlot(o);
            if (item != null)
            	if (item.getItem() == Items.dye && item.getItemDamage() == 15)
                	return o;
        }
        return -1;
    }
	
	private boolean hasSelectedBlock() {
		return block != null && block.getMaterial() != Material.air;
	}

	@Override
	protected void onEnable() {
		Huzuni.registerUpdateListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterUpdateListener(this);
		reset();
	}
	
    private int getGrowthLevel(int x, int y, int z) {
        return mc.theWorld.getBlockMetadata(x, y, z);
    }

    private int getMaxGrowthLevel(Block block) {
        if (block == Blocks.carrots || block == Blocks.potatoes || block == Blocks.wheat)
        	return 7;
        else if (block == Blocks.nether_wart)
        	return 3;
        else if (block == Blocks.cocoa)
        	return 2;
        else
        	return 0;
    }
    
    private Block getSoil(Item item) {
    	if (item == Items.carrot || item == Items.potato || item == Items.wheat_seeds)
        	return Blocks.farmland;
        else if (item == Items.nether_wart)
        	return Blocks.soul_sand;
        else
        	return null;
    }
    
    private boolean isOpenSoil() {
    	if (replantItem == Items.carrot || replantItem == Items.potato || replantItem == Items.wheat_seeds || replantItem == Items.nether_wart) {
    		return mc.theWorld.getBlock(x, y + 1, z) == Blocks.air;
    	}
    	return false;
    }
    
    private void sendPacket(int mode) {
		mc.getNetHandler().addToSendQueue(new C07PacketPlayerDigging(mode, x, y, z, side));
	}
    
    private void sendPlacePacket() {
    	mc.getNetHandler().addToSendQueue(new C08PacketPlayerBlockPlacement(x, y, z, side, mc.thePlayer.inventory.getCurrentItem(), 0, 0, 0));
    }
    
    private void reset() {
		block = null;
		x = -1;
		y = -1;
		z = -1;
		side = -1;
		timer.reset();
	}

	@Override
	public String[] getAliases() {
		return new String[] { "autofarm", "af", "ah", "autoharvest" };
	}

	@Override
	public String[] getHelp() {
		return new String[] { ".autofarm <replant item>" };
	}

	@Override
	public void run(String input, String[] args) {
		String itemName = StringUtils.getAfter(input, 1);
		Item item = GameUtils.getItemFromName(itemName);
		if (item != null) {
			replantItem = item;
			Huzuni.addChatMessage("Now replanting " + itemName + ".");
		} else {
			Huzuni.addChatMessage("Unable to find item!");
		}
	}
	
	public class FarmOption extends Option {

		private final RenderItem itemRenderer = new RenderItem();
		
		private ItemStack[] seeds = new ItemStack[] {
			new ItemStack(Items.wheat_seeds),
			new ItemStack(Items.potato),
			new ItemStack(Items.carrot),
			new ItemStack(Items.nether_wart),
			new ItemStack(Items.reeds)
		};
		
		public FarmOption() {
			super("Farm Option");	
		}

		@Override
		public boolean mouseClicked(int x, int y, int buttonId) {
			return this.isPointInside(x, y);
		}

		@Override
		public void mouseReleased(int x, int y, int buttonId) {
			int size = (int) ((getWidth() - 2) / 5F);
			int count = 0, yPos = 12, xPos = 0;
			for (int i = 0; i < seeds.length; i++) {
				if (count >= 5) {
					yPos += size + 1;
					xPos = 0;
					count = 0;
				}
				float[] boxArea = new float[] { area[0] + xPos, area[1] + yPos, size, size };
				if (MathUtils.inside(x, y, boxArea)) {
					replantItem = seeds[i].getItem();
				}
				xPos += size + 1;
				count++;
			}
		}

		@Override
		public void render(Theme theme, int index, float[] area,
				boolean mouseOver, boolean mouseDown) {
			Huzuni.drawStringWithShadow("Replant Item", area[0] + 1, area[1] + 1, mode.getSelected() == 1 ? 0xAAFFAA : 0xFFFFFF);
			int size = (int) ((getWidth() - 2) / 5F);
			int count = 0, yPos = 12, xPos = 1;
			for (int i = 0; i < seeds.length; i++) {
				if (count >= 5) {
					yPos += size + 1;
					xPos = 0;
					count = 0;
				}
				float[] boxArea = new float[] { area[0] + xPos, area[1] + yPos, size, size };
				theme.renderSlot(0, 0, index, boxArea, seeds[i].getItem() == replantItem, mouseOver && MathUtils.inside(GLUtils.getMouseX(), GLUtils.getMouseY(), boxArea), mouseDown);
				renderItem(seeds[i], (int) boxArea[0] + 1, (int) boxArea[1] + 1, 0F);
				xPos += size + 1;
				count++;
			}
		}
	    
		private void renderItem(ItemStack itemStack, int x, int y,
				float delta) {
			GL11.glPushMatrix();
            GL11.glDisable(GL11.GL_BLEND);
            GL11.glEnable(GL12.GL_RESCALE_NORMAL);
            RenderHelper.enableGUIStandardItemLighting();
            GL11.glDepthMask(true);
            GL11.glDisable(GL11.GL_DEPTH_TEST);
            try {
            	itemRenderer.renderItemIntoGUI(mc.fontRenderer, mc.getTextureManager(), itemStack, x, y);
            	itemRenderer.renderItemOverlayIntoGUI(mc.fontRenderer, mc.getTextureManager(), itemStack, x, y);
            } catch (Exception e) {
            	e.printStackTrace();
            }
            RenderHelper.disableStandardItemLighting();
            GL11.glDisable(GL12.GL_RESCALE_NORMAL);
            GL11.glEnable(GL11.GL_DEPTH_TEST);
            GL11.glDepthMask(true);
            GL11.glPopMatrix();
		}
		
		@Override
		public void setArea(float[] area) {
			super.setArea(area);
			area[3] = 30;
		}

		@Override
		public void load(Element element) {
		}

		@Override
		public void save(Element element) {
		}
	}

}
