package net.halalaboos.huzuni.mods;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;
import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.client.Settings;
import net.halalaboos.huzuni.events.game.EventKeyPressed;
import net.halalaboos.huzuni.mods.misc.*;
import net.halalaboos.huzuni.mods.movement.*;
import net.halalaboos.huzuni.mods.player.*;
import net.halalaboos.huzuni.mods.pvp.*;
import net.halalaboos.huzuni.mods.render.*;
import net.halalaboos.huzuni.mods.world.*;
import net.halalaboos.lib.io.XMLFileHandler;
import net.halalaboos.lib.reflection.ReflectionHelper;

import org.lwjgl.input.Keyboard;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.transform.TransformerException;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Holds all modules within the client.
 */
public final class ModuleManager implements Listener {
   
	private final File modsFolder = new File(Huzuni.getSaveDirectory(), "mods");
    private final XMLFileHandler fileHandler = new XMLFileHandler(new File(Huzuni.getSaveDirectory(), "Modules.xml"));

    private final Module[] mods = new Module[4096];
        
    public ModuleManager() {
        super();
        registry.registerListener(this);
    }

    /**
     * This is a very cryptive method. You probably wouldn't understand how it works.
     */
    public void loadMods() {
        Huzuni.logger.log("Loading modules...");
        add(new AntiAFK());
        add(new AutoTPAccept());
        add(new Casper());
        add(new Friendsafe());
        add(new GuiOpener());
        add(new MiddleClickFriends());
        add(new Respawn());
        add(new AutoHarvest());
        add(new KillAura());
        add(new AntiKnockback());
        add(new AutoDisconnect());
        add(new AutoSoup());
        add(new Criticals());
        add(new Projectiles());
        add(new Regen());
        add(new TriggerBot());
        add(new Nuker());
        add(new AutoFish());
        add(new VehicleOneHit());
        add(new Dolphin());
        add(new Flight());
        add(new Glide());
        add(new NoFall());
        add(new Spider());
        add(new Sprint());
        add(new Breadcrumb());
        add(new Bright());
        add(new ChestESP());
        add(new NameTags());
        add(new StatusHUD());
        add(new Tracer());
        add(new WireFrame());
        add(new AutoTool());
        add(new Fastplace());
        add(new Freecam());
        add(new NightVision());
        add(new Retard());
        add(new Sneak());
        add(new Speedmine());
        add(new Step());
        add(new AutoBuild());
        add(new ChestStealer());
        add(new AntiCensor());
        add(new DolanSpeak());
        add(new AutoArmor());
        add(new Tips());
        add(new FastLadder());
        Huzuni.logger.log("Loaded " + this.size() + " modules.");
    }

    private int modID = 0;
    
    public void add(Module module) {
    	mods[modID] = module;
        Huzuni.logger.log("Loaded module: [" + modID + "] " + module.getName() + ".");
    	modID++;
    }
    
    public int size() {
    	return modID;
    }
    
	public List<Module> getList() {
		return Arrays.asList(mods).subList(0, modID);
	}
    
    /**
     * @return All default modules.
     * */
    public List<DefaultModule> getDefaultModules() {
        List<DefaultModule> tempList = new ArrayList<DefaultModule>();
        for (int i = 0; i < modID; i++) {
        	Module module = mods[i];
        	if (module != null) {
        		if (module instanceof DefaultModule)
        			tempList.add((DefaultModule) module);
        	}
        }
        return tempList;
    }
    
    /**
     * @return List of all modules that implement Keybindable. Primarily made for Keybind screen.
     * */
    public List<Module> getKeybindableModules() {
        List<Module> tempList = new ArrayList<Module>();
        for (int i = 0; i < modID; i++) {
        	Module module = mods[i];
        	if (module != null) {
        		if (module instanceof Keybindable)
        			tempList.add(module);
        	}
        }
        return tempList;
    }

    /**
     * @return Loaded module with that name.
     * */
    public Module getMod(String name) {
    	for (int i = 0; i < modID; i++) {
        	Module module = mods[i];
        	if (module != null) {
        		if (module.getName().replaceAll(" ", "").equalsIgnoreCase(name.replaceAll(" ", "")))
        			return module;
        	}
        }
        return null;
    }

    /**
     * @return Module with the corresponding id.
     * */
    private Module getMod(int id) {
    	return mods[id];
	}
    
    /**
     * Loads all modules from our /mods/ folder.
     * */
    public void loadExternalModules() {
        if (!modsFolder.exists()) {
            modsFolder.mkdirs();
            return;
        }

        for (File file : modsFolder.listFiles()) {
            if (file.isDirectory())
                continue;
            loadModule(file);
        }
    }

    /**
     * Loads the file as a module.
     * @return 0 if loaded properly with no problems, 1 if (for some reason) it was null, 2 for not being able to load the module, 3 for the file not ending with .jar.
     */
    public int loadModule(File file) {
        if (file.getName().endsWith(".jar")) {
            try {
                Module mod = ModuleLoader.instance.loadModule(file);
                if (mod != null) {
                    add(mod);
                    return 0;
                } else
                    return 1;

            } catch (Exception e) {
                e.printStackTrace();
                return 2;
            }
        } else
            return 3;
    }

    @EventTarget
    public void onKeyPressed(EventKeyPressed event) {
        for (Module module : getKeybindableModules()) {
            if (module instanceof Keybindable) {
                Keybindable bindable = (Keybindable) module;
                if (event.keyCode == bindable.getKeyCode()) {
                    bindable.onKeyPressed();
                }
            }
        }
    }
    	
    /**
     * Reads all data from xml file and applies it to the loaded modules.
     * */
    public void load() {
        try {
        	// Read the document.
            Document doc = fileHandler.read();
            // Get a nodelist of all the module elements.
            NodeList elementList = doc.getElementsByTagName("Module");
            for (int i = 0; i < elementList.getLength(); i++) {
                Node node = elementList.item(i);
                // If it's an element.
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    try {
                    	String name = element.getAttribute("Name");
    	                    Module module = getMod(name);                    		
    	                    if (module != null) {
    	                        boolean moduleState = Boolean.parseBoolean(element.getElementsByTagName("State").item(0).getTextContent().toUpperCase());
    	                        boolean vipModule = module instanceof VipModule || module instanceof VipDefaultModule;
    	                        boolean checkModuleState = vipModule ? Settings.isVip() : true;
    	                        
    	                        if (checkModuleState) {
	    	                        if (module.isEnabled() != moduleState) {//Hotfix to prevent registering the Listener twice.
		    	                    	module.setEnabled(moduleState);
		    	                    }
    	                        }
    	                        if (module instanceof Keybindable) {
    	                            String keyName = element.getElementsByTagName("Keybind").item(0).getTextContent();
    	                            int keyCode = keyName.endsWith("-1") ? -1 : Keyboard.getKeyIndex(keyName);
    	                            ((Keybindable) module).setKeyCode(keyCode);
    	                        }
    	                    }
                    	
                    } catch (Exception e) {
                    	e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

	/**
     * Saves all module data to the xml file handler.
     * */
    public void save() {
    	// Setup the xml document.
        fileHandler.createDocument();
        // Module root, we'll store all modules inside of this directory.
        Element modulesRoot = fileHandler.createElement("Modules");
        // Add the root..
        fileHandler.addElement(modulesRoot);
        for (int i = 0; i < modID; i++) {
        	Module module = mods[i];
        	if (module != null) {
	        	// New module element.
	            Element moduleElement = fileHandler.createElement("Module");
	            // Add it to the root.
	            modulesRoot.appendChild(moduleElement);
	            // Set the attribute 'name' to the modules name
	            moduleElement.setAttribute("Name", module.getName());
	            moduleElement.setAttribute("ID", "" + i);
	            // Add state element to the module.
	            moduleElement.appendChild(fileHandler.createElement("State", Boolean.toString(module.isEnabled())));
	            // Add keybinds (if necessary) to the module.
	            if (module instanceof Keybindable)
	                moduleElement.appendChild(fileHandler.createElement("Keybind", ((Keybindable) module).getKeyName()));
        	}
        }
        try {
            fileHandler.write();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }
    
    public Module loadJar(File jarFile, String classPath, Object... args) throws Exception {
        return ReflectionHelper.<Module>loadInstance(jarFile, classPath, args);
    }

}
