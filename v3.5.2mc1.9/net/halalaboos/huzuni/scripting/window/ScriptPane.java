package net.halalaboos.huzuni.scripting.window;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.plaf.basic.BasicButtonUI;
import javax.swing.text.Document;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;

import net.halalaboos.huzuni.util.FileUtils;
import net.halalaboos.huzuni.util.WindowUtils;

public class ScriptPane extends JPanel implements ActionListener, KeyListener {
   
	private final ScriptWindow window;
	
	private final JTextArea textArea = new JTextArea();
    
	private JLabel title; 
	
	private final JPanel titlePanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
	
	private String originalTitle = "";
	
	private boolean saved = true;
	
	private File file;
	
	public ScriptPane(File file, ScriptWindow window) {
		this(file.getName(), window);
		this.file = file;
		List<String> lines = FileUtils.readFile(file);
		for (String line : lines)
			textArea.append(line + "\n");
	}
	
    public ScriptPane(String title, ScriptWindow window) {
        super();
        this.originalTitle = title;
        this.window = window;
        this.setLayout(new BorderLayout());
        this.setOpaque(false);
        createTitlePanel(title);
        textArea.addKeyListener(this);
		textArea.setForeground(Color.GREEN);
		textArea.setCaretColor(Color.GREEN);
		textArea.setBackground(Color.BLACK);
		this.add(new JScrollPane(textArea), BorderLayout.CENTER);

		getActionMap().put("Save", new AbstractAction("Save") {

		    @Override
		    public void actionPerformed(ActionEvent event) {
		        saveScript();
		    }

		});
		textArea.getInputMap().put(KeyStroke.getKeyStroke("control S"), "Save");
    
		final UndoManager undo = new UndoManager();
	    Document doc = textArea.getDocument();
	    
	    // Listen for undo and redo events
	    doc.addUndoableEditListener(new UndoableEditListener() {
	        public void undoableEditHappened(UndoableEditEvent evt) {
	            undo.addEdit(evt.getEdit());
	        }
	    });
	    
	    // Create an undo action and add it to the text component
	    textArea.getActionMap().put("Undo",
	        new AbstractAction("Undo") {
	            public void actionPerformed(ActionEvent evt) {
	                try {
	                    if (undo.canUndo()) {
	                        undo.undo();
	                    }
	                } catch (CannotUndoException e) {
	                }
	            }
	       });
	    
	    // Bind the undo action to ctl-Z
	    textArea.getInputMap().put(KeyStroke.getKeyStroke("control Z"), "Undo");
	    
	    // Create a redo action and add it to the text component
	    textArea.getActionMap().put("Redo",
	        new AbstractAction("Redo") {
	            public void actionPerformed(ActionEvent evt) {
	                try {
	                    if (undo.canRedo()) {
	                        undo.redo();
	                    }
	                } catch (CannotRedoException e) {
	                }
	            }
	        });
	    
	    // Bind the redo action to ctl-Y
	    textArea.getInputMap().put(KeyStroke.getKeyStroke("control Y"), "Redo");
    }
    
    private void createTitlePanel(String title) {        
    	titlePanel.setOpaque(false);
        this.title = new JLabel(title);
        this.title.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 5));
        titlePanel.add(this.title);
        
        JButton button = new TabButton();
        button.addActionListener(this);
        titlePanel.add(button);
        titlePanel.setBorder(BorderFactory.createEmptyBorder(2, 0, 0, 0));
    }

	public JPanel getTitlePanel() {
		return titlePanel;
	}

	public void saveScript() {
    	if (file == null) {
    		file = WindowUtils.chooseFile("Select");
    		if (file != null && file.isFile() && file.exists()) {
    			originalTitle = file.getName();
    			FileUtils.writeFile(file, new String[] { textArea.getText() });
        		setSaved(true);
    		} else
    			file = null;
    	} else {
    		FileUtils.writeFile(file, new String[] { textArea.getText() });
    		setSaved(true);
    	}
    }
    
    private void setSaved(boolean saved) {
    	this.saved = saved;
    	if (saved)
    		this.title.setText(originalTitle);
    	else
    		this.title.setText(originalTitle + " *");
    }
    
    public boolean isSaved() {
    	return saved;
    }
    
    public JLabel getTitle() {
    	return title;
    }
    
    public String getScript() {
    	return textArea.getText();
    }
    
    private class TabButton extends JButton {
        public TabButton() {
            int size = 17;
            setPreferredSize(new Dimension(size, size));
            setToolTipText("Close");
            setUI(new BasicButtonUI());
            setContentAreaFilled(false);
            setFocusable(false);
            setBorder(BorderFactory.createEtchedBorder());
            setBorderPainted(false);
            addMouseListener(buttonMouseListener);
            setRolloverEnabled(true);
        }
        
        @Override
        public void updateUI() {
        }

        //paint the cross
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D g2 = (Graphics2D) g.create();
            if (getModel().isPressed()) {
                g2.translate(1, 1);
            }
            g2.setStroke(new BasicStroke(2));
            g2.setColor(Color.BLACK);
            if (getModel().isRollover()) {
                g2.setColor(Color.ORANGE);
            }
            int delta = 6;
            g2.drawLine(delta, delta, getWidth() - delta - 1, getHeight() - delta - 1);
            g2.drawLine(getWidth() - delta - 1, delta, delta, getHeight() - delta - 1);
            g2.dispose();
        }
    }

    private final static MouseListener buttonMouseListener = new MouseAdapter() {
        public void mouseEntered(MouseEvent event) {
            Component component = event.getComponent();
            if (component instanceof AbstractButton) {
                AbstractButton button = (AbstractButton) component;
                button.setBorderPainted(true);
            }
        }

        public void mouseExited(MouseEvent event) {
            Component component = event.getComponent();
            if (component instanceof AbstractButton) {
                AbstractButton button = (AbstractButton) component;
                button.setBorderPainted(false);
            }
        }
    };

	@Override
	public void keyPressed(KeyEvent event) {
	}

	@Override
	public void keyReleased(KeyEvent event) {
	}

	@Override
	public void keyTyped(KeyEvent event) {
		this.setSaved(false);
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		window.onClose(this);
	}
}