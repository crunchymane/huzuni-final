package net.halalaboos.huzuni.api.console;

public interface Command {
    
	/**
     * @return An array of aliases available for the command.
     */
    public String[] getAliases();

    /**
     * @return An array of helpful information about the command.
     */
    public String[] getHelp();

    /**
     * @return Description of the command.
     */
    public String getDescription();

    /**
     * Handles processing the commands.
     */
    public void run(String input, String[] args);

}