package net.halalaboos.huzuni.ui.screen.module;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.mods.Module;
import net.halalaboos.lib.ui.BackgroundContainer;
import net.halalaboos.lib.ui.theme.DefaultTheme;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class ModuleManager extends GuiScreen {
   
	private final GuiScreen parentGui;
    private GuiTextField field;
    private GuiButton toggle, clearKey;

    private final BackgroundContainer container = new BackgroundContainer(new DefaultTheme());
    
    private final SlotModules slotModules = new SlotModules(new Dimension(200, 500));
        
    public ModuleManager(GuiScreen par1GuiScreen) {
        parentGui = par1GuiScreen;
    	container.add(slotModules);
    }

    @Override
    public void initGui() {
    	int slotWidth = 300,
    	slotHeight = height / 2;
    	slotModules.setArea(new Rectangle(this.width / 2 - slotWidth / 2, this.height / 2 - slotHeight / 2, slotWidth, slotHeight));
    	slotModules.setBackgroundColor(new Color(0, 0, 0, 50));
    	slotModules.setComponents(Huzuni.modManager.getList());

    	field = new GuiTextField(mc.fontRenderer, width / 2 - 75 - 35, height - 60, 220, 20);
    	field.setFocused(true);
    	
    	buttonList.add(new GuiButton(0, this.width / 2 + 2, this.height - 28, 152, 20, "Done"));
        toggle = new GuiButton(1, this.width / 2 - 154, this.height - 28, 76, 20, "Toggle");
        buttonList.add(toggle);
        
        clearKey = new GuiButton(2, this.width / 2 - 154 + 78, this.height - 28, 76, 20, "Clear key");
        buttonList.add(clearKey);
        toggle.enabled = slotModules.isSelected();
        clearKey.enabled = slotModules.isKeybindableSelected();
    }

    /**
     * Fired when a control is clicked. This is the equivalent of
     * ActionListener.actionPerformed(ActionEvent e).
     */
    @Override
    protected void actionPerformed(GuiButton par1GuiButton) {
        if (!par1GuiButton.enabled) {
            return;
        }

        switch (par1GuiButton.id) {
            case 0:
                mc.displayGuiScreen(parentGui);
                break;
            case 1:
                if (slotModules.getSelectedItem() >= 0 && slotModules.getComponents().size() > 0)
                	slotModules.toggle();
                break;
            case 2:
                if (slotModules.getSelectedItem() >= 0 && slotModules.getComponents().size() > 0)
                	slotModules.clearKeybind();
                break;
            default:

            	break;
        }
    }

    public void confirmClicked(boolean confirm, int id) {
        mc.displayGuiScreen(this);
    }

    /**
     * Fired when a key is typed. This is the equivalent of
     * KeyListener.keyTyped(KeyEvent e).
     */
    @Override
    protected void keyTyped(char c, int keyCode) {
        super.keyTyped(c, keyCode);
        if (!field.isFocused()) {
        	slotModules.onKeyTyped(keyCode, c);
        }
        field.textboxKeyTyped(c, keyCode);

        if (field.getText().length() > 0) {
        	List<Module> tempList = new CopyOnWriteArrayList<Module>();
        	for (Module mod  : Huzuni.modManager.getList()) {
        		if (mod.getName().toLowerCase().contains(field.getText().toLowerCase())) {
        			tempList.add(mod);
        		}
        	}
        	slotModules.setComponents(tempList);
        } else
        	slotModules.setComponents(Huzuni.modManager.getList());

    }

    /**
     * Called when the mouse is clicked.
     */
    @Override
    protected void mouseClicked(int par1, int par2, int par3) {
        super.mouseClicked(par1, par2, par3);
        container.onMousePressed(par1, par2, par3);
        field.mouseClicked(par1, par2, par3);
    }

    /**
     * Draws the screen and all the components in it.
     */
    @Override
    public void drawScreen(int par1, int par2, float par3) {
    	this.drawDefaultBackground();
        drawCenteredString(fontRenderer, "Modules", width / 2, 16, 0xFFFFFF);
        toggle.enabled = slotModules.isSelected();
        clearKey.enabled = slotModules.isKeybindableSelected();
        super.drawScreen(par1, par2, par3);
        container.render(par1, par2);
        field.drawTextBox();
    }

    /**
     * Called from the main game loop to update the screen.
     */
    @Override
    public void updateScreen() {
        super.updateScreen();
       	field.updateCursorCounter();
    }

    @Override
    public void onGuiClosed() {
        super.onGuiClosed();
        Huzuni.modManager.save();
    }

	public SlotModules getSlotAlts() {
		return slotModules;
	}
}
