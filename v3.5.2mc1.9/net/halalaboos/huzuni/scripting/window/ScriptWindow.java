package net.halalaboos.huzuni.scripting.window;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.io.File;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTabbedPane;
import javax.swing.UIManager;

import net.halalaboos.huzuni.scripting.ScriptManager;
import net.halalaboos.huzuni.util.WindowUtils;

public class ScriptWindow extends JFrame {
	
	private final JTabbedPane tabbedPane = new JTabbedPane();
	
	private final JMenuItem newItem = new JMenuItem(new AbstractAction("new") {
		@Override
		public void actionPerformed(ActionEvent event) {
			addScript("Untitled");
		}
	}),
	saveItem = new JMenuItem(new AbstractAction("save") {
		@Override
		public void actionPerformed(ActionEvent event) {
			if (tabbedPane.getSelectedComponent() != null) {
				ScriptPane pane = (ScriptPane) tabbedPane.getSelectedComponent();
				pane.saveScript();
			}
		}
	}),
	loadItem = new JMenuItem(new AbstractAction("load") {
		@Override
		public void actionPerformed(ActionEvent event) {
			File file = WindowUtils.chooseFile("Select");
			if (file != null && file.isFile() && file.exists()) {
				addScript(file);
			}
		}
	});
	
	public ScriptWindow() {
		super("Script Writer");
		setLookAndFeel();
		addComponents();
		this.setSize(480, 320);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	private void addComponents() {
		tabbedPane.setTabLayoutPolicy(JTabbedPane.WRAP_TAB_LAYOUT);
		this.add(tabbedPane, BorderLayout.CENTER);
		this.add(new JButton(new AbstractAction("Run") {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (tabbedPane.getSelectedComponent() != null) {
					ScriptPane pane = (ScriptPane) tabbedPane.getSelectedComponent();
					ScriptManager.runScript(pane.getScript());
				}
			}
			
		}), BorderLayout.SOUTH);
		JMenuBar menuBar = new JMenuBar();
		JMenu fileMenu = new JMenu("File");
		fileMenu.add(newItem);
		fileMenu.add(saveItem);
		fileMenu.add(loadItem);
		menuBar.add(fileMenu);
		this.setJMenuBar(menuBar);
		addScript("Untitled");
	}
	
	private void addScript(String name) {
		ScriptPane pane = new ScriptPane(name, this);
		tabbedPane.add(pane.getTitle().getText(), pane);
		tabbedPane.setTabComponentAt(tabbedPane.indexOfComponent(pane), pane.getTitlePanel());
	}
	
	private void addScript(File file) {
		ScriptPane pane = new ScriptPane(file, this);
		tabbedPane.add(pane.getTitle().getText(), pane);
		tabbedPane.setTabComponentAt(tabbedPane.indexOfComponent(pane), pane.getTitlePanel());
	}

	private void setLookAndFeel() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void onClose(ScriptPane pane) {
		if (pane.isSaved())
			this.tabbedPane.remove(pane);
		else { 
			int choice = WindowUtils.displayOptionQuestion("Unsaved changes", pane.getTitle().getText() + " has some unsaved changes. What would you like to do?", new Object[] { "Save", "Don't save", "Cancel"});
			if (choice == 1) {
				this.tabbedPane.remove(pane);
			} else if (choice == 0) {
				pane.saveScript();
				if (!pane.isSaved())
					this.onClose(pane);
				else
					this.tabbedPane.remove(pane);
			}
		}
	}

}
