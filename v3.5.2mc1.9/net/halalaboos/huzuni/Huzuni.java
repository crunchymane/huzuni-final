package net.halalaboos.huzuni;

import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import org.lwjgl.input.Keyboard;

import net.halalaboos.huzuni.api.io.config.ConfigListener;
import net.halalaboos.huzuni.api.mod.Keybind;
import net.halalaboos.huzuni.api.request.Applicant;
import net.halalaboos.huzuni.event.packet.PacketEvent;
import net.halalaboos.huzuni.event.packet.PacketEventManager;
import net.halalaboos.huzuni.event.packet.PacketListener;
import net.halalaboos.huzuni.event.render.RenderEvent;
import net.halalaboos.huzuni.event.render.RenderEventManager;
import net.halalaboos.huzuni.event.render.Renderable;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent;
import net.halalaboos.huzuni.event.tick.MotionUpdateEventManager;
import net.halalaboos.huzuni.event.tick.UpdateListener;
import net.halalaboos.huzuni.queue.rotation.RotationQueue;
import net.halalaboos.huzuni.rendering.font.MinecraftFontRenderer;
import net.halalaboos.huzuni.util.FileUtils;
import net.halalaboos.huzuni.util.io.config.DefaultConfig;
import net.minecraft.client.Minecraft;

public final class Huzuni {

	/**
	 * TODO:
	 * -------------------------
	 * brendan's chores
	 * glow mod?
	 * -------------------------
	 * halalaboos's chores
	 * AntiAFK
	 * -------------------------
	 * BUGS or features:
	 * -------------------------
	 * IMPORTANT
	 * -------------------------
	 *
	 * 	AUTOARMOR, AUTOBUILD, AND NUKER MAY NOT BE FUNCTIONAL, AUTOPOTION
	 *
	 * 	custom font global broken
	 *
	 * 	AUTOFARM
	 * 	nametags
	 * 	AUTOSOUP
	 */

	public static final String TITLE = "Huzuni";

	public static final String UPDATE_NAME = "Cocoa";

	public static final String VERSION = "3.5.2";
	
	public static final String CAPE_URL = "http://halalaboos.net/client/capes/HuzuniCape.png";

	public static final File SAVE_DIRECTORY = genSaveDirectory();
	
	public static final File FRIEND_FILE = new File(Huzuni.SAVE_DIRECTORY, "Friends.txt");
	
	public static final File MACROS_FILE = new File(Huzuni.SAVE_DIRECTORY, "Macros.txt");

	public static final File KEYBINDS_FILE = new File(Huzuni.SAVE_DIRECTORY, "Keybinds.txt");

	public static final File QUEUE_FILE = new File(Huzuni.SAVE_DIRECTORY, "Queue.txt");

	public static final DefaultConfig CONFIG = new DefaultConfig("Config", genConfigFile());

	public static final MinecraftFontRenderer fontRenderer = new MinecraftFontRenderer(new Font("Verdana", Font.PLAIN, 18), true, false);
	
	public static final PacketEventManager packetEventManager = new PacketEventManager();
	
	public static final MotionUpdateEventManager motionUpdateEventManager = new MotionUpdateEventManager();

	public static final RenderEventManager renderManager = new RenderEventManager();
	
	public static final RotationQueue rotationQueue = new RotationQueue();
		
	public static final String[] MACROS = new String[256];
	
	public static final List<String> DONORS = new ArrayList<String>();
	
	public static final Set<String> FRIENDS = new HashSet<String>();
	
	public static final List<Keybind> KEYBINDS = new ArrayList<Keybind>();
	
	private static boolean antialias, vip, donator, customFont;

	private static boolean enabled = true;
	
	private static float lineSize;

	private static int updateCode = -2;

	private static Color colorTheme = new Color(0, 167, 255, 167);

	private static List<String> updateList = null;
	
	private static String splash = null;
	
	private static Team team = new Team();
		
	public static final Huzuni instance = new Huzuni();
	
	private Huzuni() {

	}
	
	public static void registerPacketListener(PacketListener listener) {
		packetEventManager.registerListener(listener);
	}
	
	public static void unregisterPacketListener(PacketListener listener) {
		packetEventManager.unregisterListener(listener);
	}
	
	public static void firePacketEvent(PacketEvent event) {
		packetEventManager.fire(event);
	}
	
	public static void registerUpdateListener(UpdateListener listener) {
		motionUpdateEventManager.registerListener(listener);
	}
	
	public static void unregisterUpdateListener(UpdateListener listener) {
		motionUpdateEventManager.unregisterListener(listener);
	}
	
	public static void fireUpdateEvent(MotionUpdateEvent event) {
		motionUpdateEventManager.fire(event);
	}

	public static void registerRenderable(Renderable listener) {
		renderManager.registerListener(listener);
	}

	public static void unregisterRenderable(Renderable listener) {
		renderManager.unregisterListener(listener);
	}

	public static void fireRenderEvent(RenderEvent event) {
		renderManager.fire(event);
	}

	public static void setupDefaultConfig() {
		CONFIG.put("First use", true);
		setEnabled(true);
		setAntialias(true);
		setLineSize(1.0F);
		setFont(new Font("Verdana", Font.PLAIN, 18));
		setFontAntialias(true);
		setFontFractionalMetrics(false);
		setDefaultColorTheme();
	}
	
	public static void saveConfig() {
		CONFIG.save();
	}

	public static void loadConfig() {
		CONFIG.load();
		antialias = CONFIG.getBoolean("Antialias");
		customFont = CONFIG.getBoolean("Custom Font");
		lineSize = CONFIG.getFloat("Line Size");
		if (lineSize <= 0)
			lineSize = 1.0F;
		fontRenderer.setFont(getFont());
		fontRenderer.setAntiAlias(isFontAntialias());
		fontRenderer.setFractionalMetrics(isFontFractionalMetrics());
		colorTheme = new Color(CONFIG.getInt("themeRed"), CONFIG.getInt("themeGreen"), CONFIG.getInt("themeBlue"), 167);
	}
	
	public static void saveRotationQueue() {
		List<String> lines = new ArrayList<String>();
		for (int i = 0; i < rotationQueue.getApplicants().size(); i++) {
			Applicant<?> applicant = rotationQueue.getApplicants().get(i);
			lines.add(applicant.getMod().getName());
		}
		FileUtils.writeFile(QUEUE_FILE, lines);
	}
	
	public static void loadRotationQueue() {
		List<String> mods = FileUtils.readFile(QUEUE_FILE);
		List<Applicant<?>> applicantsReordered = new ArrayList<Applicant<?>>();
		for (String line : mods) {
			for (int i = 0; i < rotationQueue.getApplicants().size(); i++) {
				Applicant<?> applicant = rotationQueue.getApplicants().get(i);
				if (applicant.getMod().getName().equals(line)) {
					if (applicantsReordered.isEmpty())
						applicantsReordered.add(applicant);
					else
						applicantsReordered.add(applicantsReordered.size(), applicant);
				}
			}
		}
		if (applicantsReordered.size() == rotationQueue.getApplicants().size())
			rotationQueue.setApplicants(applicantsReordered);
	}

	public static void addConfigListener(ConfigListener<?, ?> listener) {
		CONFIG.addListener(listener);
	}
	
	public static void removeConfigListener(ConfigListener<?, ?> listener) {
		CONFIG.addListener(listener);
	}
	
	public static boolean isAntialias() {
		return antialias;
	}

	public static void setAntialias(boolean antialias) {
		Huzuni.antialias = antialias;
		CONFIG.put("Antialias", antialias);
	}

	public static List<String> getUpdateList() {
		return Huzuni.updateList;
	}

	public static void setUpdateList(List<String> updateList) {
		Huzuni.updateList = updateList;
	}
	
	public static String getSplash() {
		return splash;
	}

	public static void setSplash(String splash) {
		Huzuni.splash = splash;
	}

	public static boolean isVip() {
		return vip;
	}

	public static void setVip(boolean vip) {
		Huzuni.vip = vip;
	}

	public static float getLineSize() {
		return lineSize;
	}

	public static void setLineSize(float lineSize) {
		Huzuni.lineSize = lineSize;
		CONFIG.put("Line Size", lineSize);
	}

	public static boolean isEnabled() {
		return enabled;
	}

	public static void setEnabled(boolean enabled) {
		Huzuni.enabled = enabled;
	}
	
	public static boolean isCustomFont() {
		return customFont;
	}

	public static void setCustomFont(boolean customFont) {
		Huzuni.customFont = customFont;
		CONFIG.put("Custom Font", customFont);
	}

	public static int getUpdateCode() {
		return updateCode;
	}

	public static void setUpdateCode(int updateCode) {
		Huzuni.updateCode = updateCode;
	}

	public static void addDonator(String username) {
		DONORS.add(username);
	}
	
	public static void removeDonator(String username) {
		DONORS.remove(username);
	}

	public static boolean isDonator(String username) {
		return DONORS.contains(username.toLowerCase());
	}
	
	public static boolean isDonator() {
		if (!donator)
			donator = Minecraft.getMinecraft().getSession() != null && DONORS.contains(Minecraft.getMinecraft().getSession().getUsername().toLowerCase());
		return donator || vip;
	}
	
	public static Color getColorTheme() {
		return colorTheme;
	}

	public static void setColorTheme(Color colorTheme) {
		Huzuni.colorTheme = colorTheme;
		CONFIG.put("themeRed", colorTheme.getRed());
		CONFIG.put("themeGreen", colorTheme.getGreen());
		CONFIG.put("themeBlue", colorTheme.getBlue());
	}

	public static void setDefaultColorTheme() {
		setColorTheme(new Color(0, 167, 255, 167));
	}
	
	public static Team getTeam() {
		return team;
	}
	
	public static void loadFriends() {
		FRIENDS.clear();
		FRIENDS.addAll(FileUtils.readFile(FRIEND_FILE));
	}
	
	public static void saveFriends() {
		FileUtils.writeFile(FRIEND_FILE, FRIENDS);
	}
	
	public static void loadMacros() {
		List<String> macros = FileUtils.readFile(MACROS_FILE);
		for (String line : macros) {
			try {
				int keyCode = Keyboard.getKeyIndex(line.substring(0, line.indexOf(":")));
				MACROS[keyCode] = line.substring(line.indexOf(":") + 1);
			} catch (Exception e) {
				
			}
		}
	}
	
	public static void saveMacros() {
		List<String> lines = new ArrayList<String>();
		for (int i = 0; i < MACROS.length; i++) {
			String macro = MACROS[i];
			if (macro != null)
				lines.add(Keyboard.getKeyName(i) + ":" + macro);
		}
		FileUtils.writeFile(MACROS_FILE, lines);
	}
	
	public static void loadKeybinds() {
		List<String> lines = FileUtils.readFile(KEYBINDS_FILE);
		for (String line : lines) {
			try {
				String name = line.substring(0, line.indexOf(":"));
				int keyCode = Integer.parseInt(line.substring(line.indexOf(":") + 1));
				for (int i = 0; i < KEYBINDS.size(); i++) {
					Keybind keybind = KEYBINDS.get(i);
					if (keybind.getName().equals(name))
						keybind.setKeybind(keyCode);
				}
			} catch (Exception e) {
				
			}
		}
	}
	
	public static void saveKeybinds() {
		List<String> lines = new ArrayList<String>();
		for (int i = 0; i < KEYBINDS.size(); i++) {
			Keybind keybind = (Keybind) KEYBINDS.get(i);
			lines.add(keybind.getName() + ":" + keybind.getKeycode());
		}
		FileUtils.writeFile(KEYBINDS_FILE, lines);
	}
	
	public static void runKeybinds(int keyCode) {
		for (Keybind keybind : Huzuni.KEYBINDS) {
			if (keybind.getKeycode() == keyCode)
				keybind.pressed();
		}
	}
	
	public static void addFriend(String name) {
		if (!isFriend(name))
			FRIENDS.add(name.toLowerCase());
	}
	
	public static void removeFriend(String name) {
		if (isFriend(name))
			FRIENDS.remove(name.toLowerCase());
	}
	
	public static boolean isFriend(String name) {
		return FRIENDS.contains(name.toLowerCase());
	}

	public static float drawStringWithShadow(String text, double x, double y, int color) {
		return fontRenderer.drawStringWithShadow(text, x, y, color);
	}

	public static float drawString(String text, double x, double y, int color) {
		return fontRenderer.drawString(text, x, y, color);
	}

	public static int getStringWidth(String text) {
		return fontRenderer.getStringWidth(text);
	}

	public static int getStringHeight(String text) {
		return fontRenderer.getStringHeight(text);
	}

	public static MinecraftFontRenderer getFontRenderer() {
		return fontRenderer;
	}

	private static File genSaveDirectory() {
		File file = new File(Minecraft.getMinecraft().mcDataDir, "huzuni");
		if (!file.exists())
			file.mkdirs();
		return file;
	}
	
	private static File genConfigFile() {
		File file = new File(Huzuni.SAVE_DIRECTORY, "Settings.txt");
		if (!file.exists())
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		return file;
	}

	public static void addChatMessage(String message) {
		if (Minecraft.getMinecraft().thePlayer != null)
			addChatMessage(new TextComponentString(TextFormatting.DARK_GREEN + "[H] " + TextFormatting.RESET + message));
	}

	public static void addChatMessage(ITextComponent component) {
		Minecraft.getMinecraft().ingameGUI.getChatGUI().printChatMessage(component);
	}

	public static void setFont(Font font) {
		CONFIG.put("Font", font.getFontName());
		CONFIG.put("Font Size", font.getSize());
	}

	public static Font getFont() {
		return new Font(CONFIG.get("Font"), Font.TRUETYPE_FONT, CONFIG.getInt("Font Size"));
	}
	
	public static void setFontFractionalMetrics(boolean fractionalMetrics) {
		CONFIG.put("Font Fractional Metrics", fractionalMetrics);
	}
	
	public static boolean isFontFractionalMetrics() {
		return CONFIG.getBoolean("Font Fractional Metrics");
	}

	public static void setFontAntialias(boolean antiAlias) {
		CONFIG.put("Font Antialias", antiAlias);
	}
	
	public static boolean isFontAntialias() {
		return CONFIG.getBoolean("Font Antialias");
	}

	public static void addKeybind(Keybind keybind) {
		KEYBINDS.add(keybind);
	}
	
}
