package net.halalaboos.huzuni.mods.pvp;

import net.halalaboos.huzuni.client.Settings;
import net.halalaboos.huzuni.events.game.EventPostMotionUpdate;
import net.halalaboos.huzuni.events.game.EventPostStartup;
import net.halalaboos.huzuni.events.game.EventPreMotionUpdate;
import net.halalaboos.huzuni.helpers.Helper;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.DefaultModule;
import net.halalaboos.lib.io.config.DefaultConfigListener;
import net.halalaboos.lib.ui.Component;
import net.halalaboos.lib.ui.components.Dropdown;
import net.halalaboos.lib.ui.containers.Window;
import net.halalaboos.lib.ui.listeners.ActionListener;
import net.minecraft.entity.EntityLivingBase;

import org.lwjgl.input.Keyboard;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;
import com.darkmagician6.eventapi.types.Priority;
import com.darkmagician6.morbidlib.utils.timer.AccurateTimer;
import com.darkmagician6.morbidlib.utils.timer.Timer;

public class KillAura extends DefaultModule implements Listener, Helper, ActionListener<Dropdown> {
	
	private EntityLivingBase selectedEntity;
    private final Timer timer = new AccurateTimer();
    
	private DefaultConfigListener<Float> speed = new DefaultConfigListener<Float>("Kill Aura Speed", 8F);
	private DefaultConfigListener<Float> reach = new DefaultConfigListener<Float>("Kill Aura Reach", 3.8F);
	private DefaultConfigListener<Integer> mode = new DefaultConfigListener<Integer>("Kill Aura Mode", 0);

	private String[] modes = new String[] {
			"All", "Players", "Mobs", "Animals"
	};
    
	public KillAura() {
		super("Kill Aura", Keyboard.KEY_R);
		setCategory(Category.PVP);
		setDescription("Automatically attacks other players / mobs.");
		Settings.addListener(speed);
		Settings.addListener(reach);
		Settings.addListener(mode);
		Settings.put("Kill Aura Speed", 8);
		Settings.put("Kill Aura Reach", 3.8F);
		Settings.put("Kill Aura Mode", 0);
		registry.registerListener(this, EventPostStartup.class);

	}
	
	@Override
	protected void onToggle() {
		
	}

	@Override
	protected void onEnable() {
		registry.registerListener(this, EventPreMotionUpdate.class);
		registry.registerListener(this, EventPostMotionUpdate.class);
	}

	@Override
	protected void onDisable() {
		registry.unregisterListener(this, EventPreMotionUpdate.class);
		registry.unregisterListener(this, EventPostMotionUpdate.class);
	}

    @EventTarget(Priority.LOWEST)
    public void onPreMotionUpdate(EventPreMotionUpdate event) {
        selectedEntity = entityHelper.getClosestEntity(true, getReach(), 2.5F, mode.getValue() == 0 || mode.getValue() == 2, 
        		mode.getValue() == 0 || mode.getValue() == 3, 
        		mode.getValue() == 0 || mode.getValue() == 1);
        if (selectedEntity != null) {
            if (isHittableNotNull(selectedEntity, true))
                entityHelper.faceEntity(selectedEntity);
            if (isHittableNotNull(selectedEntity, false))
                mc.thePlayer.setSprinting(false);
        }
    }

    @EventTarget(Priority.HIGHEST)
    public void onPostMotionUpdate(EventPostMotionUpdate event) {
        if (isHittableNotNull(selectedEntity, false)) {
            if (timer.hasReach(1000F / getSpeed())) {
                mc.thePlayer.swingItem();
                mc.playerController.attackEntity(mc.thePlayer, selectedEntity);
                timer.reset();
            }
        }
    }
    @EventTarget
    public void onPostStartup(EventPostStartup event) {
		Window window = huzuni.windowManager.getWindow(Category.PVP.formalName);
		Dropdown dropdown = new Dropdown("PVP Modes");
		dropdown.setComponents(modes);
		dropdown.setSelectedComponent(mode.getValue());
		dropdown.addActionListener(this);
		window.add(dropdown);
		window.layout();
    }

    /**
     * @return Reach distance for kill aura.
     */
    public final float getReach() {
        return reach.getValue();
    }

    /**
     * @return Kill Aura speed in attacks per second.
     */
    public final float getSpeed() {
        return speed.getValue();
    }

    /**
     * @return True if the entity is alive and within distance.
     */
    private boolean isHittableNotNull(EntityLivingBase entity, boolean justLook) {
        return !entity.isInvisible() && entityHelper.isAliveNotUs(entity) && entityHelper.isWithinDistance(entity, justLook ? getReach() + 2.5F : getReach()) && mc.thePlayer.canEntityBeSeen(entity);
    }

	@Override
	public void onAction(Dropdown dropdown) {
		Settings.put("Kill Aura Mode", dropdown.getSelectedComponent());
	}
}
