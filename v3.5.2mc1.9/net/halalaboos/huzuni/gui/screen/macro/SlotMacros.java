package net.halalaboos.huzuni.gui.screen.macro;

import org.lwjgl.input.Keyboard;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.gui.render.Theme;
import net.halalaboos.huzuni.gui.clickable.slot.SlotComponent;

public class SlotMacros extends SlotComponent<String> {
	
	public SlotMacros() {
		super(0, 0);
	}

	@Override
	protected void render(Theme theme, int index, String line, float[] area, boolean highlight, boolean mouseOver,
			boolean mouseDown) {
		String key = "";
		String macro = "";
		if (line.split(":").length >= 1) {
			key = line.split(":")[0];
			macro = line.substring(line.indexOf(":") + 1);
		}
		int macroX = (int) area[0];
		int keyX = (int) area[0];
		
		theme.renderSlot(this.getOffsetX(), this.getOffsetY(), index, area, highlight, mouseOver, mouseDown);

		Huzuni.drawStringWithShadow(Keyboard.getKeyName(Integer.parseInt(key)), macroX + 2, area[1] + 2, 0xFFFFFF);
		Huzuni.drawStringWithShadow(macro, keyX + 2, area[1] + 12, 0xCCCCCC);
	}
	
	@Override
	public int getComponentHeight(String line) {
		return 22;
	}

	@Override
	protected void onReleased(int index, String component, int mouseX,
			int mouseY) {
	}

	@Override
	protected void onClicked(int index, String component, float[] area,
			int mouseX, int mouseY, int buttonId) {
	}
	
}