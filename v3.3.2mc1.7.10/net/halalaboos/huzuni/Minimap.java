package net.halalaboos.huzuni;

import net.halalaboos.huzuni.rendering.Texture;
import net.halalaboos.huzuni.util.GLUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.MathHelper;
import static org.lwjgl.opengl.GL11.*;

public final class Minimap {
	
	private static final int diameter = 128, entityDiameter = 8, borderSize = 2;
	
    private static final Texture minimap = new Texture("huzuni/minimap.png"),
    		entity = new Texture("huzuni/entity.png");
	
	private Minimap() {
		
	}
	
	public static void render(Minecraft mc, int screenWidth, int screenHeight, boolean right, boolean top) {
		float padding = borderSize + 1, chatHeight = mc.ingameGUI.getChatGUI().func_146241_e() ? 12 : 0,
			x = right ? screenWidth - diameter / 2 - padding : diameter / 2 + padding, 
			y = top ? diameter / 2 + padding : screenHeight - diameter / 2 - padding - chatHeight,
			entitySize = entityDiameter / 2;
		glPushMatrix();
		glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glTranslatef(x, y, 0F);
		glColor3f(1F, 1F, 1F);
		minimap.render(-diameter / 2, -diameter / 2, diameter, diameter);
		entity.render(-entitySize, -entitySize, entityDiameter, entityDiameter);
		
		GLUtils.glScissor((int) (x - diameter / 2) + borderSize, (int) (y - diameter / 2) + borderSize,  (int) (x + diameter / 2) - borderSize, (int) (y + diameter / 2) - borderSize);
		glEnable(GL_SCISSOR_TEST);
		
		for (int i = 0; i < mc.theWorld.playerEntities.size(); i++) {
			EntityPlayer player = (EntityPlayer) mc.theWorld.playerEntities.get(i);
			
			if (mc.thePlayer == player)
				continue;
			
			float distance = mc.thePlayer.getDistanceToEntity(player);
			double angle = MathHelper.wrapAngleTo180_double(mc.thePlayer.rotationYaw - (Math.atan2(RenderManager.renderPosZ - interpolate(player.prevPosZ, player.posZ, mc.timer.renderPartialTicks), RenderManager.renderPosX - interpolate(player.prevPosX, player.posX, mc.timer.renderPartialTicks)) * 180.0D / Math.PI));
			double pX = Math.cos(Math.toRadians(angle)) * distance;
			double pY = -Math.sin(Math.toRadians(angle)) * distance;
			boolean friend = Huzuni.isFriend(player.getCommandSenderName()), sneaking = player.isSneaking();
			int color = friend ? 0xFF5ABDFC : sneaking ? 0xFFFF0080 : 0xFFFFFFFF;
			String name = NameProtect.replace(player.getCommandSenderName());
			glPushMatrix();
			if (pX > (diameter / 2) - entitySize - borderSize)
				pX = (diameter / 2) - entitySize - borderSize;
			
			if (pX < -(diameter / 2) + entitySize + borderSize)
				pX = -(diameter / 2) + entitySize + borderSize;
			
			if (pY > (diameter / 2) - entitySize - borderSize)
				pY = (diameter / 2) - entitySize - borderSize;
			
			if (pY < -(diameter / 2) + entitySize + borderSize)
				pY = -(diameter / 2) + entitySize + borderSize;
			glTranslated(pX, pY, 0F);
			GLUtils.glColor(color);
			glRotatef(player.rotationYaw, 0F, 0F, 1F);
			entity.render(-entitySize, -entitySize, entityDiameter, entityDiameter);
			glRotatef(-player.rotationYaw, 0F, 0F, 1F);
			
			glScalef(0.75F, 0.75F, 0.75F);
			glColor4f(0F, 0F, 0F, 0.5F);
			GLUtils.drawRect(-2, -9, Huzuni.getStringWidth(name) + 2, 2);
			Huzuni.drawStringWithShadow(name, 0, -7, color);
			glPopMatrix();
		}
		
		glDisable(GL_SCISSOR_TEST);
		glPopMatrix();
	}
	
	private static double interpolate(double prev, double cur, double delta) {
		return prev + ((cur - prev) * delta);
	}
	
}
