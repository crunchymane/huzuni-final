package in.brud.huzuni;

import com.darkmagician6.morbidlib.utils.timer.AccurateTimer;
import com.darkmagician6.morbidlib.utils.timer.Timer;
import in.brud.huzuni.util.PanoramaRenderer;
import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.client.DonatorLoader;
import net.halalaboos.huzuni.client.Settings;
import net.halalaboos.huzuni.helpers.Helper;
import net.halalaboos.huzuni.rendering.Texture;
import net.halalaboos.huzuni.ui.particle.ParticleEngine;
import net.halalaboos.huzuni.ui.screen.HuzuniButton;
import net.halalaboos.huzuni.ui.screen.HuzuniDonorFeatures;
import net.halalaboos.huzuni.ui.screen.HuzuniOptions;
import net.halalaboos.huzuni.ui.screen.alts.AltManager;
import net.halalaboos.huzuni.utils.RenderUtils;
import net.minecraft.client.gui.*;
import net.minecraft.client.resources.I18n;
import org.lwjgl.Sys;
import org.lwjgl.opengl.GL11;

import java.awt.*;

/**
 * The new main menu.
 *
 * @author brudin
 * @version 1.0
 * @since 1/4/14
 */
public class MainMenu extends GuiScreen implements Helper {

    private PanoramaRenderer panoramaRenderer;
    private static final Texture TITLE = new Texture("huzuni/logoredux.png");
    private static final ParticleEngine PARTICLE_ENGINE = new ParticleEngine(false);
    private static final Timer ACCURATE_TIMER = new AccurateTimer();
    private Color color;
    private int alpha = 255;

    public MainMenu() {}

    @Override
    public boolean doesGuiPauseGame() {
        return false;
    }

    @Override
    public void initGui() {
        panoramaRenderer = new PanoramaRenderer(width, height);
        panoramaRenderer.init();
        color = RenderUtils.getRandomColor(125, 125);
        buttonList.clear();
        int size = width / 6 + 1;
        int i = 0;
        buttonList.add(new HuzuniButton(1, i, 0, size, 20, I18n.getStringParams("menu.singleplayer")));
        buttonList.add(new HuzuniButton(2, i += size, 0, size, 20, I18n.getStringParams("menu.multiplayer")));
        buttonList.add(new HuzuniButton(3, i += size, 0, size, 20, "Accounts"));
        buttonList.add(new HuzuniButton(7, i += size, 0, size, 20, DonatorLoader.isDonator() ? "Donator Perks" : "VIP/Donate"));
        buttonList.add(new HuzuniButton(0, i += size, 0, size, 20, "Options"));
        buttonList.add(new HuzuniButton(5, i += size, 0, size, 20, "Huzuni"));

        buttonList.add(new HuzuniButton(4, width - 80, height - 20, 80, 20, I18n.getStringParams("menu.quit")));
        if (huzuniHelper.getUpdateCode() == 1)
            buttonList.add(new HuzuniButton(6, width / 2 - 22, this.height / 2 + 30, 45, 20, "Update"));
    }

    @Override
    protected void actionPerformed(GuiButton button) {
        switch (button.id) {
            case 0:
                this.mc.displayGuiScreen(new GuiOptions(this, this.mc.gameSettings));
                break;

            case 1:
                this.mc.displayGuiScreen(new GuiSelectWorld(this));
                break;

            case 2:
                this.mc.displayGuiScreen(new GuiMultiplayer(this));
                break;

            case 3:
                this.mc.displayGuiScreen(new AltManager(this));
                break;

            case 4:
                this.mc.shutdown();
                break;

            case 5:
                this.mc.displayGuiScreen(new HuzuniOptions(this));
                break;

            case 6:
                Sys.openURL("http://halalaboos.net/huzuni.html");
                break;

            case 7:
                if (DonatorLoader.isDonator()) {
                    mc.displayGuiScreen(new HuzuniDonorFeatures(this));
                } else
                    Sys.openURL("http://halalaboos.net/donate.html");
                break;
        }
    }

    @Override
    protected void mouseClicked(int x, int y, int buttonID) {
        super.mouseClicked(x, y, buttonID);
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        String versionStatus = getReturnCodeStatus(huzuniHelper.getUpdateCode());
        Color background = new Color(0,0,0,alpha);
        float titleX = width / 2 - 150, titleY = height / 2 - 70;
        GL11.glColor3f(1, 1, 1);
        super.drawDefaultBackground();
        panoramaRenderer.renderSkybox(mouseX, mouseY, partialTicks);
        PARTICLE_ENGINE.render();
        TITLE.renderTexture(titleX, titleY + 10, 300, 100, new Color(-2101508));
        Huzuni.display.guiFontRenderer.drawString("v" + Huzuni.VERSION + (Settings.isVip() ? " (VIP)" : ""), 2, height - Huzuni.display.guiFontRenderer.getHeight() - 2, 0x4fcccccc);
        super.drawScreen(mouseX, mouseY, partialTicks);
        Huzuni.display.guiFontRenderer.drawString(versionStatus, width / 2 - Huzuni.display.guiFontRenderer.getStringWidth(versionStatus) / 2, titleY + 90, 0xCFCCCCCC);
        Huzuni.display.guiFontRenderer.drawString(mc.getSession().getUsername(), 2, height - Huzuni.display.guiFontRenderer.getHeight()*2 - 5, 0x4fcccccc);
        panoramaRenderer.renderFade();
    }

    /**
     * @return String representing what the version return code means.
     */
    private String getReturnCodeStatus(int versionReturnCode) {
        switch (versionReturnCode) {
            case -1:
                return "\247oUnable to connect.";
            case 0:
                return Settings.isVip() ? "Thank you :)" : "Donate!!";
            case 1:
                return "\2474Out of date!";
            case -2:
                return "Retrieving...";
        }
        return "" + versionReturnCode;
    }

    @Override
    public void updateScreen() {
        if(alpha > 0)
            alpha -= 15;
        super.updateScreen();
        panoramaRenderer.panoramaTick();
        PARTICLE_ENGINE.updateParticles();
    }
}