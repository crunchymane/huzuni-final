package net.halalaboos.huzuni.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.request.Applicant;
import net.halalaboos.huzuni.mod.mods.player.Freecam;
import net.halalaboos.huzuni.queue.rotation.RotationData;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.network.NetworkPlayerInfo;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.IMob;
import net.minecraft.entity.passive.IAnimals;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.ShapedRecipes;
import net.minecraft.item.crafting.ShapelessRecipes;
import net.minecraft.nbt.JsonToNBT;
import net.minecraft.nbt.NBTException;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.registry.RegistryNamespaced;
import net.minecraft.util.text.translation.I18n;

public final class GameUtils {

	private static final Minecraft mc = Minecraft.getMinecraft();
	
	private GameUtils() {
		
	}
	
	public static void requestFace(Applicant<?> applicant, boolean silent, BlockPos position) {
		requestFace(applicant, silent, position.getX(), position.getY(), position.getZ());
	}
	public static void requestFaceOffset(Applicant<?> applicant, boolean silent, BlockPos position, float offset) {
		requestFace(applicant, silent, position.getX() + offset, position.getY() + offset, position.getZ() + offset);
	}
	
	public static void requestFace(Applicant<?> applicant, boolean silent, double posX, double posY, double posZ) {
        double diffX = (posX) - mc.thePlayer.posX;
        double diffZ = (posZ) - mc.thePlayer.posZ;
        double diffY = (posY) - (mc.thePlayer.posY + (double) mc.thePlayer.getEyeHeight());


        double dist = (double) MathHelper.sqrt_double(diffX * diffX + diffZ * diffZ);
        float yaw0 = (float) (Math.atan2(diffZ, diffX) * 180.0D / Math.PI) - 90.0F;
        float pitch0 = (float) (-(Math.atan2(diffY, dist) * 180.0D / Math.PI));
        float yaw = mc.thePlayer.rotationYaw + MathHelper.wrapAngleTo180_float(yaw0 - mc.thePlayer.rotationYaw);
        float pitch = mc.thePlayer.rotationPitch + MathHelper.wrapAngleTo180_float(pitch0 - mc.thePlayer.rotationPitch);
        Huzuni.rotationQueue.request(applicant, new RotationData(yaw, pitch, silent));
    }

	public static float getDistance(BlockPos position, float offset) {
		return getDistance(position.getX() + offset, position.getY() + offset, position.getZ() + offset);
	}
	
	public static float getDistance(float x, float y, float z) {
        float xDiff = (float) (mc.thePlayer.posX - x);
        float yDiff = (float) (mc.thePlayer.posY - y);
        float zDiff = (float) (mc.thePlayer.posZ - z);
        return MathHelper.sqrt_float(xDiff * xDiff + yDiff * yDiff + zDiff * zDiff);
	}
	
	public static boolean rayTraceBlock(int x, int y, int z) {
		RayTraceResult position = rayTrace(x + 0.5F, y + 0.5F, z + 0.5F);
		return position != null && position.typeOfHit == RayTraceResult.Type.BLOCK && position.getBlockPos().getX() == x && position.getBlockPos().getY() == y && position.getBlockPos().getZ() == z;
	}

	public static RayTraceResult rayTraceOffset(BlockPos position, float offset) {
		return rayTrace(position.getX() + offset, position.getY() + offset, position.getZ() + offset);
	}
	
	public static RayTraceResult rayTrace(BlockPos position) {
		return rayTrace(position.getX(), position.getY(), position.getZ());
	}
	
	public static RayTraceResult rayTrace(double x, double y, double z) {
        Vec3d player = Vec3d.createVectorHelper(mc.thePlayer.posX, mc.thePlayer.posY + mc.thePlayer.getEyeHeight(), mc.thePlayer.posZ),
                position = Vec3d.createVectorHelper(x, y, z);
        return mc.theWorld.rayTraceBlocks(player, position);
	}
	
	public static void breakBlock(int x, int y, int z) {
		breakBlock(new BlockPos(x, y, z));
	}
	
	public static void breakBlock(BlockPos position) {
		mc.theWorld.setBlockToAir(position);
	}
	
	public static Block getBlock(BlockPos blockPos) {
		return Minecraft.getMinecraft().theWorld.getBlockState(blockPos).getBlock();
	}
	
	public static Block getBlock(int x, int y, int z) {
		return Minecraft.getMinecraft().theWorld.getBlockState(new BlockPos(x, y, z)).getBlock();
	}
	
	public static String getBlockName(Block block) {
		String name = new ItemStack(block).getDisplayName();
		return name == null ? "null" : I18n.translateToLocal(name).trim();
	}
	
	public static int findHotbarItemById(int itemId) {
        for (int o = 0; o < 9; o++) {
            ItemStack item = mc.thePlayer.inventory.getStackInSlot(o);
            if (item != null) {
            	if (Item.getItemById(itemId) == item.getItem())
                	return o;
            }
        }
        return -1;
    }
	
	public static int findHotbarItem(Item item) {
        for (int o = 0; o < 9; o++) {
            ItemStack item2 = mc.thePlayer.inventory.getStackInSlot(o);
            if (item2 != null) {
            	if (item == item2.getItem())
                	return o;
            }
        }
        return -1;
    }
	
	public static Item getItemFromName(String name) {
		for (Object o : Item.itemRegistry) {
			Item item = (Item) o;
			String name2 = item.getUnlocalizedName() == null ? "" : I18n.translateToLocal(item.getUnlocalizedName() + ".name").trim();
			if (name.replaceAll(" ", "").equalsIgnoreCase(name2.replaceAll(" ", "")))
				return item;
		}
		return null;
	}
	
	public static IRecipe findRecipe(ItemStack itemStack) {
		for (IRecipe recipe : (List<IRecipe>) CraftingManager.getInstance().getRecipeList()) {
			if (compare(itemStack, recipe.getRecipeOutput()))
				return recipe;
		}
		return null;
	}
	
	public static boolean isCraftable(IRecipe recipe) {
		if (recipe instanceof ShapedRecipes)
			return isCraftable((ShapedRecipes) recipe);
		else if (recipe instanceof ShapelessRecipes)
			return isCraftable((ShapelessRecipes) recipe);
		else
			return false;
	}
	
	public static boolean isCraftable(ShapedRecipes shapedRecipe) {
		ItemStack[] items = shapedRecipe.getRecipeItems().clone();
		boolean failed = false;
		for (ItemStack item : (List<ItemStack>) mc.thePlayer.inventoryContainer.getInventory()) {
			if (item != null) {
				ItemStack inventoryItem = item.copy();
				for (int i = 0; i < items.length; i++) {
					ItemStack neededItem = items[i];
					if (neededItem != null && inventoryItem != null) {
						if (compare(inventoryItem, neededItem))
							items[i] = null;
						inventoryItem.stackSize--;
						if (inventoryItem.stackSize <= 0)
							inventoryItem = null;
					}
				}
			}
		}
		for (ItemStack neededItem : items)
			if (neededItem != null)
				failed = true;
		return !failed;
	}
	
	public static boolean isCraftable(ShapelessRecipes shapelessRecipe) {
		List<ItemStack> items = new ArrayList<ItemStack>(shapelessRecipe.getRecipeItems()); 
		boolean failed = false;
		for (ItemStack item : (List<ItemStack>) mc.thePlayer.inventoryContainer.getInventory()) {
			if (item != null) {
				ItemStack inventoryItem = item.copy();
				for (int i = 0; i < items.size(); i++) {
					ItemStack neededItem = items.get(i);
					if (neededItem != null && inventoryItem != null) {
						if (compare(inventoryItem, neededItem))
							items.remove(i);
						inventoryItem.stackSize--;
						if (inventoryItem.stackSize <= 0)
							inventoryItem = null;
					}
				}
			}
		}
		for (ItemStack neededItem : items)
			if (neededItem != null)
				failed = true;
		return !failed;
	}
	
	public static boolean compare(ItemStack item1, ItemStack item2) {
		if (item1 != null || item2 != null) {
			if (item1 == null && item2 != null || item1 != null && item2 == null) {
				return false;
			}

			if (item2.getItem() != item1.getItem()) {
				return false;
			}

			if (item2.getItemDamage() != 32767 && item2.getItemDamage() != item1.getItemDamage()) {
				return false;
			}
		}
		return true;
	}
	
	public static EntityLivingBase getClosestEntityToPlayer(Entity toPlayer, float distance, float extender, boolean invisible, boolean mob, boolean animal, boolean player) {
		EntityLivingBase currentEntity = null;
		for (int i = 0; i < mc.theWorld.loadedEntityList.size(); i++) {
    		if (mc.theWorld.loadedEntityList.get(i) instanceof EntityLivingBase) {
    			EntityLivingBase entity = (EntityLivingBase) mc.theWorld.loadedEntityList.get(i);
    			if (isAliveNotUs(entity) && checkType(entity, invisible, mob, animal, player) && checkTeam(entity)) {
    				if (entity instanceof EntityPlayer && Huzuni.isFriend(entity.getName()))
    					continue;
    				if (currentEntity != null) {
    	                if (toPlayer.getDistanceToEntity(entity) < toPlayer.getDistanceToEntity(currentEntity))
        					currentEntity = entity;
    				} else {
    					if (toPlayer.getDistanceToEntity(entity) < distance + extender)
    						currentEntity = entity;
    				}
    			}
    		}
    	}
		return currentEntity;
	}
	
	
	public static EntityLivingBase getClosestEntity(float distance, float extender, boolean invisible, boolean mob, boolean animal, boolean player) {
		EntityLivingBase currentEntity = null;
		for (int i = 0; i < mc.theWorld.loadedEntityList.size(); i++) {
    		if (mc.theWorld.loadedEntityList.get(i) instanceof EntityLivingBase) {
    			EntityLivingBase entity = (EntityLivingBase) mc.theWorld.loadedEntityList.get(i);
    			if (isAliveNotUs(entity) && checkType(entity, invisible, mob, animal, player) && checkTeam(entity)) {
    				if (entity instanceof EntityPlayer && Huzuni.isFriend(entity.getName()))
    					continue;
    				if (currentEntity != null) {
    	                if (isWithinDistance(entity, distance + extender) && isClosestToMouse(currentEntity, entity, distance, extender))
        					currentEntity = entity;
    				} else {
    					if (isWithinDistance(entity, distance + extender))
    						currentEntity = entity;
    				}
    			}
    		}
    	}
		return currentEntity;
	}
	
	public static boolean checkTeam(EntityLivingBase entity) {
		 if (!Huzuni.getTeam().isEnabled() || !Huzuni.getTeam().hasSelected())
			 return true;
		if (entity instanceof EntityPlayer) {
			return !Huzuni.getTeam().hasColor(entity.getDisplayName().getFormattedText());
		} else
			return true;
	}

	/**
     * @return the closest entity to your mouse.
     */
	public static boolean isClosestToMouse(EntityLivingBase currentEntity, EntityLivingBase otherEntity, float distance, float extender) {

        // If we can't reach our current entity without the extender, but we CAN with our OTHER entity, return true.
        if (!isWithinDistance(currentEntity, distance) && isWithinDistance(otherEntity, distance))
            return true;

        float otherDist = getDistanceFromMouse(otherEntity),
        		currentDist = getDistanceFromMouse(currentEntity);
        return (otherDist < currentDist || currentDist == -1) && otherDist != -1;
    }
	
	public static boolean isWithinDistance(EntityLivingBase entity, float distance) {
        return mc.thePlayer.getDistanceToEntity(entity) < distance;
    }

	public static boolean isAliveNotUs(EntityLivingBase entity) {
		if (Freecam.instance.isEnabled()) {
			if (entity.getName().equalsIgnoreCase(mc.thePlayer.getName())) {
				return false;
			}
		}
		if (entity instanceof EntityPlayer && ((EntityPlayer) entity).getGameProfile().getProperties().size() == 0) return false;
		return (entity != null) && (entity.isEntityAlive() && entity != mc.thePlayer && entity.getAge() != 7);
	}
	
	public static boolean checkType(Entity entity, boolean invisible, boolean mob, boolean animal, boolean player) {
		if (!entity.isEntityAlive())
			return false;
		if (entity.isInvisible() && !invisible) {
			return false;
		}
    	if (mob && entity instanceof IMob)
    		return true;
    	if (animal && entity instanceof IAnimals && !(entity instanceof IMob))
    		return true;
    	if (player && entity instanceof EntityPlayer && ((EntityPlayer) entity).getGameProfile().getProperties().size() > 0)
    		return true;
    	return false;
    }
	
	/**
     * @return Distance the entity is from our mouse.
     */
	public static float getDistanceFromMouse(EntityLivingBase entity) {
        float[] neededRotations = getRotationsNeeded(entity);
        if (neededRotations != null) {
            float neededYaw = mc.thePlayer.rotationYaw - neededRotations[0],
                    neededPitch = mc.thePlayer.rotationPitch - neededRotations[1];
			return MathHelper.sqrt_float(neededYaw * neededYaw + neededPitch * neededPitch);
        }
        return -1F;
    }
	
	   /**
     * @return Rotations needed to face the entity.
     */
	public static float[] getRotationsNeeded(double x, double y, double z) {
        double xSize = x - mc.thePlayer.posX;
        double ySize = y - (mc.thePlayer.posY + (double) mc.thePlayer.getEyeHeight());
        double zSize = z - mc.thePlayer.posZ;
        
        double theta = (double) MathHelper.sqrt_double(xSize * xSize + zSize * zSize);
        float yaw = (float) (Math.atan2(zSize, xSize) * 180.0D / Math.PI) - 90.0F;
        float pitch = (float) (-(Math.atan2(ySize, theta) * 180.0D / Math.PI));
        return new float[] {
                mc.thePlayer.rotationYaw + MathHelper.wrapAngleTo180_float(yaw - mc.thePlayer.rotationYaw),
                mc.thePlayer.rotationPitch + MathHelper.wrapAngleTo180_float(pitch - mc.thePlayer.rotationPitch)
        };
	}

    /**
     * @return Rotations needed to face the entity.
     */
	public static float[] getRotationsNeeded(EntityLivingBase entity) {
        if (entity == null)
            return null;
        return getRotationsNeeded(entity.posX, entity.posY + ((double) entity.getEyeHeight() / 2F), entity.posZ);
	}

	public static void addEnchant(Enchantment ench, short level, ItemStack itemStack) {
		if (itemStack.getTagCompound() == null) {
			itemStack.setTagCompound(new NBTTagCompound());
		}
		if (!itemStack.getTagCompound().hasKey("ench", 9)) {
			itemStack.getTagCompound().setTag("ench", new NBTTagList());
		}
		NBTTagList nbttaglist = itemStack.getTagCompound().getTagList("ench", 10);
		NBTTagCompound nbttagcompound = new NBTTagCompound();
		nbttagcompound.setShort("id", (short)Enchantment.getEnchantmentID(ench));
		nbttagcompound.setShort("lvl", level);
		nbttaglist.appendTag(nbttagcompound);
	}

	public static void addEnchants(RegistryNamespaced<ResourceLocation, Enchantment> ench, short level, ItemStack itemStack) {
		for (Enchantment e : ench) {
			if (e != null) addEnchant(e, level, itemStack);
		}
	}

	public static NetworkPlayerInfo getPlayerInfo(EntityPlayer entityPlayer) {
		return mc.thePlayer.sendQueue.getPlayerInfo(entityPlayer.getUniqueID());
	}

	public static Material getMaterial(BlockPos pos) {
		return mc.theWorld.getBlockState(pos).getMaterial();
	}

	public static void swingItem() {
		mc.thePlayer.swingArm(EnumHand.MAIN_HAND);
	}

	public static IBlockState getState(BlockPos pos) {
		return mc.theWorld.getBlockState(pos);
	}
}
