package net.halalaboos.huzuni.hook;


import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.NameProtect;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.util.ResourceLocation;

public class FontRendererHook extends FontRenderer {

	public FontRendererHook(GameSettings gameSettings,
			ResourceLocation resourceLocation,
			TextureManager textureManager, boolean unicodeFlag) {
		super(gameSettings, resourceLocation, textureManager, unicodeFlag);
		
	}

	@Override
	public int drawString(String text, float x, float y, int color, boolean shadow) {
		if (text.length() > 2000 && Huzuni.isEnabled())
			text = "[Huzuni has blocked this text. It was " + text.length() + " characters.]";
		if (Huzuni.isEnabled() && Huzuni.isCustomFont()) {
			GlStateManager.enableAlpha();
			int width = (int) (shadow ? Huzuni.drawStringWithShadow(text, x, y, color) : Huzuni.drawString(text, x, y, color));
			return width;
		} else
			return super.drawString(text, x, y, color, shadow);
	}
	
	@Override
	public int getStringWidth(String text) {
		if (text.length() > 2000 && Huzuni.isEnabled())
			text = "[Huzuni has blocked this text. It was " + text.length() + " characters.]";
		if (Huzuni.isEnabled() && Huzuni.isCustomFont())
			return Huzuni.getStringWidth(text);
		else
			return super.getStringWidth(text);
    }
}
