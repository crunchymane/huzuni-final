/**
 *
 */
package net.halalaboos.lib.ui.layout;

import net.halalaboos.lib.ui.Component;
import net.halalaboos.lib.ui.Container;

import java.awt.Rectangle;
import java.util.List;

/**
 * @author Halalaboos
 * @since Aug 27, 2013
 */
public interface Layout {

    public Rectangle layoutComponents(Container container, List<Component> components);

}
