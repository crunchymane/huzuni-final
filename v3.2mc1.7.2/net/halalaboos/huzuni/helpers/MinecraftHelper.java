package net.halalaboos.huzuni.helpers;

import net.minecraft.client.Minecraft;

/**
 * Simple 'MinecraftHelper' interface. Should really be implemented by utility classes.
 */
public interface MinecraftHelper extends Helper {
    Minecraft mc = Minecraft.getMinecraft();

}
