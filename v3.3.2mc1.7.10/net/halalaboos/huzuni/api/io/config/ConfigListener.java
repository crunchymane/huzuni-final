/**
 * 
 */
package net.halalaboos.huzuni.api.io.config;

/**
 * @author Halalaboos
 *
 */
public interface ConfigListener <ID, VALUE> {

	public void onConfigUpdate(ID id, VALUE value);
	
	public void onClear();

}
