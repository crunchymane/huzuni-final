/**
 * 
 */
package net.halalaboos.lib.ui.layout;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.List;

import net.halalaboos.lib.ui.Component;
import net.halalaboos.lib.ui.Container;

/**
 * @author Halalaboos
 *
 * @since Oct 6, 2013
 */
public class FittingLayout implements Layout {
    private final int COMPONENT_PADDING = 1;

    /**
     * @see net.halalaboos.lib.ui.layout.Layout#layoutComponent(net.halalaboos.lib.ui.Container, net.halalaboos.lib.ui.Component, java.util.List)
     */
    @Override
    public Rectangle layoutComponents(Container container, List<Component> components) {
        int biggestWidth = container.getArea().width;
        int biggestHeight = container.getArea().height / components.size();
        
    	Point lastComponent = new Point(COMPONENT_PADDING, COMPONENT_PADDING);
    	
        for (Component component : components) {
        	Dimension componentSize = component.getDimensions();
            if (biggestWidth < componentSize.width) {
                biggestWidth = componentSize.width;
            }
        }
        
        
        for (Component component : components) {     	
        	Dimension componentSize = component.getDimensions();
        	component.setArea(new Rectangle(COMPONENT_PADDING, lastComponent.y, biggestWidth - COMPONENT_PADDING, (biggestHeight >= componentSize.height ? biggestHeight : componentSize.height)));
        	Rectangle componentArea = component.getArea();
        	lastComponent.y += (biggestHeight >= componentSize.height ? biggestHeight : componentSize.height) + COMPONENT_PADDING;
        }
        return new Rectangle(0, 0, biggestWidth + COMPONENT_PADDING, lastComponent.y);
    }

}
