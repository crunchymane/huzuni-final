package net.halalaboos.huzuni.mods.player;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;
import com.darkmagician6.eventapi.types.Priority;

import net.halalaboos.huzuni.events.game.EventPreMotionUpdate;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.DefaultModule;

import java.util.Random;

public class Retard extends DefaultModule implements Listener {
    private final Random random = new Random();

    public Retard() {
        super("Retard", -1);
        setCategory(Category.PLAYER);
        setDescription("Look as if you were brendan on a lazy sunday afternoon.");
    }

    @Override
    protected void onEnable() {
        registry.registerListener(this);
    }

    @Override
    protected void onDisable() {
        registry.unregisterListener(this);
    }

    @Override
    protected void onToggle() {
    }

    @EventTarget(Priority.HIGHEST)
    public void onPreMotionUpdate(EventPreMotionUpdate event) {
        float yaw = random.nextBoolean() ? random.nextInt(180)
                : -random.nextInt(180);
        float pitch = random.nextBoolean() ? random.nextInt(90)
                : -random.nextInt(90);
        mc.thePlayer.rotationYaw = yaw;
        mc.thePlayer.rotationPitch = pitch;
        if ((!this.mc.thePlayer.isSprinting())
                && (!this.mc.thePlayer.isUsingItem() || !this.mc.thePlayer.isSwingInProgress || mc.thePlayer.swingProgress < 0)) {
            this.mc.thePlayer.rotationYawHead = yaw;
            if ((!this.mc.thePlayer.isSwingInProgress)
                    && (random.nextBoolean()))
                this.mc.thePlayer.swingItem();
        }
    }
}
