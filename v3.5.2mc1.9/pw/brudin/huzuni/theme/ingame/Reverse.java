package pw.brudin.huzuni.theme.ingame;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.gui.ingame.IngameTheme;
import net.halalaboos.huzuni.mod.DefaultMod;
import net.halalaboos.huzuni.mod.ModManager;
import net.minecraft.client.Minecraft;

/**
 * @since 7:08 PM on 3/19/2015
 */
public class Reverse implements IngameTheme {
	@Override
	public void render(Minecraft mc, int screenWidth, int screenHeight) {
		mc.fontRenderer.drawStringWithShadow(Huzuni.TITLE + "\247r v" + Huzuni.VERSION, 2, 2, 0xFFFFFF);
		int yPos = 2;
		for (DefaultMod Mod : ModManager.getDefaultMods()) {
			if (Mod.isEnabled() && Mod.getVisible()) {
				mc.fontRenderer.drawStringWithShadow(Mod.getRenderName(), screenWidth -
						mc.fontRenderer.getStringWidth(Mod.getRenderName()) - 2, yPos, Mod.getColor().getRGB());
				yPos += 10;
			}
		}
	}

	@Override
	public String getName() {
		return "Reverse";
	}

	@Override
	public void onKeyTyped(int keyCode) {

	}
}
