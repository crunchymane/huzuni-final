package net.halalaboos.huzuni.plugin.plugins.movement;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.plugin.Category;
import net.halalaboos.huzuni.api.plugin.ValueOption;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent;
import net.halalaboos.huzuni.event.tick.UpdateListener;
import net.halalaboos.huzuni.plugin.DefaultPlugin;
import net.halalaboos.huzuni.plugin.value.Value;
import net.halalaboos.huzuni.plugin.value.ValueManager;

public final class Step extends DefaultPlugin implements UpdateListener {

	private final Value height = new Value("Step Height", 1F, 1F, 10F, 0.5F);

	public Step() {
		super("Step");
		setCategory(Category.MOVEMENT);
		setDescription("Makes your step height higher.");
		ValueManager.add(height);
		setVisible(false);
		setOptions(new ValueOption(height));
	}

	@Override
	protected void onEnable() {
		Huzuni.registerUpdateListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterUpdateListener(this);
		if (mc.thePlayer != null)
			mc.thePlayer.stepHeight = 0.5F;
	}

	@Override
	public void onPreUpdate(MotionUpdateEvent event) {
		mc.thePlayer.stepHeight = height.getValue();
	}

	@Override
	public void onPostUpdate(MotionUpdateEvent event) { }
}
