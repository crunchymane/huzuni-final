/**
 *
 */
package net.halalaboos.lib.ui.components;

import net.halalaboos.lib.ui.DefaultComponent;
import net.halalaboos.lib.ui.utils.GLUtils;

import java.awt.*;

/**
 * @author Halalaboos
 * @since Aug 10, 2013
 */
public class Button extends DefaultComponent {
    protected boolean highlight = false;
    protected String title;
    protected Color textColor = Color.WHITE,
            highlightColor = new Color(0F, 0.5F, 1F, 0.75F),
            defaultColor = new Color(0.3F, 0.3F, 0.3F, 0.75F);

    public Button(String title) {
        super(new Dimension(110, 13));
        this.title = title;
        if (GLUtils.getStringWidth(title) > dimensions.width)
            dimensions.width = GLUtils.getStringWidth(title) + 2;
    }

    /**
     * @see net.halalaboos.lib.ui.Component#render(boolean)
     */
    @Override
    public void update(Point mouse) {
    }

    /**
     * @see net.halalaboos.lib.ui.Component#onClicked(java.awt.Point, int)
     */
    @Override
    public boolean onMousePressed(int x, int y, int buttonId) {
    	return mouseOver;
    }

    /**
     * @see net.halalaboos.lib.ui.Component#onReleased(boolean)
     */
    @Override
    public void onMouseReleased(int x, int y, int buttonId) {
    	if (mouseOver) {
            invokeActionListeners();
        }
    }

    /**
     * @see net.halalaboos.lib.ui.Component#onTyped(int)
     */
    @Override
    public void onKeyTyped(int keyCode, char c) {
    }

    public boolean isHighlight() {
        return highlight;
    }

    public void setHighlight(boolean highlight) {
        this.highlight = highlight;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Color getTextColor() {
        return textColor;
    }

    public void setTextColor(Color textColor) {
        this.textColor = textColor;
    }

    public Color getHighlightColor() {
        return highlightColor;
    }

    public void setHighlightColor(Color highlightColor) {
        this.highlightColor = highlightColor;
    }

    public Color getDefaultColor() {
        return defaultColor;
    }

    public void setDefaultColor(Color defaultColor) {
        this.defaultColor = defaultColor;
    }
}
