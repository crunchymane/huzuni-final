package pw.brudin.huzuni.option;

import org.w3c.dom.Element;

import net.halalaboos.huzuni.api.gui.render.Theme;
import net.halalaboos.huzuni.api.mod.Option;
import net.halalaboos.huzuni.util.GLUtils;
import net.halalaboos.huzuni.util.MathUtils;
import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;

/**
 * @since 5:23 PM on 8/11/2015
 */
public class BlockOption extends Option {

	private ItemStack[] items;
	
	private String[] names;
	
	private boolean[] state;

	public BlockOption(Block[] blocks, String[] names, String name, String description) {
		super(name, description);
		this.items = new ItemStack[blocks.length];
		this.names = names;
		this.state = new boolean[blocks.length];
		for (int i = 0; i < blocks.length; i++) {
			items[i] = new ItemStack(blocks[i]);
			state[i] = false;
		}
	}
	
	@Override
	public void mouseReleased(int x, int y, int buttonId) {
		int size = 18;
		int count = 0, yPos = 0, xPos = 0;
		for (int i = 0; i < items.length; i++) {
			if (count >= 5) {
				yPos += size + 1;
				xPos = 0;
				count = 0;
			}
			float[] boxArea = new float[] { area[0] + xPos, area[1] + yPos, size, size };
			if (MathUtils.inside(x, y, boxArea)) {
				state[i] = !state[i];
			}
			xPos += size + 1;
			count++;
		}
	}

	@Override
	public void render(Theme theme, int index, float[] area, boolean mouseOver, boolean mouseDown) {
		int size = 18;
		int count = 0, yPos = 0, xPos = 0;
		for (int i = 0; i < items.length; i++) {
			if (count >= 5) {
				yPos += size + 1;
				xPos = 0;
				count = 0;
			}
			float[] boxArea = new float[] { area[0] + xPos, area[1] + yPos, size, size };
			theme.renderSlot(0, 0, index, boxArea, state[i], mouseOver && MathUtils.inside(GLUtils.getMouseX(), GLUtils.getMouseY(), boxArea), mouseDown);
			GLUtils.draw2dItem(items[i], (int) boxArea[0] + 1, (int) boxArea[1] + 1, 0F);
			xPos += size + 1;
			count++;
		}
	}

	@Override
	public void setArea(float[] area) {
		super.setArea(area);
		area[2] = items.length * (18 + 1);
		area[3] = 18;
	}
	
	public boolean isEnabled(int index) {
		return state[index];
	}

	public void setEnabled(int index, boolean state) {
		this.state[index] = state;
	}
	
	@Override
	public boolean mouseClicked(int x, int y, int buttonId) {
		return this.isPointInside(x, y);
	}

	@Override
	public void load(Element element) {
		for (int i = 0; i < items.length; i++) {
			this.state[i] = Boolean.parseBoolean(element.getAttribute(names[i].replaceAll(" ", "_")));
		}
	}

	@Override
	public void save(Element element) {
		for (int i = 0; i < items.length; i++) {
			element.setAttribute(names[i].replaceAll(" ", "_"), Boolean.toString(state[i]));
		}
	}
}
