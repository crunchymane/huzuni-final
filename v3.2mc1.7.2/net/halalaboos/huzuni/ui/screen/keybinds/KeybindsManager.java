package net.halalaboos.huzuni.ui.screen.keybinds;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.ui.screen.HuzuniButton;
import net.halalaboos.lib.io.FileUtils;
import net.halalaboos.lib.ui.BackgroundContainer;
import net.halalaboos.lib.ui.Container;
import net.halalaboos.lib.ui.DefaultContainer;
import net.halalaboos.lib.ui.theme.DefaultTheme;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.gui.GuiYesNo;

import javax.swing.*;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class KeybindsManager extends GuiScreen {
   
	private final GuiScreen parentGui;
    private GuiTextField field;
    private GuiButton clear;

    private final BackgroundContainer container = new BackgroundContainer(new DefaultTheme());
    
    private final SlotKeybinds slotKeybinds = new SlotKeybinds(this, new Dimension(200, 500));
    
    public KeybindsManager(GuiScreen par1GuiScreen) {
        parentGui = par1GuiScreen;
    	container.add(slotKeybinds);
    }

    @Override
    public void initGui() {
    	int slotWidth = 300,
    	slotHeight = height / 2;
    	slotKeybinds.setArea(new Rectangle(this.width / 2 - slotWidth / 2, this.height / 2 - slotHeight / 2, slotWidth, slotHeight));
    	slotKeybinds.setComponents(Huzuni.modManager.getKeybindableModules());

    	field = new GuiTextField(mc.fontRenderer, width / 2 - 75 - 35, height - 60, 220, 14);
       
    	buttonList.add(new GuiButton(0, this.width / 2 + 2, this.height - 28, 152, 20, "Done"));
        clear = new GuiButton(1, this.width / 2 - 154, this.height - 28, 152, 20, "Clear");
        buttonList.add(clear);
        clear.enabled = slotKeybinds.getSelectedItem() >= 0;
    }

    /**
     * Fired when a control is clicked. This is the equivalent of
     * ActionListener.actionPerformed(ActionEvent e).
     */
    @Override
    protected void actionPerformed(GuiButton par1GuiButton) {
        if (!par1GuiButton.enabled) {
            return;
        }

        switch (par1GuiButton.id) {
            case 0:
                mc.displayGuiScreen(parentGui);
                break;
            case 1:
                if (slotKeybinds.getSelectedItem() >= 0 && slotKeybinds.getComponents().size() > 0)
                	slotKeybinds.clear();
                break;
            default:

            	break;
        }
    }

    public void confirmClicked(boolean confirm, int id) {
        mc.displayGuiScreen(this);
    }

    /**
     * Fired when a key is typed. This is the equivalent of
     * KeyListener.keyTyped(KeyEvent e).
     */
    @Override
    protected void keyTyped(char c, int keyCode) {
        super.keyTyped(c, keyCode);
        slotKeybinds.onKeyTyped(keyCode, c);
        //		field.textboxKeyTyped(par1, par2);
        //
        //		if (field.getText().length() > 0) {
        //			ArrayList l = new ArrayList();
        //			// TODO look through alts and if their name matches 'x'
        //			altList.setList(l);
        //		} else
        //			altList.setList(readAlts());
    }

    /**
     * Called when the mouse is clicked.
     */
    @Override
    protected void mouseClicked(int par1, int par2, int par3) {
        super.mouseClicked(par1, par2, par3);
        container.onMousePressed(par1, par2, par3);
        // field.mouseClicked(par1, par2, par3);
    }

    /**
     * Draws the screen and all the components in it.
     */
    @Override
    public void drawScreen(int par1, int par2, float par3) {
        this.drawDefaultBackground();
        drawCenteredString(fontRenderer, "Keybinds", width / 2, 16, 0xFFFFFF);
        clear.enabled = slotKeybinds.getSelectedItem() >= 0;
        super.drawScreen(par1, par2, par3);
        container.render(par1, par2);
        // field.drawTextBox();
    }

    /**
     * Called from the main game loop to update the screen.
     */
    @Override
    public void updateScreen() {
        super.updateScreen();
        // field.updateCursorCounter();
    }

    @Override
    public void onGuiClosed() {
        super.onGuiClosed();
        Huzuni.modManager.save();
    }

	public SlotKeybinds getSlotAlts() {
		return slotKeybinds;
	}
}
