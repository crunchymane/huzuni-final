package net.halalaboos.huzuni.plugin.plugins.combat;

import net.minecraft.item.ItemSword;
import org.lwjgl.input.Keyboard;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.plugin.Category;
import net.halalaboos.huzuni.api.plugin.ToggleOption;
import net.halalaboos.huzuni.api.plugin.ValueOption;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent;
import net.halalaboos.huzuni.event.tick.UpdateListener;
import net.halalaboos.huzuni.plugin.DefaultPlugin;
import net.halalaboos.huzuni.plugin.value.Value;
import net.halalaboos.huzuni.plugin.value.ValueManager;
import net.halalaboos.huzuni.util.Timer;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.IMob;
import net.minecraft.entity.passive.IAnimals;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.MathHelper;

public class KillAura extends DefaultPlugin implements UpdateListener {

	private float lastYaw, lastPitch;
	
	private EntityLivingBase entity;
	
	private final Timer timer = new Timer();
	
	private final Value speed = new Value("Kill Speed", 1F, 8F, 15F);
	private final Value reach = new Value("Kill Reach", 3F, 3.8F, 6F);
    private final Value yawRate = new Value("Kill Radius", 10F, 25F, 180F, 1F);
    
	private final ToggleOption silent  = new ToggleOption("Silent"), 
			players = new ToggleOption("Players"), 
			mobs = new ToggleOption("Mobs"), 
			animals = new ToggleOption("Animals"),
			trigger  = new ToggleOption("Triggerbot"),
			block = new ToggleOption("Auto Block");

	public KillAura() {
		super("Kill Aura", Keyboard.KEY_R);
		setAuthor("Halalaboos");
		setDescription("Attacks entities around you.");
		ValueManager.add(speed);
		ValueManager.add(reach);
		ValueManager.add(yawRate);
		setCategory(Category.COMBAT);
		setOptions(new ValueOption(speed), new ValueOption(reach), new ValueOption(yawRate), silent, players, mobs, animals, trigger, block);
		players.setEnabled(true);
		silent.setEnabled(true);
	}

	@Override
	public void onPreUpdate(MotionUpdateEvent event) {
		lastYaw = mc.thePlayer.rotationYaw;
		lastPitch = mc.thePlayer.rotationPitch;
		if (trigger.isEnabled()) {
			if (mc.objectMouseOver != null) {
				Entity entity = mc.objectMouseOver.entityHit;
				if (entity instanceof EntityLivingBase && checkType((EntityLivingBase) entity) && isWithinDistance((EntityLivingBase) entity, reach.getValue())) {
					this.entity = (EntityLivingBase) entity;
					float[] rotations = getRotationsNeeded(this.entity);
			        if (rotations != null) {
			        	mc.thePlayer.rotationYaw = rotations[0];
			           	mc.thePlayer.rotationPitch = rotations[1];
			        } else
			        	this.entity = null;
				} else
					this.entity = null;
			} else
				this.entity = null;
			return;
		}
		entity = getClosestEntity(reach.getValue(), 2.5F);
		if (entity != null) {
			float[] rotations = getRotationsNeeded(entity);
	        if (rotations != null && Math.abs(mc.thePlayer.rotationYaw - rotations[0]) <= yawRate.getValue()) {
	        	mc.thePlayer.rotationYaw = rotations[0];
	           	mc.thePlayer.rotationPitch = rotations[1];
	        } else
	        	entity = null;
		}
	}

	@Override
	public void onPostUpdate(MotionUpdateEvent event) {
		if (entity != null && isWithinDistance(entity, reach.getValue())) {
			if (timer.hasReach((int) (1000F / speed.getValue()))) {
				if(hasSword() && block.isEnabled()) {
					mc.thePlayer.getCurrentEquippedItem().useItemRightClick(mc.theWorld, mc.thePlayer);
				}
				mc.thePlayer.swingItem();
				mc.playerController.attackEntity(mc.thePlayer, entity);
				timer.reset();
			}
		}
		if (silent.isEnabled()) {
			mc.thePlayer.rotationYaw = lastYaw;
			mc.thePlayer.rotationPitch = lastPitch;
		}
	}

	private boolean hasSword() {
		if(mc.thePlayer.inventory.getCurrentItem() != null) {
			if(mc.thePlayer.inventory.getCurrentItem().getItem() instanceof ItemSword) {
				return true;
			}
		}
		return false;
	}

	@Override
	protected void onEnable() {
		Huzuni.registerUpdateListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterUpdateListener(this);
	}
	
	private EntityLivingBase getClosestEntity(float distance, float extender) {
		EntityLivingBase currentEntity = null;
		for (int i = 0; i < mc.theWorld.loadedEntityList.size(); i++) {
    		if (mc.theWorld.loadedEntityList.get(i) instanceof EntityLivingBase) {
    			EntityLivingBase entity = (EntityLivingBase) mc.theWorld.loadedEntityList.get(i);
    			if (isAliveNotUs(entity) && checkType(entity)) {
    				if (entity instanceof EntityPlayer && Huzuni.isFriend(entity.getCommandSenderName()))
    					continue;
    				if (currentEntity != null) {
    	                if (isClosestToMouse(currentEntity, entity, distance, extender) && isWithinDistance(entity, distance + extender))
        					currentEntity = entity;
    				} else {
    					if (isWithinDistance(entity, distance + extender))
    						currentEntity = entity;
    				}
    			}
    		}
    	}
		return currentEntity;
	}
	
	 /**
     * @return the closest entity to your mouse.
     */
    private boolean isClosestToMouse(EntityLivingBase currentEntity,
                                   EntityLivingBase otherEntity, float distance, float extender) {
        // If we can't reach the other entity, even with our extender, don't return true.
        if (!isWithinDistance(otherEntity, distance + extender))
            return false;

        // If we can't reach our current entity without the extender, but we CAN with our OTHER entity, return true.
        if (!isWithinDistance(currentEntity, distance) && isWithinDistance(otherEntity, distance))
            return true;

        float otherDist = getDistanceFromMouse(otherEntity),
        		currentDist = getDistanceFromMouse(currentEntity);
        return (otherDist < currentDist || currentDist == -1) && otherDist != -1;
    }
	
    private boolean isWithinDistance(EntityLivingBase entity, float distance) {
        return mc.thePlayer.getDistanceToEntity(entity) < distance;
    }

    private boolean isAliveNotUs(EntityLivingBase entity) {
		return (entity == null) ? false : (entity.isEntityAlive() && entity != mc.thePlayer && entity.getAge() != 7);
	}
	
	private boolean checkType(EntityLivingBase entity) {
    	if (mobs.isEnabled() && entity instanceof IMob)
    		return true;
    	if (animals.isEnabled() && entity instanceof IAnimals)
    		return true;
    	if (players.isEnabled() && entity instanceof EntityPlayer)
    		return true;
    	return false;
    }
	
	/**
     * @return Distance the entity is from our mouse.
     */
    private float getDistanceFromMouse(EntityLivingBase entity) {
        float[] neededRotations = getRotationsNeeded(entity);
        if (neededRotations != null) {
            float neededYaw = mc.thePlayer.rotationYaw - neededRotations[0],
                    neededPitch = mc.thePlayer.rotationPitch - neededRotations[1];
            float distanceFromMouse = MathHelper.sqrt_float(neededYaw * neededYaw + neededPitch * neededPitch);
            return distanceFromMouse;
        }
        return -1F;
    }

    /**
     * @return Rotations needed to face the entity.
     */
	private float[] getRotationsNeeded(EntityLivingBase entity) {
        if (entity == null)
            return null;
        
        double xSize = entity.posX - mc.thePlayer.posX;
        double ySize = entity.posY + (double) entity.getEyeHeight() - (mc.thePlayer.posY + (double) mc.thePlayer.getEyeHeight());
        double zSize = entity.posZ - mc.thePlayer.posZ;
        
        double theta = (double) MathHelper.sqrt_double(xSize * xSize + zSize * zSize);
        float yaw = (float) (Math.atan2(zSize, xSize) * 180.0D / Math.PI) - 90.0F;
        float pitch = (float) (-(Math.atan2(ySize, theta) * 180.0D / Math.PI));
        return new float[] {
                mc.thePlayer.rotationYaw + MathHelper.wrapAngleTo180_float(yaw - mc.thePlayer.rotationYaw),
                mc.thePlayer.rotationPitch + MathHelper.wrapAngleTo180_float(pitch - mc.thePlayer.rotationPitch)
        };
	}

}
