package net.halalaboos.huzuni.mod.mods.unfinished;

import static org.lwjgl.opengl.GL11.*;

import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import org.lwjgl.util.glu.Cylinder;
import org.lwjgl.util.glu.GLU;
import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.mod.Category;
import net.halalaboos.huzuni.event.render.RenderEvent;
import net.halalaboos.huzuni.event.render.Renderable;
import net.halalaboos.huzuni.mod.DefaultMod;
import net.halalaboos.huzuni.util.GLUtils;

public class JumpPrediction extends DefaultMod implements Renderable {
	
	private final Cylinder cylinder = new Cylinder();
    
	private boolean hasJumped = false;
	
	public JumpPrediction() {
		super("Jump Prediction");
		setCategory(Category.UNFINISHED);
		setDescription("Predicts your landing position when jumping (UNFINISHED)");
	}

	@Override
	public void render(RenderEvent event) {
    	if (!mc.thePlayer.onGround || mc.thePlayer.moveForward == 0) {
    		return;
    	}
        glBegin(GL_LINE_STRIP);
        GLUtils.glColor(1F, 0F, 0F, 1F);
        float yaw = mc.thePlayer.rotationYaw * 0.017453292F;
        double x = mc.getRenderManager().renderPosX - (double) (MathHelper.cos(yaw) * 0.16F),
                y = (mc.getRenderManager().renderPosY),
                z = mc.getRenderManager().renderPosZ - (double) (MathHelper.sin(yaw) * 0.16F),
                motionX = mc.thePlayer.isJumping ? mc.thePlayer.motionX : mc.thePlayer.motionX - (mc.thePlayer.isSprinting() ? (MathHelper.sin(yaw) * 0.2F) : 0),
                motionY = mc.thePlayer.isJumping ? mc.thePlayer.motionY : 0.42F,
                motionZ = mc.thePlayer.isJumping ? mc.thePlayer.motionZ : mc.thePlayer.motionZ + (mc.thePlayer.isSprinting() ? (MathHelper.cos(yaw) * 0.2F) : 0);
        boolean justthetip = false;
        float count = 0;
        while ((Math.abs(motionX) > 0.005D || Math.abs(motionY) > 0.005D || Math.abs(motionZ) > 0.005D) && count < 100) {
            Vec3d present = Vec3d.createVectorHelper(x, y, z);
            Vec3d future = Vec3d.createVectorHelper(x + motionX, y + motionY, z + motionZ);
            RayTraceResult possibleLandingStrip = mc.theWorld.rayTraceBlocks(present, future, false, true, false);
            if (possibleLandingStrip != null) {
            	justthetip = true;
               	break;
            }
        	x += motionX;
        	y += motionY;
        	z += motionZ;
        	motionY *= 0.9800000190734863D;
        	motionY -= 0.08D;
            glVertex3d(x - mc.getRenderManager().renderPosX, y - mc.getRenderManager().renderPosY, z - mc.getRenderManager().renderPosZ);
            count++;
        }
        glEnd();
        if (justthetip) {
        	if (mc.thePlayer.onGround && (y - mc.getRenderManager().renderPosY > 1 || mc.thePlayer.isCollidedHorizontally)) {
        		if (mc.thePlayer.onGround) {
        			mc.thePlayer.jump();
//                	System.out.println("jumping!");
                	hasJumped = true;
        		}
        	}
	    	glPushMatrix();
	    	glTranslated(x - mc.getRenderManager().renderPosX, y - mc.getRenderManager().renderPosY, z - mc.getRenderManager().renderPosZ);
	    	renderPoint();
		    glPopMatrix();
        }
    }
	
	private void renderPoint() {
		float size = 0.1F;
		glBegin(GL_LINES);
	    glVertex3d(-size, 0, 0);
	    glVertex3d(0, 0, 0);
	    glVertex3d(0, 0, -size);
	    glVertex3d(0, 0, 0);
	
	    glVertex3d(size, 0, 0);
	    glVertex3d(0, 0, 0);
	    glVertex3d(0, 0, size);
	    glVertex3d(0, 0, 0);
	    glEnd();
	
	    glRotatef(-90, 1, 0, 0);
	    cylinder.setDrawStyle(GLU.GLU_LINE);
	    cylinder.draw(size, size, size * 0.1F, 24, 1);
    }

	@Override
	protected void onEnable() {
		Huzuni.registerRenderable(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterRenderable(this);
	}

}
