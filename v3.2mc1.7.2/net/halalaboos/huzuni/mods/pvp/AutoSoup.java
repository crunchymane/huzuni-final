package net.halalaboos.huzuni.mods.pvp;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;
import com.darkmagician6.eventapi.types.Priority;

import net.halalaboos.huzuni.events.game.EventHealthChange;
import net.halalaboos.huzuni.events.game.EventTick;
import net.halalaboos.huzuni.helpers.Helper;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.DefaultModule;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class AutoSoup extends DefaultModule implements Listener, Helper {
	private static final int BOWL_SOUP = 282, BOWL_SOUP_EMPTY = 281;
	
    private int oldItem = -1;

    public AutoSoup() {
        super("AutoSoup", -1);
        setCategory(Category.PVP);
        setDescription("Instantly eats soup for you. Primarily for KitPvP");
    }

    @Override
    protected void onEnable() {
        registry.registerListener(this, EventHealthChange.class);
    }

    @Override
    protected void onDisable() {
        registry.unregisterListener(this, EventHealthChange.class);
    }

    @Override
    protected void onToggle() {
    }

    @EventTarget(Priority.HIGHEST)
    public void onHealthChange(EventHealthChange event) {

        if (event.health < 14)
            registry.registerListener(this, EventTick.class);
        else {
            registry.unregisterListener(this, EventTick.class);
            // Reset our item.
            if (oldItem != -1) {
                mc.playerController.onStoppedUsingItem(mc.thePlayer);

            	if (oldItem != mc.thePlayer.inventory.currentItem)
            		swap(oldItem);
            	
                oldItem = -1;
            }
        }

    }

    @EventTarget(Priority.HIGHEST)
    public void onTick(EventTick event) {
        // Find a soup within the hotbar
        int soupIndex = inventoryHelper.findHotbarItem(BOWL_SOUP);
        if (soupIndex != -1) {
            // Set our old item to our current item (prior to switching to the soup).
        	if (oldItem == -1)
        		oldItem = mc.thePlayer.inventory.currentItem;
            // Switch to the soup
            swap(soupIndex);
            // Use the soup (also updates the server of us swapping to the soup)
            useItem(mc.thePlayer.inventory.getStackInSlot(soupIndex));
            // Unregister, cause we've sent the server to eat our soup
            registry.unregisterListener(this, EventTick.class);
            return;
        } else {
            // Find an available slot within our inventory for this empty bowl.
            int avSlot = inventoryHelper.findAvailableSlotInventory(BOWL_SOUP_EMPTY);
            // Find the replacement soup for our hotbar.
            int replacementSoup = inventoryHelper.findInventoryItem(BOWL_SOUP);

            // If we found BOTH
            if (replacementSoup != -1 && avSlot != -1) {
                ItemStack badItem = mc.thePlayer.inventoryContainer.getSlot(avSlot).getStack();
                if (isShiftable(badItem)) {
                    // Shift click the soup (so they start stacking on eachother)
                    clickSlot(avSlot, true);
                    clickSlot(replacementSoup, true);
                } else {
                    // Otherwise just manually click them.
                    clickSlot(replacementSoup, false);
                    clickSlot(avSlot, false);
                }
            }
            return;
        }
    }

    @Override
    public String getRenderName() {
        return super.getRenderName() + " [" + getSoupCount() + " Soup]";
    }

    private int getSoupCount() {
        return inventoryHelper.getItemCountInventory(BOWL_SOUP) + inventoryHelper.getItemCountHotbar(BOWL_SOUP);
    }

    /**
     * @return True if the item is shift clickable.
     */
    private boolean isShiftable(ItemStack preferedItem) {
        if (preferedItem == null)
            return true;
        for (int o = 9; o < 36; o++) {
            if (mc.thePlayer.inventoryContainer.getSlot(o).getHasStack()) {
                ItemStack item = mc.thePlayer.inventoryContainer.getSlot(o).getStack();
                if (item == null)
                    return true;
                else if (Item.func_150891_b(item.getItem()) == Item.func_150891_b(preferedItem.getItem())) {
                    if (item.stackSize + preferedItem.stackSize <= preferedItem.getMaxStackSize())
                        return true;
                }

            } else
                return true;
        }
        return false;
    }

    private void swap(int soupIndex) {
        mc.thePlayer.inventory.currentItem = soupIndex;
    }

    private void useItem(ItemStack item) {
        mc.playerController.sendUseItem(mc.thePlayer, mc.theWorld, item);
    }

    private void clickSlot(int slot, boolean shiftClick) {
        mc.playerController.windowClick(
                mc.thePlayer.inventoryContainer.windowId, slot, 0, shiftClick ? 1 : 0,
                mc.thePlayer);
    }

}