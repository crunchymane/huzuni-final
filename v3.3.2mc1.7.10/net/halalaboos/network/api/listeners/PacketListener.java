package net.halalaboos.network.api.listeners;

import net.halalaboos.network.api.Packet;


public interface PacketListener {

	public void onRecievedPacket(Packet packet, Object data);
	
}
