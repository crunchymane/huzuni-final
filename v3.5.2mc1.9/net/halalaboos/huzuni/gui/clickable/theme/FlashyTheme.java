package net.halalaboos.huzuni.gui.clickable.theme;

import java.awt.Color;

import net.halalaboos.huzuni.api.gui.Component;
import net.halalaboos.huzuni.gui.clickable.button.Button;
import net.halalaboos.huzuni.util.GLUtils;
import net.halalaboos.huzuni.util.OldGLUtils;
import net.halalaboos.huzuni.util.TimedInterpolator;

public class FlashyTheme extends DefaultTheme {

	private TimedInterpolator frameColor = new TimedInterpolator(2000), backgroundColor = new TimedInterpolator(2000),
			buttonColor = new TimedInterpolator(2000);
	
	public FlashyTheme() {
		super();
		this.frameColor.setAlpha(0.6F);
		this.backgroundColor.setAlpha(0.6F);
		this.buttonColor.setAlpha(0.6F);
	}
	
	@Override
	public void renderBackground(int screenWidth, int screenHeight) {
		frameColor.update(true, 0.01F);
		backgroundColor.update(true, 0.01F);
		buttonColor.update(true, 0.01F);
	}
	
	@Override
	public void renderComponent(float xOffset, float yOffset, Component component) {
		super.renderComponent(xOffset, yOffset, component);
	}
	
	@Override
	public String getName() {
		return "Flashy";
	}

	@Override
	protected int getButtonTextColor(Button button) {
		return 0xFFFFFF;
	}

	@Override
	protected int getWindowTextColor() {
		return 0xFFFFFF;
	}

	@Override
	protected int getTooltipTextColor() {
		return 0xFFFFFF;
	}

	@Override
	protected void renderButtonRect(float x, float y, float x1, float y1, boolean highlight, boolean mouseOver, boolean mouseDown) {
		GLUtils.drawBorder(x, y, x1, y1, 0.5F, 0xFF000000);
		Color color = new Color(buttonColor.getRed(), buttonColor.getGreen(), buttonColor.getBlue(), buttonColor.getAlpha());
		GLUtils.glColor(GLUtils.getColorWithAffects(highlight ? color.brighter() : color, mouseOver, mouseDown));
		GLUtils.drawRect(x, y, x1, y1);
	}

	@Override
	protected void renderBackgroundRect(float x, float y, float x1, float y1, boolean highlight, boolean mouseOver, boolean mouseDown) {
		GLUtils.glColor(backgroundColor.getRed(), backgroundColor.getGreen(), backgroundColor.getBlue(), backgroundColor.getAlpha());
		GLUtils.drawRect(x, y, x1, y1);
	}

	@Override
	protected void renderWindowRect(float x, float y, float x1, float y1, float tabHeight, float lineWidth) {
		OldGLUtils.drawBorder(x, y, x1, y1, lineWidth, 0xFF000000);
		GLUtils.glColor(frameColor.getRed(), frameColor.getGreen(), frameColor.getBlue(), frameColor.getAlpha());
		GLUtils.drawRect(x, y, x1, y1);
	}


	@Override
	protected void renderDropdownBackgroundRect(float x, float y, float x1,
			float y1, boolean highlight, boolean mouseOver, boolean mouseDown) {
		this.renderBackgroundRect(x, y, x1, y1, highlight, mouseOver, mouseDown);
	}
	
}
