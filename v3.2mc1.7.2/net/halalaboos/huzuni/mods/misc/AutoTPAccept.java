package net.halalaboos.huzuni.mods.misc;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;
import com.darkmagician6.eventapi.types.Priority;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.events.game.EventRecieveMessage;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.DefaultModule;
import net.halalaboos.huzuni.mods.Module;

public class AutoTPAccept extends Module implements Listener {

    public AutoTPAccept() {
        super("Auto TPA Accept", "brudin");
        setCategory(Category.MISC);
        setEnabled(true);
        this.setDescription("Automatically accepts /TPA requests from friends.");
    }

    @Override
    protected void onEnable() {
        registry.registerListener(this);
    }

    @Override
    protected void onDisable() {
        registry.unregisterListener(this);
    }

    @EventTarget(Priority.HIGH)
    public void onRecieveMessage(EventRecieveMessage event) {
        if (event.getMessage().contains("has requested to teleport to you")) {
            for (String friend : Huzuni.friendManager.getList()) {
                if (event.getMessage().toLowerCase().contains(friend.toLowerCase())) {
                    mc.thePlayer.sendChatMessage("/tpaccept");
                    break;
                }
            }
        }
    }
}
