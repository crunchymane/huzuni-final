package net.halalaboos.huzuni.gui.xray;

import java.util.ArrayList;
import java.util.List;

import net.halalaboos.huzuni.api.gui.Component;
import net.halalaboos.huzuni.api.gui.event.KeyListener;
import net.halalaboos.huzuni.gui.clickable.slot.SlotComponent;
import net.halalaboos.huzuni.gui.clickable.textfield.TextField;
import net.halalaboos.huzuni.gui.clickable.window.Window;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class XrayWindow extends Window implements KeyListener {
    
	private final SlotComponent<BlockInfo[]> slotBlocks;
   
    private final TextField textField;
    
    private List<BlockInfo> totalBlocks = new ArrayList<BlockInfo>();
    
	public XrayWindow() {
		super("Xray Manager");
		textField = new TextField();
        textField.addKeyListener(this);
        textField.setTooltip("Search for blocks.");
        add(textField);
        totalBlocks = setupTotalBlocks();
        slotBlocks = new SlotBlocks(136, 68);
        slotBlocks.setComponents(setupBlocks());
        add(slotBlocks);
        this.layout();
	}
	
	/**
     * Finds blocks and sets up a list with an array of itemstacks in it.
     * */
    private List<BlockInfo[]> searchBlock(String blockName) {
        List<BlockInfo[]> goodBlocks = new ArrayList<BlockInfo[]>();
        List<BlockInfo> tempBlocks = new ArrayList<BlockInfo>();
        for (BlockInfo blockInfo : totalBlocks) {
        	if (blockInfo.getName().contains(blockName.toLowerCase())) {
        		tempBlocks.add(blockInfo);
        		if (tempBlocks.size() >= 6) {
        			goodBlocks.add(tempBlocks.toArray(new BlockInfo[tempBlocks.size()]));
        			tempBlocks.clear();
        		}
        	}
    	 }
    	 if (!tempBlocks.isEmpty())
    		 goodBlocks.add(tempBlocks.toArray(new BlockInfo[tempBlocks.size()]));
    	 tempBlocks.clear();
    	 return goodBlocks;
    }
    
    /**
     * Sets up all blocks for the x-ray window.
	 * @return
	 */
	private List<BlockInfo[]> setupBlocks() {
        List<BlockInfo[]> goodBlocks = new ArrayList<BlockInfo[]>();
        List<BlockInfo> tempBlocks = new ArrayList<BlockInfo>();
        for (BlockInfo blockInfo : totalBlocks) {
        	tempBlocks.add(blockInfo);
        	if (tempBlocks.size() >= 6) {
        		goodBlocks.add(tempBlocks.toArray(new BlockInfo[tempBlocks.size()]));
        		tempBlocks.clear();
        	}
        	
        }

        if (!tempBlocks.isEmpty())
        	goodBlocks.add(tempBlocks.toArray(new BlockInfo[tempBlocks.size()]));
        tempBlocks.clear();
        return goodBlocks;
	}
	
	private List<BlockInfo> setupTotalBlocks() {
        List<BlockInfo> blocks = new ArrayList<BlockInfo>();
       
        blocks.add(new BlockInfo(new ItemStack(Items.water_bucket), new Block[] { Blocks.water, Blocks.flowing_water }));
        blocks.add(new BlockInfo(new ItemStack(Items.lava_bucket), new Block[] { Blocks.lava, Blocks.flowing_lava }));
        blocks.add(new BlockInfo(new ItemStack(Blocks.redstone_ore), new Block[] { Blocks.redstone_ore, Blocks.lit_redstone_ore }));

        for (Object o : Block.blockRegistry) {
        	Block block = (Block) o;
        	if (block == Blocks.redstone_ore)
        		continue;
        	if (block != null && Item.getItemFromBlock(block) != null) {
        		blocks.add(new BlockInfo(new ItemStack(block), new Block[] { block }));
        	}
        }
        
        return blocks;
	}

	@Override
	public void keyTyped(Component component, int keyCode, char c) {
		if (textField.getText().length() > 0) {
			slotBlocks.setComponents(searchBlock(textField.getText()));
		} else {
    		slotBlocks.setComponents(setupBlocks());
		}
	}
}
