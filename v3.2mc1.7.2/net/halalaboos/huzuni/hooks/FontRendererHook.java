/**
 *
 */
package net.halalaboos.huzuni.hooks;


import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.client.Settings;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.util.ResourceLocation;


/**
 * @author Halalaboos
 * @since Jul 25, 2013
 */
public class FontRendererHook extends FontRenderer {
    
	/**
     * @param gameSettings
     * @param resourceLocation
     * @param textureManager
     * @param unicodeFlag
     */
    public FontRendererHook(GameSettings gameSettings,
                            ResourceLocation resourceLocation,
                            TextureManager textureManager, boolean unicodeFlag) {
        super(gameSettings, resourceLocation, textureManager, unicodeFlag);
    }

    @Override
    public int drawStringWithShadow(String text, int x, int y, int color) {
    	if (text.length() > 1000 && Settings.isCensorLongMessages())
    		text = "[Huzuni has blocked this text. It was " + text.length() + " characters.]";
    	if (Settings.isGlobalFont()) {
            return (int) Huzuni.display.fontRenderer.drawStringWithShadow(text, x, y, color);
        } else {
        	return super.drawStringWithShadow(text, x, y, color);
        }
    }

    @Override
    public int drawString(String text, int x, int y, int color) {
    	if (text.length() > 1000 && Settings.isCensorLongMessages())
    		text = "[Huzuni has blocked this text. It was " + text.length() + " characters.]";
    	if (Settings.isGlobalFont()) {
            return (int) Huzuni.display.fontRenderer.drawString(text, x, y, color);
        } else {
        	return super.drawString(text, x, y, color);
        }
    }

    @Override
    public int getStringWidth(String text) {
    	if (text.length() > 1000 && Settings.isCensorLongMessages())
    		text = "[Huzuni has blocked this text. It was " + text.length() + " characters.]";
    	if (Settings.isGlobalFont()) {
            return Huzuni.display.fontRenderer.getStringWidth(text);
        } else {
        	return super.getStringWidth(text);
        }
    }
    
}
