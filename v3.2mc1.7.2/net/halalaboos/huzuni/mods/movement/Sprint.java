package net.halalaboos.huzuni.mods.movement;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;
import com.darkmagician6.eventapi.types.Priority;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.client.DonatorLoader;
import net.halalaboos.huzuni.events.game.EventPreMotionUpdate;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.DefaultModule;

import net.halalaboos.huzuni.mods.ModuleManager;
import org.lwjgl.input.Keyboard;

public class Sprint extends DefaultModule implements Listener {

	private float speed = 1.4F;
	
    public Sprint() {
        super("Sprint", Keyboard.KEY_M);
        setCategory(Category.MOVEMENT);
        setDescription("Automagically sprints for you.");
    }

    @Override
    protected void onEnable() {
        registry.registerListener(this);
    }

    @Override
    protected void onDisable() {
        mc.thePlayer.setSprinting(false);
        registry.unregisterListener(this);
    }

    @Override
    protected void onToggle() {
    }

    @EventTarget(Priority.HIGHEST)
    public void onPreMotionUpdate(EventPreMotionUpdate event) {
    	mc.thePlayer.setSprinting(canSprint());
    	if (DonatorLoader.isDonator()) {
	    	if (isCtrlDown() && mc.thePlayer.movementInput.moveForward > 0)
	    		mc.timer.timerSpeed = speed;
	    	else
	    		mc.timer.timerSpeed = 1;
    	}

    }

    /**
     * @return True if you'd be allowed to sprint.
     */
    private boolean canSprint() {
        return !mc.thePlayer.isSneaking()
                && mc.thePlayer.movementInput.moveForward > 0 && !mc.thePlayer.isCollidedHorizontally && !mc.thePlayer.isInWater()
                && (mc.thePlayer.getFoodStats().getFoodLevel() > 6 || mc.playerController.isInCreativeMode()) && !Huzuni.modManager.getMod("Freecam").isEnabled();
    }
    
    private boolean isCtrlDown() {
    	return Keyboard.isKeyDown(Keyboard.KEY_LCONTROL) || Keyboard.isKeyDown(Keyboard.KEY_RCONTROL);
    }
    
}
