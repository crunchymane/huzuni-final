package net.halalaboos.huzuni.gui.clickable.window;

import org.lwjgl.input.Mouse;

import net.halalaboos.huzuni.gui.clickable.DefaultContainer;
import net.halalaboos.huzuni.gui.clickable.DefaultLayout;
import net.halalaboos.huzuni.util.GLUtils;

public class Window extends DefaultContainer {

	private String title;
	
	private int tabHeight = 12;
	
	private float lastWindowX, lastWindowY, lastMouseX, lastMouseY;
	
	private boolean dragging = false;
	
	public Window(String title) {
		super(new DefaultLayout());
		this.title = title;
		this.setOffset(new int[] { 0, tabHeight });
	}

	public int getTabHeight() {
		return tabHeight;
	}

	public void setTabHeight(int tabHeight) {
		this.tabHeight = tabHeight;
		this.setOffset(new int[] { 0, tabHeight });
	}
	
	@Override
	public boolean mouseClicked(int x, int y, int buttonId) {
		if (buttonId == 0) {
			boolean insideTab = x > this.getX() && x < this.getX() + this.getWidth() && y > this.getY() && y < this.getY() + this.tabHeight;
			if (insideTab) {
				dragging = true;
				lastWindowX = this.getX();
				lastWindowY = this.getY();
				lastMouseX = x;
				lastMouseY = y;
				return true;
			} else
				return super.mouseClicked(x, y, buttonId);
		} else
			return super.mouseClicked(x, y, buttonId);
	}
	
	@Override
	public void update() {
		super.update();
		handleDragging();
	}
	
	private void handleDragging() {
		if (dragging && Mouse.isButtonDown(0)) {
			this.setX(GLUtils.getMouseX() - (lastMouseX - lastWindowX));
			this.setY(GLUtils.getMouseY() - (lastMouseY - lastWindowY));
		} else {
			dragging = false;
		}
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	@Override
	public String toString() {
		return title;
	}
}
