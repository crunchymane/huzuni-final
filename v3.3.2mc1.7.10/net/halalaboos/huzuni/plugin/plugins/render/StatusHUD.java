package net.halalaboos.huzuni.plugin.plugins.render;

import net.halalaboos.huzuni.api.plugin.Category;
import net.halalaboos.huzuni.api.plugin.ValueOption;
import net.halalaboos.huzuni.plugin.DefaultPlugin;
import net.halalaboos.huzuni.plugin.value.Value;
import net.halalaboos.huzuni.plugin.value.ValueManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.resources.I18n;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import java.util.Collection;
import static org.lwjgl.opengl.GL11.*;

/**
 * @author brudin
 * @version 1.0
 * @since 1:11 AM on 8/20/2014
 */
public class StatusHUD extends DefaultPlugin {

	public static final StatusHUD instance = new StatusHUD();
	
	private ResourceLocation inventory = new ResourceLocation("textures/gui/container/inventory.png");
	
	private final RenderItem itemRenderer = new RenderItem();
	
	private final Value hudScale = new Value("Hud Scale", 0.5F, 1F, 2F, 0.5F);
		
	private StatusHUD() {
		super("Status HUD");
		setAuthor("Halalaboos");
		setDescription("Renders active potion effects and armor durability.");
		setCategory(Category.RENDER);
		setVisible(false);
		ValueManager.add(hudScale);
		setOptions(new ValueOption(hudScale));
	}

	@Override
	protected void onEnable() { }

	@Override
	protected void onDisable() { }

	public void renderHUD(int screenWidth, int screenHeight) {
		int left = screenWidth / 2 - 110;
		int right = left + 9 * 20 + 20;
		int top = screenHeight - 39;
		int bottom = top + 20;
		renderItemStack(getWearingArmor(0).getStack(), left, top);
		renderItemStack(getWearingArmor(1).getStack(), left, bottom);
		renderItemStack(getWearingArmor(2).getStack(), right, top);
		renderItemStack(getWearingArmor(3).getStack(), right, bottom);
        float scale = hudScale.getValue(), inverse = 1F / scale, iconSize = 20 * scale;
        int renderY = (int) (screenHeight - iconSize / 2 - (2 * scale)), ySeparator = (int) (Math.ceil(4 * scale));
        for (PotionEffect potionEffect : (Collection<PotionEffect>) mc.thePlayer.getActivePotionEffects()) {
            Potion potion = Potion.potionTypes[potionEffect.getPotionID()];
            String amplifier = "";
            if (potionEffect.getAmplifier() == 1)
                amplifier = " II";
            else if (potionEffect.getAmplifier() == 2)
                amplifier = " III";
            else if (potionEffect.getAmplifier() == 3)
                amplifier = " IV";
            
            String renderText = I18n.format(potionEffect.getEffectName()) + " " + amplifier;

            glScalef(scale, scale, scale);
            if (potion.hasStatusIcon()) {
                int iconIndex = potion.getStatusIconIndex();
                mc.getTextureManager().bindTexture(inventory);
                GL11.glColor3f(1, 1, 1);
                renderTexture(256, 256, (screenWidth - iconSize) * inverse, (renderY - ySeparator - (2 * scale)) * inverse, 18, 18, 0 + iconIndex % 8 * 18, 198 + iconIndex / 8 * 18, 18, 18);
            }

            mc.fontRenderer.drawStringWithShadow(renderText, (int) ((screenWidth - (mc.fontRenderer.getStringWidth(renderText) * scale) - (potion.hasStatusIcon() ? iconSize : 2)) * inverse), (int) ((renderY - ySeparator) * inverse), 0xFFFFFF);
            mc.fontRenderer.drawStringWithShadow(Potion.getDurationString(potionEffect), (int) ((screenWidth - (mc.fontRenderer.getStringWidth(Potion.getDurationString(potionEffect)) * scale) - (potion.hasStatusIcon() ? iconSize : 2)) * inverse), (int) ((renderY + ySeparator) * inverse), 0xFFFFFF);
            renderY -= iconSize;
            glScalef(inverse, inverse, inverse);
        }
	}

	private void renderItemStack(ItemStack item, int x, int y) {
		if (item != null) {
			GL11.glPushMatrix();
			GL11.glDisable(GL11.GL_BLEND);
			RenderHelper.enableGUIStandardItemLighting();
			GL11.glEnable(GL12.GL_RESCALE_NORMAL);
			itemRenderer.zLevel = 200.0F;
			itemRenderer.renderItemIntoGUI(mc.fontRenderer, this.mc.getTextureManager(), item, x + 2, y + 2);
			itemRenderer.renderItemOverlayIntoGUI(this.mc.fontRenderer, this.mc.getTextureManager(), item, x + 2, y + 2);
			itemRenderer.zLevel = 0.0F;
			RenderHelper.disableStandardItemLighting();
			GL11.glDisable(GL12.GL_RESCALE_NORMAL);
			GL11.glPopMatrix();
		}
	}

	private Slot getWearingArmor(int armorType) {
		return mc.thePlayer.inventoryContainer.getSlot(5 + armorType);
	}

	private void renderTexture(int textureWidth, int textureHeight, float x, float y, float width, float height, float srcX, float srcY, float srcWidth, float srcHeight) {
		float renderSRCX = srcX / textureWidth, renderSRCY = srcY / textureHeight, renderSRCWidth = (srcWidth) / textureWidth, renderSRCHeight = (srcHeight) / textureHeight;
		boolean tex2D = glGetBoolean(GL_TEXTURE_2D);
		boolean blend = glGetBoolean(GL_BLEND);
		glPushMatrix();
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_TEXTURE_2D);
		glBegin(GL_TRIANGLES);
		glTexCoord2f(renderSRCX + renderSRCWidth, renderSRCY);
		glVertex2d(x + width, y);
		glTexCoord2f(renderSRCX, renderSRCY);
		glVertex2d(x, y);
		glTexCoord2f(renderSRCX, renderSRCY + renderSRCHeight);
		glVertex2d(x, y + height);
		glTexCoord2f(renderSRCX, renderSRCY + renderSRCHeight);
		glVertex2d(x, y + height);
		glTexCoord2f(renderSRCX + renderSRCWidth, renderSRCY + renderSRCHeight);
		glVertex2d(x + width, y + height);
		glTexCoord2f(renderSRCX + renderSRCWidth, renderSRCY);
		glVertex2d(x + width, y);
		glEnd();
		if (!tex2D)
			glDisable(GL_TEXTURE_2D);
		if (!blend)
			glDisable(GL_BLEND);
		glPopMatrix();
	}
}
