/**
 *
 */
package net.halalaboos.huzuni.mods;

/**
 * @author Halalaboos
 * @since Sep 1, 2013
 */
public interface Keybindable {
    //They are public by default, huehuehue.

    int getKeyCode();

    void setKeyCode(int keyCode);

    void onKeyPressed();

    String getKeyName();
}
