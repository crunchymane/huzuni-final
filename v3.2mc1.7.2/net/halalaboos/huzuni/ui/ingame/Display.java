package net.halalaboos.huzuni.ui.ingame;

import java.awt.Font;
import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;
import in.brud.huzuni.ui.ingame.Compact;
import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.client.Settings;
import net.halalaboos.huzuni.events.game.EventKeyPressed;
import net.halalaboos.huzuni.helpers.Helper;
import net.halalaboos.huzuni.helpers.MinecraftHelper;
import net.halalaboos.huzuni.rendering.font.MinecraftFontRenderer;
import net.halalaboos.huzuni.ui.ingame.themes.*;
import net.halalaboos.huzuni.ui.ingame.themes.tabbed.Tabbed;
import net.halalaboos.lib.manager.Manager;

public final class Display extends Manager<IngameTheme> implements Helper, MinecraftHelper, Listener {
    
	public final MinecraftFontRenderer fontRenderer = new MinecraftFontRenderer(new Font("Verdana", Font.BOLD, 18), false, false);
   
	public final MinecraftFontRenderer guiFontRenderer = new MinecraftFontRenderer(new Font("Tahoma", Font.TRUETYPE_FONT, 18), true, true);

    public final MinecraftFontRenderer titleFontRenderer = new MinecraftFontRenderer(new Font("Tahoma", Font.TRUETYPE_FONT, 32), true, true);
    
	private IngameTheme theme;
	
    public Display() {
        registry.registerListener(this);
    }

    /**
     * Loads all of our in-game themes.
     */
    public void loadThemes() {
        Huzuni.logger.log("Loading themes...");
        add(new Default());
        add(new OldSchool());
        add(new Informative());
        add(new Nyan());
        add(new Tabbed());
        add(new Paged());
        add(new PigPlus());
        add(new Morbid());
        add(new Skidded());
        add(new Simple());
        add(new Reliant());
        add(new Iridium());
        add(new Compact());
        add(new None());
        Huzuni.logger.log("Loaded " + this.size() + " themes.");
        theme = getCurrentTheme();
        // The first theme we've got.
    }

    /**
     * Renders the theme ingame.
     */
    public void render() {
        if (mc.gameSettings.showDebugInfo || !Settings.isHuzuniEnabled())
            return;
        theme.render(mc, gameHelper.getScreenWidth(), gameHelper.getScreenHeight());
    }

    /**
     * Switches to the next theme.
     */
    public void nextTheme() {
        int index = Settings.getIngameTheme();
        int maxSize = size();
        if (index != -1) {
            index++;
            if (index >= maxSize)
                index = 0;
            Settings.setIngameTheme(index);
            theme = get(index);
        }
    }

    public IngameTheme getCurrentTheme() {
        return get(Settings.getIngameTheme());
    }

    /**
     * @return The theme with the name specified.
     */
    public IngameTheme getTheme(String name) {
        for (IngameTheme theme : this.getList()) {
            if (theme.getName().equals(theme))
                return theme;
        }
        return null;
    }

    @EventTarget
    public void onKeyEvent(EventKeyPressed event) {
        getCurrentTheme().onKeyPressed(event.keyCode);
    }
    
    public void setTheme(int index) {
    	this.theme = get(index);
    }
}
