package net.halalaboos.huzuni.mods.player;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;

import net.halalaboos.huzuni.events.game.EventTick;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.DefaultModule;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;

import org.lwjgl.input.Keyboard;

public class NightVision extends DefaultModule implements Listener {

    public NightVision() {
        super("Night Vision", Keyboard.KEY_C);
        setCategory(Category.PLAYER);
        setRenderWhenEnabled(true);
        setDescription("Gives you the night vision potion effect.");
    }

    @Override
    protected void onEnable() {
        registry.registerListener(this);
    }

    @Override
    protected void onDisable() {
        registry.unregisterListener(this);
        mc.thePlayer.removePotionEffect(Potion.nightVision.id);
    }

    @Override
    protected void onToggle() {
    }

    @EventTarget
    public void onTick(EventTick event) {
        int duration = 0;
        duration += 220;
        PotionEffect nightVision = new PotionEffect(Potion.nightVision.id, duration, 1);
        nightVision.setPotionDurationMax(true);
        mc.thePlayer.addPotionEffect(nightVision);
    }

}