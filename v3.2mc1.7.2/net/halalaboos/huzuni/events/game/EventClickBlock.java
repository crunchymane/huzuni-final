package net.halalaboos.huzuni.events.game;

import com.darkmagician6.eventapi.events.Event;

public class EventClickBlock implements Event {
    public final int x, y, z, side;

    public EventClickBlock(int x, int y, int z, int side) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.side = side;
    }

}
