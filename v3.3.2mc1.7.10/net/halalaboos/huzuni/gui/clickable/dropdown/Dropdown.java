package net.halalaboos.huzuni.gui.clickable.dropdown;

import org.lwjgl.input.Mouse;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.gui.render.Theme;
import net.halalaboos.huzuni.gui.clickable.DefaultComponent;
import net.halalaboos.huzuni.util.GLUtils;
import net.halalaboos.huzuni.util.MathUtils;

public class Dropdown extends DefaultComponent {
	
	private String title;
	
	private int selectedComponent = -1, textPadding = 1, preferredComponents = 6;
	
	private boolean down = false;
	
	private String[] components;
		
	private float scrollPercentage = 0;
	
	public Dropdown(String title) {
		super();
		this.setWidth(110);
		this.setHeight(13);
		this.title = title;
		this.components = new String[] { "" };
	}
	
	@Override
	public void update() {
		super.update();
		handleScrollWheel();
	}
	
	private void handleScrollWheel() {
		if (this.isMouseOver()) {
			if (Mouse.hasWheel()) {
				int amount = Mouse.getDWheel();
				if (amount > 0)
					amount = -1;
				else if (amount < 0)
					amount = 1;
				else if (amount == 0)
					return;
				if (hasEnoughToScroll()) {
					scrollPercentage += ((float) amount / (float) getObjectsNotRendered());
					keepSafe();
				}
				
			}
		}
	}
	
	public boolean hasEnoughToScroll() {
		return preferredComponents < components.length;
	}
	
	private int getObjectsNotRendered() {
		return components.length - preferredComponents;
	}
	
	public int getDownHeight() {
		int totalHeight = 0, scrolledIndex = getScrolledIndex();
		for (int i = 0; scrolledIndex + i < components.length && i < preferredComponents && scrolledIndex + i >= 0; i++) 
			totalHeight +=  getComponentHeight(components[scrolledIndex + i]) + textPadding;
		return totalHeight;
	}
	
	private void keepSafe() {
		if (scrollPercentage > 1)
			scrollPercentage = 1;
		if (scrollPercentage < 0)
			scrollPercentage = 0;
	}
	
	private int getScrolledIndex() {
		return (int) (scrollPercentage * getObjectsNotRendered());
	}
	
	@Override
	public boolean mouseClicked(int x, int y, int buttonId) {
		if (down) {
			int yPos = (int) (getHeight() + 2);
			int scrolledIndex = getScrolledIndex();
			for (int i = 0; scrolledIndex + i < components.length && i < preferredComponents && scrolledIndex + i >= 0; i++) {
				float[] componentBoundaries = new float[] { this.getX() + this.getOffsetX() + 1, this.getY() + this.getOffsetY() + yPos, this.getWidth() - 5, getComponentHeight(components[scrolledIndex + i]) };
				if (MathUtils.inside(x, y, componentBoundaries)) {
					return true;
				}
				yPos += componentBoundaries[3] + textPadding;
			}
			// Since it was down and you didn't click anything.
			down = this.isPointInside(x, y) && !super.isPointInside(x, y);
		} else
			down = isMouseOver();
		return isMouseOver();
	}
	
	@Override
	public void mouseReleased(int x, int y, int buttonId) {
		if (down) {
			int yPos = (int) (getHeight() + 2);
			int scrolledIndex = getScrolledIndex();
			for (int i = 0; scrolledIndex + i < components.length && i < preferredComponents && scrolledIndex + i >= 0; i++) {
				float[] componentBoundaries = new float[] { this.getX() + this.getOffsetX() + 1, this.getY() + this.getOffsetY() + yPos, this.getWidth() - 5, getComponentHeight(components[scrolledIndex + i]) };
				if (MathUtils.inside(x, y, componentBoundaries)) {
					if (selectedComponent != scrolledIndex + i) {
						this.selectedComponent = scrolledIndex + i;
						this.invokeActionListeners();
					}
					// Since you released onto this component.
					// down = false;
					return;
				}
				yPos += componentBoundaries[3] + textPadding;
			}
		}
	}
	
	public void renderComponents(Theme theme, float offsetX, float offsetY, boolean mouseOver, boolean mouseDown) {
		int yPos = (int) (getHeight() + 2);
		int scrolledIndex = getScrolledIndex();
		float[] mouse = new float[] { GLUtils.getMouseX(), GLUtils.getMouseY() };
		for (int i = 0; scrolledIndex + i < components.length && i < preferredComponents && scrolledIndex + i >= 0; i++) {
			String component = components[scrolledIndex + i];
			float[] componentArea = new float[] { getX() + offsetX + 2, getY() + offsetY + yPos, getWidth() - (this.hasEnoughToScroll() ? 6 : 4), getComponentHeight(component) };
			render(theme, scrolledIndex + i, component, componentArea, selectedComponent == scrolledIndex + i, (mouseOver && MathUtils.inside(mouse[0], mouse[1], componentArea)), mouseDown);
			yPos += componentArea[3] + textPadding;
		}
	}
	
	private int getComponentHeight(String component) {
		return 12;
	}
	
	protected void render(Theme theme, int index, String text, float[] area, boolean highlight, boolean mouseOver,
			boolean mouseDown) {
		theme.renderSlot(this.getOffsetX(), this.getOffsetY(), index, area, highlight, mouseOver, mouseDown);
		Huzuni.drawString(text, area[0] + 2, area[1] + 2, 0xFFFFFF);
	}
	
	@Override
	public boolean isPointInside(int x, int y) {
		if (down) {
			float x1 = this.getX() + this.getOffsetX();
			float y1 = this.getY() + this.getOffsetY();
			float height = this.getHeight() + this.getDownHeight();
			float width = getWidth() - 2;
			return x1 < x && x1 + width > x && y1 < y && y1 + height > y;
		} else
			return super.isPointInside(x, y);
	}
	
	public float[] getSliderPoint() {
		int scrollbarY = (int) (this.scrollPercentage * getHeightForPoint());
		return new float[] { getOffsetX() + getX() + getWidth() - 2 - 1,
				getOffsetY() + getY() + getHeight() + scrollbarY + 1,
				2,
				8 };
	}
	
	protected float getHeightForPoint() {
		float maxPointForRendering = (float) (getDownHeight() - 8 - 1),
				beginPoint = (1);
		return maxPointForRendering - beginPoint;
	}
	
	public int getSelectedComponent() {
		return selectedComponent;
	}
	
	public String getSelectedComponentName() {
		return selectedComponent == -1 ?  "" : components[selectedComponent];
	}
	
	public boolean isDown() {
		return down;
	}

	public void setDown(boolean down) {
		this.down = down;
	}

	public String[] getComponents() {
		return components;
	}

	public void setComponents(String[] components) {
		this.components = components;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setSelectedComponent(int selectedComponent) {
		this.selectedComponent = selectedComponent;
	}

	public int getTextPadding() {
		return textPadding;
	}

	public void setTextPadding(int textPadding) {
		this.textPadding = textPadding;
	}

	public float getScrollPercentage() {
		return scrollPercentage;
	}

	public void setScrollPercentage(float scrollPercentage) {
		this.scrollPercentage = scrollPercentage;
	}
	
}
