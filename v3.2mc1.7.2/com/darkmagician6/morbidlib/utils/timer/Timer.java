package com.darkmagician6.morbidlib.utils.timer;

public class Timer {
	private long previousTime;
	
	public Timer() {
		previousTime = -1L;
	}
	
	public boolean hasReach(float milliseconds) {
		return getCurrentTime() - previousTime >= milliseconds;
	}
	
	public void reset() {
		previousTime = getCurrentTime();
	}
	
	public short convert(float perSecond) {
		return (short) (1000 / perSecond);
	}
	
	public long get() {
		return previousTime;
	}
	
	public long getCurrentTime() {
		return System.currentTimeMillis();
	}

}
