/**
 *
 */
package net.halalaboos.huzuni.ui.screen;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.client.Settings;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;

import org.lwjgl.input.Keyboard;

import java.awt.*;

/**
 * @author Halalaboos
 * @since Aug 5, 2013
 */
public class FontChanger extends GuiScreen {
    private GuiScreen lastScreen;
    private GuiTextField fontTextField,
            sizeTextField;

    public FontChanger(GuiScreen lastScreen) {
        this.lastScreen = lastScreen;
    }

    @Override
    public void initGui() {
        Keyboard.enableRepeatEvents(true);
        buttonList.clear();

        int leftAlign = this.width / 2 - 152,
                rightAlign = this.width / 2 + 2,
                top = this.height / 6 + 96 - 6,
                center = this.height / 6 + 120 - 6,
                bottom = this.height / 6 + 144 - 6;

        fontTextField = new GuiTextField(mc.fontRenderer, leftAlign + 30, center, 120, 20);
        fontTextField.setText(Huzuni.display.fontRenderer.getFont().getFontName());
        fontTextField.setFocused(true);
        fontTextField.setMaxStringLength(500);
        sizeTextField = new GuiTextField(mc.fontRenderer, leftAlign + 30, bottom, 120, 20);
        sizeTextField.setText("" + Huzuni.display.fontRenderer.getFont().getSize());

        buttonList.add(new GuiButton(1, rightAlign, bottom, 150, 20, "Set Font"));
        buttonList.add(new GuiButton(2, rightAlign, top, 150, 20, "Antialiasing: " + parseState(Huzuni.display.fontRenderer.isAntiAlias())));
        buttonList.add(new GuiButton(3, rightAlign, center, 150, 20, "Fractional Metrics: " + parseState(Huzuni.display.fontRenderer.isFractionalMetrics())));
        buttonList.add(new GuiButton(0, this.width / 2 - 100, this.height / 6 + 168, "Done"));
    }

    @Override
    protected void actionPerformed(GuiButton button) {
        switch (button.id) {
            case 0:
                mc.displayGuiScreen(lastScreen);
                break;
            case 1:
                if (fontTextField.getText().length() > 0) {
                    int size = 18;
                    String name = fontTextField.getText();
                    if (sizeTextField.getText().length() > 0)
                        if (isInteger(sizeTextField.getText()))
                            size = Integer.parseInt(sizeTextField.getText());
                    Font font = new Font(name, Font.TRUETYPE_FONT, size);
                    Huzuni.display.fontRenderer.setFont(font);
                    Settings.setFont(font);
                }
                break;
            case 2:
                Huzuni.display.fontRenderer.setAntiAlias(!Huzuni.display.fontRenderer.isAntiAlias());
                Settings.setFontAntiAlias(Huzuni.display.fontRenderer.isAntiAlias());
                initGui();
                break;
            case 3:
                Huzuni.display.fontRenderer.setFractionalMetrics(!Huzuni.display.fontRenderer.isFractionalMetrics());
                Settings.setFontFractionalMetrics(Huzuni.display.fontRenderer.isFractionalMetrics());
                initGui();
                break;
            default: {

            }
        }
        Settings.save();
    }

    private String parseState(boolean state) {
        return state ? "On" : "Off";
    }

    @Override
    public void onGuiClosed() {
        Keyboard.enableRepeatEvents(false);
    }

    @Override
    public void drawScreen(int par1, int par2, float par3) {
        super.drawDefaultBackground();
        super.drawScreen(par1, par2, par3);
        drawCenteredString(fontRenderer, "Font Options", width / 2, 16, 0xFFFFFF);
        fontTextField.drawTextBox();
        sizeTextField.drawTextBox();
        int leftAlign = this.width / 2 - 152,
                rightAlign = this.width / 2 + 2,
                top = this.height / 6 + 96 - 6,
                center = this.height / 6 + 120 - 6,
                bottom = this.height / 6 + 144 - 6;
        mc.fontRenderer.drawString("Name", leftAlign, center + 8, 0xFFFFFF);
        mc.fontRenderer.drawString("Size", leftAlign, bottom + 8, 0xFFFFFF);

    }

    @Override
    protected void keyTyped(char c, int keyCode) {
        super.keyTyped(c, keyCode);
        fontTextField.textboxKeyTyped(c, keyCode);
        sizeTextField.textboxKeyTyped(c, keyCode);
        if (keyCode == Keyboard.KEY_RETURN && (fontTextField.isFocused() || sizeTextField.isFocused())) {
            actionPerformed((GuiButton) buttonList.get(0));
        }
    }

    /**
     * @return True if the input can be parsed as a number.
     */
    private boolean isInteger(String input) {
        try {
            Integer.parseInt(input);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    protected void mouseClicked(int x, int y, int buttonID) {
        super.mouseClicked(x, y, buttonID);
        fontTextField.mouseClicked(x, y, buttonID);
        sizeTextField.mouseClicked(x, y, buttonID);
    }

    @Override
    public void updateScreen() {
        super.updateScreen();
        fontTextField.updateCursorCounter();
        sizeTextField.updateCursorCounter();
    }
}
