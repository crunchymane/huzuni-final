package net.halalaboos.huzuni.plugin;

import java.awt.Color;

import com.google.gson.annotations.SerializedName;

import net.minecraft.client.Minecraft;

import org.lwjgl.input.Keyboard;

import net.halalaboos.huzuni.api.plugin.Keybind;
import net.halalaboos.huzuni.api.plugin.Plugin;
import net.halalaboos.huzuni.util.GLUtils;

public abstract class DefaultPlugin extends Plugin implements Keybind {

	protected Minecraft mc = Minecraft.getMinecraft();

	private int keyCode = -1;

	@SerializedName("renderName")
	private String renderName = "";
	
	private Color color = GLUtils.getRandomColor();

	private boolean visible = true;
	
	public DefaultPlugin(String name, int keyCode) {
		super(name, "Huzuni dev");
		this.keyCode = keyCode;
		this.renderName = name;
	}

	public DefaultPlugin(String name) {
		this(name, -1);
	}

	@Override
	public void setKeybind(int keyCode) {
		this.keyCode = keyCode;
	}

	@Override
	public int getKeycode() {
		return keyCode;
	}

	@Override
	public boolean isBound() {
		return keyCode != -1 && keyCode != 0;
	}

	@Override
	public String getKeyName() {
		return isBound() ? Keyboard.getKeyName(keyCode) : "None";
	}

	@Override
	public boolean isPressed() {
		return isBound() ? Keyboard.isKeyDown(keyCode) : false;
	}
	
	public String getRenderName() {
		return renderName;
	}

	public void setRenderName(String renderName) {
		this.renderName = renderName;
	}

	public Color getColor() {
		return color;
	}

	public boolean getVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}
	
	public void toggle() {
		super.toggle();
		color = GLUtils.getRandomColor();
	}
	
	@Override
	public void pressed() {
		this.toggle();
	}
}
