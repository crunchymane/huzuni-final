package net.halalaboos.huzuni.event.render;

import net.halalaboos.huzuni.api.event.Listener;

/**
 * @author brudin
 * @version 1.0
 * @since 4/10/14
 */
public interface Renderable extends Listener {

	void render(RenderEvent event);

}
