package net.halalaboos.huzuni.command.commands;

import net.halalaboos.huzuni.api.console.Command;
import net.minecraft.client.Minecraft;

public class Clear implements Command {

	@Override
	public String[] getAliases() {
		return new String[] { "clear", "clr" };
	}

	@Override
	public String[] getHelp() {
		return new String[] { "clear" };
	}

	@Override
	public String getDescription() {
		return "Clears all messages from chat";
	}

	@Override
	public void run(String input, String[] args) {
		Minecraft.getMinecraft().ingameGUI.getChatGUI().clearChatMessages();
	}

}
