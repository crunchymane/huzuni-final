/**
 * 
 */
package net.halalaboos.huzuni.ui.windows.hub;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;

import net.halalaboos.huzuni.ui.clickable.containers.HuzuniWindow;
import net.halalaboos.lib.ui.components.SlotComponent;
import net.halalaboos.lib.ui.containers.Window;
import net.halalaboos.lib.ui.utils.GLUtils;

/**
 * @author Halalaboos
 *
 * @since Oct 6, 2013
 */
public class SlotHub extends SlotComponent<HuzuniWindow> {
	private final Color highlightColor = new Color(0F, 0.5F, 1F, 0.75F),
	defaultColor = new Color(0.3F, 0.3F, 0.3F, 0.75F);
	
	/**
	 * @param dimensions
	 */
	public SlotHub(Dimension dimensions) {
		super(dimensions);
		
	}

	/**
	 * @see net.halalaboos.lib.ui.components.SlotComponent#render(int, java.lang.Object, java.awt.Rectangle, boolean, boolean)
	 */
	@Override
	public void render(int index, HuzuniWindow window, Rectangle boundaries,
			boolean mouseOver, boolean mouseDown) {
		GLUtils.setColor(GLUtils.getColorWithAffects(window.isActive() ? highlightColor : defaultColor, mouseOver, mouseDown));
		GLUtils.drawFilledRect(boundaries.x, boundaries.y, boundaries.x + boundaries.width, boundaries.y + boundaries.height);
		GLUtils.drawString(window.getTitle(), boundaries.x + 2, boundaries.y + 2, 0xFFFFFF);
	}

	/**
	 * @see net.halalaboos.lib.ui.components.SlotComponent#onClicked(int, java.lang.Object, java.awt.Rectangle, java.awt.Point, int)
	 */
	@Override
	public void onClicked(int index, HuzuniWindow window, Rectangle boundaries,
			Point mouse, int buttonID) {
	}

	/**
	 * @see net.halalaboos.lib.ui.components.SlotComponent#onReleased(int, java.lang.Object, java.awt.Point)
	 */
	@Override
	public void onReleased(int index, HuzuniWindow window, Point mouse) {
		window.setActive(!window.isActive());
	}

	/**
	 * @see net.halalaboos.lib.ui.components.SlotComponent#getElementHeight()
	 */
	@Override
	public int getElementHeight() {
		return 12;
	}

}
