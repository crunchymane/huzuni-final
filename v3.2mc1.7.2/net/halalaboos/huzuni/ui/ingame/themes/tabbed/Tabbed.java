/**
 *
 */
package net.halalaboos.huzuni.ui.ingame.themes.tabbed;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.DefaultModule;
import net.halalaboos.huzuni.ui.ingame.IngameTheme;
import net.halalaboos.huzuni.utils.RenderUtils;
import net.minecraft.client.Minecraft;
import org.lwjgl.input.Keyboard;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Halalaboos
 * @since Aug 3, 2013
 */
public class Tabbed implements IngameTheme {
    private final List<Tab> tabs = new ArrayList<Tab>();
    private int tabIndex = 0;
    boolean tabOpened = false;

    public Tabbed() {
        initGui();
    }

    private void initGui() {
        for (Category category : Category.values()) {
            List<DefaultModule> mods = new ArrayList<DefaultModule>();
            Tab tab = new Tab(category.formalName, mods);
            for (DefaultModule module : Huzuni.modManager.getDefaultModules())
                if (module.getCategory() == category)
                    mods.add(module);
            tabs.add(tab);
        }
    }

    /**
     * @see net.halalaboos.huzuni.ui.ingame.IngameTheme#render(net.minecraft.src.Minecraft, int, int)
     */
    @Override
    public void render(Minecraft mc, int screenWidth, int screenHeight) {
        int x = 2,
                y = 2,
                width = getMaxTabWidth(),
                height = 14;
        Tab selectedTab = getSelectedTab();

        mc.fontRenderer.drawStringWithShadow(Huzuni.TITLE + "\247r v" + Huzuni.VERSION, 2, 2, 0xFFFFFF);
        y += 10;
        for (Tab tab : tabs) {
            String renderName = tab.name;
            Rectangle boundaries = new Rectangle(x, y, width, height);
            RenderUtils.drawRect(boundaries, selectedTab == tab ? 0x9F00B3FF : 0x9F5E5E5E);
            if (tab == selectedTab) {
                if (tabOpened)
                    selectedTab.render(x + width + 2, y);
            }
            mc.fontRenderer.drawStringWithShadow(renderName, x + 2, y + 3, 0xFFFFFF);
            y += height;
        }
        y += 2;

        for (DefaultModule module : Huzuni.modManager.getDefaultModules()) {
            if (module.isEnabled()) {
                if (module.shouldRenderWhenEnabled()) {
                    mc.fontRenderer.drawStringWithShadow(module.getRenderName(), 2, y, 0xFFFF99);
                    y += 11;
                }
            }
        }
    }

    private int getMaxTabWidth() {
        int width = 0;
        Tab selectedTab = getSelectedTab();
        for (Tab tab : tabs) {
            String renderName = tab.name + ">>";
            if (mc.fontRenderer.getStringWidth(renderName) + 4 > width)
                width = mc.fontRenderer.getStringWidth(renderName) + 4;
        }
        return width;
    }

    /**
     * @see net.halalaboos.huzuni.ui.ingame.IngameTheme#getName()
     */
    @Override
    public String getName() {
        return "Tabbed";
    }

    private void keepTabIndexInBounds() {
        if (tabIndex >= tabs.size()) {
            tabIndex = 0;
        } else if (tabIndex < 0)
            tabIndex = tabs.size() - 1;
    }

    private Tab getSelectedTab() {
        return tabs.get(tabIndex);
    }

    /**
     * @see net.halalaboos.huzuni.ui.ingame.IngameTheme#onKeyPressed(int)
     */
    @Override
    public void onKeyPressed(int keyCode) {
        Tab selectedTab = getSelectedTab();
        switch (keyCode) {
            case Keyboard.KEY_UP:
                if (tabOpened)
                    selectedTab.onKeyTyped(keyCode);
                else
                    tabIndex--;
                break;
            case Keyboard.KEY_DOWN:
                if (tabOpened)
                    selectedTab.onKeyTyped(keyCode);
                else
                    tabIndex++;
                break;
            case Keyboard.KEY_RIGHT:
                if (tabOpened)
                    selectedTab.onKeyTyped(keyCode);
                else
                    tabOpened = true;
                break;
            case Keyboard.KEY_LEFT:
                tabOpened = false;
                break;
            default:
                if (tabOpened)
                    selectedTab.onKeyTyped(keyCode);
                break;
        }
        keepTabIndexInBounds();
    }

}
