/**
 *
 */
package net.halalaboos.lib.ui.theme;

import net.halalaboos.lib.ui.Container;
import net.halalaboos.lib.ui.components.*;
import net.halalaboos.lib.ui.components.Button;
import net.halalaboos.lib.ui.components.Label;
import net.halalaboos.lib.ui.components.TextArea;
import net.halalaboos.lib.ui.components.TextField;
import net.halalaboos.lib.ui.containers.Window;
import net.halalaboos.lib.ui.utils.GLUtils;

import java.awt.*;
import java.text.DecimalFormat;

/**
 * @author Halalaboos
 * @since Aug 30, 2013
 */
public class DefaultTheme extends Theme {
	protected static final Color tooltipColor = new Color(0F, 0.5F, 1F, 0.75F);

    public DefaultTheme() {
        registerRenderer(Button.class, new ComponentRenderer<Button>() {

            @Override
            public void render(Container container, Point offset, Button button, Point mouse) {
            	Rectangle area = button.getRenderableArea();
            	GLUtils.setColor(GLUtils.getColorWithAffects(button.isHighlight() ? button.getHighlightColor() : button.getDefaultColor(), button.isMouseOver(), button.isMouseDown()));
                GLUtils.drawFilledRect(area.x, area.y, area.x + area.width, area.y + area.height);
                GLUtils.drawString(button.getTitle(), area.x + area.width / 2 - GLUtils.getStringWidth(button.getTitle()) / 2,
                        area.y + area.height / 2 - GLUtils.getStringHeight(button.getTitle()) / 2 + 2, button.getTextColor().getRGB());
            }
        });
        registerRenderer(TextArea.class, new ComponentRenderer<TextArea>() {
			@Override
			public void render(Container container, Point offset, TextArea textArea, Point mouse) {
            	Rectangle area = textArea.getRenderableArea();
				GLUtils.setColor(GLUtils.getColorWithAffects(textArea.getBackgroundColor(), textArea.isMouseOver() && textArea.isEditable(), false));
                GLUtils.drawFilledRect(area.x, area.y, area.x + area.width, area.y + area.height);
                GLUtils.setColor(GLUtils.getColorWithAffects(textArea.getBackgroundColor().darker().darker(), false, false));
                GLUtils.drawRect(area.x, area.y, area.x + area.width, area.y + area.height, 1F);
                textArea.renderText(area, 1);
			}
        });
        registerRenderer(Dropdown.class, new ComponentRenderer<Dropdown>() {

            @Override
            public void render(Container container, Point offset, Dropdown dropdown, Point mouse) {
            	Rectangle area = dropdown.getRenderableArea();
            	GLUtils.setColor(GLUtils.getColorWithAffects(dropdown.getTopColor(), dropdown.isMouseOver(), dropdown.isMouseDown()));
                GLUtils.drawFilledRect(area.x, area.y, area.x + area.width, area.y + area.height);
                
                GLUtils.drawString(dropdown.getTitle() + " (" + dropdown.getSelectedComponentName() + ")", area.x + 2, area.y + area.height / 2 - GLUtils.getStringHeight(dropdown.getTitle()) / 2 + 2, Color.WHITE.getRGB());
                if (dropdown.isDown()) {
                	String[] components = dropdown.getComponents();
        			int yPos = dropdown.getTextPadding() + 2;
        			
        			GLUtils.setColor(GLUtils.getColorWithAffects(dropdown.getBackgroundColor(), dropdown.isMouseOver(), dropdown.isMouseDown()));
                    GLUtils.drawFilledRect(area.x, area.y + dropdown.getTextPadding() + 1, area.x + area.width, area.y + yPos + (dropdown.getTextPadding() * components.length) + 1);
                    
                    for (int i = 0; i < components.length; i++) {
        				Rectangle componentBoundaries = new Rectangle(area.x + 1, area.y + yPos, area.width - 2, dropdown.getTextPadding());
        				boolean mouseOverComponent = componentBoundaries.contains(GLUtils.getMouseX(), GLUtils.getMouseY());
        				Color componentColor = GLUtils.getColorWithAffects(dropdown.getSelectedComponent() == i ? dropdown.getSelectedComponentColor() : Color.WHITE, mouseOverComponent, dropdown.isMouseDown());
        				
            			GLUtils.setColor(GLUtils.getColorWithAffects(dropdown.getDropdownColor(), mouseOverComponent, dropdown.isMouseDown()));
                        GLUtils.drawFilledRect(componentBoundaries.x, componentBoundaries.y, componentBoundaries.x + componentBoundaries.width, componentBoundaries.y + componentBoundaries.height);
        				GLUtils.drawStringWithShadow(components[i], componentBoundaries.x + 2, componentBoundaries.y + 2, componentColor.getRGB());
        				yPos += dropdown.getTextPadding();
                	}
                }
            }

        });
        registerRenderer(ProgressBar.class, new ComponentRenderer<ProgressBar>() {

            public void render(Container container, Point offset, ProgressBar progressBar, Point mouse) {
            	Rectangle area = progressBar.getRenderableArea();
                GLUtils.setColor(GLUtils.getColorWithAffects(progressBar.getBackgroundColor(), progressBar.isMouseOver(), false));
                GLUtils.drawFilledRect(area.x, area.y, area.x + area.width, area.y + area.height);

                GLUtils.setColor(GLUtils.getColorWithAffects(progressBar.getBarColor(), progressBar.isMouseOver(), false));
                GLUtils.drawFilledRect(area.x, area.y, area.x + (int) ((progressBar.getProgress() / progressBar.getMaximumVal()) * area.width), area.y + area.height);

                String renderString = progressBar.getTitle() + " " + (int) progressBar.getProgress() + " " + progressBar.getProgressWatermark();
                GLUtils.drawString(renderString, area.x + area.width / 2 - GLUtils.getStringWidth(renderString) / 2, area.y + area.height / 2 - GLUtils.getStringHeight(renderString) / 2 + 1, progressBar.getTextColor().getRGB());
            }

        });
        registerRenderer(Slider.class, new ComponentRenderer<Slider>() {
            protected DecimalFormat formatter = new DecimalFormat("#.#");

            @Override
            public void render(Container container, Point offset, Slider slider, Point mouse) {
            	Rectangle area = slider.getRenderableArea();
                Rectangle sliderPoint = slider.getSliderPoint();

                GLUtils.setColor(GLUtils.getColorWithAffects(slider.getBackgroundColor(), slider.isMouseOver() && !sliderPoint.contains(mouse), false));
                GLUtils.drawFilledRect(area.x, area.y, area.x + area.width, area.y + area.height);

                GLUtils.drawString(slider.getLabel(), area.x + 2, area.y + area.height / 2 - GLUtils.getStringHeight(slider.getLabel()) / 2 + 2, 0xFFFFFF);

                String formattedValue = formatter.format(slider.getValue()) + slider.getValueWatermark();
                GLUtils.drawString(formattedValue, area.x + area.width - GLUtils.getStringWidth(formattedValue) - 2, area.y + area.height / 2 - GLUtils.getStringHeight(formattedValue) / 2 + 2, 0xFFFFFF);

                GLUtils.setColor(GLUtils.getColorWithAffects(slider.getPointColor(), slider.isMouseOver() && sliderPoint.contains(mouse), slider.isMouseDown()));
                GLUtils.drawFilledRect(sliderPoint.x, sliderPoint.y, sliderPoint.x + sliderPoint.width, sliderPoint.y + sliderPoint.height);
            }

        });
        registerRenderer(TextField.class, new ComponentRenderer<TextField>() {

            @Override
            public void render(Container container, Point offset, TextField textField, Point mouse) {
            	Rectangle area = textField.getRenderableArea();

                GLUtils.setColor(GLUtils.getColorWithAffects(textField.getBackgroundColor(), textField.isMouseOver() || textField.isEnabled(), false));
                GLUtils.drawFilledRect(area.x, area.y, area.x + area.width, area.y + area.height);
                GLUtils.drawStringWithShadow(textField.getTextForRender(area) + textField.getSelection(), area.x + 2, area.y + area.height / 2 - GLUtils.getStringHeight(textField.getText()) / 2 + 2, textField.getTextColor().getRGB());
            }

        });
        registerRenderer(SlotComponent.class, new ComponentRenderer<SlotComponent>() {

            @Override
            public void render(Container container, Point offset, SlotComponent slotComponent, Point mouse) {
            	Rectangle area = slotComponent.getRenderableArea();
            	if (slotComponent.shouldRenderBackground()) {
                	GLUtils.setColor(slotComponent.getBackgroundColor());
	                GLUtils.drawFilledRect(area.x, area.y, area.x + area.width, area.y + area.height);
                }
                if (slotComponent.hasEnoughToScroll() && slotComponent.hasSlider()) {
                    Rectangle sliderPoint = slotComponent.getDraggableArea();
                	GLUtils.setColor(slotComponent.getSliderColor());
                	GLUtils.drawFilledRect(sliderPoint.x, sliderPoint.y, sliderPoint.x + sliderPoint.width, sliderPoint.y + sliderPoint.height);
                }
                slotComponent.renderElements(area, slotComponent.isMouseOver(), slotComponent.isMouseDown());
            }


        });
        this.registerRenderer(Label.class, new ComponentRenderer<Label>() {

            @Override
            public void render(Container container, Point offset, Label label, Point mouse) {
            	Rectangle area = label.getRenderableArea();
                GLUtils.drawString(label.getText(), area.x + 2, area.y + 2, label.getTextColor().getRGB());
            }

        });
    }
    
    /**
	 * @see net.halalaboos.lib.ui.theme.Theme#renderToolTip(java.lang.String, java.awt.Point)
	 */
	@Override
	public void renderToolTip(String tooltip, Point mouse) {
		int aboveMouse = 8;
		int width = GLUtils.getStringWidth(tooltip);
		GLUtils.setColor(tooltipColor.brighter());
		GLUtils.drawRect(mouse.x - 1, mouse.y - aboveMouse - 1, mouse.x + width + 5, mouse.y + GLUtils.getStringHeight(tooltip) - aboveMouse + 3, 1);
		GLUtils.setColor(tooltipColor);
		GLUtils.drawFilledRect(mouse.x, mouse.y - aboveMouse, mouse.x + width + 4, mouse.y + GLUtils.getStringHeight(tooltip) - aboveMouse + 2);
		GLUtils.drawStringWithShadow(tooltip, mouse.x + 2, mouse.y - aboveMouse + 2, 0xFFFFFF);
	}

	/**
	 * @see net.halalaboos.lib.ui.theme.Theme#renderContainer(java.awt.Container, java.awt.Point)
	 */
	@Override
	public void renderContainer(Container container, Point mouse) {
		if (container instanceof Window) {
			Window window = (Window) container;
            // Top draggable part
            Rectangle draggableDimensions = window.getDraggableArea();
            int borderPadding = window.getBorderPadding();

            GLUtils.setColor(window.getBorderColor());
            GLUtils.drawFilledRect(draggableDimensions.x - borderPadding, draggableDimensions.y - borderPadding, draggableDimensions.x + draggableDimensions.width + borderPadding, draggableDimensions.y + draggableDimensions.height);

            // Minimize button
            Rectangle minimizeArea = window.getMinimizeArea();
            GLUtils.setColor(GLUtils.getColorWithAffects(window.isMinimized() ? window.getMinimizeColor().darker() : window.getMinimizeColor(), window.isMouseOver() && minimizeArea.contains(mouse), false));
            GLUtils.drawFilledRect(minimizeArea.x, minimizeArea.y, minimizeArea.x + minimizeArea.width, minimizeArea.y + minimizeArea.height);

            // Title
            GLUtils.drawString(window.getTitle(), draggableDimensions.x + 2, draggableDimensions.y + (draggableDimensions.height / 2) - (GLUtils.getStringHeight(window.getTitle()) / 2) + 1, 0xFFFFFF);

            // Render slightly below the actual dimensions.
            if (!window.isMinimized()) {
                Rectangle windowArea = window.getArea();
                int barSize = window.getBarSize();
                GLUtils.setColor(window.getInsideColor());
                GLUtils.drawFilledRect(windowArea.x, windowArea.y + barSize, windowArea.x + windowArea.width, windowArea.y + windowArea.height);
                GLUtils.setColor(window.getBorderColor());
                GLUtils.drawRect(windowArea.x - borderPadding, windowArea.y + barSize - borderPadding, windowArea.x + windowArea.width + borderPadding, windowArea.y + windowArea.height + borderPadding, borderPadding);
            }
        
		}
	}
    
}
