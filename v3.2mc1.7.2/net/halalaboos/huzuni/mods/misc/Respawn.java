package net.halalaboos.huzuni.mods.misc;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;
import com.darkmagician6.eventapi.types.Priority;

import net.halalaboos.huzuni.events.game.EventHealthChange;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.DefaultModule;

public class Respawn extends DefaultModule implements Listener {

    public Respawn() {
        super("Respawn", -1);
        setCategory(Category.MISC);
        setDescription("Automagically respawns you when you die.");
        setRenderWhenEnabled(false);
    }

    @Override
    protected void onEnable() {
        registry.registerListener(this);
    }

    @Override
    protected void onDisable() {
        registry.unregisterListener(this);
    }

    @Override
    protected void onToggle() {
    }

    @EventTarget(Priority.HIGH)
    public void onHealthChange(EventHealthChange event) {
        if (event.health <= 0) {
            mc.thePlayer.respawnPlayer();
        }
    }

}
