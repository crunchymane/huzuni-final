package net.halalaboos.huzuni.utils;

import net.halalaboos.huzuni.client.Settings;
import net.halalaboos.huzuni.helpers.Helper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.util.AxisAlignedBB;

import java.awt.*;
import java.util.Random;

import static org.lwjgl.opengl.GL11.*;

/**
 * Simple class with open GL methods.
 */
public final class RenderUtils implements Helper {
	
	private static final Random random = new Random();
	
	private static final Minecraft mc = Minecraft.getMinecraft();
	
    private RenderUtils() {
    	
    }

    /**
     * Enables GL constants for 3D rendering.
     */
    public static void enableGL3D() {
        glDisable(GL_ALPHA_TEST);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glDisable(GL_TEXTURE_2D);
        glDisable(GL_DEPTH_TEST);
        glDepthMask(false);
        glEnable(GL_CULL_FACE);
       	mc.entityRenderer.disableLightmap(0);
        if (Settings.isAntiAlias()) {
	        glEnable(GL_LINE_SMOOTH);
	        glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	        glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
        }
        glLineWidth(Settings.getLineSize());
    }

    /**
     * Disables GL constants for 3D rendering.
     */
    public static void disableGL3D() {
        glEnable(GL_TEXTURE_2D);
        glEnable(GL_DEPTH_TEST);
        glDisable(GL_BLEND);
        glEnable(GL_ALPHA_TEST);
        glDepthMask(true);
        glCullFace(GL_BACK);
        if (Settings.isAntiAlias()) {
            glDisable(GL_LINE_SMOOTH);
            glHint(GL_LINE_SMOOTH_HINT, GL_DONT_CARE);
            glHint(GL_POLYGON_SMOOTH_HINT, GL_DONT_CARE);
        }
    }

    /**
     * Draws a rectangle on the rectangle instance.
     */
    public static void drawRect(Rectangle rectangle, int color) {
        drawRect(rectangle.x, rectangle.y, rectangle.x + rectangle.width, rectangle.y + rectangle.height, color);
    }

    /**
     * Draws a rectangle at the coordinates specified with the hexadecimal color.
     */
    public static void drawRect(double x, double y, double x1, double y1, int color) {
        enableGL2D();
        glColor(color);
        drawRect((double) x, (double) y, (double) x1, (double) y1);
        disableGL2D();
    }

    /**
     * Draws a bordered rectangle at the coordinates specified with the hexadecimal color.
     */
    public static void drawBorderedRect(double x, double y, double x1, double y1, float width, int internalColor, int borderColor) {
        enableGL2D();
        glColor(internalColor);
        drawRect(x + width, y + width, x1 - width, y1 - width);
        glColor(borderColor);
        drawRect(x + width, y, x1 - width, y + width);
        drawRect(x, y, x + width, y1);
        drawRect(x1 - width, y, x1, y1);
        drawRect(x + width, y1 - width, x1 - width, y1);
        disableGL2D();
    }

    /**
     * Draws a bordered rectangle at the coordinates specified with the hexadecimal color.
     */
    public static void drawBorderedRect(Rectangle rectangle, float width, int internalColor, int borderColor) {
        enableGL2D();
        double x = rectangle.x,
                y = rectangle.y, x1 = rectangle.x + rectangle.width,
                y1 = rectangle.y + rectangle.height;
        glColor(internalColor);
        drawRect(x + width, y + width, x1 - width, y1 - width);
        glColor(borderColor);
        drawRect(x + 1, y, x1 - 1, y + width);
        drawRect(x, y, x + width, y1);
        drawRect(x1 - width, y, x1, y1);
        drawRect(x + 1, y1 - width, x1 - 1, y1);
        disableGL2D();
    }

    /**
     * Draws a rectangle at the coordinates specified.
     */
    public static void drawRect(double x, double y, double x1, double y1, float r,
                                float g, float b, float a) {
        enableGL2D();
        glColor4f(r, g, b, a);
        drawRect(x, y, x1, y1);
        disableGL2D();
    }

    /**
     * Enables 2D GL constants for 2D rendering.
     */
    public static void enableGL2D() {
        glDisable(GL_DEPTH_TEST);
        glEnable(GL_BLEND);
        glDisable(GL_TEXTURE_2D);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glDepthMask(true);
        if (Settings.isAntiAlias()) {
            glEnable(GL_LINE_SMOOTH);
            glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
            glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
        }
    }

    /**
     * Disables 2D GL constants for 2D rendering.
     */
    public static void disableGL2D() {
        glEnable(GL_TEXTURE_2D);
        glDisable(GL_BLEND);
        glEnable(GL_DEPTH_TEST);
        if (Settings.isAntiAlias()) {
            glDisable(GL_LINE_SMOOTH);
            glHint(GL_LINE_SMOOTH_HINT, GL_DONT_CARE);
            glHint(GL_POLYGON_SMOOTH_HINT, GL_DONT_CARE);
        }
    }

    public static void drawRect(double x, double y, double x1, double y1) {
        // glRectd(x, y, x1, y1);
        glBegin(GL_QUADS);
        glVertex2d(x, y1);
        glVertex2d(x1, y1);
        glVertex2d(x1, y);
        glVertex2d(x, y);
        glEnd();
    }

    /**
     *
     * */
    public static void glColor(Color color) {
        glColor4f((color.getRed() / 255F), (color.getGreen() / 255F), (color.getBlue() / 255F), (color.getAlpha() / 255F));
    }

    /**
     *
     * */
    public static void glColor(int hex) {
        float alpha = (float) (hex >> 24 & 255) / 255.0F;
        float red = (float) (hex >> 16 & 255) / 255.0F;
        float green = (float) (hex >> 8 & 255) / 255.0F;
        float blue = (float) (hex & 255) / 255.0F;
        glColor4f(red, green, blue, alpha);
    }

    /**
     * @return A java.awt.Color instance with a random r, g, b value.
     */
    public static Color getRandomColor() {
        return getRandomColor(1000, 0.6F);
    }
    
    /**
     * This isn't mine, but it's the most beautiful random color generator I've seen. Reminds me of easter.
     * */
    public static Color getRandomColor(int saturationRandom, float luminance) {
        final float hue = random.nextFloat();
        final float saturation = (random.nextInt(saturationRandom) + 1000) / (float) saturationRandom + 1000F;
        return Color.getHSBColor(hue, saturation, luminance);
    }
    
    public static void draw3DString(FontRenderer fontRenderer, String text, float x, float y, float z, int color) {
        glPushMatrix();
        glTranslated(x, y, z);
		glNormal3f(0.0F, 1.0F, 0.0F);
        glRotatef(-RenderManager.instance.playerViewY, 0.0F, 1.0F, 0.0F);
		glRotatef(RenderManager.instance.playerViewX, 1.0F, 0.0F, 0.0F);
		glScalef(-0.0266666688F, -0.0266666688F, 0.0266666688F);
        fontRenderer.drawStringWithShadow(text, 0, 0, color);
        glPopMatrix();
    }
    
    public static void draw3DStringCentered(FontRenderer fontRenderer, String text, float x, float y, float z, int color) {
        glPushMatrix();
        glTranslated(x, y, z);
		glNormal3f(0.0F, 1.0F, 0.0F);
        glRotatef(-RenderManager.instance.playerViewY, 0.0F, 1.0F, 0.0F);
		glRotatef(RenderManager.instance.playerViewX, 1.0F, 0.0F, 0.0F);
		glScalef(-0.0266666688F, -0.0266666688F, 0.0266666688F);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glEnable(GL_TEXTURE_2D);
        fontRenderer.drawStringWithShadow(text, -fontRenderer.getStringWidth(text) / 2, 0, color);
        glDisable(GL_TEXTURE_2D);
        glPopMatrix();
    }

    /**
     * Stole from RenderGlobal.
     * Draws a box outline on the AxisAlignedBB instance.
     */
    public static void drawOutlinedBox(AxisAlignedBB boundingBox) {
        if (boundingBox == null)
            return;

        glBegin(GL_LINE_STRIP);
        glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.minZ);
        glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.minZ);
        glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.maxZ);
        glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.maxZ);
        glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.minZ);
        glEnd();
        glBegin(GL_LINE_STRIP);
        glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.minZ);
        glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.minZ);
        glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.maxZ);
        glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.maxZ);
        glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.minZ);
        glEnd();
        glBegin(GL_LINES);
        glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.minZ);
        glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.minZ);
        glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.minZ);
        glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.minZ);
        glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.maxZ);
        glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.maxZ);
        glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.maxZ);
        glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.maxZ);
        glEnd();
    }
    
    /**
     * Renders criss-cross lines.
     * */
    public static void renderCrosses(AxisAlignedBB boundingBox) {
        glBegin(GL_LINES);
        glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.maxZ);
        glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.minZ);
        glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.minZ);

        glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.maxZ);
        glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.minZ);
        glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.minZ);

        glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.maxZ);
        glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.maxZ);
        glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.maxZ);

        glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.minZ);
        glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.minZ);
        glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.maxZ);
        glEnd();
    }

    /**
     * Overly complicated box method that will draw a box at the AxisAlignedBB instance.
     */
    public static void drawBox(AxisAlignedBB boundingBox) {
        if (boundingBox == null)
            return;
        // back
        glBegin(GL_QUADS);
        glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.maxZ);
        glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.maxZ);
        glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.maxZ);
        glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.maxZ);
        glEnd();
        glBegin(GL_QUADS);
        glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.maxZ);
        glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.maxZ);
        glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.maxZ);
        glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.maxZ);
        glEnd();
        // left
        glBegin(GL_QUADS);
        glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.minZ);
        glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.maxZ);
        glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.maxZ);
        glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.minZ);
        glEnd();
        glBegin(GL_QUADS);
        glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.maxZ);
        glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.minZ);
        glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.minZ);
        glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.maxZ);
        glEnd();
        // right
        glBegin(GL_QUADS);
        glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.maxZ);
        glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.minZ);
        glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.minZ);
        glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.maxZ);
        glEnd();
        glBegin(GL_QUADS);
        glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.minZ);
        glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.maxZ);
        glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.maxZ);
        glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.minZ);
        glEnd();
        // front
        glBegin(GL_QUADS);
        glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.minZ);
        glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.minZ);
        glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.minZ);
        glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.minZ);
        glEnd();
        glBegin(GL_QUADS);
        glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.minZ);
        glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.minZ);
        glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.minZ);
        glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.minZ);
        glEnd();
        // top
        glBegin(GL_QUADS);
        glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.minZ);
        glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.minZ);
        glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.maxZ);
        glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.maxZ);
        glEnd();

        glBegin(GL_QUADS);
        glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.minZ);
        glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.minZ);
        glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.maxZ);
        glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.maxZ);
        glEnd();

        // bottom
        glBegin(GL_QUADS);
        glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.minZ);
        glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.minZ);
        glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.maxZ);
        glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.maxZ);
        glEnd();

        glBegin(GL_QUADS);
        glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.minZ);
        glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.minZ);
        glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.maxZ);
        glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.maxZ);
        glEnd();
    }

    /**
     * Draws a rectangle with a vertical gradient between the specified colors.
     */
    public static void drawGradientRect(float x, float y, float x1, float y1,
                                        int topColor, int bottomColor) {
        enableGL2D();
        glShadeModel(GL_SMOOTH);
        glBegin(GL_QUADS);
        glColor(topColor);
        glVertex2d(x, y1);
        glVertex2d(x1, y1);
        glColor(bottomColor);
        glVertex2d(x1, y);
        glVertex2d(x, y);
        glEnd();
        glShadeModel(GL_FLAT);
        disableGL2D();
    }

    /**
     * @return Used for nifty things ;)
     * @author Jonalu
     */
    public static void prepareScissorBox(float x, float y, float x2, float y2) {
        ScaledResolution scaledResolution = gameHelper.getScaledResolution();
        int factor = scaledResolution.getScaleFactor();
        glScissor((int) (x * factor),
                (int) ((scaledResolution.getScaledHeight() - y2) * factor),
                (int) ((x2 - x) * factor), (int) ((y2 - y) * factor));
    }
}
