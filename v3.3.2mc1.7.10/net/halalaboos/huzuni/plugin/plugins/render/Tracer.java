package net.halalaboos.huzuni.plugin.plugins.render;

import pw.brudin.huzuni.util.render.BoxBeams;
import pw.brudin.huzuni.util.render.OutlinedBox;
import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.plugin.Category;
import net.halalaboos.huzuni.api.plugin.ToggleOption;
import net.halalaboos.huzuni.event.render.RenderEvent;
import net.halalaboos.huzuni.event.render.Renderable;
import net.halalaboos.huzuni.plugin.DefaultPlugin;
import net.halalaboos.huzuni.util.GLUtils;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.IMob;
import net.minecraft.entity.passive.IAnimals;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.AxisAlignedBB;

import org.lwjgl.input.Keyboard;

import static org.lwjgl.opengl.GL11.*;

/**
 * @author brudin
 * @version 1.0
 * @since 4/10/14
 */
public class Tracer extends DefaultPlugin implements Renderable {

	private final OutlinedBox outlinedBox;
	private final BoxBeams boxBeams;
	
	private final ToggleOption distance = new ToggleOption("Distance"), 
			players = new ToggleOption("Players"), 
			mobs = new ToggleOption("Mobs"), 
			animals = new ToggleOption("Animals"),
			boxes = new ToggleOption("Boxes"),
			lines = new ToggleOption("Lines");
	
	public Tracer() {
		super("Tracer", Keyboard.KEY_B);
		setVisible(false);
		setAuthor("brudin");
		setDescription("Draws lines to players and draws boxes around them.");
		setCategory(Category.RENDER);
		setOptions(distance, players, mobs, animals, boxes, lines);
		boxes.setEnabled(true);
		lines.setEnabled(true);
		players.setEnabled(true);
		distance.setEnabled(true);
		float width = 0.6F / 1.5F;
		float minX = -width, minY = 0, minZ = -width, maxX = width, maxY = 1.9F, maxZ = width;
		outlinedBox = new OutlinedBox(AxisAlignedBB.getBoundingBox(minX, minY, minZ, maxX, maxY, maxZ));
		boxBeams = new BoxBeams(AxisAlignedBB.getBoundingBox(minX, minY, minZ, maxX, maxY, maxZ));
	}

	@Override
	protected void onEnable() {
		Huzuni.registerRenderable(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterRenderable(this);
	}

	@Override
	public void render(RenderEvent event) {
		for (Object o : mc.theWorld.loadedEntityList) {
			if (o instanceof EntityLivingBase) {
				EntityLivingBase entity = (EntityLivingBase) o;
				if (entity == mc.thePlayer || entity.isDead || !shouldRender(entity))
					continue;
				float renderX = (float) (event.interpolate(entity.prevPosX, entity.posX) - RenderManager.renderPosX);
				float renderY = (float) (event.interpolate(entity.prevPosY, entity.posY) - RenderManager.renderPosY);
				float renderZ = (float) (event.interpolate(entity.prevPosZ, entity.posZ) - RenderManager.renderPosZ);
				float distance = mc.thePlayer.getDistanceToEntity(entity);
				boolean friend = entity instanceof EntityPlayer && Huzuni.isFriend(entity.getCommandSenderName());
				if (lines.isEnabled()) {
					colorLines(distance, friend, 1F);
					glBegin(GL_LINES);
					glVertex3d(0, 0, 0);
					glVertex3d(renderX, renderY, renderZ);
					glEnd();
				}
				
				if (boxes.isEnabled() && entity instanceof EntityPlayer) {
					colorLines(distance, friend, 0.75F);
					glPushMatrix();
					glTranslatef(renderX, renderY, renderZ);
					glRotatef(-entity.rotationYaw, 0F, 1F, 0F);
					outlinedBox.render();
					colorLines(distance, friend, 0.55F);
					boxBeams.render();
					glPopMatrix();
				}
			}
		}
	}
	
	private boolean shouldRender(EntityLivingBase entityLiving) {
		if (players.isEnabled() && entityLiving instanceof EntityPlayer)
			return true;
		if (mobs.isEnabled() && entityLiving instanceof IMob)
			return true;
		if (animals.isEnabled() && entityLiving instanceof IAnimals)
			return true;
		return false;
	}

	private void colorLines(float distance, boolean friend, float opacity) {
		if (friend) {
			glColor4f(0, 0.5F, 0.75F, opacity);
		} else {
			if (this.distance.isEnabled())
				glColor4f(1F, distance / 64F, 0F, opacity);
			else
				GLUtils.glColor(Huzuni.getColorTheme());
		}
	}
}
