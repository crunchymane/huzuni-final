/**
 *
 */
package net.halalaboos.lib.ui.containers;

import net.halalaboos.lib.ui.DefaultContainer;
import net.halalaboos.lib.ui.modes.Draggable;
import net.halalaboos.lib.ui.theme.Theme;
import net.halalaboos.lib.ui.utils.GLUtils;

import java.awt.*;

import org.lwjgl.input.Mouse;

/**
 * Window used to hold components.
 * 
 * @author Halalaboos
 * @since Aug 16, 2013
 */
public class Window extends DefaultContainer implements Draggable {
	protected int barSize = 12;
    protected int minimizeButtonPadding = 2;
    protected int minimizeButtonSize = 8;
    protected int borderPadding = 2;
    
    protected boolean dragging = false, minimized = true, resizing = false, resizable = true;
    protected Point lastMouseClick = new Point(), lastMouseClickWindowPosition = new Point();
    protected String title;

    protected Color borderColor = new Color(0F, 0F, 0F, 0.75F),
            insideColor = new Color(0F, 0F, 0F, 0.75F),
            minimizeColor = new Color(0F, 0.5F, 1F, 0.75F);

    /**
     * @param area
     * 			The default area for the window.
     * @param title
     * 			The title of the window.
     */
    public Window(String title, Rectangle area) {
        super(area);
        this.title = title;
    }
    
    @Override
    public void update(Point mouse) {
    	super.update(mouse);
    	handleDragging(mouse);
    	handleResizing(mouse);
    }
    
    @Override
	public void renderComponents(Theme theme, Point mouse) {
    	this.mouse.setLocation(mouse);
    	this.offset.setLocation(area.x, area.y + barSize);
    	 if (!minimized) {
            super.renderComponents(theme, mouse);
         }
    }
    
    private void handleResizing(Point mouse) {
    	if (resizing && Mouse.isButtonDown(0)) {
             this.area.width = mouse.x - area.x;
             this.area.height = mouse.y - area.y;
    		 layout();
         } else {
        	 resizing = false;
         }
    }
    
    /**
     * @see net.halalaboos.lib.ui.modes.Draggable#handleDragging(java.awt.Point)
     */
    @Override
    public void handleDragging(Point mouse) {
        if (dragging && Mouse.isButtonDown(0)) {
            this.area.x = mouse.x - (lastMouseClick.x - lastMouseClickWindowPosition.x);
            this.area.y = mouse.y - (lastMouseClick.y - lastMouseClickWindowPosition.y);
        } else
        	dragging = false;
    }

    @Override
    public boolean onMousePressed(int x, int y, int buttonId) {
    	if (mouseOver) {
	        lastMouseClick.setLocation(x, y);
	        lastMouseClickWindowPosition.setLocation(area.x, area.y);
        	if (resizable && isWithinResizablePoint(x, y)) {
        		resizing = true;
        	} else
        		resizing = false;
	        if (getDraggableArea().contains(x, y) && buttonId == 0) {
	            if (getMinimizeArea().contains(x, y)) {
	                minimized = !minimized;
	                dragging = false;
	            } else
	            	dragging = true;
	            return true;
	        }
    	}
        return super.onMousePressed(x, y, buttonId);
    }

    @Override
    public void layout() {
        Rectangle componentArea = layout.layoutComponents(this, components);
        if (area != null) {
            if (componentArea.width > area.width)
                area.width = componentArea.width;
            if (componentArea.height + barSize > area.height)
                area.height = componentArea.height + barSize;
            if (GLUtils.getStringWidth(title) > this.area.width + borderPadding + minimizeButtonPadding + minimizeButtonSize)
            	area.width = GLUtils.getStringWidth(title) - borderPadding - minimizeButtonPadding - minimizeButtonSize - 2;
        }
    }

    public boolean isDragging() {
        return dragging;
    }

    public void setDragging(boolean dragging) {
        this.dragging = dragging;
    }

    public Rectangle getDraggableArea() {
        return new Rectangle(area.x, area.y, area.width, barSize);
    }

    protected boolean isWithinResizablePoint(int x, int y) {
    	int size = 5;
    	Rectangle bottomRight = new Rectangle(area.x + area.width - size, area.y + area.height - size, size, size);
    	return bottomRight.contains(x, y);
    }
    
    @Override
    public Rectangle getArea() {
        if (minimized)
            return getDraggableArea();
        else {
            return area;
        }
    }
    @Override
	public boolean isPointInside(Point point) {
    	if (minimized) {
    		return getDraggableArea().contains(point);
    	} else {
    		return super.isPointInside(point);
    	}
    }
    
    public void center() {
    	area.x = GLUtils.getScreenWidth() / 2 - area.width / 2;
    	area.y = GLUtils.getScreenHeight() / 2 - area.height / 2;
    }

    public Rectangle getMinimizeArea() {
        Rectangle top = getDraggableArea();
        return new Rectangle(top.x + top.width - minimizeButtonSize - minimizeButtonPadding, top.y + minimizeButtonPadding - 1, minimizeButtonSize + 1, top.height - minimizeButtonPadding * 2 + 1);
    }

    public boolean isMinimized() {
        return minimized;
    }

    public void setMinimized(boolean minimized) {
        this.minimized = minimized;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Color getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(Color foregroundColor) {
        this.borderColor = foregroundColor;
    }

    public Color getInsideColor() {
        return insideColor;
    }

    public void setInsideColor(Color backgroundColor) {
        this.insideColor = backgroundColor;
    }

    public Color getMinimizeColor() {
        return minimizeColor;
    }

    public void setMinimizeColor(Color minimizeColor) {
        this.minimizeColor = minimizeColor;
    }

    public int getBarSize() {
        return barSize;
    }

    public void setBarSize(int barSize) {
        this.barSize = barSize;
    }

    public int getMinimizeButtonPadding() {
        return minimizeButtonPadding;
    }

    public void setMinimizeButtonPadding(int minimizeButtonPadding) {
        this.minimizeButtonPadding = minimizeButtonPadding;
    }

    public int getBorderPadding() {
        return borderPadding;
    }

    public void setBorderPadding(int borderPadding) {
        this.borderPadding = borderPadding;
    }

	public boolean isResizable() {
		return resizable;
	}

	public void setResizable(boolean resizable) {
		this.resizable = resizable;
	}

}
