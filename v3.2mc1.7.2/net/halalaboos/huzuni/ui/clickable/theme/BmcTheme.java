/**
 * 
 */
package net.halalaboos.huzuni.ui.clickable.theme;

import static org.lwjgl.opengl.GL11.*;

import java.awt.Color;
import java.awt.Point;
import java.awt.Rectangle;
import java.text.DecimalFormat;

import net.halalaboos.lib.ui.Component;
import net.halalaboos.lib.ui.Container;
import net.halalaboos.lib.ui.components.Button;
import net.halalaboos.lib.ui.components.Dropdown;
import net.halalaboos.lib.ui.components.Label;
import net.halalaboos.lib.ui.components.Slider;
import net.halalaboos.lib.ui.components.SlotComponent;
import net.halalaboos.lib.ui.components.TextField;
import net.halalaboos.lib.ui.containers.Window;
import net.halalaboos.lib.ui.theme.ComponentRenderer;
import net.halalaboos.lib.ui.theme.Theme;
import net.halalaboos.lib.ui.utils.GLUtils;

/**
 * @author Halalaboos
 *
 * @since Oct 6, 2013
 */
public class BmcTheme extends Theme {

	private final Color inside = new Color(70, 70, 70, 151),
	border = new Color(93, 93, 93, 151),
	buttons = new Color(0.4F, 0.4F, 0.4F, 0.75F);
	
	public BmcTheme() {
		super();
		registerRenderer(Button.class, new ComponentRenderer<Button>() {
			
			@Override
			public void render(Container container, Point offset, Button button, Point mouse) {
				Rectangle area = button.getRenderableArea();
				renderButtonRect(area, button.isHighlight(), button.isMouseOver(), button.isMouseDown());
				GLUtils.drawString(button.getTitle(), area.x + area.width / 2 - GLUtils.getStringWidth(button.getTitle()) / 2, area.y + area.height / 2 - GLUtils.getStringHeight(button.getTitle()) / 2 + 2, button.isHighlight() ? 0x00FF00 : 0xFF0000);
			}
			
		});
		registerRenderer(TextField.class, new ComponentRenderer<TextField>() {

            @Override
            public void render(Container container, Point offset, TextField textField, Point mouse) {
				Rectangle area = textField.getRenderableArea();
            	renderButtonRect(area, false, textField.isMouseOver() || textField.isEnabled(), false);
                GLUtils.drawStringWithShadow(textField.getTextForRender(area) + textField.getSelection(), area.x + 2, area.y + area.height / 2 - GLUtils.getStringHeight(textField.getText()) / 2 + 2, textField.getTextColor().getRGB());
            }

        });
		registerRenderer(Slider.class, new ComponentRenderer<Slider>() {
			protected DecimalFormat formatter = new DecimalFormat("#.#");

			@Override
			public void render(Container container, Point offset, Slider slider, Point mouse) {
				Rectangle area = slider.getRenderableArea();
				Rectangle sliderPoint = slider.getSliderPoint();
				
				// Inside
				renderButtonRect(area, false, slider.isMouseOver() && !sliderPoint.contains(mouse), slider.isMouseDown());
				
				// Text
				GLUtils.drawString(slider.getLabel(), area.x + 2, area.y + area.height / 2 - GLUtils.getStringHeight(slider.getLabel()) / 2 + 2, 0xFFFFFF);
				String formattedValue = formatter.format(slider.getValue()) + slider.getValueWatermark();
				GLUtils.drawString(formattedValue, area.x + area.width - GLUtils.getStringWidth(formattedValue) - 2, area.y + area.height / 2 - GLUtils.getStringHeight(formattedValue) / 2 + 2, 0xFFFFFF);
				
				// slider part
				renderButtonRect(sliderPoint, true, slider.isMouseOver() && sliderPoint.contains(mouse), slider.isMouseDown());
			}
			
		});
        registerRenderer(Dropdown.class, new ComponentRenderer<Dropdown>() {

            @Override
            public void render(Container container, Point offset, Dropdown dropdown, Point mouse) {
				Rectangle area = dropdown.getRenderableArea();

            	renderButtonRect(area, false, dropdown.isMouseOver(), dropdown.isMouseDown());
                GLUtils.drawString(dropdown.getTitle() + " (" + dropdown.getSelectedComponentName() + ")", area.x + 2, area.y + area.height / 2 - GLUtils.getStringHeight(dropdown.getTitle()) / 2 + 2, Color.WHITE.getRGB());
                if (dropdown.isDown()) {
                	String[] components = dropdown.getComponents();
        			int yPos = dropdown.getTextPadding() + 2;
        			renderButtonRect(area.x, area.y + dropdown.getTextPadding() + 1, area.x + area.width, area.y + yPos + (dropdown.getTextPadding() * components.length) + 1, false, dropdown.isMouseOver(), dropdown.isMouseDown());
                    
                    for (int i = 0; i < components.length; i++) {
        				Rectangle componentBoundaries = new Rectangle(area.x + 1, area.y + yPos, area.width - 2, dropdown.getTextPadding());
        				boolean mouseOverComponent = componentBoundaries.contains(GLUtils.getMouseX(), GLUtils.getMouseY());
        				renderButtonRect(componentBoundaries, dropdown.getSelectedComponent() == i, mouseOverComponent, dropdown.isMouseDown());
        				GLUtils.drawStringWithShadow(components[i], componentBoundaries.x + 2, componentBoundaries.y + 2, 0xFFFFFF);
        				yPos += dropdown.getTextPadding();
                	}
                }
            }

        });
        registerRenderer(SlotComponent.class, new ComponentRenderer<SlotComponent>() {

            @Override
            public void render(Container container, Point offset, SlotComponent slotComponent, Point mouse) {
				Rectangle area = slotComponent.getRenderableArea();

            	if (slotComponent.shouldRenderBackground()) {
            		renderButtonRect(area, false, false, false);	
            	}
                if (slotComponent.hasEnoughToScroll() && slotComponent.hasSlider()) {
                    Rectangle sliderPoint = slotComponent.getDraggableArea();
                	GLUtils.setColor(slotComponent.getSliderColor());
                	renderButtonRect(sliderPoint, true, slotComponent.isMouseOver(), false);	
                }
                slotComponent.renderElements(area, slotComponent.isMouseOver(), slotComponent.isMouseDown());
            }

        });
        registerRenderer(Label.class, new ComponentRenderer<Label>() {

            @Override
            public void render(Container container, Point offset, Label label, Point mouse) {
            	Rectangle area = label.getRenderableArea();
            	GLUtils.drawString(label.getText(), area.x + 2, area.y + 2, label.getTextColor().getRGB());
            }

        });
	}
	
	private void renderWindowRect(int x, int y, int x1, int y1, float lineWidth) {
		drawBorderedRect(x, y, x1, y1, lineWidth, border.getRGB(), inside.getRGB());
	}
	
	private void renderWindowRect(Rectangle area, float lineWidth) {
		drawBorderedRect(area, lineWidth, border.getRGB(), inside.getRGB());
	}
	
	private void renderButtonRect(Rectangle area, boolean highlight, boolean mouseOver, boolean mouseDown) {
		renderButtonRect(area.x, area.y, area.x + area.width, area.y + area.height, highlight, mouseOver, mouseDown);
	}
	
	private void renderButtonRect(int x, int y, int x1, int y1, boolean highlight, boolean mouseOver, boolean mouseDown) {
		Color inside = GLUtils.getColorWithAffects(highlight ? buttons : this.inside, mouseOver, mouseDown);
		drawRect(x, y, x1, y1, inside.getRGB());
		start2D();
		color(highlight ? inside.getRGB() : border.getRGB());
		glLineWidth(1.0F);
		glBegin(GL_LINES);
		glVertex2d(x, y);
		glVertex2d(x, y1);
		glVertex2d(x1, y1);
		glVertex2d(x1, y);
		glVertex2d(x, y);
		glVertex2d(x1, y);
		glVertex2d(x, y1);
		glVertex2d(x1, y1);
		glEnd();
		end2D();
	}

	public static void drawRect(Rectangle rect, int color) {
		drawRect(rect.getX(), rect.getY(), rect.getMaxX(), rect.getMaxY(), color);
	}

	public static void drawBorderedRect(double x, double y, double x1, double y1, float lineWidth, int borderColor, int insideColor) {
		drawRect(x, y, x1, y1, insideColor);

		start2D();
		startSmooth();
		color(borderColor);
		glLineWidth(lineWidth);
		glBegin(GL_LINES);
		glVertex2d(x, y);
		glVertex2d(x, y1);
		glVertex2d(x1, y1);
		glVertex2d(x1, y);
		glVertex2d(x, y);
		glVertex2d(x1, y);
		glVertex2d(x, y1);
		glVertex2d(x1, y1);
		glEnd();
		end2D();
		endSmooth();
	}

	public static void drawBorderedRect(Rectangle rect, float lineWidth, int borderColor, int insideColor) {
		drawBorderedRect(rect.getX(), rect.getY(), rect.getMaxX(), rect.getMaxY(), lineWidth, borderColor, insideColor);
	}

	public static void drawRect(double x, double y, double x1, double y1, int color) {
		start2D();
		startSmooth();
		glShadeModel(GL_SMOOTH);

		glBegin(GL_QUADS);
		color(color);
		glVertex2d(x1, y);
		glVertex2d(x, y);
		glVertex2d(x, y1);
		glVertex2d(x1, y1);
		glEnd();

		glShadeModel(GL_FLAT);
		endSmooth();
		end2D();
	}
	public static void start2D() {
		glPushMatrix();
		glDisable(GL_DEPTH_TEST);
		glEnable(GL_BLEND);
		glDisable(GL_TEXTURE_2D);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}

	public static void end2D() {
		glEnable(GL_TEXTURE_2D);
		glDisable(GL_BLEND);
		glEnable(GL_DEPTH_TEST);
		glPopMatrix();
	}

	public static void startSmooth() {
		glEnable(GL_LINE_SMOOTH);
		glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	}

	public static void endSmooth() {
		glDisable(GL_LINE_SMOOTH);
		glHint(GL_LINE_SMOOTH_HINT, GL_DONT_CARE);

	}
	public static void color(int color) {
		float red = (float)(color >> 16 & 255) / 255.0F;
		float green = (float)(color >> 8 & 255) / 255.0F;
		float blue = (float)(color & 255) / 255.0F;
		float alpha = (float)(color >> 24 & 255) / 255.0F;
		glColor4f(red, green, blue, alpha);
	}

	/**
	 * @see net.halalaboos.lib.ui.theme.Theme#renderToolTip(java.lang.String, java.awt.Point)
	 */
	@Override
	public void renderToolTip(String tooltip, Point mouse) {
		GLUtils.drawStringWithShadow(tooltip, mouse.x, mouse.y, 0xFFFFFF);
	}

	/**
	 * @see net.halalaboos.lib.ui.theme.Theme#renderContainer(net.halalaboos.lib.ui.Container, java.awt.Point)
	 */
	@Override
	public void renderContainer(Container container, Point mouse) {
		if (container instanceof Window) {
			Window window = (Window) container;
			
			Rectangle draggableDimensions = window.getDraggableArea();
			Rectangle area = window.getArea();
			int barSize = window.getBarSize();
			int borderPadding = window.getBorderPadding();
			
			if (window.isMinimized()) {
            	renderWindowRect(draggableDimensions.x - borderPadding, draggableDimensions.y - borderPadding, draggableDimensions.x + draggableDimensions.width + borderPadding, draggableDimensions.y + draggableDimensions.height, 2F);
			} else
            	renderWindowRect(area.x - borderPadding, area.y - borderPadding, area.x + area.width + borderPadding, area.y + area.height + borderPadding, 2F);

			GLUtils.drawString(window.getTitle(), draggableDimensions.x + 2, draggableDimensions.y + (draggableDimensions.height / 2) - (GLUtils.getStringHeight(window.getTitle()) / 2) + 1, 0xFFFFFF);

			// Minimize button
			Rectangle minimizeDimensions = window.getMinimizeArea();
			renderButtonRect(minimizeDimensions, false, window.isMouseOver() && minimizeDimensions.contains(mouse), false);				
		
		}
	}
}
