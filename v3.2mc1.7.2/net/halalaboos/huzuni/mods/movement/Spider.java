/**
 * 
 */
package net.halalaboos.huzuni.mods.movement;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;

import net.halalaboos.huzuni.events.game.EventTick;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.DefaultModule;

/**
 * @author Halalaboos
 *
 * @since Oct 10, 2013
 */
public class Spider extends DefaultModule implements Listener {

	/**
	 * @param name
	 */
	public Spider() {
		super("Spider");
		setCategory(Category.MOVEMENT);
		setDescription("You climb walls like a spider.");
	}

	/**
	 * @see net.halalaboos.huzuni.mods.DefaultModule#onToggle()
	 */
	@Override
	protected void onToggle() {
	}

	/**
	 * @see net.halalaboos.huzuni.mods.Module#onEnable()
	 */
	@Override
	protected void onEnable() {
		registry.registerListener(this);
	}

	/**
	 * @see net.halalaboos.huzuni.mods.Module#onDisable()
	 */
	@Override
	protected void onDisable() {
		registry.unregisterListener(this);
	}
	
	@EventTarget
	public void onTick(EventTick event) {
		if (mc.thePlayer.isCollidedHorizontally) {
			mc.thePlayer.motionY = 0.2F;
			mc.thePlayer.onGround = true;
		}
	}

}
