package net.halalaboos.huzuni.plugin.plugins.movement;

import org.lwjgl.input.Keyboard;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.plugin.Category;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent;
import net.halalaboos.huzuni.event.tick.UpdateListener;
import net.halalaboos.huzuni.plugin.DefaultPlugin;
import net.minecraft.block.material.Material;

/**
 * @author brudin
 * @version 1.0
 * @since 4/10/14
 */
public class Dolphin extends DefaultPlugin implements UpdateListener {

	public Dolphin() {
		super("Dolphin", Keyboard.KEY_K);
		setDescription("Automatically swims for you.");
		setAuthor("brudin");
		setCategory(Category.MOVEMENT);
	}

	@Override
	protected void onEnable() {
		Huzuni.registerUpdateListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterUpdateListener(this);
	}

	@Override
	public void onPreUpdate(MotionUpdateEvent event) {
		if ((mc.thePlayer.isInWater() || mc.thePlayer.isInsideOfMaterial(Material.lava)) && !mc.gameSettings.keyBindSneak.getIsKeyPressed()
				&& !mc.gameSettings.keyBindJump.getIsKeyPressed()) {
			mc.thePlayer.motionY += 0.039;
		}
	}

	@Override
	public void onPostUpdate(MotionUpdateEvent event) {

	}
}
