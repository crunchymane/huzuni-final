package net.halalaboos.huzuni.mod.mods.world;

import net.minecraft.block.state.IBlockState;
import net.minecraft.network.play.client.CPacketPlayerBlockPlacement;
import net.minecraft.network.play.client.CPacketPlayerDigging;
import net.minecraft.network.play.client.CPacketPlayerTryUseItem;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import org.lwjgl.input.Keyboard;
import org.w3c.dom.Element;
import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.console.Command;
import net.halalaboos.huzuni.api.gui.render.Theme;
import net.halalaboos.huzuni.api.mod.ModeOption;
import net.halalaboos.huzuni.api.mod.Category;
import net.halalaboos.huzuni.api.mod.Option;
import net.halalaboos.huzuni.api.request.SimpleApplicant;
import net.halalaboos.huzuni.command.CommandManager;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent;
import net.halalaboos.huzuni.event.tick.UpdateListener;
import net.halalaboos.huzuni.mod.DefaultMod;
import net.halalaboos.huzuni.util.GLUtils;
import net.halalaboos.huzuni.util.GameUtils;
import net.halalaboos.huzuni.util.MathUtils;
import net.halalaboos.huzuni.util.StringUtils;
import net.halalaboos.huzuni.util.Timer;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class Autofarm extends DefaultMod implements Command, UpdateListener {

	private final Timer timer = new Timer();

	private Block block = null;

	private Item replantItem = Items.wheat_seeds;

	private BlockPos position = null;

	private EnumFacing facing = null;

	private final ModeOption mode;

	private final SimpleApplicant<Object> application = new SimpleApplicant<Object>(this);

	public Autofarm() {
		super("Auto Farm", Keyboard.KEY_H);
		setCategory(Category.WORLD);
		setAliases("farm");
		setDescription("Automatically farms / harvests crops.");
		setOptions(mode = new ModeOption("Farm mode", null, "Harvest", "Plant", "Bonemeal"), new FarmOption());
		CommandManager.registerCommand(this);
		Huzuni.rotationQueue.registerApplicant(application);
	}

	@Override
	public void onPreUpdate(MotionUpdateEvent event) {
		if (hasSelectedBlock()) {
			GameUtils.requestFace(application, true, position.getX() + 0.5F, position.getY() + (mode.getSelected() == 1 ? 1.0F : 0.5F), position.getZ() + 0.5F);
			if (GameUtils.getDistance(position, 0.5F) > 4)
				reset();
			return;
		}
		int radius = 4;

		BlockPos closestPosition = null;
		EnumFacing closestFace = null;
		Block closestBlock = null;

		for (int i = radius; i >= -radius; i--) {
			for (int j = -radius; j <= radius; j++) {
				for (int k = radius; k >= -radius; k--) {
					position = new BlockPos(mc.thePlayer.posX + i, mc.thePlayer.posY + j, mc.thePlayer.posZ + k);
					block = GameUtils.getBlock(position);
					if ((block != null && GameUtils.getMaterial(position) != Material.air) && GameUtils.getDistance(position, 0.5F) < radius) {
						RayTraceResult result = GameUtils.rayTraceOffset(position, 0.5F);
						if (result != null && result.typeOfHit == RayTraceResult.Type.BLOCK && result.getBlockPos().equals(position))
							facing = result.sideHit;
						else
							facing = EnumFacing.UP;

						// bonemeal
						if (mode.getSelected() == 2 && (block == Blocks.carrots || block == Blocks.potatoes || block == Blocks.wheat) && getGrowthLevel(position) < getMaxGrowthLevel(block)) {
							if (closestBlock == null) {
								closestBlock = block;
								closestPosition = position;
								closestFace = facing;
							} else {
								if (GameUtils.getDistance(position, 0.5F) < GameUtils.getDistance(closestPosition, 0.5F)) {
									closestBlock = block;
									closestPosition = position;
									closestFace = facing;
								}
							}
							//replant
						} else if (mode.getSelected() == 1 && block == getSoil(replantItem) && isOpenSoil()) {
							if (closestBlock == null) {
								closestBlock = block;
								closestPosition = position;
								closestFace = facing;
							} else {
								if (GameUtils.getDistance(position, 0.5F) < GameUtils.getDistance(closestPosition, 0.5F)) {
									closestBlock = block;
									closestPosition = position;
									closestFace = facing;
								}
							}
							// Harvest
						} else if (mode.getSelected() == 0) {
							if (block == Blocks.reeds) {
								if (GameUtils.getBlock(position.getX(), position.getY() - 1, position.getZ()) == Blocks.reeds) {
									if (GameUtils.getBlock(position.getX(), position.getY() - 2, position.getZ()) == Blocks.reeds)
										position.add(0, -1, 0);
									if (closestBlock == null) {
										closestBlock = block;
										closestPosition = position;
										closestFace = facing;
									} else {
										if (GameUtils.getDistance(position, 0.5F) < GameUtils.getDistance(closestPosition, 0.5F)) {
											closestBlock = block;
											closestPosition = position;
											closestFace = facing;
										}
									}
								}
							} else if (block == Blocks.cocoa || block == Blocks.nether_wart || block == Blocks.carrots || block == Blocks.potatoes || block == Blocks.wheat) {
								int growthLevel = getGrowthLevel(new BlockPos(position));
								if (block == Blocks.cocoa)
									growthLevel = (growthLevel & 12) >> 2;
								if (growthLevel >= getMaxGrowthLevel(block)) {
									if (closestBlock == null) {
										closestBlock = block;
										closestPosition = position;
										closestFace = facing;
									} else {
										if (GameUtils.getDistance(position, 0.5F) < GameUtils.getDistance(closestPosition, 0.5F)) {
											closestBlock = block;
											closestPosition = position;
											closestFace = facing;
										}
									}
								}
							}
						}
					}
				}
			}
		}
		if (closestBlock != null) {
			position = closestPosition;
			facing = closestFace;
			block = closestBlock;
			GameUtils.requestFace(application, true, position.getX() + 0.5F, position.getY() + (mode.getSelected() == 1 ? 1.0F : 0.5F), position.getZ() + 0.5F);
		} else {
			position = null;
			block = null;
			facing = null;
		}
	}

	@Override
	public void onPostUpdate(MotionUpdateEvent event) {
		if (hasSelectedBlock() && application.hasRequest()) {
			if (timer.hasReach(100)) {
				application.reset();
				if (mode.getSelected() == 0) {
					GameUtils.swingItem();
					sendPacket(0);
					GameUtils.breakBlock(position);
					reset();
				} else if (mode.getSelected() == 1) {
					int itemSlot = GameUtils.findHotbarItem(replantItem);
					if (itemSlot != -1 && mc.thePlayer.inventory.currentItem != itemSlot) {
						mc.thePlayer.inventory.currentItem = itemSlot;
						mc.playerController.updateController();
					}
					if (getGoodHand(replantItem) != null) {
						EnumHand hand = getGoodHand(replantItem);
						sendPlacePacket(hand);
						mc.thePlayer.getHeldItem(hand).onItemUse(mc.thePlayer, mc.theWorld, position, hand, facing, 0, 0, 0);
						mc.thePlayer.swingArm(hand);
						reset();
					}
				} else if (mode.getSelected() == 2) {
					int itemSlot = findBonemeal();
					if (itemSlot != -1 && mc.thePlayer.inventory.currentItem != itemSlot) {
						mc.thePlayer.inventory.currentItem = itemSlot;
						mc.playerController.updateController();
					}
					if (getGoodHand(Items.dye) != null) {
						EnumHand hand = getGoodHand(Items.dye);
						ItemStack item = mc.thePlayer.getHeldItem(hand);
						if (item.getItemDamage() == 15) {
							sendPlacePacket(getGoodHand(Items.dye));
							mc.thePlayer.getHeldItem(hand).onItemUse(mc.thePlayer, mc.theWorld, position, hand, facing, 0, 0, 0);
							mc.thePlayer.swingArm(hand);
							reset();
						}
					}
				}
			}
		}
	}

	private int findBonemeal() {
		for (int o = 0; o < 9; o++) {
			ItemStack item = mc.thePlayer.inventory.getStackInSlot(o);
			if (item != null)
				if (item.getItem() == Items.dye && item.getItemDamage() == 15)
					return o;
		}
		return -1;
	}

	private EnumHand getGoodHand(Item item) {
		ItemStack main = mc.thePlayer.getHeldItem(EnumHand.MAIN_HAND);
		ItemStack off = mc.thePlayer.getHeldItem(EnumHand.OFF_HAND);
		if (main != null && main.getItem() == item) return EnumHand.MAIN_HAND;
		if (off != null && off.getItem() == item) return EnumHand.OFF_HAND;
		return null;
	}

	private boolean hasSelectedBlock() {
		return block != null && block.getMaterial() != Material.air;
	}

	@Override
	protected void onEnable() {
		Huzuni.registerUpdateListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterUpdateListener(this);
		reset();
	}

	private int getGrowthLevel(BlockPos blockPos) {
		IBlockState blockState = mc.theWorld.getBlockState(blockPos);
		return blockState.getBlock().getMetaFromState(blockState);
	}

	private int getMaxGrowthLevel(Block block) {
		if (block == Blocks.carrots || block == Blocks.potatoes || block == Blocks.wheat)
			return 7;
		else if (block == Blocks.nether_wart)
			return 3;
		else if (block == Blocks.cocoa)
			return 2;
		else
			return 0;
	}

	private Block getSoil(Item item) {
		if (item == Items.carrot || item == Items.potato || item == Items.wheat_seeds)
			return Blocks.farmland;
		else if (item == Items.nether_wart)
			return Blocks.soul_sand;
		else
			return null;
	}

	private boolean isOpenSoil() {
		if (replantItem == Items.carrot || replantItem == Items.potato || replantItem == Items.wheat_seeds || replantItem == Items.nether_wart) {
			return GameUtils.getBlock(position.getX(), position.getY() + 1, position.getZ()) == Blocks.air;
		}
		return false;
	}

	private void sendPacket(int mode) {
		CPacketPlayerDigging.Action action = mode == 0 ? CPacketPlayerDigging.Action.START_DESTROY_BLOCK : (mode == 1 ? CPacketPlayerDigging.Action.ABORT_DESTROY_BLOCK : (mode == 2 ? CPacketPlayerDigging.Action.STOP_DESTROY_BLOCK : null));
		mc.getNetHandler().addToSendQueue(new CPacketPlayerDigging(action, position, mc.thePlayer.getHorizontalFacing()));
	}

	private void sendPlacePacket(EnumHand hand) {
//		mc.getNetHandler().addToSendQueue(new CPacketPlayerBlockPlacement(hand));
    	mc.getNetHandler().addToSendQueue(new CPacketPlayerTryUseItem(position, facing, hand, 0, 0, 0));
	}

	private void reset() {
		block = null;
		position = null;
		facing = null;
		timer.reset();
	}

	@Override
	public String[] getAliases() {
		return new String[] { "autofarm", "af", "ah", "autoharvest" };
	}

	@Override
	public String[] getHelp() {
		return new String[] { "autofarm <replant item>" };
	}

	@Override
	public void run(String input, String[] args) {
		String itemName = StringUtils.getAfter(input, 1);
		Item item = GameUtils.getItemFromName(itemName);
		if (item != null) {
			replantItem = item;
			Huzuni.addChatMessage("Now replanting " + itemName + ".");
		} else {
			Huzuni.addChatMessage("Unable to find item!");
		}
	}

	public class FarmOption extends Option {

		private ItemStack[] seeds = new ItemStack[] {
				new ItemStack(Items.wheat_seeds),
				new ItemStack(Items.potato),
				new ItemStack(Items.carrot),
				new ItemStack(Items.nether_wart),
				new ItemStack(Items.reeds)
		};

		public FarmOption() {
			super("Farm Option", null);
		}

		@Override
		public boolean mouseClicked(int x, int y, int buttonId) {
			return this.isPointInside(x, y);
		}

		@Override
		public void mouseReleased(int x, int y, int buttonId) {
			int size = (int) ((getWidth() - 2) / 5F);
			int count = 0, yPos = 12, xPos = 0;
			for (int i = 0; i < seeds.length; i++) {
				if (count >= 5) {
					yPos += size + 1;
					xPos = 0;
					count = 0;
				}
				float[] boxArea = new float[] { area[0] + xPos, area[1] + yPos, size, size };
				if (MathUtils.inside(x, y, boxArea)) {
					replantItem = seeds[i].getItem();
				}
				xPos += size + 1;
				count++;
			}
		}

		@Override
		public void render(Theme theme, int index, float[] area, boolean mouseOver, boolean mouseDown) {
			Huzuni.drawString("Replant Item", area[0] + 1, area[1] + 1, mode.getSelected() == 1 ? 0xAAFFAA : 0xFFFFFF);
			int size = (int) ((getWidth() - 2) / 5F);
			int count = 0, yPos = 12, xPos = 1;
			for (int i = 0; i < seeds.length; i++) {
				if (count >= 5) {
					yPos += size + 1;
					xPos = 0;
					count = 0;
				}
				float[] boxArea = new float[] { area[0] + xPos, area[1] + yPos, size, size };
				theme.renderSlot(0, 0, index, boxArea, seeds[i].getItem() == replantItem, mouseOver && MathUtils.inside(GLUtils.getMouseX(), GLUtils.getMouseY(), boxArea), mouseDown);
				GLUtils.draw2dItem(seeds[i], (int) boxArea[0] + 1, (int) boxArea[1] + 1, 0F);
				xPos += size + 1;
				count++;
			}
		}

		@Override
		public void setArea(float[] area) {
			super.setArea(area);
			area[3] = 30;
		}

		@Override
		public void load(Element element) {
		}

		@Override
		public void save(Element element) {
		}
	}

}
