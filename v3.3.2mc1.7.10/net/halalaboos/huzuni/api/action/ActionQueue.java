package net.halalaboos.huzuni.api.action;

import java.util.ArrayList;
import java.util.List;

public final class ActionQueue <T extends Action> {

	private List<T> actions = new ArrayList<T>();
	
	private ActionQueue() {
		
	}
	
	public boolean runQueue() {
		T action = (T) actions.get(0);
		action.run();
		actions.remove(action);
		return hasQueue();
	}
	
	public boolean hasQueue() {
		return !actions.isEmpty();
	}
	
	public void add(T action) {
		actions.add(action);
	}
	
	public void clear() {
		actions.clear();
	}
}
