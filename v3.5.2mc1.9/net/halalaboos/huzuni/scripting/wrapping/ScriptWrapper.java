package net.halalaboos.huzuni.scripting.wrapping;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.mod.Mod;
import net.halalaboos.huzuni.api.mod.Option;
import net.halalaboos.huzuni.api.mod.ValueOption;
import net.halalaboos.huzuni.mod.DefaultMod;
import net.halalaboos.huzuni.mod.ModManager;
import net.halalaboos.huzuni.scripting.ScriptFunction;
import net.halalaboos.huzuni.scripting.action.Action;
import net.halalaboos.huzuni.scripting.action.ChangeFont;

public final class ScriptWrapper {

	public static final ScriptWrapper instance = new ScriptWrapper();
	
	private Queue<Action> actionQueue = new PriorityQueue<Action>();
		
	private Map<String, ScriptFunction> renderList = new HashMap<String, ScriptFunction>();
	
	private ScriptWrapper() {
		
	}
	
	public void setFont(String fontName, int size) {
		addAction(new ChangeFont(fontName, size));
	}
	
	public void addRenderableFunction(String functionName) {
		addRenderable(new ScriptFunction(functionName));
	}
	
	public Mod[] getMods() {
		return ModManager.getMods().toArray(new Mod[ModManager.getMods().size()]);
	}
	
	public Option get(String mod, String name) {
		return get(getMod(mod), name);
	}
	
	public Option get(Mod mod, String name) {
		for (int i = 0; i < mod.length(); i++) {
			if (mod.get(i).getName().equalsIgnoreCase(name)) {
					return mod.get(i);
			}
		}
		return null;
	}
	
	public Mod getMod(String name) {
		return ModManager.getMod(name);
	}
	
	public boolean isDefaultMod(Mod mod) {
		return mod instanceof DefaultMod;
	}
	
	public void runActions() {
		if (!actionQueue.isEmpty()) {
			Action action = actionQueue.poll();
			action.run();
		}
	}
	
	public void renderList() {
		Collection<ScriptFunction> renderables = renderList.values();
		for (ScriptFunction scriptFunction : renderables) {
			scriptFunction.invoke();
		}
	}
	
	public void addChatMessage(String message) {
		Huzuni.addChatMessage(message);
	}
	
	protected void addAction(Action action) {
		actionQueue.add(action);
	}
	
	protected void removeAction(Action action) {
		actionQueue.remove(action);
	}
	
	protected void addRenderable(ScriptFunction scriptFunction) {
		if (!this.renderList.containsKey(scriptFunction.getFunctionName()))
			this.renderList.put(scriptFunction.getFunctionName(), scriptFunction);
	}
	
	public void removeRenderable(ScriptFunction scriptFunction) {
		if (this.renderList.containsKey(scriptFunction.getFunctionName()))
			this.renderList.remove(scriptFunction.getFunctionName());
	}
	
}
