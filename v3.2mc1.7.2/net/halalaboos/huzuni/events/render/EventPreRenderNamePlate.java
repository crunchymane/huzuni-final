package net.halalaboos.huzuni.events.render;

import com.darkmagician6.eventapi.events.premade.EventCancellable;

import net.minecraft.entity.Entity;

public class EventPreRenderNamePlate extends EventCancellable {

    public final Entity entity;
    private String titleRendered;

    public EventPreRenderNamePlate(Entity entity, String titleRendered) {
        this.entity = entity;
        this.titleRendered = titleRendered;
    }

    public String getTitleRendered() {
        return titleRendered;
    }

    public void setTitleRendered(String titleRendered) {
        this.titleRendered = titleRendered;
    }

}
