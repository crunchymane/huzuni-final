package net.halalaboos.huzuni.command.commands;

import net.halalaboos.huzuni.api.console.Command;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiPlayerInfo;
import net.minecraft.util.StringUtils;

/**
 * @author Halalaboos
 * @since Aug 4, 2013
 */
public class MassMessage implements Command {

    @Override
    public String[] getAliases() {
        return new String[] {"massmessage", "mass", "mm"};
    }

    @Override
    public String[] getHelp() {
        return new String[] {".massmessage <message>"};
    }

    @Override
    public String getDescription() {
        return "Attempts sending that message to all players.";
    }

    @Override
    public void run(String input, String[] args) {
        String message = input.substring((input.split(" ")[0] + " ").length());
        sendMessage(message);
    }

    private void sendMessage(final String message) {
        new Thread() {
            @Override
            public void run() {
                for (int i = 0; i < Minecraft.getMinecraft().getNetHandler().playerInfoList.size(); i++) {
                    GuiPlayerInfo playerInfo = (GuiPlayerInfo) Minecraft.getMinecraft().getNetHandler().playerInfoList.get(i);
                    String name = StringUtils.stripControlCodes(playerInfo.name);
                    if (name.equals(Minecraft.getMinecraft().thePlayer.getCommandSenderName()))
                        continue;
                    Minecraft.getMinecraft().thePlayer.sendChatMessage("/msg " + name + " " + message);
                    try {
                        Thread.sleep(1000L);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (NullPointerException ex) {
                        ex.printStackTrace();
                    }
                }

            }
        }.start();
    }
}
