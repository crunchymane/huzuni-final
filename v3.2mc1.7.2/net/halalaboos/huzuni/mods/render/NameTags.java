package net.halalaboos.huzuni.mods.render;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;

import net.halalaboos.huzuni.client.Settings;
import net.halalaboos.huzuni.events.render.EventPostRenderNamePlate;
import net.halalaboos.huzuni.events.render.EventPreRenderNamePlate;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.DefaultModule;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.EnumChatFormatting;

import org.lwjgl.input.Keyboard;
import static org.lwjgl.opengl.GL11.*;

import static org.lwjgl.opengl.GL11.glScaled;

public class NameTags extends DefaultModule implements Listener {

    public NameTags() {
        super("NameTags", Keyboard.KEY_P);
        setCategory(Category.RENDER);
        setRenderWhenEnabled(false);
        setDescription("Scales nametags when they're far.");
        Settings.config.put("NameTag Scale", 11F);

    }

    @Override
    protected void onEnable() {
        registry.registerListener(this);
    }

    @Override
    protected void onDisable() {
        registry.unregisterListener(this);
    }

    @Override
    protected void onToggle() {
    }

    @EventTarget
    public void onPreRenderNamePlate(EventPreRenderNamePlate event) {
        if (event.entity instanceof EntityPlayer) {
        	EntityPlayer entityPlayer = (EntityPlayer) event.entity;
            glDisable(GL_FOG);
            glEnable(GL_POLYGON_OFFSET_FILL);
            glPolygonOffset(1, -10000000);
            this.mc.entityRenderer.disableLightmap(0);
            double distance = mc.thePlayer.getDistanceToEntity(entityPlayer);
            float scale = 2 + (15 - getNametagScale());
            if ((distance / scale) > 1) {
                glScaled((distance / scale), (distance / scale), (distance / scale));
            }
            for (String friend : huzuni.friendManager.getList())
                event.setTitleRendered(huzuni.friendManager.replaceIgnoreCase(event.getTitleRendered(), friend, "\2479" + friend + "\247r"));

            if ((entityPlayer.getHealth()) + 1 < 15)
                event.setTitleRendered(event.getTitleRendered() + " " + ((entityPlayer.getHealth() + 1) < 5 ? EnumChatFormatting.RED :
                        EnumChatFormatting.GOLD) + (byte) (entityPlayer.getHealth() + 1));
        }
    }

    private float getNametagScale() {
        return Settings.config.getFloat("NameTag Scale");
    }

    @EventTarget
    public void onPostRenderNamePlate(EventPostRenderNamePlate event) {
        if (event.entity instanceof EntityPlayer) {
        	glEnable(GL_FOG);
            this.mc.entityRenderer.enableLightmap(0);
            glDisable(GL_POLYGON_OFFSET_FILL);
            glPolygonOffset(1, 10000000);
        }
    }

}
