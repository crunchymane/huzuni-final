/**
 *
 */
package net.halalaboos.huzuni.console.commands;

import net.halalaboos.huzuni.console.Command;

import java.awt.*;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;

/**
 * @author Halalaboos
 * @since Aug 31, 2013
 */
public class GetIP implements Command {

    /**
     * @see net.halalaboos.huzuni.console.Command#getAliases()
     */
    @Override
    public String[] getAliases() {
        return new String[] {"getip", "ip"};
    }

    /**
     * @see net.halalaboos.huzuni.console.Command#getHelp()
     */
    @Override
    public String[] getHelp() {
        return new String[] {"no"};
    }

    /**
     * @see net.halalaboos.huzuni.console.Command#getDescription()
     */
    @Override
    public String getDescription() {
        return "Copies the server IP to your clipboard.";
    }

    /**
     * @see net.halalaboos.huzuni.console.Command#run(java.lang.String, java.lang.String[])
     */
    @Override
    public void run(String input, String[] args) {
        String ip = mc.currentServerData.serverIP;
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(ip), (ClipboardOwner) null);
        huzuni.addChatMessage(ip + " copied to your clipboard.");
    }

}
