package in.brud.huzuni.ui.window.theme;

import net.halalaboos.huzuni.utils.RenderUtils;
import net.halalaboos.lib.ui.components.*;
import net.halalaboos.lib.ui.theme.ComponentRenderer;
import net.halalaboos.lib.ui.theme.Theme;
import net.halalaboos.lib.ui.utils.GLUtils;
import net.minecraft.client.Minecraft;

import java.awt.*;
import java.text.DecimalFormat;

/**
 * DESCRIPTION
 *
 * @author brudin
 * @version 1.0
 * @since 1/26/14
 */
public class HuzuniThemeRedux extends Theme {
    protected static final Color tooltipColor = new Color(0F, 0.5F, 1F, 0.75F);

    public HuzuniThemeRedux() {
        registerRenderer(net.halalaboos.lib.ui.components.Button.class, new ComponentRenderer<net.halalaboos.lib.ui.components.Button>() {

            @Override
            public void render(net.halalaboos.lib.ui.Container container, Point offset, net.halalaboos.lib.ui.components.Button button, Point mouse) {
                Rectangle area = button.getRenderableArea();
                boolean on = button.isHighlight();
                renderButton(area, on, button.isMouseOver(), button.isMouseDown());
                GLUtils.drawString(button.getTitle(), area.x + area.width / 2 - GLUtils.getStringWidth(button.getTitle()) / 2,
                        area.y + area.height / 2 - GLUtils.getStringHeight(button.getTitle()) / 2 + 2, button.getTextColor().getRGB());
            }
        });
        registerRenderer(net.halalaboos.lib.ui.components.TextArea.class, new ComponentRenderer<net.halalaboos.lib.ui.components.TextArea>() {
            @Override
            public void render(net.halalaboos.lib.ui.Container container, Point offset, net.halalaboos.lib.ui.components.TextArea textArea, Point mouse) {
                Rectangle area = textArea.getRenderableArea();
                GLUtils.setColor(GLUtils.getColorWithAffects(textArea.getBackgroundColor(), textArea.isMouseOver() && textArea.isEditable(), false));
                GLUtils.drawFilledRect(area.x, area.y, area.x + area.width, area.y + area.height);
                GLUtils.setColor(GLUtils.getColorWithAffects(textArea.getBackgroundColor().darker().darker(), false, false));
                GLUtils.drawRect(area.x, area.y, area.x + area.width, area.y + area.height, 1F);
                textArea.renderText(area, 1);
            }
        });
        registerRenderer(Dropdown.class, new ComponentRenderer<Dropdown>() {

            @Override
            public void render(net.halalaboos.lib.ui.Container container, Point offset, Dropdown dropdown, Point mouse) {
                Rectangle area = dropdown.getRenderableArea();
                GLUtils.setColor(GLUtils.getColorWithAffects(dropdown.getTopColor(), dropdown.isMouseOver(), dropdown.isMouseDown()));
                GLUtils.drawFilledRect(area.x, area.y, area.x + area.width, area.y + area.height);

                GLUtils.drawString(dropdown.getTitle() + " (" + dropdown.getSelectedComponentName() + ")", area.x + 2, area.y + area.height / 2 - GLUtils.getStringHeight(dropdown.getTitle()) / 2 + 2, Color.WHITE.getRGB());
                if (dropdown.isDown()) {
                    String[] components = dropdown.getComponents();
                    int yPos = dropdown.getTextPadding() + 2;

                    GLUtils.setColor(GLUtils.getColorWithAffects(dropdown.getBackgroundColor(), dropdown.isMouseOver(), dropdown.isMouseDown()));
                    GLUtils.drawFilledRect(area.x, area.y + dropdown.getTextPadding() + 1, area.x + area.width, area.y + yPos + (dropdown.getTextPadding() * components.length) + 1);

                    for (int i = 0; i < components.length; i++) {
                        Rectangle componentBoundaries = new Rectangle(area.x + 1, area.y + yPos, area.width - 2, dropdown.getTextPadding());
                        boolean mouseOverComponent = componentBoundaries.contains(GLUtils.getMouseX(), GLUtils.getMouseY());
                        Color componentColor = GLUtils.getColorWithAffects(dropdown.getSelectedComponent() == i ? dropdown.getSelectedComponentColor() : Color.WHITE, mouseOverComponent, dropdown.isMouseDown());

                        GLUtils.setColor(GLUtils.getColorWithAffects(dropdown.getDropdownColor(), mouseOverComponent, dropdown.isMouseDown()));
                        GLUtils.drawFilledRect(componentBoundaries.x, componentBoundaries.y, componentBoundaries.x + componentBoundaries.width, componentBoundaries.y + componentBoundaries.height);
                        GLUtils.drawStringWithShadow(components[i], componentBoundaries.x + 2, componentBoundaries.y + 2, componentColor.getRGB());
                        yPos += dropdown.getTextPadding();
                    }
                }
            }

        });
        registerRenderer(ProgressBar.class, new ComponentRenderer<ProgressBar>() {

            public void render(net.halalaboos.lib.ui.Container container, Point offset, ProgressBar progressBar, Point mouse) {
                Rectangle area = progressBar.getRenderableArea();
                GLUtils.setColor(GLUtils.getColorWithAffects(progressBar.getBackgroundColor(), progressBar.isMouseOver(), false));
                GLUtils.drawFilledRect(area.x, area.y, area.x + area.width, area.y + area.height);

                GLUtils.setColor(GLUtils.getColorWithAffects(progressBar.getBarColor(), progressBar.isMouseOver(), false));
                GLUtils.drawFilledRect(area.x, area.y, area.x + (int) ((progressBar.getProgress() / progressBar.getMaximumVal()) * area.width), area.y + area.height);

                String renderString = progressBar.getTitle() + " " + (int) progressBar.getProgress() + " " + progressBar.getProgressWatermark();
                GLUtils.drawString(renderString, area.x + area.width / 2 - GLUtils.getStringWidth(renderString) / 2, area.y + area.height / 2 - GLUtils.getStringHeight(renderString) / 2 + 1, progressBar.getTextColor().getRGB());
            }

        });
        registerRenderer(Slider.class, new ComponentRenderer<Slider>() {
            protected DecimalFormat formatter = new DecimalFormat("#.#");

            @Override
            public void render(net.halalaboos.lib.ui.Container container, Point offset, Slider slider, Point mouse) {
                Rectangle area = slider.getRenderableArea();
                Rectangle sliderPoint = slider.getSliderPoint();
               
                renderButton(area, false, slider.isMouseOver(), slider.isMouseDown());

                GLUtils.drawString(slider.getLabel(), area.x + 2, area.y + area.height / 2 - GLUtils.getStringHeight(slider.getLabel()) / 2 + 2, 0xFFFFFF);

                String formattedValue = formatter.format(slider.getValue()) + slider.getValueWatermark();
                GLUtils.drawString(formattedValue, area.x + area.width - GLUtils.getStringWidth(formattedValue) - 2, area.y + area.height / 2 - GLUtils.getStringHeight(formattedValue) / 2 + 2, 0xFFFFFF);

                renderButton(sliderPoint, true);
            }

        });
        registerRenderer(net.halalaboos.lib.ui.components.TextField.class, new ComponentRenderer<net.halalaboos.lib.ui.components.TextField>() {
            @Override
            public void render(net.halalaboos.lib.ui.Container container, Point offset, net.halalaboos.lib.ui.components.TextField textField, Point mouse) {
                Rectangle area = textField.getRenderableArea();

                GLUtils.setColor(GLUtils.getColorWithAffects(textField.getBackgroundColor(), textField.isMouseOver() || textField.isEnabled(), false));
                GLUtils.drawFilledRect(area.x, area.y, area.x + area.width, area.y + area.height);
                GLUtils.drawStringWithShadow(textField.getTextForRender(area) + textField.getSelection(), area.x + 2, area.y + area.height / 2 - GLUtils.getStringHeight(textField.getText()) / 2 + 2, textField.getTextColor().getRGB());
            }

        });
        registerRenderer(SlotComponent.class, new ComponentRenderer<SlotComponent>() {

            @Override
            public void render(net.halalaboos.lib.ui.Container container, Point offset, SlotComponent slotComponent, Point mouse) {
                Rectangle area = slotComponent.getRenderableArea();
                if (slotComponent.shouldRenderBackground()) {
                    RenderUtils.drawRect(area.x, area.y, area.x + area.width, area.y + area.height, 0x5F222222);
                }
                if (slotComponent.hasEnoughToScroll() && slotComponent.hasSlider()) {
                    Rectangle sliderPoint = slotComponent.getDraggableArea();
                    boolean isHighlight = slotComponent.isMouseOver() && Minecraft.getMinecraft().currentScreen != null;
                    renderButton(sliderPoint, isHighlight);
                }
                slotComponent.renderElements(area, slotComponent.isMouseOver(), slotComponent.isMouseDown());
            }


        });
        this.registerRenderer(net.halalaboos.lib.ui.components.Label.class, new ComponentRenderer<net.halalaboos.lib.ui.components.Label>() {

            @Override
            public void render(net.halalaboos.lib.ui.Container container, Point offset, net.halalaboos.lib.ui.components.Label label, Point mouse) {
                Rectangle area = label.getRenderableArea();
                GLUtils.drawString(label.getText(), area.x + 2, area.y + 2, label.getTextColor().getRGB());
            }

        });
    }

    /**
     * @see net.halalaboos.lib.ui.theme.Theme#renderToolTip(java.lang.String, java.awt.Point)
     */
    @Override
    public void renderToolTip(String tooltip, Point mouse) {
        int aboveMouse = 8;
        int width = GLUtils.getStringWidth(tooltip);
        GLUtils.setColor(tooltipColor.brighter());
        GLUtils.drawRect(mouse.x - 1, mouse.y - aboveMouse - 1, mouse.x + width + 5, mouse.y + GLUtils.getStringHeight(tooltip) - aboveMouse + 3, 1);
        GLUtils.setColor(tooltipColor);
        GLUtils.drawFilledRect(mouse.x, mouse.y - aboveMouse, mouse.x + width + 4, mouse.y + GLUtils.getStringHeight(tooltip) - aboveMouse + 2);
        GLUtils.drawStringWithShadow(tooltip, mouse.x + 2, mouse.y - aboveMouse + 2, 0xFFFFFF);
    }

    @Override
    public void renderContainer(net.halalaboos.lib.ui.Container container, Point mouse) {
        if (container instanceof net.halalaboos.lib.ui.containers.Window) {
            net.halalaboos.lib.ui.containers.Window window = (net.halalaboos.lib.ui.containers.Window) container;
            // Top draggable part
            Rectangle draggableDimensions = window.getDraggableArea();
            int borderPadding = window.getBorderPadding();

            RenderUtils.drawGradientRect(draggableDimensions.x - borderPadding, draggableDimensions.y - borderPadding,
                    draggableDimensions.x + draggableDimensions.width + borderPadding,
                    draggableDimensions.y + draggableDimensions.height, 0xFF262626, 0xFF333436);

            // Minimize button
            Rectangle minimizeArea = window.getMinimizeArea();
            renderButton(minimizeArea, !window.isMinimized(), false, window.isMouseDown() && minimizeArea.contains(mouse));
//            renderButton(minimizeArea, !window.isMinimized());

            // Title
            GLUtils.drawString(window.getTitle(), draggableDimensions.x + 2, draggableDimensions.y + (draggableDimensions.height / 2) - (GLUtils.getStringHeight(window.getTitle()) / 2) + 1, 0xFFFFFF);

            // Render slightly below the actual dimensions.
            if (!window.isMinimized()) {
                Rectangle windowArea = window.getArea();
                int barSize = window.getBarSize();
                RenderUtils.drawGradientRect(windowArea.x - borderPadding, windowArea.y + barSize - borderPadding, windowArea.x +
                        windowArea.width + borderPadding, windowArea.y + windowArea.height + borderPadding, 0x7F0F0F0F, 0x7F151517);
            }
        }
    }

    private void renderButton(Rectangle area, boolean on, boolean hover, boolean down) {
        int borderColor = down && hover ? on ? 0xFF05171e : 0xFF939393 : on ? 0xFF05171e : 0xFF5c5c5c;
        int borderColorTwo = down && hover ? on ? 0xFF05171e : 0xFF5c5c5c : on ? 0xFF05171e : 0xFF939393;
        int gradTopBG = down && hover ? on ? 0xFF006793 : 0xFF333333 : on ? 0xFF00405c  : 0xFF1c1c1c;
        int gradBottomBG = down && hover ? on ? 0xFF00405c  : 0xFF1c1c1c : on ? 0xFF006793 : 0xFF333333;
        int gradTopMain = down && hover ? on ? 0xFF005478 : 0xFF262626 : on ? 0xFF003c56 : 0xFF222222;
        int gradBottomMain = down && hover ? on ? 0xFF003c56 : 0xFF222222 : on ? 0xFF005478 : 0xFF262626;

        RenderUtils.drawBorderedRect(area.x, area.y, area.x + area.width, area.y + area.height, 0.5F, borderColor, borderColorTwo);
        RenderUtils.drawGradientRect(area.x, area.y, area.x + area.width, area.y + area.height,
                gradTopBG, gradBottomBG);
        RenderUtils.drawGradientRect(area.x + 0.5F, area.y + 0.5F, area.x + area.width - 0.5F, area.y + area.height - 0.5F,
                gradTopMain, gradBottomMain);
        if(hover) {
            RenderUtils.drawRect(area.x + 0.5F, area.y + 0.5F, area.x + area.width - 0.5F, area.y + area.height - 0.5F, 0x1F000000);
        }
    }

    private void renderButton(Rectangle area, boolean on) {
        renderButton(area, on, false, false);
    }
}
