package net.halalaboos.huzuni.mod.mods.combat;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.mod.Category;
import net.halalaboos.huzuni.api.mod.ValueOption;
import net.halalaboos.huzuni.event.packet.PacketEvent;
import net.halalaboos.huzuni.event.packet.PacketListener;
import net.halalaboos.huzuni.mod.DefaultMod;
import net.minecraft.network.play.server.SPacketEntityVelocity;
import net.minecraft.network.play.server.SPacketExplosion;

/**
 * @author brudin
 * @version 1.0
 * @since 3/25/14
 */
public class Antiknockback extends DefaultMod implements PacketListener {

	public static final Antiknockback instance = new Antiknockback();
	
	private ValueOption ratio = new ValueOption("Ratio", 0F, 80F, 100F, 5F, "Ratio of knockback that will be ignored."); 
	
	private Antiknockback() {
		super("Anti Knockback");
		setAuthor("brudin");
		setDescription("Stops you from receiving knockback velocity.");
		setAliases("antivelocity", "ave", "nokb");
		setCategory(Category.COMBAT);
		setVisible(false);
		this.setOptions(ratio);
	}

	@Override
	public void onPacket(PacketEvent event, PacketEvent.PacketType type) {
		if(event.getPacket() instanceof SPacketEntityVelocity) {
			SPacketEntityVelocity packet = (SPacketEntityVelocity) event.getPacket();
			if (packet.getEntityID() == mc.thePlayer.getEntityId()) {
				if (ratio.getValue() == 1F) {
					event.setCancelled(true);
				} else {
					float percent = 1F - (ratio.getValue() / 100F);
					event.setPacket(new SPacketEntityVelocity(packet.getEntityID(), percent * (packet.getMotionX() / 8000.0D), percent * (packet.getMotionY() / 8000.0D), percent * (packet.getMotionZ() / 8000.0D)));
				}
			}
		} else if (event.getPacket() instanceof SPacketExplosion) {
			event.setCancelled(true);
		}
	}

	@Override
	protected void onEnable() {
		Huzuni.registerPacketListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterPacketListener(this);
	}

	public boolean isPushedByWater() {
		return false;
	}
}
