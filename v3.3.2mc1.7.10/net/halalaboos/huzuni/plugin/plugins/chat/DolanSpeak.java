package net.halalaboos.huzuni.plugin.plugins.chat;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.plugin.Category;
import net.halalaboos.huzuni.event.packet.PacketEvent;
import net.halalaboos.huzuni.event.packet.PacketListener;
import net.halalaboos.huzuni.plugin.VipPlugin;
import net.minecraft.network.play.client.C01PacketChatMessage;

/**
 * @author brudin
 * @version 1.0
 * @since 3/25/14
 */
public class DolanSpeak extends VipPlugin implements PacketListener {

	public DolanSpeak() {
		super("Dolan Speak");
		setAuthor("BnB Corp");
		setDescription("maeks yu tlak lik this");
		setCategory(Category.CHAT);
	}

	@Override
	public void onPacket(PacketEvent event, PacketEvent.PacketType type) {
		if(type == PacketEvent.PacketType.SENT) {
			if(event.getPacket() instanceof C01PacketChatMessage) {
				C01PacketChatMessage packetChatMessage = (C01PacketChatMessage)event.getPacket();
				String message = packetChatMessage.func_149439_c();
				if(!message.startsWith("/") || !message.startsWith(Huzuni.COMMAND_PREFIX)) {
					event.setPacket(new C01PacketChatMessage(toDolanSpeak(message)));
				}
			}
		}
	}

	@Override
	protected void onEnable() {
		Huzuni.registerPacketListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterPacketListener(this);
	}

	private String toDolanSpeak(String s) {
		if (s.startsWith("/"))
			return s;

		s = s.replaceAll("(?i) and ", " & ")
				.replaceAll("(?i) are ", " r ").replaceAll("(?i)that", "tht")
				.replaceAll("(?i)this", "dis").replaceAll("(?i)the", "da")
				.replaceAll("(?i)er ", "a ").replaceAll("(?i)er", "ir")
				.replaceAll("(?i)eou", "u").replaceAll("(?i)se", "s")
				.replaceAll("(?i)ce", "s").replaceAll("(?i)ci", "s")
				.replaceAll("(?i)ai", "a").replaceAll("(?i)ie", "ei")
				.replaceAll("(?i)cr", "kr").replaceAll("(?i)ft", "f")
				.replaceAll("(?i)ke", "k").replaceAll("(?i)kr", "qw")
				.replaceAll("(?i)ll", "l").replaceAll("(?i)what", "wat")
				.replaceAll("(?i)aw", "ow").replaceAll("(?i) i ", " me ")
				.replaceAll("(?i) am ", " is ").replaceAll("(?i)cks", "x")
				.replaceAll("(?i)ks", "x").replaceAll("(?i)ck", "k")
				.replaceAll("(?i)ng", "n").replaceAll("(?i)mb", "m")
				.replaceAll("(?i)ot", "it").replaceAll("(?i)nn", "n")
				.replaceAll("(?i)gh ", " ").replaceAll("(?i)check", "czech")
				.replaceAll("(?i)ou", "u").replaceAll("(?i)es ", "s ")
				.replaceAll("(?i)your", "ur").replaceAll("(?i)jesus", "jebus")
				.replaceAll("(?i)ith", "if").replaceAll("(?i)ph", "f");
		return s;
	}
}
