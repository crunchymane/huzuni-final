package net.halalaboos.huzuni.mod.mods.world;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.mod.Category;
import net.halalaboos.huzuni.event.packet.PacketEvent;
import net.halalaboos.huzuni.event.packet.PacketEvent.PacketType;
import net.halalaboos.huzuni.event.packet.PacketListener;
import net.halalaboos.huzuni.mod.DefaultMod;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityBoat;
import net.minecraft.entity.item.EntityMinecart;
import net.minecraft.network.play.client.CPacketUseEntity;
import net.minecraft.util.EnumHand;

public class Vehicleonehit extends DefaultMod implements PacketListener {

    public Vehicleonehit() {
        super("Vehicle 1-hit", -1);
        setCategory(Category.WORLD);
		setAliases("1hit", "v1h");
        setDescription("Breaks vehicles in one hit.");
		setVisible(false);
    }

    @Override
    protected void onEnable() {
    	Huzuni.registerPacketListener(this);
    }

    @Override
    protected void onDisable() {
    	Huzuni.unregisterPacketListener(this);
    }
    
    private boolean isVehicle(Entity entity) {
        return entity != null && (entity instanceof EntityBoat || entity instanceof EntityMinecart);
    }
    
    int count = 0;

	@Override
	public void onPacket(PacketEvent event, PacketType type) {
		if (type == PacketType.SENT) {
			if (event.getPacket() instanceof CPacketUseEntity) {
				if (count > 0)
					count--;
				else {
					Entity entity = ((CPacketUseEntity) event.getPacket()).getEntityFromWorld(mc.theWorld);
			    	if (isVehicle(entity)) {
			    		count = 6;
			    		for (byte b = 0; b < 6; b++)
			    			mc.getNetHandler().addToSendQueue(new CPacketUseEntity(entity));
			    	}
				}
			}
		}
	}

}
