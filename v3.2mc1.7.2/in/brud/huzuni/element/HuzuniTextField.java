package in.brud.huzuni.element;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.utils.Timer;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiTextField;

import java.awt.*;

/**
 * A custom text field.  Looks pretty fancy.
 *
 * @author brudin
 * @version 1.0
 * @since 1/5/14
 *
 * TODO: Fix strings being too long in the text field.
 */
public class HuzuniTextField extends GuiTextField {

    int alpha = 10;
    private long fade;

    public HuzuniTextField(FontRenderer par1FontRenderer, int par2, int par3, int par4, int par5) {
        super(par1FontRenderer, par2, par3, par4, par5);
    }

    @Override
    public void drawTextBox(){
        Color background = new Color(0, 0, 0, alpha);
        drawRect(x, y, x + width, y + height, background.getRGB());
        Huzuni.display.guiFontRenderer.drawString(text, x + 4, y + 7, -1);
        updateColor();
    }

    private void updateColor() {
        if (Timer.getSystemTime() - fade >= 15) {
            if (isFocused())
                alpha += 5;
            else
                alpha -= 5;
            fade = Timer.getSystemTime();
        }
        if (alpha < 75)
            alpha = 75;
        if (alpha > 175)
            alpha = 175;
    }
}
