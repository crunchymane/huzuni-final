package net.halalaboos.huzuni.gui.widgets;

import net.halalaboos.huzuni.api.gui.WidgetManager;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.chunk.Chunk;

public class BiomeWidget extends BackgroundWidget {

	public BiomeWidget(WidgetManager menuManager) {
		super("Biome", "Renders the biome you are currently inside", menuManager);
	}

	@Override
	public void renderMenu(int x, int y, int width, int height) {
		super.renderMenu(x, y, width, height);
        Chunk currentChunk = mc.theWorld.getChunkFromBlockCoords(new BlockPos(MathHelper.floor_double(mc.thePlayer.posX), ((int) mc.thePlayer.posY), MathHelper.floor_double(mc.thePlayer.posZ)));
        String biome = "Biome: " + currentChunk.getBiome(new BlockPos(MathHelper.floor_double(mc.thePlayer.posX) & 15, mc.thePlayer.posY, MathHelper.floor_double(mc.thePlayer.posZ) & 15), mc.theWorld.getBiomeProvider()).getBiomeName();
		theme.drawStringWithShadow(biome, x, y, 0xFFFFFF);
		this.setWidth(theme.getStringWidth(biome) + 2);
		this.setHeight(theme.getStringHeight(biome));
	}
}
