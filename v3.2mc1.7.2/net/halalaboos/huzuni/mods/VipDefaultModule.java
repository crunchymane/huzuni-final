/**
 * 
 */
package net.halalaboos.huzuni.mods;

import net.halalaboos.huzuni.client.Settings;

/**
 * @author Halalaboos
 *
 */
public abstract class VipDefaultModule extends DefaultModule {

	/**
	 * @param name
	 */
	public VipDefaultModule(String name) {
		super(name);
		
	}

	@Override
	public void setEnabled(boolean enabled) {
		if (Settings.isVip()) {
			super.setEnabled(enabled);
		} else {
			huzuni.addChatMessage("You must be VIP to use this!");
		}
	}

}
