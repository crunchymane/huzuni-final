/**
 *
 */
package net.halalaboos.huzuni.gui.ingame.themes;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.mod.DefaultMod;
import net.halalaboos.huzuni.mod.ModManager;
import net.halalaboos.huzuni.util.GLUtils;
import net.minecraft.client.Minecraft;
import net.halalaboos.huzuni.api.mod.*;
import net.halalaboos.huzuni.gui.ingame.IngameTheme;

import org.lwjgl.input.Keyboard;

import java.awt.*;

/**
 * @author Halalaboos
 * @since Aug 3, 2013
 */
public class Paged implements IngameTheme {
    
	private int page = 0;

	private final Minecraft mc = Minecraft.getMinecraft();

	private final Color inside = new Color(70, 70, 70, 151),
			border = new Color(93, 93, 93, 255);
	
    public Paged() {
    }

    /**
     * @see net.halalaboos.huzuni.ui.ingame.IngameTheme#render(net.minecraft.src.Minecraft, int, int)
     */
    @Override
    public void render(Minecraft mc, int screenWidth, int screenHeight) {
        mc.fontRenderer.drawStringWithShadow(Huzuni.TITLE + "\247r v" + Huzuni.VERSION, 2, 2, 0xFFFFFF);

        Category category = Category.values()[page];
        String title = category.formalName;
        int[] dimensions = getMaxDimensions();
        int y = 12,
                width = dimensions[0], height = dimensions[1];

        GLUtils.drawBorderedRect(2, y, 4 + width, y + height + 2, 1F, inside.getRGB(), border.getRGB());
        
        mc.fontRenderer.drawStringWithShadow(title, 4 + width / 2 - mc.fontRenderer.getStringWidth(title) / 2, y + 2, 0xFFFFFF);
        y += 11;
        
        String left = (Keyboard.isKeyDown(Keyboard.KEY_LEFT) ? "\247a" : "\247f") + "<--";
        mc.fontRenderer.drawStringWithShadow(left, 4, y + 2, 0xFFFFFF);
        String right = (Keyboard.isKeyDown(Keyboard.KEY_RIGHT) ? "\247a" : "\247f") + "-->";
        mc.fontRenderer.drawStringWithShadow(right, width - mc.fontRenderer.getStringWidth(right) + 2, y + 2, 0xFFFFFF);
        y += 11;

        for (DefaultMod mod : ModManager.getDefaultMods()) {
            if (mod.getCategory() == category && mod.isBound()) {
                String key = "[" + mod.getKeyName() + "]";
            	String name = mod.getName();
                if (mod.isEnabled()) {
	                GLUtils.glColor(Huzuni.getColorTheme(), 0.25F);
	                GLUtils.drawRect(4, y, 2 + width, y + 11);
                }
                mc.fontRenderer.drawStringWithShadow(key, 6, y + 2, mod.isEnabled() ? 0x00FF00 : 0xFF0000);
                mc.fontRenderer.drawStringWithShadow(name, 6 + mc.fontRenderer.getStringWidth(key) + 2, y + 2, 0xFFFFFF);
                y += 11;
            }
        }
    }

    /**
     * @return Maximum width of the current page.
     */
    private int[] getMaxDimensions() {
    	Category category = Category.values()[page];
        int width = 80, height = 22;
        for (DefaultMod mod : ModManager.getDefaultMods()) {
            if (mod.getCategory() == category && mod.isBound()) {
                String name = (mod.isEnabled() ? "\2472" : "\2474") + "[" + mod.getKeyName() + "] \247f" + mod.getName();
                if (mc.fontRenderer.getStringWidth(name) + 6 > width)
                    width = mc.fontRenderer.getStringWidth(name) + 6;
                height += 11;
            }
        }
        return new int[] { width, height };
    }

    /**
     * @see net.halalaboos.huzuni.ui.ingame.IngameTheme#getName()
     */
    @Override
    public String getName() {
        return "Paged";
    }

    /**
     * Keeps our page within the page limits.
     */
    private void keepWithinBoundaries() {
        int size = Category.values().length;
        
        if (page >= size)
            page = 0;
        else if (page < 0)
            page = size - 1;
    }

    /**
     * @see net.halalaboos.huzuni.ui.ingame.IngameTheme#onKeyTyped(int)
     */
    @Override
    public void onKeyTyped(int keyCode) {
        if (keyCode == Keyboard.KEY_RIGHT)
            page++;
        else if (keyCode == Keyboard.KEY_LEFT)
            page--;
        keepWithinBoundaries();
        if (!isValidPage())
        	onKeyTyped(keyCode == Keyboard.KEY_RIGHT || keyCode == Keyboard.KEY_LEFT ? keyCode : Keyboard.KEY_RIGHT);
        	
    }
    
    private boolean isValidPage() {
        Category category = Category.values()[page];
        for (DefaultMod mod : ModManager.getDefaultMods()) {
            if (mod.getCategory() == category && mod.isBound()) {
            	return true;
            }
        }
        return false;
    }

}
