package net.halalaboos.huzuni.helpers;

import net.halalaboos.huzuni.Huzuni;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.monster.IMob;
import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MathHelper;
import net.minecraft.util.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class EntityHelper implements MinecraftHelper {
    public PriorityType priorityType = PriorityType.CLOSEST_MOUSE;

    /**
     * @return List of all the living entities.
     */
    public List<EntityLivingBase> getLivingEntities() {
    	List<EntityLivingBase> tempList = Collections.synchronizedList(new ArrayList<EntityLivingBase>());
    	for (Entity entity : (List<Entity>) mc.theWorld.loadedEntityList) {
    		if (entity instanceof EntityLivingBase) {
    			tempList.add((EntityLivingBase) entity);
    		}
    	}
    	return tempList;

    }

    /**
     * @return List of players.
     */
    public List<EntityPlayer> getPlayers() {
        return mc.theWorld.playerEntities;
    }

    /**
     * @param entityType
     *         the Type of entity we're looking for.
     * @param checkThread
     *         Whether or not to check if they're a bigger threat or not.
     * @return The closest entity that is an instance of the class specified.
     */
    public EntityLivingBase getClosestEntity(Class<? extends EntityLivingBase> entityType, boolean mob, boolean animal, boolean player) {
        EntityLivingBase closestSpecifiedEntity = null;
        for (EntityLivingBase entity : getLivingEntities()) {
            if (!isAliveNotUs(entity) && !checkType(entity, mob, animal, player))
                continue;
            if (entity.getClass() == entityType) {
                if (closestSpecifiedEntity != null) {
                    if (isCloser(closestSpecifiedEntity, entity))
                        closestSpecifiedEntity = entity;
                } else
                    closestSpecifiedEntity = entity;

            }
        }
        return closestSpecifiedEntity;
    }

    /**
     * @return the most bad-ass entity between the two specified.
     */
    private boolean isBiggerThreat(EntityLivingBase currentEntity,
                                   EntityLivingBase otherEntity, float distance, float extender) {
        boolean isPriority = false,
                currentIsThreat = isPlayer(currentEntity) || isMob(currentEntity),
                otherIsThreat = isPlayer(otherEntity) || isMob(otherEntity);

        // If we can't reach the other entity, even with our extender, don't return true.
        if (!(isWithinDistance(otherEntity, distance + extender)))
            return false;

        // If we can't reach our current entity without the extender, but we CAN with our OTHER entity, return true.
        if (!isWithinDistance(currentEntity, distance) && isWithinDistance(otherEntity, distance))
            return true;

        switch (priorityType) {
            case CLOSEST:
                isPriority = isCloser(currentEntity, otherEntity);
                break;

            case CLOSEST_MOUSE:
            	float otherDist = getDistanceFromMouse(otherEntity),
            	currentDist = getDistanceFromMouse(currentEntity);
                isPriority = (otherDist < currentDist || currentDist == -1) && otherDist != -1;
                break;
        }

        return isPriority;
    }

    /**
     * @return True if the entity is alive and not equal to us.
     */
    public boolean isAliveNotUs(EntityLivingBase entity) {
        return (entity == null) ? false : (entity.isEntityAlive() && entity != mc.thePlayer);
    }
    
    private boolean checkType(EntityLivingBase entity, boolean mob, boolean animal, boolean player) {
    	if (mob && entity instanceof EntityMob)
    		return true;
    	if (animal && entity instanceof EntityAnimal)
    		return true;
    	if (player && entity instanceof EntityPlayer)
    		return true;
    	return false;
    }

    /**
     *
     * */
    public boolean hasBetterArmor(EntityLivingBase currentEntity, EntityLivingBase otherEntity) {
        ItemStack[] currentEntityArmor = getEntityArmor(currentEntity),
                otherEntityArmor = getEntityArmor(otherEntity);
        // TODO Compare armor.
        return false;
    }

    /**
     * @return An array of the entities armor.
     */
    public ItemStack[] getEntityArmor(EntityLivingBase entity) {
        ItemStack[] entityArmor = new ItemStack[4];

        if (entity instanceof EntityLivingBase) {
            EntityLivingBase entityLivingBase = (EntityLivingBase) entity;
            for (int i = 0; i < 4; i++)
                entityArmor[i] = entityLivingBase.getCurrentItemOrArmor(i);
        }
        return entityArmor;
    }

    /**
     * @param checkThreat
     *         Whether or not we should find the first threat.
     * @return The closest (Living) entity.
     */
    public EntityLivingBase getClosestEntity(boolean checkThreat, float distance, float extender, boolean mob, boolean animal, boolean player) {
        EntityLivingBase closestEntity = null;

        for (EntityLivingBase entity : getLivingEntities()) {
            if (!isAliveNotUs(entity) || !checkType(entity, mob, animal, player))
                continue;

            if (entity instanceof EntityPlayer)
                if (Huzuni.friendManager.contains(StringUtils.stripControlCodes(entity.getCommandSenderName())))
                    continue;

            if (closestEntity != null) {
                if ((checkThreat ? isBiggerThreat(closestEntity, entity, distance, extender) : isCloser(closestEntity, entity)) && isWithinDistance(entity, distance))
                    closestEntity = entity;
            } else
                closestEntity = entity;

        }
        return closestEntity;
    }

    private boolean isCloser(EntityLivingBase specifiedEntity, EntityLivingBase otherEntity) {
        return mc.thePlayer.getDistanceToEntity(otherEntity) < mc.thePlayer.getDistanceToEntity(specifiedEntity);
    }

    private boolean isMob(EntityLivingBase entity) {
        return entity instanceof IMob;
    }

    private boolean isPlayer(EntityLivingBase entity) {
        return entity instanceof EntityPlayer;
    }

    public boolean isWithinDistance(EntityLivingBase entity, float distance) {
        return mc.thePlayer.getDistanceToEntity(entity) < distance;
    }

    /**
     * Makes le player face le entity specified.
     */
    public void faceEntity(EntityLivingBase entity) {
        float[] rotations = getRotationsNeeded(entity);
        if (rotations != null) {
            mc.thePlayer.rotationYaw = rotations[0];
            mc.thePlayer.rotationPitch = rotations[1];
        }
    }

    /**
     * @return Rotations needed to face the entity.
     */
    public float[] getRotationsNeeded(Entity entity) {

        if (entity == null)
            return null;
        double diffX = entity.posX - mc.thePlayer.posX;
        double diffZ = entity.posZ - mc.thePlayer.posZ;
        double diffY;

        if (entity instanceof EntityLivingBase) {
            EntityLivingBase entityLivingBase = (EntityLivingBase) entity;
            diffY = entityLivingBase.posY + (double) entityLivingBase.getEyeHeight() - (mc.thePlayer.posY + (double) mc.thePlayer.getEyeHeight());
        } else
            diffY = (entity.boundingBox.minY + entity.boundingBox.maxY) / 2.0D - (mc.thePlayer.posY + (double) mc.thePlayer.getEyeHeight());

        double dist = (double) MathHelper.sqrt_double(diffX * diffX + diffZ * diffZ);
        float yaw = (float) (Math.atan2(diffZ, diffX) * 180.0D / Math.PI) - 90.0F;
        float pitch = (float) (-(Math.atan2(diffY, dist) * 180.0D / Math.PI));
        return new float[] {
                mc.thePlayer.rotationYaw + MathHelper.wrapAngleTo180_float(yaw - mc.thePlayer.rotationYaw),
                mc.thePlayer.rotationPitch + MathHelper.wrapAngleTo180_float(pitch - mc.thePlayer.rotationPitch)
        };

    }

    /**
     * @return Distance the entity is from our mouse.
     */
    private float getDistanceFromMouse(EntityLivingBase entity) {
        float[] neededRotations = getRotationsNeeded(entity);
        if (neededRotations != null) {
            float neededYaw = mc.thePlayer.rotationYaw - neededRotations[0],
                    neededPitch = mc.thePlayer.rotationPitch - neededRotations[1];
            float distanceFromMouse = MathHelper.sqrt_float(neededYaw * neededYaw + neededPitch * neededPitch);
            return distanceFromMouse;
        }
        return -1F;
    }

    public enum PriorityType {
        CLOSEST,
        CLOSEST_MOUSE;
    }
}
