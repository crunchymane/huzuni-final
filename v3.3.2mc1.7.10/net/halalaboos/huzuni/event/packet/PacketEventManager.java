package net.halalaboos.huzuni.event.packet;

import net.halalaboos.huzuni.api.event.EventManager;

public class PacketEventManager extends EventManager <PacketEvent, PacketListener> {

	@Override
	protected void invoke(PacketListener listener, PacketEvent event) {
		listener.onPacket(event, event.getPacketType());
	}

}
