/**
 *
 */
package net.halalaboos.huzuni.ui.ingame.themes.tabbed;

import net.halalaboos.huzuni.helpers.Helper;
import net.halalaboos.huzuni.helpers.MinecraftHelper;
import net.halalaboos.huzuni.mods.DefaultModule;
import net.halalaboos.huzuni.utils.RenderUtils;

import org.lwjgl.input.Keyboard;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Halalaboos
 * @since Aug 3, 2013
 */
public class Tab implements Helper, MinecraftHelper {
    int scrolledIndex;
    String name;
    List<DefaultModule> mods = new ArrayList<DefaultModule>();

    public Tab(String name, List<DefaultModule> mods) {
        this.name = name;
        this.mods = mods;
    }

    public void render(int x, int y) {
        int width = getMaxWidth();
        int height = 12;
        DefaultModule selectedModule = getSelectedMod();
        for (DefaultModule module : mods) {
            int renderColor = module.isEnabled() ? 0x00B3FF : 0xFEFEFE;
            String renderName = module.getName();
            Rectangle boundaries = new Rectangle(x, y, width, height);
            RenderUtils.drawRect(boundaries, selectedModule == module ? 0x9F00B3FF : 0x9F5E5E5E);
            mc.fontRenderer.drawStringWithShadow(renderName, boundaries.x + 2, boundaries.y + 2, renderColor);
            y += height;
        }
    }

    private int getMaxWidth() {
        int width = 0;
        for (DefaultModule module : mods) {
            if (mc.fontRenderer.getStringWidth(module.getName()) + 4 > width)
                width = mc.fontRenderer.getStringWidth(module.getName()) + 4;
        }
        return width;
    }

    private void keepModIndexInBounds() {
        if (scrolledIndex >= mods.size()) {
            scrolledIndex = 0;
        } else if (scrolledIndex < 0)
            scrolledIndex = mods.size() - 1;
    }

    private DefaultModule getSelectedMod() {
        return mods.get(scrolledIndex);
    }

    public void onKeyTyped(int keyCode) {
        switch (keyCode) {
            case Keyboard.KEY_UP:
                scrolledIndex--;
                break;
            case Keyboard.KEY_DOWN:
                scrolledIndex++;
                break;
            case Keyboard.KEY_RETURN:
            case Keyboard.KEY_RIGHT:
                DefaultModule mod = getSelectedMod();
                mod.toggle();
                break;
        }
        keepModIndexInBounds();
    }
}
