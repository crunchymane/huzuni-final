/**
 * 
 */
package net.halalaboos.huzuni.mods.misc;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;
import com.darkmagician6.eventapi.types.Priority;
import com.darkmagician6.morbidlib.utils.timer.SecondTimer;
import com.darkmagician6.morbidlib.utils.timer.Timer;

import net.halalaboos.huzuni.events.game.EventTick;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.Module;

/**
 * @author Halalaboos
 *
 */
public final class Tips extends Module implements Listener {

	private static final String[] tips = new String[] {
		"Can't find a mod in the windows? Go to the mod manager!",
		"Hate these tips? Disable them in the GUI!",
		"VIP features will only improve experience. They aren't game changing!",
		"Want to add a friend? Click them with your middle mouse button!",
		"Can't find a friend? Type '.find <username>'!",
		"Auto Tool will switch back to your original item! Isn't that cool?",
		"Auto build is customizable! Try it!",
		"Try status hud! Type '.t status hud'.",
		"Eventually, the windows will be customizable. Doesn't that sound cool?",
		""
	};
	
	private final Timer timer = new SecondTimer();
	
	/**
	 * @param name
	 * @param author
	 */
	public Tips() {
		super("Tips", "Halalaboos");
		setEnabled(true);
		setDescription("In-game tips that occur every 10 minutes.");
		setCategory(Category.MISC);
	}

	/**
	 * @see net.halalaboos.huzuni.mods.Module#onEnable()
	 */
	@Override
	protected void onEnable() {
		registry.registerListener(this);
		timer.reset();
	}

	/**
	 * @see net.halalaboos.huzuni.mods.Module#onDisable()
	 */
	@Override
	protected void onDisable() {
		registry.unregisterListener(this);
	}
	
	@EventTarget(Priority.LOWEST)
	public void onTick(EventTick event) {
		if (timer.hasReach(600)) {
			huzuni.addChatMessage(getTip());
			timer.reset();
		}
	}
	
	private String getTip() {
		return tips[(int) ((tips.length - 1) * Math.random())];
	}

}
