/**
 * 
 */
package net.halalaboos.huzuni.helpers;

/**
 * @author Halalaboos
 *
 * @since Sep 15, 2013
 */
public final class StringHelper {

	/**
	 * Gives you everything after 'x' words in a sentence.
	 * <br>
	 * EX: getEverythingAfter("this is an interesting sentence", 2)
	 * <br>
	 * Returns 'an interesting sentence'
	 * */
	public String getAfter(String text, int index) {
		String[] words = text.split(" ");
		if (words.length < index)
			return null;
		int splitIndex = 0;
		for (int i = 0; i < index; i++)
			splitIndex += words[i].length() + 1;
		return text.substring(splitIndex);
	}
	
	public String getBefore(String text, int index) {
		String[] words = text.split(" ");
		if (words.length < index)
			return null;
		int splitIndex = 0;
		for (int i = 0; i < index; i++)
			splitIndex += words[i].length() + 1;
		return text.substring(0, splitIndex);
	}
	
	/**
	 * @return true if the text could be parsed as an integer.
	 * */
	public boolean isInteger(String text) {
		try {
			Integer.parseInt(text);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	/**
	 * @return true if the text could be parsed as a double.
	 * */
	public boolean isDouble(String text) {
		try {
			Double.parseDouble(text);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	/**
	 * @return true if the text could be parsed as a double.
	 * */
	public boolean isFloat(String text) {
		try {
			Float.parseFloat(text);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
}
