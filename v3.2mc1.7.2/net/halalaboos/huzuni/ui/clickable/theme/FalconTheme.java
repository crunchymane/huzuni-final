/**
 * 
 */
package net.halalaboos.huzuni.ui.clickable.theme;

import static org.lwjgl.opengl.GL11.*;
import java.awt.Color;
import java.awt.Point;
import java.awt.Rectangle;
import java.text.DecimalFormat;

import net.halalaboos.lib.ui.Component;
import net.halalaboos.lib.ui.Container;
import net.halalaboos.lib.ui.components.Button;
import net.halalaboos.lib.ui.components.Dropdown;
import net.halalaboos.lib.ui.components.Slider;
import net.halalaboos.lib.ui.components.SlotComponent;
import net.halalaboos.lib.ui.components.TextField;
import net.halalaboos.lib.ui.containers.Window;
import net.halalaboos.lib.ui.theme.ComponentRenderer;
import net.halalaboos.lib.ui.theme.Theme;

/**
 * @author Halalaboos
 *
 * @since Oct 6, 2013
 */
public class FalconTheme extends Theme {
	
	/*private final Color topWindowColor = new Color(0xFF2F2F2F),
	 borderWindowColor = new Color(0xFF676765),
	 insideWindowColor = new Color(0xFF444444);
	
	public FalconTheme() {
		super();
		registerRenderer(Button.class, new ComponentRenderer<Button>() {
			@Override
			public void render(Button button, Rectangle area,
					boolean mouseOver, boolean mouseDown) {
				renderNkryptButton(area, button.isHighlight(), mouseOver, mouseDown);
				utils.getRenderer().drawString(button.getTitle(), area.x + area.width / 2 - utils.getRenderer().getStringWidth(button.getTitle()) / 2, area.y + area.height / 2 - utils.getRenderer().getStringHeight(button.getTitle()) / 2 + 1, button.isHighlight() ? 0xFFFFFF : 0x999999);
			}
			
		});
		registerRenderer(TextField.class, new ComponentRenderer<TextField>() {

            @Override
            public void render(TextField textField, Rectangle area,
                               boolean mouseOver, boolean mouseDown) {
            	renderNkryptButton(area, false, mouseOver || textField.isEnabled(), false);
                utils.getRenderer().drawStringWithShadow(textField.getTextForRender(area) + textField.getSelection(), area.x + 2, area.y + area.height / 2 - utils.getRenderer().getStringHeight(textField.getText()) / 2, textField.getTextColor().getRGB());
            }

        });
		registerRenderer(Slider.class, new ComponentRenderer<Slider>() {
			protected DecimalFormat formatter = new DecimalFormat("#.#");

			@Override
			public void render(Slider slider, Rectangle area,
					boolean mouseOver, boolean mouseDown) {
				Point mouse = new Point(utils.getMouseX(), utils.getMouseY());
				Rectangle sliderPoint = slider.getSliderPoint(area);
				
				// Inside
				renderNkryptButton(area, false, mouseOver && sliderPoint.contains(mouse), mouseDown);
				
				// Text
				utils.getRenderer().drawString(slider.getLabel(), area.x + 2, area.y + 2, 0xFFFFFF);
				String formattedValue = formatter.format(slider.getValue()) + slider.getValueWatermark();
				utils.getRenderer().drawString(formattedValue, area.x + area.width - utils.getRenderer().getStringWidth(formattedValue) - 2, area.y + 2, 0xFFFFFF);
				
				// Blue slider part
				renderNkryptButton(sliderPoint, true, mouseOver && sliderPoint.contains(mouse), mouseDown);
			}
			
		});
		registerRenderer(Window.class, new ComponentRenderer<Window>() {

			@Override
			public void render(Window window, Rectangle area,
					boolean mouseOver, boolean mouseDown) {
				Point mouse = new Point(utils.getMouseX(), utils.getMouseY());
				Rectangle draggableDimensions = window.getDraggableArea(area);
				int barSize = window.getBarSize();
				int barPadding = window.getBorderPadding();
				
				if (window.isMinimized()) {
					utils.getRenderer().setColor(topWindowColor);
					utils.getRenderer().drawFilledRect(draggableDimensions.x, draggableDimensions.y, draggableDimensions.x + draggableDimensions.width, draggableDimensions.y + draggableDimensions.height);
					utils.getRenderer().setColor(borderWindowColor);
					float borderSize = 0.5F;
					utils.getRenderer().drawRect(draggableDimensions.x - borderSize, draggableDimensions.y - borderSize, draggableDimensions.x + draggableDimensions.width + borderSize, draggableDimensions.y + draggableDimensions.height + borderSize, borderSize);
				} else {
					utils.getRenderer().setColor(topWindowColor);
					utils.getRenderer().drawFilledRect(draggableDimensions.x, draggableDimensions.y, draggableDimensions.x + draggableDimensions.width, draggableDimensions.y + draggableDimensions.height);
					utils.getRenderer().setColor(borderWindowColor);
					float borderSize = 0.5F;
					utils.getRenderer().drawRect(draggableDimensions.x - borderSize, draggableDimensions.y - borderSize, draggableDimensions.x + draggableDimensions.width + borderSize, area.y + area.height + borderSize, borderSize);
					utils.getRenderer().setColor(insideWindowColor);
					utils.getRenderer().drawFilledRect(draggableDimensions.x, draggableDimensions.y + draggableDimensions.height, area.x + area.width, area.y + area.height);
				}
				utils.getRenderer().drawString(window.getTitle(), draggableDimensions.x + 2, draggableDimensions.y + (draggableDimensions.height / 2) - (utils.getRenderer().getStringHeight(window.getTitle()) / 2) + 1, 0xFFFFFF);

				// Minimize button
				Rectangle minimizeArea = window.getMinimizeArea();
				renderNkryptButton(minimizeArea, !window.isMinimized(), mouseOver, mouseDown);				
			}
			
		});
        registerRenderer(Dropdown.class, new ComponentRenderer<Dropdown>() {

            @Override
            public void render(Dropdown dropdown, Rectangle area,
                               boolean mouseOver, boolean mouseDown) {
            	renderNkryptButton(area, false, mouseOver, mouseDown);
                utils.getRenderer().drawString(dropdown.getTitle() + " (" + dropdown.getSelectedComponentName() + ")", area.x + 2, area.y + area.height / 2 - utils.getRenderer().getStringHeight(dropdown.getTitle()) / 2 + 1, Color.WHITE.getRGB());
                if (dropdown.isDown()) {
                	String[] components = dropdown.getComponents();
        			int yPos = dropdown.getTextPadding() + 2;
                    for (int i = 0; i < components.length; i++) {
        				Rectangle componentBoundaries = new Rectangle(area.x + 1, area.y + yPos, area.width - 2, dropdown.getTextPadding());
        				boolean mouseOverComponent = componentBoundaries.contains(utils.getMouseX(), utils.getMouseY());
        				renderNkryptButton(componentBoundaries, dropdown.getSelectedComponent() == i, mouseOverComponent, mouseDown);
        				utils.getRenderer().drawString(components[i], componentBoundaries.x + 2, componentBoundaries.y + 2, 0xFFFFFF);
        				yPos += dropdown.getTextPadding();
                	}
                }
            }

        });
        registerRenderer(SlotComponent.class, new ComponentRenderer<SlotComponent>() {

            @Override
            public void render(SlotComponent slotComponent, Rectangle area,
                               boolean mouseOver, boolean mouseDown) {
            	if (slotComponent.shouldRenderBackground()) {
            		renderNkryptButton(area, false, mouseOver, false);	
            	}
                if (slotComponent.hasEnoughToScroll() && slotComponent.hasSlider()) {
                    Rectangle sliderPoint = slotComponent.getDraggableArea(area);
                	utils.getRenderer().setColor(slotComponent.getSliderColor());
                	renderNkryptButton(sliderPoint, true, mouseOver, false);	
                }
                slotComponent.renderElements(area, mouseOver, mouseDown);
            }

        });
	}
	
	private void renderNkryptButton(Rectangle area, boolean highlight, boolean mouseOver, boolean mouseDown) {
		renderNkryptButton(area.x, area.y, area.x + area.width, area.y + area.height, highlight, mouseOver, mouseDown);
	}
	
	private void renderNkryptButton(int x, int y, int x1, int y1, boolean highlight, boolean mouseOver, boolean mouseDown) {
		drawBorderedRect(x, y, x1, y1, 0.5F, 0xff000000, 0x00);
		drawBorderedRect(x + 0.5F, y + 0.5F, x1 - 0.5F, y1 - 0.5F, 0.5F, 0x30ffffff, 0x00);
		drawGradientRect(x + 1, y + 1, x1 - 1, y1 - 1, highlight ? 0xff333333 : 0xff444444, highlight ? 0xff444444 : 0xff333333);
	}
	
	public static void drawGradientRect(Rectangle rect, int topColor, int bottomColor) {
		drawGradientRect(rect.x, rect.y, rect.x + rect.width, rect.y + rect.height, topColor, bottomColor);
	}

	public static void drawBorderedRect(float x, float y, float x1, float y1, float borderSize, int borderColor, int insideColor) {
		color(insideColor);
		utils.getRenderer().drawFilledRect(x, y, x1, y1);

		start2D();
		color(borderColor);
		glLineWidth(borderSize);
		glBegin(GL_LINES);
		glVertex2f(x, y);
		glVertex2f(x, y1);
		glVertex2f(x1, y1);
		glVertex2f(x1, y);
		glVertex2f(x, y);
		glVertex2f(x1, y);
		glVertex2f(x, y1);
		glVertex2f(x1, y1);
		glEnd();
		end2D();
	}

	public static void drawBorderedRect(Rectangle rect, float borderSize, int borderColor, int insideColor) {
		drawBorderedRect(rect.x, rect.y, rect.x + rect.width, rect.y + rect.height, borderSize, borderColor, insideColor);
	}

	public static void drawGradientRect(float x, float y, float x1, float y1, int topColor, int bottomColor) {
		start2D();
		startSmooth();
		glShadeModel(GL_SMOOTH);

		glBegin(GL_QUADS);
		color(topColor);
		glVertex2f(x1, y);
		glVertex2f(x, y);

		color(bottomColor);
		glVertex2f(x, y1);
		glVertex2f(x1, y1);
		glEnd();

		glShadeModel(GL_FLAT);
		endSmooth();
		end2D();
	}
	public static void start2D() {
		glPushMatrix();
		glDisable(GL_DEPTH_TEST);
		glEnable(GL_BLEND);
		glDisable(GL_TEXTURE_2D);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}

	public static void end2D() {
		glEnable(GL_TEXTURE_2D);
		glDisable(GL_BLEND);
		glEnable(GL_DEPTH_TEST);
		glPopMatrix();
	}

	public static void startSmooth() {
		glEnable(GL_LINE_SMOOTH);
		glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	}

	public static void endSmooth() {
		glDisable(GL_LINE_SMOOTH);
	}
	public static void color(int color) {
		float red = (float)(color >> 16 & 255) / 255.0F;
		float green = (float)(color >> 8 & 255) / 255.0F;
		float blue = (float)(color & 255) / 255.0F;
		float alpha = (float)(color >> 24 & 255) / 255.0F;
		glColor4f(red, green, blue, alpha);
	}*/
	/**
	 * @see net.halalaboos.lib.ui.theme.Theme#renderToolTip(net.halalaboos.lib.ui.Component, java.lang.String, java.awt.Point)
	 */
	@Override
	public void renderToolTip(String tooltip, Point mouse) {
	}

	/**
	 * @see net.halalaboos.lib.ui.theme.Theme#renderContainer(net.halalaboos.lib.ui.Container, java.awt.Point)
	 */
	@Override
	public void renderContainer(Container container, Point mouse) {
	}
}
