/**
 *
 */
package net.halalaboos.huzuni.ui.clickable.containers;

import net.halalaboos.lib.ui.containers.Window;
import java.awt.*;

/**
 * @author Halalaboos
 * @since Aug 19, 2013
 */
public class HuzuniWindow extends Window {
	
    /**
     * @param title
     * @param dimensions
     */
    public HuzuniWindow(String title) {
        super(title, new Rectangle());
        setMinimized(true);
        borderColor = new Color(0, 0, 0, 191);
        insideColor = new Color(0, 0, 0, 191);
        minimizeColor = new Color(0, 127, 255, 191);
        this.borderPadding = 1;
        setResizable(false);
    }
	
}
