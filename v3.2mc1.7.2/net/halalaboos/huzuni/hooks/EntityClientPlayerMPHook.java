package net.halalaboos.huzuni.hooks;

import com.darkmagician6.eventapi.Dispatcher;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.client.Settings;
import net.halalaboos.huzuni.events.game.*;
import net.halalaboos.huzuni.mods.Module;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.entity.Entity;
import net.minecraft.stats.StatFileWriter;
import net.minecraft.util.MathHelper;
import net.minecraft.util.Session;
import net.minecraft.world.World;

public class EntityClientPlayerMPHook extends EntityClientPlayerMP {

	private Module freecam = Huzuni.modManager.getMod("Freecam");
	
    public EntityClientPlayerMPHook(Minecraft mc, World world,
                                    Session session, NetHandlerPlayClient netHandlerPlayClient, StatFileWriter statFileWriter) {
        super(mc, world, session, netHandlerPlayClient, statFileWriter);
        Huzuni.logger.log("Initialized new " + this.getClass().getSimpleName() + ".");
    }

    @Override
    public void moveEntity(double motionX, double motionY, double motionZ) {
        EventMovement event = new EventMovement(motionX, motionY, motionZ);
        Dispatcher.call(event);
        motionX = event.motionX;
        motionY = event.motionY;
        motionZ = event.motionZ;
        super.moveEntity(motionX, motionY, motionZ);
    }

    @Override
    public void onUpdate() {
        super.onUpdate();
        Dispatcher.call(new EventTick());
    }

    @Override
    public void sendMotionUpdates() {
        float oldRotationYaw = rotationYaw, oldRotationPitch = rotationPitch;
        EventPreMotionUpdate preEvent = new EventPreMotionUpdate(this);
        Dispatcher.call(preEvent);
        if (!preEvent.isCancelled()) {
            super.sendMotionUpdates();
        }
        this.rotationYaw = oldRotationYaw;
        this.rotationPitch = oldRotationPitch;
        EventPostMotionUpdate postEvent = new EventPostMotionUpdate(this);
        Dispatcher.call(postEvent);
    }

    @Override
    public void sendChatMessage(String message) {
        if (message.startsWith(".") && message.length() >= 2 && Settings.isHuzuniEnabled()) {
            if (Huzuni.commandManager.processCommand(message.substring(1))) ;
            return;
        }
        EventSendMessage event = new EventSendMessage(message);
        Dispatcher.call(event);
        message = event.getMessage();
        if (!event.isCancelled())
            super.sendChatMessage(message);
    }

    @Override
    public boolean isEntityInsideOpaqueBlock() {
    	if (freecam == null)
    		freecam = Huzuni.modManager.getMod("Freecam");
    	else if (freecam.isEnabled() && Settings.isHuzuniEnabled())
    		return false;
    	return super.isEntityInsideOpaqueBlock();
    }

    @Override
    protected boolean pushOutOfBlocks(double par1, double par3, double par5) {
    	if (freecam == null) {
    		freecam = Huzuni.modManager.getMod("Freecam");
    	} else if (freecam.isEnabled() && Settings.isHuzuniEnabled()) {
    		return false;
    	}
        return super.pushOutOfBlocks(par1, par3, par5);
    }

    @Override
    public void addVelocity(double velocityX, double velocityY, double velocityZ) {
    	EventRecieveVelocity event = new EventRecieveVelocity(velocityX, velocityY, velocityZ);
    	Dispatcher.call(event);
    	velocityX = event.getVelocityX();
    	velocityY = event.getVelocityY();
    	velocityZ = event.getVelocityZ();
        super.addVelocity(velocityX, velocityY, velocityZ);
    }

    @Override
    public void setVelocity(double velocityX, double velocityY, double velocityZ) {
    	EventRecieveVelocity event = new EventRecieveVelocity(velocityX, velocityY, velocityZ);
    	Dispatcher.call(event);
    	velocityX = event.getVelocityX();
    	velocityY = event.getVelocityY();
    	velocityZ = event.getVelocityZ();
    	super.setVelocity(velocityX, velocityY, velocityZ);
    }

    @Override
    public float getDistanceToEntity(Entity entity) {
        float xDiff = (float) (this.posX - entity.posX);
        float yDiff = (float) ((this.posY + this.getEyeHeight()) - (entity.posY + entity.getEyeHeight()));
        float zDiff = (float) (this.posZ - entity.posZ);
        return MathHelper.sqrt_float(xDiff * xDiff + yDiff * yDiff + zDiff * zDiff);
    }

    @Override
    public void setHealth(float health) {
        super.setHealth(health);
        Dispatcher.call(new EventHealthChange(health));
    }

}
