package net.halalaboos.huzuni.mod.mods.combat;

import static org.lwjgl.opengl.GL11.GL_ALPHA_TEST;
import static org.lwjgl.opengl.GL11.GL_LINES;
import static org.lwjgl.opengl.GL11.GL_LINE_STRIP;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glNormal3f;
import static org.lwjgl.opengl.GL11.glPopMatrix;
import static org.lwjgl.opengl.GL11.glPushMatrix;
import static org.lwjgl.opengl.GL11.glRotatef;
import static org.lwjgl.opengl.GL11.glScalef;
import static org.lwjgl.opengl.GL11.glTranslated;
import static org.lwjgl.opengl.GL11.glVertex3d;

import java.util.ArrayList;
import java.util.List;

import net.halalaboos.huzuni.NameProtect;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Items;
import net.minecraft.item.*;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.*;
import net.minecraft.world.gen.structure.StructureBoundingBox;

import org.lwjgl.util.glu.Cylinder;
import org.lwjgl.util.glu.GLU;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.mod.Category;
import net.halalaboos.huzuni.api.mod.ToggleOption;
import net.halalaboos.huzuni.event.render.RenderEvent;
import net.halalaboos.huzuni.event.render.Renderable;
import net.halalaboos.huzuni.mod.DefaultMod;
import net.halalaboos.huzuni.util.GLUtils;
import net.halalaboos.huzuni.util.GameUtils;
import net.minecraft.block.Block;
import net.minecraft.block.BlockLiquid;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityArrow;

public class Projectiles extends DefaultMod implements Renderable {

	private final Cylinder cylinder = new Cylinder();
	
	private ToggleOption arrows = new ToggleOption("Arrows", "Renders all arrow's trajectories");

    private boolean foundEntity = false;

	/**
	 *	MAIN		OFF			RESULT
	 *
	 *  Splash		Bow			Splash
	 *	Bow			Splash		Bow
	 *
	 *  Shield		Bow			Shield
	 *	Bow			Shield		Bow
	 *
	 *	Splash		Shield		Splash
	 *	Shield		Splash		Shield
	 *
	 */
	
	public Projectiles() {
		super("Projectiles");
		setCategory(Category.COMBAT);
        setAliases("proj");
		setAuthor("Halalaboos");
		setDescription("Shows the trajectory of throwable objects.");
		this.setOptions(arrows);
	}

	@Override
	public void render(RenderEvent event) {
        if (arrows.isEnabled()) {
        	this.renderArrows(event);
        }
        foundEntity = false;
		
        int mode = 0;
        EntityPlayer player = mc.thePlayer;
        /*check for bow and if the item is throwable*/
        if (getApplicableItem() != null) {
            Item item = getApplicableItem().getItem();
            if (!(item instanceof ItemBow || item instanceof ItemSnowball || item instanceof ItemEnderPearl || item instanceof ItemEgg || item instanceof ItemSplashPotion))
                return;
            if ((item instanceof ItemBow)) {
                mode = 1;
            } else if (item instanceof ItemSplashPotion) {
            	mode = 2;
            }
        } else
            return;

		/*copypasta from EntityArrow.java and EntityThrowable.java*/
        double posX = mc.getRenderManager().renderPosX - (double) (MathHelper.cos(player.rotationYaw / 180.0F * (float) Math.PI) * 0.16F),
                posY = (mc.getRenderManager().renderPosY + (double) player.getEyeHeight()) - 0.10000000149011612D,
                posZ = mc.getRenderManager().renderPosZ - (double) (MathHelper.sin(player.rotationYaw / 180.0F * (float) Math.PI) * 0.16F),
                motionX = (double) (-MathHelper.sin(player.rotationYaw / 180.0F * (float) Math.PI) * MathHelper.cos(player.rotationPitch / 180.0F * (float) Math.PI)) * (mode == 1 ? 1.0 : 0.4),
                motionY = (double) (-MathHelper.sin(player.rotationPitch / 180.0F * (float) Math.PI)) * (mode == 1 ? 1.0 : 0.4),
                motionZ = (double) (MathHelper.cos(player.rotationYaw / 180.0F * (float) Math.PI) * MathHelper.cos(player.rotationPitch / 180.0F * (float) Math.PI)) * (mode == 1 ? 1.0 : 0.4);

        //POWER: ((power ranging from 0 ~ 1) * 2) * 1.5
		/*ItemBow.java*/
        if (player.getItemInUseCount() <= 0 && mode == 1) {
            return;
        }
        int var6 = 72000 - player.getItemInUseCount();
        float power = (float) var6 / 20.0F;
        power = (power * power + power * 2.0F) / 3.0F;
        if ((double) power < 0.1D)
            return;
        if (power > 1.0F)
            power = 1.0F;

        if (this.foundEntity) {
            GLUtils.glColor(1, 0, 0, 1);
        } else
        	GLUtils.glColor((1 - power), power, 0, 1F);

		/*prep motion (EntityArrow.java)*/
        float theta = MathHelper.sqrt_double(motionX * motionX + motionY * motionY + motionZ * motionZ);
        motionX /= (double) theta;
        motionY /= (double) theta;
        motionZ /= (double) theta;
		/*power (more EntityArrow.java)*/
        motionX *= (mode == 1 ? (power * 2) : 1) * getMult(mode);
        motionY *= (mode == 1 ? (power * 2) : 1) * getMult(mode);
        motionZ *= (mode == 1 ? (power * 2) : 1) * getMult(mode);

        glBegin(GL_LINE_STRIP);
        boolean hasLanded = false, isEntity = false;
        RayTraceResult landingPosition = null;
        float size = (float) (mode == 1 ? 0.3 : 0.25);
        float gravity = getGravity(mode);
        for (; !hasLanded && posY > 0;) {
			/*Check for landing on a block*/
            Vec3d present = Vec3d.createVectorHelper(posX, posY, posZ);
            Vec3d future = Vec3d.createVectorHelper(posX + motionX, posY + motionY, posZ + motionZ);
            RayTraceResult possibleLandingStrip = mc.theWorld.rayTraceBlocks(present, future, false, true, false);
            present = Vec3d.createVectorHelper(posX, posY, posZ);
            future = Vec3d.createVectorHelper(posX + motionX, posY + motionY, posZ + motionZ);
            if (possibleLandingStrip != null) {
                hasLanded = true;
                landingPosition = possibleLandingStrip;
            }
	        /*Check for landing on an entity*/
            AxisAlignedBB arrowBox = AxisAlignedBB.fromBounds(posX - size, posY - size, posZ - size, posX + size, posY + size, posZ + size);
            List entities = getEntitiesWithinAABB(arrowBox.addCoord(motionX, motionY, motionZ).expand(1.0D, 1.0D, 1.0D));

            for (int index = 0; index < entities.size(); ++index) {
                Entity entity = (Entity) entities.get(index);

                if (entity.canBeCollidedWith() && (entity != player)) {
                    float var11 = 0.3F;
                    AxisAlignedBB var12 = entity.getEntityBoundingBox().expand((double) var11, (double) var11, (double) var11);
                    RayTraceResult possibleEntityLanding = var12.calculateIntercept(present, future);
                    if (possibleEntityLanding != null) {
                        hasLanded = true;
                        isEntity = true;
                        landingPosition = possibleEntityLanding;
                        this.foundEntity = true;
                    } else {
                        this.foundEntity = false;
                    }
                }
            }
	        /*Arrow rendering and calculation math stuff*/
            posX += motionX;
            posY += motionY;
            posZ += motionZ;
            float motionAdjustment = 0.99F;
            AxisAlignedBB boundingBox = AxisAlignedBB.fromBounds(posX - size, posY - size, posZ - size, posX + size, posY + size, posZ + size);
            if (isInMaterial(boundingBox, Material.water))
                motionAdjustment = 0.8F;

            motionX *= motionAdjustment;
            motionY *= motionAdjustment;
            motionZ *= motionAdjustment;
            motionY -= gravity;
            glVertex3d(posX - mc.getRenderManager().renderPosX, posY - mc.getRenderManager().renderPosY, posZ - mc.getRenderManager().renderPosZ);
        }
        glEnd();
        glPushMatrix();
        glTranslated(posX - mc.getRenderManager().renderPosX, posY - mc.getRenderManager().renderPosY, posZ - mc.getRenderManager().renderPosZ);
        if (landingPosition != null) {
            switch (landingPosition.sideHit.getIndex()) {
                case 2://east
                    glRotatef(90, 1, 0, 0);
                    break;
                case 3://west
                    glRotatef(90, 1, 0, 0);
                    break;
                case 4://north
                    glRotatef(90, 0, 0, 1);
                    break;
                case 5://south
                    glRotatef(90, 0, 0, 1);
                    break;
                default:
                    break;
            }
            if (isEntity)
            	GLUtils.glColor(1, 0, 0, 1F);
        }
        renderPoint();
        glPopMatrix();
    }

	/**
	 * Finds an item, checking both hands
	 *
	 * todo - maybe have a check if the player has arrows? if no arrows, ignore bow
	 */
	private ItemStack getApplicableItem() {
		if (mc.thePlayer.getActiveItemStack() != null) return mc.thePlayer.getActiveItemStack();
		ItemStack main = mc.thePlayer.getHeldItem(EnumHand.MAIN_HAND);
		ItemStack off = mc.thePlayer.getHeldItem(EnumHand.OFF_HAND);
		if (main != null && isThrowable(main.getItem())) return main;
		if (off != null && isThrowable(off.getItem())) return off;
		return null;
	}

	private boolean isThrowable(Item item) {
		return item instanceof ItemBow || item instanceof ItemSnowball || item instanceof ItemEnderPearl ||
				item instanceof ItemEgg || item instanceof ItemSplashPotion;
	}

    private float getMult(int mode) {
    	if (mode == 2) {
    		return 0.5F;
    	} else
    		return 1.5F;
    }

    private float getGravity(int mode) {
    	if (mode == 1 || mode == 2) {
    		return 0.05F;
    	} else
    		return 0.03F;
    }

    private void renderPoint() {
        glBegin(GL_LINES);
        glVertex3d(-.5, 0, 0);
        glVertex3d(0, 0, 0);
        glVertex3d(0, 0, -.5);
        glVertex3d(0, 0, 0);

        glVertex3d(.5, 0, 0);
        glVertex3d(0, 0, 0);
        glVertex3d(0, 0, .5);
        glVertex3d(0, 0, 0);
        glEnd();

        glRotatef(-90, 1, 0, 0);
        cylinder.setDrawStyle(GLU.GLU_LINE);
        cylinder.draw(0.5f, 0.5f, 0.1f, 24, 1);
    }

    private boolean isInMaterial(AxisAlignedBB axisalignedBB, Material material) {
        int chunkMinX = MathHelper.floor_double(axisalignedBB.minX);
        int chunkMaxX = MathHelper.floor_double(axisalignedBB.maxX + 1.0D);
        int chunkMinY = MathHelper.floor_double(axisalignedBB.minY);
        int chunkMaxY = MathHelper.floor_double(axisalignedBB.maxY + 1.0D);
        int chunkMinZ = MathHelper.floor_double(axisalignedBB.minZ);
        int chunkMaxZ = MathHelper.floor_double(axisalignedBB.maxZ + 1.0D);

		StructureBoundingBox structureBoundingBox = new StructureBoundingBox(chunkMinX, chunkMinY, chunkMinZ, chunkMaxX, chunkMaxY, chunkMaxZ);
        if (!mc.theWorld.isAreaLoaded(structureBoundingBox)) {
            return false;
        } else {
            boolean isWithin = false;
            for (int x = chunkMinX; x < chunkMaxX; ++x) {
                for (int y = chunkMinY; y < chunkMaxY; ++y) {
                    for (int z = chunkMinZ; z < chunkMaxZ; ++z) {
                        Block block = GameUtils.getBlock(x, y, z);
						IBlockState blockState = mc.theWorld.getBlockState(new BlockPos(x, y, z));
                        if (block != null && GameUtils.getMaterial(new BlockPos(x, y, z)) == material) {
                            double liquidHeight = (double) ((float) (y + 1) -
									BlockLiquid.getLiquidHeightPercent((Integer)blockState.getValue(BlockLiquid.LEVEL)));

                            if ((double) chunkMaxY >= liquidHeight) {
                                isWithin = true;
                            }
                        }
                    }
                }
            }
            return isWithin;
        }
    }

    private List getEntitiesWithinAABB(AxisAlignedBB axisalignedBB) {
        List list = new ArrayList();
        int chunkMinX = MathHelper.floor_double((axisalignedBB.minX - 2.0D) / 16.0D);
        int chunkMaxX = MathHelper.floor_double((axisalignedBB.maxX + 2.0D) / 16.0D);
        int chunkMinZ = MathHelper.floor_double((axisalignedBB.minZ - 2.0D) / 16.0D);
        int chunkMaxZ = MathHelper.floor_double((axisalignedBB.maxZ + 2.0D) / 16.0D);

        for (int x = chunkMinX; x <= chunkMaxX; ++x) {
            for (int z = chunkMinZ; z <= chunkMaxZ; ++z) {
//                if (mc.theWorld.getChunkProvider().chunkExists(x, z)) { damn good thing that chunks always exist
                    mc.theWorld.getChunkFromChunkCoords(x, z).getEntitiesWithinAABBForEntity(mc.thePlayer, axisalignedBB, list, null);
//                }
            }
        }

        return list;
    }
    
    private void renderArrows(RenderEvent event) {
		for (Object o : mc.theWorld.loadedEntityList) {
			if (o instanceof EntityArrow) {
				EntityArrow entity = (EntityArrow) o;
				if (entity.isDead || entity.inGround)
					continue;
				float size = 0.3F;
				boolean hasLanded = false;
		        RayTraceResult landingPosition = null;
				double posX = entity.posX, posY = entity.posY, posZ = entity.posZ;
				double oldPosX = entity.posX, oldPosY = entity.posY, oldPosZ = entity.posZ;
				double motionX = entity.motionX, motionY = entity.motionY, motionZ = entity.motionZ;
				
				renderName(entity, event);
				
				GLUtils.glColor(1F, 0F, 0F, 1F);
		        glBegin(GL_LINE_STRIP);
				for (; !hasLanded && posY > 0; ) {
					/*Check for landing on a block*/
		            Vec3d present = Vec3d.createVectorHelper(posX, posY, posZ);
		            Vec3d future = Vec3d.createVectorHelper(posX + motionX, posY + motionY, posZ + motionZ);
		            RayTraceResult possibleLandingStrip = mc.theWorld.rayTraceBlocks(present, future, false, true, false);
		            present = Vec3d.createVectorHelper(posX, posY, posZ);
		            future = Vec3d.createVectorHelper(posX + motionX, posY + motionY, posZ + motionZ);
		            if (possibleLandingStrip != null) {
		                hasLanded = true;
		                landingPosition = possibleLandingStrip;
		            }
			        /*Check for landing on an entity*/
		            AxisAlignedBB arrowBox = AxisAlignedBB.fromBounds(posX - size, posY - size, posZ - size, posX + size, posY + size, posZ + size);
		            List entities = getEntitiesWithinAABB(arrowBox.addCoord(motionX, motionY, motionZ).expand(1.0D, 1.0D, 1.0D));

		            for (int index = 0; index < entities.size(); ++index) {
		                Entity entity1 = (Entity) entities.get(index);

		                if (entity1.canBeCollidedWith() && (entity1 != mc.thePlayer)) {
		                    float size1 = 0.3F;
		                    AxisAlignedBB entity1Box = entity1.getEntityBoundingBox().expand((double) size1, (double) size1, (double) size1);
		                    RayTraceResult possibleEntityLanding = entity1Box.calculateIntercept(present, future);
		                    if (possibleEntityLanding != null) {
		                        hasLanded = true;
		                        landingPosition = possibleEntityLanding;
		                    }
		                }
		            }
					posX += motionX;
		            posY += motionY;
		            posZ += motionZ;
					float motionAdjustment = 0.99F;
		            AxisAlignedBB boundingBox = AxisAlignedBB.fromBounds(posX - size, posY - size, posZ - size, posX + size, posY + size, posZ + size);
		            if (isInMaterial(boundingBox, Material.water))
		                motionAdjustment = 0.8F;
		            
		            motionX *= motionAdjustment;
		            motionY *= motionAdjustment;
		            motionZ *= motionAdjustment;
					motionY -= 0.05F;
		            glVertex3d(event.interpolate(oldPosX, posX) - mc.getRenderManager().renderPosX, event.interpolate(oldPosY, posY) - mc.getRenderManager().renderPosY, event.interpolate(oldPosZ, posZ) - mc.getRenderManager().renderPosZ);
					oldPosX = posX;
					oldPosY = posY;
					oldPosZ = posZ;
				}
				glEnd();
				glPushMatrix();
				glTranslated(event.interpolate(oldPosX, posX) - mc.getRenderManager().renderPosX, event.interpolate(oldPosY, posY) - mc.getRenderManager().renderPosY, event.interpolate(oldPosZ, posZ) - mc.getRenderManager().renderPosZ);
				if (landingPosition != null) {
					switch (landingPosition.sideHit.getIndex()) {
					case 2://east
						glRotatef(90, 1, 0, 0);
						break;
					case 3://west
						glRotatef(90, 1, 0, 0);
						break;
					case 4://north
						glRotatef(90, 0, 0, 1);
						break;
					case 5://south
						glRotatef(90, 0, 0, 1);
						break;
					default:
						break;
					}
				}
				renderPoint();
				glPopMatrix();
			}
		}
	}
    
    private void renderName(EntityArrow entity, RenderEvent event) {
		glPushMatrix();
		if (entity.shootingEntity != null) {
			String name = NameProtect.replace(entity.shootingEntity.getName());
			float scale = (float) (mc.thePlayer.getDistanceToEntity(entity) / 10F);
			glTranslated(event.interpolate(entity.prevPosX, entity.posX) - mc.getRenderManager().renderPosX, event.interpolate(entity.prevPosY, entity.posY) - mc.getRenderManager().renderPosY, event.interpolate(entity.prevPosZ, entity.posZ) - mc.getRenderManager().renderPosZ);
			glScalef(scale, scale, scale);
			glNormal3f(0.0F, 1.0F, 0.0F);
			glRotatef(-mc.getRenderManager().playerViewY, 0.0F, 1.0F, 0.0F);
			glRotatef(mc.getRenderManager().playerViewX, 1.0F, 0.0F, 0.0F);
			glScalef(-(0.016666668F * 1.6F), -(0.016666668F * 1.6F), (0.016666668F * 1.6F));
	        
			glEnable(GL_TEXTURE_2D);
	        mc.fontRenderer.drawStringWithShadow(name, -mc.fontRenderer.getStringWidth(name) / 2, 0, 0xFFFFFF);
			glDisable(GL_TEXTURE_2D);
			glDisable(GL_ALPHA_TEST);
		}
		glPopMatrix();
	}

	@Override
	protected void onEnable() {
		Huzuni.registerRenderable(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterRenderable(this);
	}

}
