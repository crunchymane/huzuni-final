/**
 * 
 */
package net.halalaboos.lib.ui;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.List;

import org.lwjgl.input.Mouse;

import net.halalaboos.lib.ui.layout.Layout;
import net.halalaboos.lib.ui.theme.DefaultTheme;
import net.halalaboos.lib.ui.theme.Theme;

/**
 * Used for displaying components when you don't want it constricted to a specific container. 
 * The components will render regardless if something would normally restrict them.
 * @author Halalaboos
 *
 */
public class BackgroundContainer extends DefaultContainer {

	private Theme theme;
	
	private int savedButtonId;
	
	private Point savedMouse = new Point();
	
	/**
	 * @param area
	 */
	public BackgroundContainer(Theme theme) {
		super(null);
		this.theme = theme;
		
	}	
	
	@Override
	public void renderComponents(Theme theme, Point mouse) {
		calculateMousedComponent();
		calculateMouseDown();
		update(mouse);
		for (Component component : components) {
			boolean mouseDown = downComponent == component;
			boolean mouseOver = downComponent == null ? mousedComponent == component : mousedComponent == component && mouseDown;
			component.setOffset(offset);
			component.setMouseDown(mouseDown);
			component.setMouseOver(mouseOver);
			theme.render(this, offset, component, mouse);
		}
	}
	
	public void render(int mouseX, int mouseY) {
		this.mouse.setLocation(mouseX, mouseY);
		renderComponents(theme, mouse);
	}
	
	@Override
	public boolean onMousePressed(int x, int y, int buttonId) {
		this.savedMouse.setLocation(x, y);
		this.savedButtonId = buttonId;
		return super.onMousePressed(x, y, buttonId);
	}
	
	protected void calculateMouseDown() {
		if (savedButtonId != -1) {
			if (!Mouse.isButtonDown(savedButtonId)) {
				onMouseReleased(savedMouse.x, savedMouse.y, savedButtonId);
				savedButtonId = -1;
			}
		}
	}
}
