package net.halalaboos.huzuni.plugin.plugins.movement;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.plugin.ModeOption;
import net.halalaboos.huzuni.api.plugin.Category;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent;
import net.halalaboos.huzuni.event.tick.UpdateListener;
import net.halalaboos.huzuni.plugin.DefaultPlugin;
import net.minecraft.client.settings.KeyBinding;

import org.lwjgl.input.Keyboard;

/**
 * @author brudin
 * @version 1.0
 * @since 4/10/14
 */
public class Sneak extends DefaultPlugin implements UpdateListener {

    private final ModeOption mode;
	
	public Sneak() {
		super("Sneak", Keyboard.KEY_Z);
		setAuthor("brudin");
		setDescription("Automatically sneaks for you.");
		setCategory(Category.MOVEMENT);
		setOptions(mode = new ModeOption("Sneak mode", "Real", "Fast"));

	}

	@Override
	protected void onEnable() {
		Huzuni.registerUpdateListener(this);
	}

	@Override
	protected void onDisable() {
		KeyBinding.setKeyBindState(mc.gameSettings.keyBindSneak.getKeyCode(), false);
		Huzuni.unregisterUpdateListener(this);
	}

	@Override
	public void onPreUpdate(MotionUpdateEvent event) {
		if (mode.getSelected() == 0)
			KeyBinding.setKeyBindState(mc.gameSettings.keyBindSneak.getKeyCode(), true);
		else
			mc.thePlayer.movementInput.sneak = true;
	}

	@Override
	public void onPostUpdate(MotionUpdateEvent event) {
		if (mode.getSelected() == 0)
			KeyBinding.setKeyBindState(mc.gameSettings.keyBindSneak.getKeyCode(), true);
		else
			mc.thePlayer.movementInput.sneak = false;
	}
}
