package net.halalaboos.huzuni.rendering;

import net.halalaboos.huzuni.util.GLUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;

/**
 * Simple texture class, originally loaded a bufferedimage from file and stored a texture id like you would expect, but I decided to take advantage of Minecraft's resource shiz.
 * */
public class Texture {
	
	private final ResourceLocation texture;
	
	public Texture(String textureURL) {
		texture = new ResourceLocation(textureURL);
        Minecraft.getMinecraft().getTextureManager().bindTexture(texture);
	}
	
	public void render(float x, float y, float width, float height) {
		bindTexture();
		GLUtils.drawTexturedRect(x, y, width, height, 0F, 0F, 1F, 1F);
	}
	
	public void bindTexture() {
        Minecraft.getMinecraft().getTextureManager().bindTexture(texture);
	}
	
}
