package net.halalaboos.huzuni.gui.screen.macro;

import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glEnable;

import org.lwjgl.input.Keyboard;

import net.minecraft.client.gui.*;
import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.gui.clickable.ContainerManager;
import net.halalaboos.huzuni.gui.clickable.menu.Menu;
import net.halalaboos.huzuni.gui.clickable.theme.HuzuniTheme;
import net.halalaboos.huzuni.util.GLUtils;
import pw.brudin.huzuni.util.screen.PanoramaRenderer;

import java.io.IOException;

public class MacroManager extends GuiScreen implements GuiYesNoCallback {

	private final GuiScreen parentGui;

	private PanoramaRenderer panoramaRenderer;

    private GuiTextField findField;
    private GuiButton edit;
    private GuiButton remove;

    private final ContainerManager<Menu> container = new ContainerManager<Menu>(new HuzuniTheme());
    private final Menu menu = new Menu("Macros");
    private final SlotMacros slotMacros = new SlotMacros();
        
    public MacroManager(GuiScreen par1GuiScreen) {
        parentGui = par1GuiScreen;
        menu.add(slotMacros);
    	container.add(menu);
    	for (int i = 0; i < Huzuni.MACROS.length; i++)
    		if (Huzuni.MACROS[i] != null)
    			slotMacros.add(i + ":" + Huzuni.MACROS[i]);
    }

    @Override
    public void initGui() {
        int slotWidth = 300, slotHeight = (int) ((height - 110) * GLUtils.getScaleFactor()) / 2; // (GLUtils.getScreenHeight() - 114);

        panoramaRenderer = new PanoramaRenderer(width, height);
        panoramaRenderer.init();

        menu.setX(GLUtils.getScreenWidth() / 2 - slotWidth / 2);
        menu.setY(30);
        menu.setWidth(slotWidth);
        menu.setHeight(slotHeight);
        slotMacros.setX(2);
        slotMacros.setY(2);
        slotMacros.setWidth(slotWidth - 4);
        slotMacros.setHeight(slotHeight - 16);

    	findField = new GuiTextField(1, mc.fontRenderer, width / 2 - 110, height - 76, 220, 20);
    	findField.setFocused(true);
    	
        buttonList.add(new GuiButton(6, this.width / 2 - 154, this.height - 28, 152 * 2, 20, "Done"));
        buttonList.add(new GuiButton(14, this.width / 2 - 154, this.height - 52, 100, 20, "Add"));
        
        edit = new GuiButton(5, this.width / 2 + 50, this.height - 52, 100, 20, "Edit");
        buttonList.add(edit);
        edit.enabled = slotMacros.hasSelectedComponent();
        remove = new GuiButton(12, this.width / 2 - 52, this.height - 52, 101, 20, "Remove");
        buttonList.add(remove);
        remove.enabled = slotMacros.hasSelectedComponent();
    }

    @Override
    protected void actionPerformed(GuiButton par1GuiButton) {
        if (!par1GuiButton.enabled) {
            return;
        }

        switch (par1GuiButton.id) {
            case 5:
            	if (slotMacros.hasSelectedComponent()) {
                    mc.displayGuiScreen(new EditMacro(this, Integer.parseInt(slotMacros.getSelectedComponentO().split(":")[0]), slotMacros.getSelectedComponentO().substring(slotMacros.getSelectedComponentO().indexOf(":") + 1)));
                }
                break;
            case 6:
                mc.displayGuiScreen(parentGui);
                break;
            case 12:
                mc.displayGuiScreen(new GuiYesNo(this, "Remove macro '" + Keyboard.getKeyName(Integer.parseInt(slotMacros.getSelectedComponentO().split(":")[0])) + "'", "Are you sure?", "Yes", "No", 0));
                break;
            case 13:
                break;
            case 14:
                mc.displayGuiScreen(new EditMacro(this));
                break;
            default:
                break;
        }
    }

    @Override
    public void confirmClicked(boolean confirm, int id) {
        if (confirm && id == 0) {
        	if (slotMacros.hasSelectedComponent()) {
        		Huzuni.MACROS[Integer.parseInt(slotMacros.getSelectedComponentO().split(":")[0])] = null;
        		slotMacros.getComponents().remove(slotMacros.getSelectedComponentO());
        		slotMacros.setSelectedComponent(-1);
        	}
           findField.setText("");
           updateMacrosList();
        }
        mc.displayGuiScreen(this);
    }

	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException {
		super.keyTyped(typedChar, keyCode);
		this.container.keyTyped(keyCode, typedChar);
		findField.textboxKeyTyped(typedChar, keyCode);
		updateMacrosList();
	}

	public void updateMacrosList() {
    	slotMacros.getComponents().clear();
        if (!findField.getText().isEmpty()) {
        	for (int i = 0; i < Huzuni.MACROS.length; i++)
        		if (Huzuni.MACROS[i] != null && Huzuni.MACROS[i].toLowerCase().contains(findField.getText().toLowerCase()))
        			slotMacros.getComponents().add(i + ":" + Huzuni.MACROS[i]);
        } else {
        	for (int i = 0; i < Huzuni.MACROS.length; i++)
        		if (Huzuni.MACROS[i] != null)
        			slotMacros.add(i + ":" + Huzuni.MACROS[i]);
        }
        slotMacros.setScrollPercentage(0F);
	}

    @Override
    protected void mouseClicked(int x, int y, int buttonId) throws IOException {
        super.mouseClicked(x, y, buttonId);
        container.mouseClicked(GLUtils.getMouseX(), GLUtils.getMouseY(), buttonId);
        findField.mouseClicked(x, y, buttonId);
    }

    @Override
    public void drawScreen(int par1, int par2, float par3) {
        panoramaRenderer.renderSkybox(par1, par2, par3);
        edit.enabled = slotMacros.hasSelectedComponent();
        remove.enabled = slotMacros.hasSelectedComponent();
        super.drawScreen(par1, par2, par3);
        findField.drawTextBox();
        GLUtils.setupOverlayDefaultScale();
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        container.render();
        glDisable(GL_BLEND);
        mc.entityRenderer.setupOverlayRendering();
        panoramaRenderer.renderFade();
    }

    @Override
    public void updateScreen() {
        super.updateScreen();
        panoramaRenderer.panoramaTick();
        findField.updateCursorCounter();
    }

    @Override
    public void onGuiClosed() {
        super.onGuiClosed();
        findField.setText("");
        updateMacrosList();
    }
}
