/**
 *
 */
package net.halalaboos.huzuni.mods;

import net.halalaboos.huzuni.Huzuni;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * @author Halalaboos
 * @since Aug 25, 2013
 */
public class ModuleLoader {
    public static final String CONFIG_FILE = "Config.cfg";
    public static final ModuleLoader instance = new ModuleLoader();

    private ModuleLoader() {

    }

    /**
     * Reads the classpath file from the jar file, then tries to instantiate it as a module.
     *
     * @return Module that was sucessfully instantiated from the jar file.
     * @throws Exception
     */
    public Module loadModule(File file) throws Exception {
        Huzuni.logger.log("Loading module from the file: " + file.getName());
        String classPath = null;
        JarFile jarFile = new JarFile(file);
        JarEntry entry;
        Enumeration<JarEntry> entries = jarFile.entries();

        while (entries.hasMoreElements()) {
            entry = entries.nextElement();
            if (entry.isDirectory())
                continue;
            if (entry.getName().equalsIgnoreCase(CONFIG_FILE)) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(jarFile.getInputStream(entry)));
                for (String line = null; (line = reader.readLine()) != null; ) {
                    if (line.startsWith("#"))
                        continue;
                    else if (line.startsWith("classpath:")) {
                        classPath = line.substring("classpath:".length()).replaceAll(" ", "");
                    }
                }
                reader.close();
            }
        }
        jarFile.close();
        return Huzuni.modManager.loadJar(file, classPath, (Object[]) null);
    }

}
