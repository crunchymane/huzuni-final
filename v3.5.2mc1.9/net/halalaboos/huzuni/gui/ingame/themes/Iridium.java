/**
 *
 */
package net.halalaboos.huzuni.gui.ingame.themes;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.mod.Category;
import net.halalaboos.huzuni.gui.ingame.IngameTheme;
import net.halalaboos.huzuni.mod.DefaultMod;
import net.halalaboos.huzuni.mod.ModManager;
import net.minecraft.client.Minecraft;
import org.lwjgl.opengl.GL11;

/**
 * @author Halalaboos
 *
 * @since Sep 22, 2013
 */
public class Iridium implements IngameTheme {

	@Override
	public void render(Minecraft mc, int screenWidth, int screenHeight) {
		GL11.glEnable(GL11.GL_BLEND);
		drawIrString(Huzuni.TITLE, 2, 2, 0x80FFFFFF);
		int height = 2;
		for (DefaultMod mod : ModManager.getDefaultMods()) {
			if (mod.isEnabled() && mod.getCategory() != null) {
				drawIrString(mod.getRenderName(), screenWidth - mc.fontRenderer.getStringWidth(mod.getRenderName()) - 2, height, getColor(mod.getCategory()));
				height += 10;
			}
		}
		GL11.glDisable(GL11.GL_BLEND);
	}

	private void drawIrString(String text, int x, int y, int color) {
		GL11.glTranslatef(0.5F, 0.5F, 0);
		Minecraft.getMinecraft().fontRenderer.drawString(text, x, y, ((color >> 24 & 0xFF) << 24));
		GL11.glTranslatef(-0.5F, -0.5F, 0);
		Minecraft.getMinecraft().fontRenderer.drawString(text, x, y, color);
	}

	@Override
	public String getName() {
		return "Iridium";
	}

	@Override
	public void onKeyTyped(int keyCode) { }

	private int getColor(Category category) {
		switch (category) {
			case COMBAT:
				return 0xAD3939;
			case MOVEMENT:
				return 0x517E9C;
			case RENDER:
				return 0x5E67AB;
			case WORLD:
				return 0x519C55;
			case PLAYER:
				return 0x96844D;
			default:
				return 0x878787;
		}
	}
}
