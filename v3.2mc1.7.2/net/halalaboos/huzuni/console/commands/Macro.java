package net.halalaboos.huzuni.console.commands;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.client.keybinds.CommandKeybind;
import net.halalaboos.huzuni.console.Command;
import net.halalaboos.huzuni.helpers.Helper;

import org.lwjgl.input.Keyboard;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Macro implements Command {

    public Macro() {
    }

    @Override
	public String[] getAliases() {
		return new String[] { "macro" };
	}

	@Override
	public String[] getHelp() {
		return new String[] { ".macro add/del <key> <message>" };
	}

    @Override
    public String getDescription() {
        return "Binds a chat message to a key.";
    }

    @Override
    public void run(String input, String[] args) {
        if (args[0].equalsIgnoreCase("add")) {
            final String message = Helper.stringHelper.getAfter(input, 3);
           String key = args[1].toUpperCase();
            int keyCode = Keyboard.getKeyIndex(key);
            addMacro(keyCode, message);
            huzuni.addChatMessage("Added macro: '" + message + "' with key: " + args[1].toUpperCase());
        } else if (args[0].equalsIgnoreCase("del") || args[0].equalsIgnoreCase("rem")) {
            int keyCode = Keyboard.getKeyIndex(args[1].toUpperCase());
            huzuni.keybindManager.remove(keyCode);
        }
    }

    private void addMacro(int keyCode, final String message) {
        huzuni.keybindManager.add(keyCode, new CommandKeybind(message));
        huzuni.keybindManager.save();
    }

}
