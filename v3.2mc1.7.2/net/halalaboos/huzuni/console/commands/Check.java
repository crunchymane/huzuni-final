/**
 *
 */
package net.halalaboos.huzuni.console.commands;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.console.Command;
import net.halalaboos.lib.io.FileUtils;
import net.minecraft.client.gui.GuiPlayerInfo;
import net.minecraft.util.StringUtils;

import java.io.File;
import java.util.List;

/**
 * @author Halalaboos
 * @since Aug 11, 2013
 */
public class Check implements Command {

    /**
     * @see net.halalaboos.huzuni.console.Command#getAliases()
     */
    @Override
    public String[] getAliases() {
        return new String[] {"check", "czech"};
    }

    /**
     * @see net.halalaboos.huzuni.console.Command#getHelp()
     */
    @Override
    public String[] getHelp() {
        return new String[] {"check"};
    }

    /**
     * @see net.halalaboos.huzuni.console.Command#getDescription()
     */
    @Override
    public String getDescription() {
        return "Checks your alts to see if they are on this server.";
    }

    /**
     * @see net.halalaboos.huzuni.console.Command#run(java.lang.String, java.lang.String[])
     */
    @Override
    public void run(String input, String[] args) {
        huzuni.addChatMessage("Checking..");
        List<String> alts = FileUtils.readFile(new File(Huzuni.getSaveDirectory(), "Accounts.txt"));
        for (GuiPlayerInfo playerInfo : (List<GuiPlayerInfo>) mc.getNetHandler().playerInfoList) {
            String name = StringUtils.stripControlCodes(playerInfo.name);
            if (name.equals(mc.thePlayer.getCommandSenderName()))
                continue;
            for (String alt : alts) {
                String username = alt.split(":")[0];
                if (name.equalsIgnoreCase(username))
                    huzuni.addChatMessage("Account '" + username + "' is on this server!");
            }
        }
        huzuni.addChatMessage("Done!");
    }

}
