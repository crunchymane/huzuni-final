package net.halalaboos.huzuni.plugin.plugins.render;

import net.halalaboos.huzuni.api.plugin.ModeOption;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;

import org.lwjgl.input.Keyboard;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.plugin.Category;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent;
import net.halalaboos.huzuni.event.tick.UpdateListener;
import net.halalaboos.huzuni.plugin.DefaultPlugin;
import net.halalaboos.huzuni.util.Timer;

/**
 * @author brudin
 * @version 1.0
 * @since 3/26/14
 */
public class Bright extends DefaultPlugin implements UpdateListener {

	private float oldGamma = 0F;

	private final int GAMMA = 0, NIGHTVISION = 1;
	private final float MAX_BRIGHTNESS = 10F, INCREMENT = 0.25F;

	private final Timer timer = new Timer();
	
    private final ModeOption mode;
	
	public Bright() {
		super("Bright", Keyboard.KEY_C);
		setCategory(Category.RENDER);
		setDescription("Brightens up the world.");
		setAuthor("Halalaboos");
		/* Maybe just have a config for mode? ie nightvision, gamma, lightmap
		* This way we won't have Bright and NightVision, we could just have .bright mode nightvision
		* 	and it could display ingame like Bright (NightVision) or something. */
		setOptions(mode = new ModeOption("Bright mode", "Gamma", "NightVision"));
 	}

	@Override
	protected void onEnable() {
		if (mc.gameSettings.gammaSetting <= 1) {
			oldGamma = mc.gameSettings.gammaSetting;
		}
		Huzuni.registerUpdateListener(this);
	}

	@Override
	protected void onDisable() {
		if (mode.getSelected() == NIGHTVISION) {
			mc.thePlayer.removePotionEffect(Potion.nightVision.id);
			Huzuni.unregisterUpdateListener(this);
		}
	}

	@Override
	public void onPreUpdate(MotionUpdateEvent event) {
		if (timer.hasReach(25) && mode.getSelected() == GAMMA) {
			if (this.isEnabled()) {
				mc.thePlayer.removePotionEffect(Potion.nightVision.id);
				mc.gameSettings.gammaSetting += INCREMENT;
				if (mc.gameSettings.gammaSetting >= MAX_BRIGHTNESS) {
					mc.gameSettings.gammaSetting = MAX_BRIGHTNESS;
					Huzuni.unregisterUpdateListener(this);
				}
			} else {
				mc.gameSettings.gammaSetting -= INCREMENT;
				if (mc.gameSettings.gammaSetting <= oldGamma) {
					mc.gameSettings.gammaSetting = oldGamma;
					Huzuni.unregisterUpdateListener(this);
				}
			}
			timer.reset();
		} else if (mode.getSelected() == NIGHTVISION) {
			int duration = 1000000;
			PotionEffect nightVision = new PotionEffect(Potion.nightVision.id, duration, 1);
			nightVision.setPotionDurationMax(true);
			mc.thePlayer.addPotionEffect(nightVision);
		}
	}

	@Override
	public void onPostUpdate(MotionUpdateEvent event) {
	}
}
