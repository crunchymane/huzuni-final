package net.halalaboos.chatwork.protocol.packet;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.halalaboos.chatwork.protocol.data.Player;
import net.halalaboos.chatwork.protocol.data.User;
import net.halalaboos.chatwork.protocol.data.UserInfo;
import net.halalaboos.network.api.Handler;
import net.halalaboos.network.api.Packet;
import net.halalaboos.network.utils.NetworkUtils;

public class PacketPlayerList implements Packet <List<User>> {

	@Override
	public void write(Handler handler, List<User> data,
			DataOutputStream outputStream) throws IOException {
		byte size = (byte) data.size();
		outputStream.writeByte(size);
		for (int i = 0; i < size; i++) {
			User user = data.get(i);
			outputStream.writeByte((int) user.getId());
			NetworkUtils.writeString(outputStream, user.getInfo().getUsername());
			NetworkUtils.writeString(outputStream, user.getInfo().getLocation());
			NetworkUtils.writeString(outputStream, user.getInfo().getIngameUsername());
		}
	}

	@Override
	public List<User> read(Handler handler, DataInputStream inputStream)
			throws IOException {
		List<User> players = new ArrayList<User>();
		int size = inputStream.readByte();
		for (int i = 0; i < size; i++) {
			players.add(new Player(inputStream.readByte(), new UserInfo(NetworkUtils.readString(inputStream), null, NetworkUtils.readString(inputStream), NetworkUtils.readString(inputStream), 0)));
		}
		return players;
	}

}
