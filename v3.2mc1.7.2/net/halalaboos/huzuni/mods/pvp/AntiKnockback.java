/**
 *
 */
package net.halalaboos.huzuni.mods.pvp;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;

import net.halalaboos.huzuni.events.game.EventRecieveVelocity;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.DefaultModule;

/**
 * @author Halalaboos
 */
public class AntiKnockback extends DefaultModule implements Listener {

    /**
     * @param name
     * @param keyCode
     */
    public AntiKnockback() {
        super("Anti Knockback", -1);
        setRenderWhenEnabled(true);
        setCategory(Category.PVP);
        setDescription("Stops you from recieving knockback velocity.");
    }

    /**
     * @see net.halalaboos.huzuni.mods.DefaultModule#onEnable()
     */
    @Override
    protected void onEnable() {
    	registry.registerListener(this);
    }

    /**
     * @see net.halalaboos.huzuni.mods.DefaultModule#onDisable()
     */
    @Override
    protected void onDisable() {
    	registry.unregisterListener(this);
    }

    /**
     * @see net.halalaboos.huzuni.mods.DefaultModule#onToggle()
     */
    @Override
    protected void onToggle() {
    }
    
    @EventTarget
    public void onRecieveVelocity(EventRecieveVelocity event) {
    	event.setVelocityX(0);
    	event.setVelocityY(0);
    	event.setVelocityZ(0);

    }

}
