/**
 * 
 */
package net.halalaboos.huzuni.ui.windows.hub;

import java.awt.Color;
import java.awt.Dimension;
import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.client.Settings;
import net.halalaboos.huzuni.client.waypoints.Waypoint;
import net.halalaboos.huzuni.ui.clickable.containers.HuzuniWindow;
import net.halalaboos.huzuni.ui.clickable.theme.BmcTheme;
import net.halalaboos.huzuni.ui.clickable.theme.FalconTheme;
import net.halalaboos.huzuni.ui.clickable.theme.HuzuniTheme;
import net.halalaboos.huzuni.ui.clickable.theme.ProximityTheme;
import net.halalaboos.huzuni.ui.windows.WindowManager;
import net.halalaboos.huzuni.ui.windows.waypoints.SlotWaypoints;
import net.halalaboos.lib.ui.Component;
import net.halalaboos.lib.ui.components.Button;
import net.halalaboos.lib.ui.components.Dropdown;
import net.halalaboos.lib.ui.components.SlotComponent;
import net.halalaboos.lib.ui.components.TextArea;
import net.halalaboos.lib.ui.containers.Window;
import net.halalaboos.lib.ui.listeners.ActionListener;
import net.halalaboos.lib.ui.theme.Theme;

/**
 * @author Halalaboos
 *
 * @since Oct 6, 2013
 */
public class HubWindow extends HuzuniWindow implements ActionListener {
	private final WindowManager windowContainer;
	
	private final SlotComponent<HuzuniWindow> slotWindows;
	
	private final Dropdown themeDropdown;
	
	/**
	 * @param title
	 */
	public HubWindow(WindowManager windowContainer) {
		super("Hub");
		this.windowContainer = windowContainer;
		slotWindows = new SlotHub(new Dimension(100, 54));
		add(slotWindows);
		slotWindows.setComponents(windowContainer.getWindows());
		themeDropdown = new Dropdown("Theme");
		themeDropdown.setComponents(windowContainer.getThemeNames());
		themeDropdown.setSelectedComponent(windowContainer.getSelectedThemeIndex());
		add(themeDropdown);
		themeDropdown.addActionListener(this);
	}
	
	/**
	 * @see net.halalaboos.lib.ui.listeners.ActionListener#onAction(net.halalaboos.lib.ui.Component)
	 */
	@Override
	public void onAction(Component component) {
		if (component == themeDropdown) {
			windowContainer.updateThemes(themeDropdown.getSelectedComponent());
		}
	}
}
