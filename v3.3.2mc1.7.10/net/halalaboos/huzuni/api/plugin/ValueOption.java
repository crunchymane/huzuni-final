package net.halalaboos.huzuni.api.plugin;

import java.text.DecimalFormat;

import org.lwjgl.input.Mouse;
import org.w3c.dom.Element;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.gui.render.Theme;
import net.halalaboos.huzuni.plugin.value.Value;
import net.halalaboos.huzuni.util.GLUtils;

public class ValueOption extends Option {

	private float pointSize = 7F;
	
    private int pointPadding = 1;
    
    private float sliderPercentage;
    
	private boolean dragging = false;

	private final Value value;
	
    protected DecimalFormat formatter = new DecimalFormat("#.#");
	
	public ValueOption(Value value) {
		super(value.getName());
		this.value = value;
		this.setValue(value.getValue());
	}
	
	@Override
	public boolean mouseClicked(int x, int y, int buttonId) {
    	boolean mouseAround = this.isPointInside(x, y);
    	if (mouseAround && buttonId == 0)
    		dragging = true;
    	return mouseAround;
    }
	
	@Override
	public void mouseReleased(int x, int y, int buttonId) {
    	if (buttonId == 0) {
    		dragging = false;
    		this.updateValue();
    	}
    }
	
	private void updateValue() {
		this.value.setValue(this.getValue());
	}

	private void handleDragging() {
        if (dragging && Mouse.isButtonDown(0)) {
            //get the difference (in pixels) from the cursor and the beginning of the slider boundaries ( (mouse X) / (slider X) ) and subtract the width of the slider.
            float differenceWithMouseAndSliderBase = (float) (GLUtils.getMouseX() - this.getX()) - (pointSize / 2F);
            //converted into 0.0F ~ 1.0F percentage
            sliderPercentage = differenceWithMouseAndSliderBase / getWidthForPoint();
        	constrictSlider();
            updateValue();
        } else {
        	dragging = false;
        	setValue(value.getValue());
        }
   }

	public float getValue() {
		float calculatedValue = (sliderPercentage * (value.getMaxValue() - value.getMinValue()));
		if (value.getIncrementValue() == -1)
			return calculatedValue + value.getMinValue();
		return ((calculatedValue) - /* This is literally how much the calculated value is off from being incremented by the increment value. */((calculatedValue) % value.getIncrementValue())) + value.getMinValue();
	}

	public void setValue(float value) {
		sliderPercentage = ((float) value - this.value.getMinValue()) / (this.value.getMaxValue() - this.value.getMinValue());
		constrictSlider();
	}

	private float getWidthForPoint() {
		float maxPointForRendering = (float) (this.getWidth() - pointSize - pointPadding),
				beginPoint = (pointPadding);
		return maxPointForRendering - beginPoint;
	}

	private void constrictSlider() {
		if (sliderPercentage < 0.0F)
			sliderPercentage = 0.0F;
		if (sliderPercentage > 1.0F)
			sliderPercentage = 1.0F;
	}

	private float getPositionForPoint() {
		return ((float) sliderPercentage * getWidthForPoint());
	}

	@Override
	public void render(Theme theme, int index, float[] area, boolean mouseOver, boolean mouseDown) {
		handleDragging();
		String value = formatter.format(this.value.getValue());
		theme.renderSlot(0, 0, index, area, false, mouseOver, mouseDown);
		Huzuni.drawString(this.name, area[0] + 2, area[1] + 2, 0xFFFFFF);
		Huzuni.drawString(value, area[0] + area[2] - Huzuni.getStringWidth(value) - 2, area[1] + 2, 0xFFFFFF);
		theme.renderSlot(0, 0, index, getSliderPoint(), true, mouseOver, mouseDown);
	}
    
    public float[] getSliderPoint() {
    	return new float[] {
    			getX() + getPositionForPoint() + pointPadding,
    			getY() + pointPadding,
    			pointSize,
    			getHeight() - pointPadding * 2
    	};
    }

	@Override
	public void load(Element element) {
		//float value = Float.parseFloat(element.getAttribute(name.replaceAll(" ", "_")));
		//this.value.setValue(value);
	}

	@Override
	public void save(Element element) {
		//element.setAttribute(name.replaceAll(" ", "_"), Float.toString(this.value.getValue()));
	}
}
