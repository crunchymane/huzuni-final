package net.halalaboos.huzuni.api.request;

import java.util.ArrayList;
import java.util.List;

public class RequestQueue <A extends Applicant<?>, D> {
	
	private String title;
	
	protected List<A> applicants = new ArrayList<A>();
	
	protected A requested = null;
	
	protected D data;
	
	protected boolean hasRequest = false;
	
	public RequestQueue() {
		this.title ="Mod Priority";
	}
	
	public void request(A applicant, D data) {
		if (hasRequest) {
			if (requested == applicant || isLessThan(requested, applicant)) {
				requested.onRequestDenied();
				requested = applicant;
				this.data = data;
			} else
				applicant.onRequestDenied();
		} else {
			hasRequest = true;
			requested = applicant;
			this.data = data;
		}
	}
	
	public void reset() {
		hasRequest = false;
		requested = null;
	}
	
	public boolean hasPriority(A applicant) {
		return hasRequest ? isLessThan(requested, applicant) : true;
	}

	private boolean isLessThan(A current, A applicant) {
		return applicants.indexOf(applicant) < applicants.indexOf(current);
	}

	public boolean hasRequest() {
		return hasRequest;
	}

	public Applicant<?> getRequested() {
		return requested;
	}
	
	public void registerApplicant(A applicant) {
		if (applicants.isEmpty())
			applicants.add(applicant);
		else
			applicants.add(applicants.size(), applicant);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public List<A> getApplicants() {
		return applicants;
	}
	
	public void setApplicants(List<A> applicants) {
		this.applicants = applicants;
	}
}
