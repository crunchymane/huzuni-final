/**
 * 
 */
package net.halalaboos.huzuni.events.game;

import net.minecraft.item.ItemStack;
import net.minecraft.util.Vec3;
import com.darkmagician6.eventapi.events.premade.EventCancellable;

/**
 * @author Halalaboos
 *
 */
public class EventPlaceBlock extends EventCancellable {
	
	public final ItemStack item;
	
	public final int x, y, z, side;
	
	public final Vec3 offset;
	
	public EventPlaceBlock(ItemStack item, int x, int y, int z, int side, Vec3 offset) {
		this.item = item;
		this.x = x;
		this.y = y;
		this.z = z;
		this.side = side;
		this.offset = offset;
	}
	
}
