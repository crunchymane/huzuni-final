package net.halalaboos.huzuni.mod.mods.player;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.mod.ModeOption;
import net.halalaboos.huzuni.api.mod.Category;
import net.halalaboos.huzuni.event.packet.PacketEvent;
import net.halalaboos.huzuni.event.packet.PacketListener;
import net.halalaboos.huzuni.event.packet.PacketEvent.PacketType;
import net.halalaboos.huzuni.mod.DefaultMod;
import net.halalaboos.huzuni.util.GameUtils;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.init.MobEffects;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.client.CPacketPlayerDigging;
import net.minecraft.potion.Potion;
import net.minecraft.util.math.BlockPos;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

public class Autotool extends DefaultMod implements PacketListener {

	private boolean running = false;
	
	private int oldSlot, newSlot;
	
    private final ModeOption mode;
    
	public Autotool() {
		super("Auto Tool", Keyboard.KEY_J);
		setCategory(Category.PLAYER);
		setAliases("at", "tool");
		setAuthor("Halalaboos");
		setDescription("Automatically switches to the best tool when mining.");
		setOptions(mode = new ModeOption("Auto tool mode", null, "Switch", "Normal"));
	}

	@Override
	protected void onEnable() {
		Huzuni.registerPacketListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterPacketListener(this);
	}

	@Override
	public void onPacket(PacketEvent event, PacketType type) {
		if (type == PacketType.SENT && !mc.playerController.isInCreativeMode()) {
			if (event.getPacket() instanceof CPacketPlayerDigging) {
				CPacketPlayerDigging packet = (CPacketPlayerDigging) event.getPacket();
				if (packet.getAction() == CPacketPlayerDigging.Action.START_DESTROY_BLOCK) { // Click block
					int newSlot = bestSlot(packet.getPosition(), mc.theWorld.getBlockState(packet.getPosition()).getBlock());
					if (!running) {
						oldSlot = mc.thePlayer.inventory.currentItem;
						running = true;
					}
					if (newSlot != -420) {
						this.newSlot = newSlot;
						if (mc.thePlayer.inventory.currentItem != this.newSlot) {
							mc.thePlayer.inventory.currentItem = this.newSlot;
							mc.playerController.updateController();
						}
					}
				}
			} else  { // Break block, but probably better just in a tick event
				if (running && !Mouse.isButtonDown(0)) {
					if (mode.getSelected() == 0 && mc.thePlayer.inventory.currentItem != oldSlot) {
						mc.thePlayer.inventory.currentItem = oldSlot;
						mc.playerController.updateController();
					}
					running = false;
				}
			}
		}
	}
	
	private int bestSlot(BlockPos position, Block block) {
		int slot = -420;
		float bestHardness = 0F;
		for (int i = 0; i < 9; i++) {
			try {
				ItemStack item = mc.thePlayer.inventory.getStackInSlot(i);
				if (item != null) {
					float hardness = getRelativeBlockHardness(position, block, item);
					if (hardness > bestHardness) {
						bestHardness = hardness;
						slot = i;
					}
				}
			} catch (Exception e) { }
		}
		return slot;
	}
	
	public float getRelativeBlockHardness(BlockPos position, Block block, ItemStack item) {
        float blockHardness = block.getBlockHardness(null, null, null); //no need for fancy stuff, returns the same shit regardless
        return blockHardness < 0.0F ? 0.0F : (!canHarvestBlock(position, item) ? getStrength(block, item, position) / blockHardness / 100.0F : getStrength(block, item, position) / blockHardness / 30.0F);
    }
	
	public boolean canHarvestBlock(BlockPos pos, ItemStack item) {
		if (GameUtils.getMaterial(pos).isToolNotRequired()) {
			return true;
		} else {
			return item != null ? item.canHarvestBlock(GameUtils.getState(pos)) : false;
		}
	}
	
	public float getStrength(Block block, ItemStack item, BlockPos pos) {
		float strength = item.getStrVsBlock(GameUtils.getState(pos));

		if (strength > 1.0F) {
			int efficiency = EnchantmentHelper.getEfficiencyModifier(mc.thePlayer);

			if (efficiency > 0 && item != null) {
				strength += (float) (efficiency * efficiency + 1);
			}
		}

		if (mc.thePlayer.isPotionActive(MobEffects.digSpeed)) {
			strength *= 1.0F + (float) (mc.thePlayer.getActivePotionEffect(MobEffects.digSpeed).getAmplifier() + 1) * 0.2F;
		}

		if (mc.thePlayer.isPotionActive(MobEffects.digSlowdown)) {
			float poitionModifier = 1.0F;

			switch (mc.thePlayer.getActivePotionEffect(MobEffects.digSlowdown).getAmplifier()) {
			case 0:
				poitionModifier = 0.3F;
				break;

			case 1:
				poitionModifier = 0.09F;
				break;

			case 2:
				poitionModifier = 0.0027F;
				break;

			case 3:
			default:
				poitionModifier = 8.1E-4F;
			}

			strength *= poitionModifier;
		}

		if (mc.thePlayer.isInsideOfMaterial(Material.water) && !EnchantmentHelper.getAquaAffinityModifier(mc.thePlayer)) {
			strength /= 5.0F;
		}

		if (!mc.thePlayer.onGround) {
			strength /= 5.0F;
		}

		return strength;
	}
}
