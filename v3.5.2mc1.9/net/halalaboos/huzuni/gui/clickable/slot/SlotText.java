package net.halalaboos.huzuni.gui.clickable.slot;

import java.util.ArrayList;
import java.util.List;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.gui.render.Theme;

public class SlotText extends SlotComponent<String> {

	private List<String> lines = new ArrayList<String>();
	
	public SlotText(float width, float height) {
		super(width, height);
		
	}

	@Override
	protected void onReleased(int index, String component, int mouseX,
			int mouseY) {
	}

	@Override
	protected void onClicked(int index, String component, float[] area,
			int mouseX, int mouseY, int buttonId) {
	}
	
	protected void render(Theme theme, int index, String text, float[] area, boolean highlight, boolean mouseOver,
			boolean mouseDown) {
		Huzuni.drawString(text, area[0], area[1], 0xFFFFFF);
	}
	
	@Override
	public void add(String line) {
		lines.add(line);
		List<String> formattedLines = format(line);
		for (String formattedLine : formattedLines)
			super.add(formattedLine);
	}
	
	@Override
	public void remove(String line) {
		lines.remove(line);
		super.add(line);
	}
	
	@Override
	public void clear() {
		super.clear();
		lines.clear();
	}

	@Override
	public List<String> getComponents() {
		return lines;
	}

	@Override
	public void setComponents(List<String> lines) {
		this.lines = lines;
		formatLines();
	}
	
	public void formatLines() {
		super.clear();
		if (lines == null)
			lines = new ArrayList<String>();
		for (String line : lines) {
			List<String> formattedLines = format(line);
			for (String formattedLine : formattedLines)
				super.add(formattedLine);
		}
	}
	
	@Override
	public void setWidth(float width) {
		super.setWidth(width);
		formatLines();
	}

	@Override
	public void setHeight(float height) {
		super.setHeight(height);
		formatLines();
	}
	
	@Override
	protected void onPositionUpdated() {
		super.onPositionUpdated();
		formatLines();
	}
	
	public List<String> format(String line) {
		List<String> lines = new ArrayList<String>();
		String newLine = "";
		char[] chars = line.toCharArray();
		for (char c : chars) {
			if (Huzuni.getStringWidth(newLine + c) >= this.getWidth() - this.getSliderPoint()[2] - 2) {
				lines.add(newLine);
				newLine = "" + c;
			} else
				newLine += c;
		}
		if (!newLine.isEmpty())
			lines.add(newLine);
		return lines;
	}
	
}
