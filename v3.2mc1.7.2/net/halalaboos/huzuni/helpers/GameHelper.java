package net.halalaboos.huzuni.helpers;

import net.minecraft.client.gui.ScaledResolution;

import org.lwjgl.input.Mouse;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public final class GameHelper implements MinecraftHelper {
    /**
     * @return Mouse X.
     */
    public int getMouseX() {
        return Mouse.getX() * getScreenWidth()
                / mc.displayWidth;
    }

    /**
     * @return Mouse Y.
     */
    public int getMouseY() {
        return getScreenHeight() - Mouse.getY() * getScreenHeight()
                / mc.displayHeight - 1;
    }

    /**
     * @return Screen width.
     */
    public int getScreenWidth() {
        return getScaledResolution().getScaledWidth();
    }

    /**
     * @return Screen height.
     */
    public int getScreenHeight() {
        return getScaledResolution().getScaledHeight();
    }

    /**
     * @return a new ScaledResolution instance.
     */
    public ScaledResolution getScaledResolution() {
        return new ScaledResolution(mc.gameSettings,
                mc.displayWidth,
                mc.displayHeight);
    }
}
