/**
 * 
 */
package net.halalaboos.lib.ui;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import net.halalaboos.lib.ui.listeners.ActionListener;
import net.halalaboos.lib.ui.listeners.KeyListener;

/**
 * @author Halalaboos
 *
 */
public abstract class DefaultComponent implements Component {
	
	protected final List<ActionListener> actionListeners = new CopyOnWriteArrayList<ActionListener>();
	  
	protected final List<KeyListener> keyListeners = new CopyOnWriteArrayList<KeyListener>();
	
	protected boolean active = true;
	
	protected Rectangle area = new Rectangle();
	
	protected Dimension dimensions;
	
	protected String tooltip = null;
	
	protected boolean mouseDown, mouseOver;
	
	protected Point offset;
	
	protected Rectangle renderableArea = new Rectangle();
	
	public DefaultComponent(Dimension dimensions) {
		this.dimensions = dimensions;
	}
	
	/**
	 * @see net.halalaboos.lib.ui.Component#onKeyTyped(int, char)
	 */
	@Override
	public void onKeyTyped(int keyCode, char c) {
		this.invokeKeyListeners(keyCode, c);
	}

	/**
	 * @see net.halalaboos.lib.ui.Component#isActive()
	 */
	@Override
	public boolean isActive() {
		return active;
	}

	/**
	 * @see net.halalaboos.lib.ui.Component#getArea()
	 */
	@Override
	public Rectangle getArea() {
		return area;
	}

	/**
	 * @see net.halalaboos.lib.ui.Component#setArea(java.awt.Rectangle)
	 */
	@Override
	public void setArea(Rectangle area) {
		this.area = area;
	}

	/**
	 * @see net.halalaboos.lib.ui.Component#getDimensions()
	 */
	@Override
	public Dimension getDimensions() {
		return dimensions;
	}

	/**
	 * @see net.halalaboos.lib.ui.Component#getTooltip()
	 */
	@Override
	public String getTooltip() {
		return tooltip;
	}

	/**
	 * @see net.halalaboos.lib.ui.Component#setMouseOver(boolean)
	 */
	@Override
	public void setMouseOver(boolean mouseOver) {
		this.mouseOver = mouseOver;
	}

	/**
	 * @see net.halalaboos.lib.ui.Component#isMouseOver()
	 */
	@Override
	public boolean isMouseOver() {
		return mouseOver;
	}

	/**
	 * @see net.halalaboos.lib.ui.Component#setMouseDown(boolean)
	 */
	@Override
	public void setMouseDown(boolean mouseDown) {
		this.mouseDown = mouseDown;
	}

	/**
	 * @see net.halalaboos.lib.ui.Component#isMouseDown()
	 */
	@Override
	public boolean isMouseDown() {
		return mouseDown;
	}

	/**
	 * @see net.halalaboos.lib.ui.Component#isPointInside(java.awt.Point)
	 */
	@Override
	public boolean isPointInside(Point point) {
		return renderableArea.contains(point);
	}
	
	@Override
	public Rectangle getRenderableArea() {
		return renderableArea;
	}

	/**
	 * @see net.halalaboos.lib.ui.Component#setOffset(java.awt.Point)
	 */
	@Override
	public void setOffset(Point offset) {
		this.offset = offset;
		updateRenderableArea();
	}
	
	protected void updateRenderableArea() {
		renderableArea.setBounds(area.x + offset.x, area.y + offset.y, area.width, area.height);
	}
	
    public void setActive(boolean active) {
		this.active = active;
	}

	public void setDimensions(Dimension dimensions) {
		this.dimensions = dimensions;
	}

	public void setTooltip(String tooltip) {
		this.tooltip = tooltip;
	}

	public void setRenderableArea(Rectangle renderableArea) {
		this.renderableArea = renderableArea;
	}

	public void addActionListener(ActionListener listener) {
        actionListeners.add(listener);
    }

    public void removeActionListener(ActionListener listener) {
        actionListeners.remove(listener);
    }
    
    public void addKeyListener(KeyListener listener) {
    	keyListeners.add(listener);
    }

    public void removeKeyListener(KeyListener listener) {
    	keyListeners.remove(listener);
    }

    public void invokeActionListeners() {
    	for (ActionListener actionListener : actionListeners) {
    		actionListener.onAction(this);
    	}
    }
    
    public void invokeKeyListeners(int keyCode, char c) {
    	for (KeyListener keyListener : keyListeners) {
    		keyListener.onKeyTyped(this, c, keyCode);
    	}
    }
    
}
