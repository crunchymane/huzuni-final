package net.halalaboos.chatwork.protocol;

import net.halalaboos.chatwork.protocol.packet.*;
import net.halalaboos.network.api.ProtocolHandler;

public class ChatworkProtocol extends ProtocolHandler {
	
	public void setup() {
		addPacket(new PacketMessage());// 0
		addPacket(new PacketUser()); // 1
		addPacket(new PacketPlayerList()); // 2
		addPacket(new PacketJoin()); // 3
		addPacket(new PacketLeave()); // 4
		addPacket(new PacketPoke());
	}
	
}
