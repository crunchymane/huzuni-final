package in.brud.huzuni.element;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.utils.Timer;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.util.EnumChatFormatting;

import java.awt.*;

/**
 * DESCRIPTION
 *
 * @author brudin
 * @version 1.0
 * @since 1/13/14
 */
public class HuzuniPassField extends GuiTextField {

    int alpha = 10;
    private long fade;

    public HuzuniPassField(FontRenderer par1FontRenderer, int par2, int par3, int par4, int par5) {
        super(par1FontRenderer, par2, par3, par4, par5);
    }

    private String toObf(String s) {
        String obf = "";
        for(int i = 0; i < s.length(); i++) {
            obf += "*";
        }
        return obf;
    }

    @Override
    public void drawTextBox(){
        Color background = new Color(0, 0, 0, alpha);
        drawRect(x, y, x + width, y + height, background.getRGB());
        Huzuni.display.guiFontRenderer.drawString(EnumChatFormatting.GRAY + toObf(text), x + 4, y + 7, -1);
        updateColor();
    }

    private void updateColor() {
        if (Timer.getSystemTime() - fade >= 15) {
            if (isFocused())
                alpha += 5;
            else
                alpha -= 5;
            fade = Timer.getSystemTime();
        }
        if (alpha < 75)
            alpha = 75;
        if (alpha > 175)
            alpha = 175;
    }
}