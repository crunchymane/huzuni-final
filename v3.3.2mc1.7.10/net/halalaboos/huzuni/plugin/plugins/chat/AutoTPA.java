package net.halalaboos.huzuni.plugin.plugins.chat;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.plugin.Category;
import net.halalaboos.huzuni.event.packet.PacketEvent;
import net.halalaboos.huzuni.event.packet.PacketListener;
import net.halalaboos.huzuni.plugin.DefaultPlugin;
import net.minecraft.client.Minecraft;
import net.minecraft.network.play.server.S02PacketChat;

/**
 * @author brudin
 * @version 1.0
 * @since 4/14/14
 */
public class AutoTPA extends DefaultPlugin implements PacketListener {

	public AutoTPA() {
		super("AutoAccept");
		setVisible(false);
		setAuthor("brudin");
		setDescription("Automatically accepts teleport requests from friends.");
		setCategory(Category.CHAT);
	}

	@Override
	public void onPacket(PacketEvent event, PacketEvent.PacketType type) {
		if(type == PacketEvent.PacketType.READ) {
			if(event.getPacket() instanceof S02PacketChat) {
				S02PacketChat packet = (S02PacketChat)event.getPacket();
				if(packet.func_148915_c().getFormattedText().contains("has requested to teleport to you")) {
					for(String s : Huzuni.FRIENDS) {
						if(packet.func_148915_c().getFormattedText().toLowerCase().contains(s.toLowerCase())) {
							Minecraft.getMinecraft().thePlayer.sendChatMessage("/etpyes " + s);
							break;
						}
					}
				}
			}
		}
	}

	@Override
	protected void onEnable() {
		Huzuni.registerPacketListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterPacketListener(this);
	}
}
