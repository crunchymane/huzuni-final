package net.halalaboos.huzuni.command.commands;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.console.Command;
import net.halalaboos.huzuni.api.mod.Mod;
import net.halalaboos.huzuni.mod.ModManager;
import net.minecraft.util.text.TextFormatting;

public class Authors implements Command {

	@Override
	public String[] getAliases() {
		return new String[] { "authors" };
	}

	@Override
	public String[] getHelp() {
		return new String[] { "authors" };
	}

	@Override
	public String getDescription() {
		return "Lists the authors' contributions";
	}

	@Override
	public void run(String input, String[] args) {
		List<Mod> mods = ModManager.getMods();
		Map<String, String> authorMap = new HashMap<String, String>(); 
		for (int i = 0; i < mods.size(); i++) {
			Mod mod = mods.get(i);
			if (authorMap.containsKey(mod.getAuthor())) {
				authorMap.put(mod.getAuthor(), authorMap.get(mod.getAuthor()) + mod.getName() + ", ");
			} else {
				authorMap.put(mod.getAuthor(), mod.getAuthor() + ": " + mod.getName() + ", ");
			}
		}
		Huzuni.addChatMessage(TextFormatting.BOLD + "TOTAL LIST OF AUTHORS: ");
		Set<String> authors = authorMap.keySet();
		for (String author : authors) {
			String modsList = authorMap.get(author);
			int count = modsList.split(", ").length;
			modsList = modsList.substring(0, modsList.length() - 2);
			Huzuni.addChatMessage(modsList + " (" + TextFormatting.GOLD + count + TextFormatting.RESET + ")");
		}
	}

}
