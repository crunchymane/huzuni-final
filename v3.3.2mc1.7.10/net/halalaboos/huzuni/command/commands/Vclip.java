package net.halalaboos.huzuni.command.commands;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.console.Command;
import net.minecraft.client.Minecraft;

/**
 * @author brudin
 * @version 1.0
 * @since 4/14/14
 */
public class Vclip implements Command {
	@Override
	public String[] getAliases() {
		return new String[] {"vclip", "vc", "up"};
	}

	@Override
	public String[] getHelp() {
		return new String[] {"vclip <amount>"};
	}

	@Override
	public String getDescription() {
		return "Teleports you up (or down) the specified amount.";
	}

	@Override
	public void run(String input, String[] args) {
		double amount = Double.parseDouble(args[0]);
		Minecraft.getMinecraft().thePlayer.boundingBox.offset(0, amount, 0);
		Huzuni.addChatMessage("Teleported " + (amount >= 0 ? "up" : "down") + " " + Math.abs(amount) + " blocks.");
	}
}
