/**
 * 
 */
package net.halalaboos.lib.ui.components;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;

import net.halalaboos.lib.ui.DefaultComponent;

/**
 * @author Halalaboos
 *
 * @since Oct 5, 2013
 */
public class Dropdown extends DefaultComponent {

	protected String title;
	
	protected int textPadding = 12;
	
	protected boolean down = false;
	
	protected String[] components;
	
	protected int selectedComponent = -1;
	
    protected Color selectedComponentColor = new Color(0F, 0.5F, 1F, 0.75F),
    topColor = new Color(0.3F, 0.3F, 0.3F, 0.75F),
    dropdownColor = new Color(0.3F, 0.3F, 0.3F, 0.75F), backgroundColor = new Color(0F, 0F, 0F, 0.75F);
    
	/**
	 * 
	 */
	public Dropdown(String title) {
		super(new Dimension(110, 13));
		this.title = title;
		this.components = new String[] { "" };
	}
	
	/**
	 * 
	 */
	public Dropdown(String title, String[] components) {
		super(new Dimension(110, 13));
		this.title = title;
		this.components = components;
	}

	/**
	 * @see net.halalaboos.lib.ui.Component#update(java.awt.Rectangle, boolean, boolean)
	 */
	@Override
	public void update(Point mouse) {
	}

	/**
	 * @see net.halalaboos.lib.ui.Component#onClicked(java.awt.Rectangle, boolean, java.awt.Point, int)
	 */
	@Override
	public boolean onMousePressed(int x, int y, int buttonId) {
		if (down) {
			int yPos = textPadding + 2;
			for (int i = 0; i < components.length; i++) {
				Rectangle componentBoundaries = new Rectangle(renderableArea.x + 1, renderableArea.y + yPos, renderableArea.width - 2, textPadding);
				if (componentBoundaries.contains(x, y)) {
					return true;
				}
				yPos += textPadding;
			}
			// Since it was down and you didn't click anything.
			down = false;
		} else {
			down = mouseOver;
		}
		return mouseOver;
	}

	/**
	 * @see net.halalaboos.lib.ui.Component#onReleased(boolean)
	 */
	@Override
	public void onMouseReleased(int x, int y, int buttonId) {
		if (down) {
			int yPos = textPadding + 2;
			for (int i = 0; i < components.length; i++) {
				Rectangle componentBoundaries = new Rectangle(renderableArea.x + 1, renderableArea.y + yPos, renderableArea.width - 2, textPadding);
				if (componentBoundaries.contains(x, y)) {
					if (selectedComponent != i) {
						this.selectedComponent = i;
						this.invokeActionListeners();
					}
					// Since you released onto this component.
					down = false;
					return;
				}
				yPos += textPadding;
			}
		}
	}

	@Override
	public boolean isPointInside(Point point) {
		if (down) {
			int x = renderableArea.x;
			int y = renderableArea.y;
			int height = (components.length + 1) * textPadding + 2;
			int width = renderableArea.width - 2;
			return x < point.x && x + width > point.x && y < point.y && y + height > point.y;
		} else
			return super.isPointInside(point);
	}
	
	public String getSelectedComponentName() {
		if (selectedComponent >= 0 && selectedComponent < components.length)
			return this.components[this.selectedComponent];
		else
			return "";
	}
	
	public int getSelectedComponent() {
		return selectedComponent;
	}

	public boolean isDown() {
		return down;
	}

	public void setDown(boolean down) {
		this.down = down;
	}

	public String[] getComponents() {
		return components;
	}

	public void setComponents(String[] components) {
		this.components = components;
	}

	public int getTextPadding() {
		return textPadding;
	}

	public void setTextPadding(int textPadding) {
		this.textPadding = textPadding;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Color getSelectedComponentColor() {
		return selectedComponentColor;
	}

	public void setSelectedComponentColor(Color selectedComponentColor) {
		this.selectedComponentColor = selectedComponentColor;
	}

	public Color getTopColor() {
		return topColor;
	}

	public void setTopColor(Color topColor) {
		this.topColor = topColor;
	}

	public Color getDropdownColor() {
		return dropdownColor;
	}

	public void setDropdownColor(Color dropdownColor) {
		this.dropdownColor = dropdownColor;
	}

	public void setSelectedComponent(int selectedComponent) {
		this.selectedComponent = selectedComponent;
	}

	public Color getBackgroundColor() {
		return backgroundColor;
	}

	public void setBackgroundColor(Color backgroundColor) {
		this.backgroundColor = backgroundColor;
	}

}
