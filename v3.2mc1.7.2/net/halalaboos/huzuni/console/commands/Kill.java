/**
 * 
 */
package net.halalaboos.huzuni.console.commands;

import net.halalaboos.huzuni.console.Command;

/**
 * @author Halalaboos
 *
 */
public class Kill implements Command {

	/**
	 * @see net.halalaboos.huzuni.console.Command#getAliases()
	 */
	@Override
	public String[] getAliases() {
		return new String[] { "kill" };
	}

	/**
	 * @see net.halalaboos.huzuni.console.Command#getHelp()
	 */
	@Override
	public String[] getHelp() {
		return new String[] { "Just jump! If it doesn't work, the server doesn't have nocheat." };
	}

	/**
	 * @see net.halalaboos.huzuni.console.Command#getDescription()
	 */
	@Override
	public String getDescription() {
		return "Kills you once you jump. (Nocheat+ dependant)";
	}

	/**
	 * @see net.halalaboos.huzuni.console.Command#run(java.lang.String, java.lang.String[])
	 */
	@Override
	public void run(String input, String[] args) {
		new Thread(){
            public void run(){
                try{
                    mc.thePlayer.jump();
                    Thread.sleep(150L);
                    mc.thePlayer.motionY = -999;
                    mc.thePlayer.noClip = true;
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }.start();
	}

}
