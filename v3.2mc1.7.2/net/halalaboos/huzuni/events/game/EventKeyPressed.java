/**
 *
 */
package net.halalaboos.huzuni.events.game;

import com.darkmagician6.eventapi.events.Event;

/**
 * @author Halalaboos
 * @since Jul 14, 2013
 */
public class EventKeyPressed implements Event {

    public final int keyCode;

    public EventKeyPressed(int keyCode) {
        this.keyCode = keyCode;
    }

}
