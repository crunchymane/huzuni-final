package net.halalaboos.huzuni.gui.clickable.slot;

import java.util.ArrayList;
import java.util.List;
import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.gui.render.Theme;
import net.halalaboos.huzuni.gui.clickable.DefaultComponent;
import net.halalaboos.huzuni.util.GLUtils;
import net.halalaboos.huzuni.util.MathUtils;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

public abstract class SlotComponent <T> extends DefaultComponent {
	public static final int SLIDER_POINT_WIDTH = 6;
	public static final int SLIDER_PADDING = 1;
	
	protected float lastMouseY, lastScrollbarY;
	
	protected int componentPadding = 1;
	protected List<T> components = new ArrayList<T>();
	protected float scrollPercentage = 0;
	protected T clickedComponent;
	protected int selectedComponent = -1;
	
	protected float totalComponentHeight = 0;

	protected boolean dragging = false, renderBackground = true, hasSlider = true;

	public SlotComponent(float width, float height) {
		super();
		this.setWidth(width);
		this.setHeight(height);
	}
	
	@Override
	public void update() {
		super.update();
		handleScrollWheel();
		handleDragging();
		calculateComponentHeight();
	}

	@Override
	public void mouseReleased(int x, int y, int buttonId) {
		if (isMouseOver() && clickedComponent != null) {

			int index = components.indexOf(clickedComponent);
			int yPos = (int) (2 - getScrollHeight() + getHeightFor(clickedComponent));

			if (MathUtils.inside(x, y, (int) (this.getOffsetX() + this.getX() + 2), (int) (this.getOffsetY() + this.getY() + yPos), (int) (getWidth() - (hasEnoughToScroll() && hasSlider ? (getSliderPoint()[2]) : 0) - 4), getComponentHeight(clickedComponent))) {
				selectedComponent = index;
				onReleased(index, clickedComponent, x, y);
			}

			clickedComponent = null;
		}
		dragging = false;
	}
	
	protected float getScrollHeight() {
		return (this.totalComponentHeight - getHeight() + 2 + componentPadding) * scrollPercentage;
	}
	
	protected float getHeightFor(T component) {
		int index = components.indexOf(component);
		float height = 0;
		for (int i = 0; i < index; i++) {
			height += this.getComponentHeight(components.get(i)) + componentPadding;
		}
		return height;
	}

	protected boolean shouldRender(float[] area) {
		return area[1] + area[3] > this.getY() + getOffsetY() && area[1] < this.getY() + getOffsetY() + getHeight();
	}
	
	@Override
	public boolean mouseClicked(int x, int y, int buttonId) {
		if (isMouseOver()) {
			float[] sliderPoint = getSliderPoint();
			lastMouseY = y;
			lastScrollbarY = sliderPoint[1];
			if (MathUtils.inside(x, y, sliderPoint)) {
				dragging = true;
			}
			int yPos = 2 - (int) (getScrollHeight());
			if (!components.isEmpty()) {
				for (int i = 0; i < components.size() && i >= 0; i++) {
					T component = components.get(i);
					float[] componentArea = new float[] { (int) (this.getOffsetX() + this.getX() + 2), (int) (this.getOffsetY() + this.getY() + yPos), (int) (getWidth() - (hasEnoughToScroll() && hasSlider ? (sliderPoint[2]) : 0) - 4), getComponentHeight(component) };
					if (shouldRender(componentArea)) {
						if (MathUtils.inside(x, y, componentArea)) {
							clickedComponent = component;
							onClicked(i, component, componentArea, x, y, buttonId);
							break;
						}
					}
					yPos += componentArea[3] + componentPadding;
				}
			}
		}
		return super.mouseClicked(x, y, buttonId);
	}

	public void renderComponents(Theme theme, float offsetX, float offsetY, boolean mouseOver, boolean mouseDown) {
		if (!components.isEmpty()) {
			int yPos = 2 - (int) (getScrollHeight());
			float[] sliderPoint = getSliderPoint();
			float[] mouse = new float[] { GLUtils.getMouseX(), GLUtils.getMouseY() };
			GL11.glEnable(GL11.GL_SCISSOR_TEST);
			GLUtils.glScissor(getX() + offsetX + 2, getY() + offsetY + 2, getX() + offsetX + getWidth() - (hasEnoughToScroll() && hasSlider ? (sliderPoint[2]) : 0), getY() + offsetY + getHeight() - 2);
			for (int i = 0; i < components.size(); i++) {
				T component = components.get(i);
				float[] componentArea = new float[] { getX() + offsetX + 2, getY() + offsetY + yPos, getWidth() - (hasEnoughToScroll() && hasSlider ? (sliderPoint[2]) : 0) - 4, getComponentHeight(component) };
				if (shouldRender(componentArea)) {
					render(theme, i, component, componentArea, selectedComponent == i, (mouseOver && MathUtils.inside(mouse[0], mouse[1], componentArea)) && (clickedComponent == null ? true : clickedComponent == component), mouseDown && clickedComponent == component);
				}
				yPos += componentArea[3] + componentPadding;
			}
			GL11.glDisable(GL11.GL_SCISSOR_TEST);
		}
	}

	protected void handleScrollWheel() {
		if (this.isMouseOver()) {
			if (Mouse.hasWheel()) {
				int amount = Mouse.getDWheel();
				if (amount > 0)
					amount = -1;
				else if (amount < 0)
					amount = 1;
				else if (amount == 0)
					return;
				if (clickedComponent == null) {
					if (hasEnoughToScroll()) {
						scrollPercentage += ((float) amount / (float) this.components.size());
						keepSafe();
					}
				}
			}
		}
	}

	protected void handleDragging() {
		if (dragging) {
			if (!hasEnoughToScroll() || !hasSlider) {
				dragging = false;
				return;
			}
			//get the difference (in pixels) from the cursor and the beginning of the slider boundaries ( (mouse X) / (slider X) ) and subtract the width of the slider.
			float differenceWithMouseAndSliderBase = (float) ((GLUtils.getMouseY() - (lastMouseY - lastScrollbarY) - 1) - (this.getY() + this.getOffsetY()));

			//converted into 0.0F ~ 1.0F percentage
			scrollPercentage = differenceWithMouseAndSliderBase / getHeightForPoint();
			this.keepSafe();
		}
	}
	
	private int getScrollBarHeight() {
		float ratio = getHeight() / (this.totalComponentHeight);
		if (ratio * getHeight() < 12)
			return 12;
		else
			return (int) (ratio * getHeight());
	}

	public float[] getSliderPoint() {
		int scrollbarY = (int) (this.scrollPercentage * getHeightForPoint());
		return new float[] { getOffsetX() + getX() + getWidth() - SLIDER_POINT_WIDTH - SLIDER_PADDING,
				getOffsetY() + getY() + scrollbarY + SLIDER_PADDING,
				SLIDER_POINT_WIDTH,
				getScrollBarHeight() };
	}

	public boolean hasEnoughToScroll() {
		return totalComponentHeight > this.getHeight();
	}

	protected float getHeightForPoint() {
		float maxPointForRendering = (float) (getHeight() - getScrollBarHeight() - SLIDER_PADDING),
				beginPoint = (SLIDER_PADDING);
		return maxPointForRendering - beginPoint;
	}
	
	protected void calculateComponentHeight() {
		totalComponentHeight = 0;
		for (int i = 0; i < components.size(); i++) {
			totalComponentHeight += this.getComponentHeight(components.get(i)) + componentPadding;
		}
	}

	protected void keepSafe() {
		if (scrollPercentage > 1)
			scrollPercentage = 1;
		if (scrollPercentage < 0)
			scrollPercentage = 0;
	}
	
	public void add(T component) {
		components.add(component);
	}
	
	public void remove(T component) {
		components.add(component);
	}
	
	public boolean contains(T component) {
		return components.contains(component);
	}
	
	public void clear() {
		components.clear();
		this.scrollPercentage = 0F;
	}
	
	public int getComponentPadding() {
		return componentPadding;
	}

	public void setComponentPadding(int componentPadding) {
		this.componentPadding = componentPadding;
	}

	public List<T> getComponents() {
		return components;
	}

	public void setComponents(List<T> components) {
		this.components = components;
		this.scrollPercentage = 0F;
	}

	public int getSelectedComponent() {
		return selectedComponent;
	}
	
	public T getSelectedComponentO() {
		return components.get(selectedComponent);
	}

	public void setSelectedComponent(int selectedComponent) {
		this.selectedComponent = selectedComponent;
	}

	public boolean isRenderBackground() {
		return renderBackground;
	}

	public void setRenderBackground(boolean renderBackground) {
		this.renderBackground = renderBackground;
	}

	public boolean hasSlider() {
		return hasSlider;
	}

	public void setHasSlider(boolean hasSlider) {
		this.hasSlider = hasSlider;
	}
	

	public boolean isDragging() {
		return dragging;
	}

	public void setDragging(boolean dragging) {
		this.dragging = dragging;
	}

	public float getScrollPercentage() {
		return scrollPercentage;
	}

	public void setScrollPercentage(float scrollPercentage) {
		this.scrollPercentage = scrollPercentage;
	}

	public boolean hasSelectedComponent() {
		return selectedComponent >= 0 && selectedComponent < components.size();
	}
	
	protected abstract void onReleased(int index, T component, int mouseX, int mouseY);

	protected abstract void onClicked(int index, T component, float[] area, int mouseX, int mouseY, int buttonId);

	protected void render(Theme theme, int index, T component, float[] area, boolean highlight, boolean mouseOver,
			boolean mouseDown) {
		theme.renderSlot(this.getOffsetX(), this.getOffsetY(), index, area, highlight, mouseOver, mouseDown);
		Huzuni.drawString(component.toString(), area[0] + 2, area[1] + 2, 0xFFFFFF);
	}

	protected int getComponentHeight(T component) {
		return 12;
	}

}
