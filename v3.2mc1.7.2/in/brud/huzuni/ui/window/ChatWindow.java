package in.brud.huzuni.ui.window;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.ui.clickable.containers.HuzuniWindow;
import net.halalaboos.lib.ui.components.*;
import net.halalaboos.lib.ui.layout.FittingLayout;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiChat;

import java.awt.*;

/**
 * chat window for jesus
 *
 * @author brudin
 * @version 1.0
 * @since 1/26/14
 */
public class ChatWindow extends HuzuniWindow {

    private SlotComponent<String> textArea;

    public ChatWindow() {
        super("Chat");
        SlotComponent<String> textArea = new SlotComponent<String>(new Dimension(200, 75)) {
            @Override
            public void render(int index, String text, Rectangle boundaries, boolean mouseOver, boolean mouseDown) {
                Huzuni.display.fontRenderer.drawStringWithShadow(text, boundaries.x, boundaries.y + 2, 0xFFFFFFFF);
            }

            @Override
            public void onClicked(int index, String text, Rectangle boundaries, Point mouse, int buttonID) {}

            @Override
            public void onReleased(int index, String text, Point mouse) {}

            @Override
            public int getElementHeight() {
                return Huzuni.display.fontRenderer.getHeight();
            }
        };
        textArea.setArea(new Rectangle(200, 75));
        this.textArea = textArea;
        add(textArea);
        setLayout(new FittingLayout());
    }

    @Override
    public void update(Point mouse) {
    	if (Minecraft.getMinecraft().currentScreen instanceof GuiChat) {
    		super.update(mouse);
        }
    }
    
    public void onMessageRecieve(String message) {
    	int width = (int) (textArea.getRenderableArea().getWidth() - 35);
        for(String s : Huzuni.display.guiFontRenderer.wrapWords(message, width)) {
            textArea.add(s);
        }

        int size = textArea.getComponents().size();

        if (size > 25) {
        	textArea.setComponents(textArea.getComponents().subList(size - 25, size));
        }
    }
}
