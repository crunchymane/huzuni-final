/**
 *
 */
package net.halalaboos.huzuni.ui.windows.friends;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.client.Settings;
import net.halalaboos.huzuni.ui.clickable.containers.HuzuniWindow;
import net.halalaboos.lib.ui.Component;
import net.halalaboos.lib.ui.components.SlotComponent;
import net.halalaboos.lib.ui.components.TextField;
import net.halalaboos.lib.ui.listeners.ActionListener;
import net.halalaboos.lib.ui.listeners.KeyListener;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiPlayerInfo;
import net.minecraft.util.StringUtils;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Halalaboos
 * @since Aug 19, 2013
 */
public class FriendWindow extends HuzuniWindow implements KeyListener {
    private final SlotComponent<GuiPlayerInfo> slotFriends;
    private final TextField textField;

    /**
     * @param title
     * @param dimensions
     */
    public FriendWindow() {
        super("Add Friends");
        textField = new TextField();
        textField.setMaxLength(16);
        textField.addKeyListener(this);
        add(textField);
        slotFriends = new SlotFriends(new Dimension(100, 63));
        add(slotFriends);
    }

    @Override
    public void update(Point mouse) {
        super.update(mouse);
        if (Minecraft.getMinecraft().getNetHandler() != null && textField.getText().length() <= 0) {
            slotFriends.setComponents((List<GuiPlayerInfo>) Minecraft.getMinecraft().getNetHandler().playerInfoList);
        }
    }

	/**
	 * @see net.halalaboos.lib.ui.listeners.KeyListener#onKeyTyped(net.halalaboos.lib.ui.Component, char, int)
	 */
	@Override
	public void onKeyTyped(Component component, char c, int keyCode) {
        if (textField.getText().length() <= 0)
            return;
        List<GuiPlayerInfo> tempList = new ArrayList<GuiPlayerInfo>();
        for (GuiPlayerInfo player : (List<GuiPlayerInfo>) Minecraft.getMinecraft().getNetHandler().playerInfoList) {
            String name = StringUtils.stripControlCodes(player.name);
            if (name.toLowerCase().contains(textField.getText().toLowerCase()))
                tempList.add(player);
        }
        slotFriends.setComponents(tempList);
	}
    
}
