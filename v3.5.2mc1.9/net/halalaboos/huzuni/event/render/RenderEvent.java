package net.halalaboos.huzuni.event.render;

import net.halalaboos.huzuni.api.event.Event;

/**
 * @author brudin
 * @version 1.0
 * @since 4/10/14
 */
public class RenderEvent extends Event {

	private float delta;

	public RenderEvent(float delta) {
		this.delta = delta;
	}

	public double interpolate(double prev, double cur) {
		return prev + ((cur - prev) * delta);
	}

	public float getDelta() {
		return delta;
	}
}
