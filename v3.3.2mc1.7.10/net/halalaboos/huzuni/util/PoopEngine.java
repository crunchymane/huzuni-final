package net.halalaboos.huzuni.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PoopEngine {

	private static final String EAT = "eat", 
			PUKE = "puke", 
			POOP = "poop", 
			POOPU = "POOP",
			SNIFF = "sniff", 
			FLUSH = "flush";
	
	private static String alphabet = "0123456789abcdefghijklmnopqrstuvwxyz.,-!?+*<>#@$��%&/()[] ";
	
	public static String runPoop(String poop) {
		int pointer = 0;
		StringBuilder output = new StringBuilder();
		String temp = "";
		if (poop.contains("\n")) {
			String[] lines = poop.split("\n");
			for (String line : lines) {
				String[] split = line.split(" ");
				for (String input : split) {
					if (!input.isEmpty()) {
						if (input.equals(EAT))
							pointer++;
						else if (input.equals(PUKE))
							pointer--;
						else if (input.equals(POOP))
							temp += alphabet.charAt(pointer);
						else if (input.equals(POOPU))
							temp += Character.toUpperCase(alphabet.charAt(pointer));
						else if (input.equals(SNIFF))
							output.append(temp);
						else if (input.equals(FLUSH)) {
							pointer = 0;
							temp = "";
						}
					}
				}
			}
		} else {
			String[] split = poop.split(" ");
			for (String input : split) {
				if (!input.isEmpty()) {
					if (input.equals(EAT))
						pointer++;
					else if (input.equals(PUKE))
						pointer--;
					else if (input.equals(POOP))
						temp += alphabet.charAt(pointer);
					else if (input.equals(POOPU))
						temp += Character.toUpperCase(alphabet.charAt(pointer));
					else if (input.equals(SNIFF))
						output.append(temp);
					else if (input.equals(FLUSH)) {
						pointer = 0;
						temp = "";
					}
				}
			}
		}
		return output.toString();
	}
	
}
