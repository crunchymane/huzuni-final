package net.halalaboos.huzuni.mods.pvp;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;
import com.darkmagician6.morbidlib.utils.timer.AccurateTimer;
import com.darkmagician6.morbidlib.utils.timer.Timer;

import net.halalaboos.huzuni.events.game.EventTick;
import net.halalaboos.huzuni.helpers.Helper;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.DefaultModule;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class AutoArmor extends DefaultModule implements Listener, Helper {

	/**
	 * Holds our next slot to click.
	 * */
    private int clickSlot = -1;

    private final Timer timer = new AccurateTimer();

    public AutoArmor() {
        super("Auto Armor", -1);
        setDescription("Puts on the best armor.");
        setRenderWhenEnabled(false);
    }

    @Override
    protected void onEnable() {
        registry.registerListener(this);
    }

    @Override
    protected void onDisable() {
        registry.unregisterListener(this);
    }

    @Override
    protected void onToggle() {
    }

    @EventTarget
    public void onTick(EventTick event) {
    	if (mc.currentScreen != null || !timer.hasReach(150))
    		return;
    	// If we have a slot we need to click, click it.
        if (clickSlot != -1) {
            inventoryHelper.clickSlot(clickSlot, 0, false);
            clickSlot = -1;
            timer.reset();
            return;
        }
        Integer[] armors = getArmor();
        for (int i : armors) {
            Slot slot = mc.thePlayer.inventoryContainer.getSlot(i);
            int replacement = getReplacementSlot(slot);
            if (replacement != -1) {
                inventoryHelper.clickSlot(slot.slotNumber, 0, false);
                clickSlot = replacement;
                timer.reset();
                break;
            }
        }
    }
    
    /**
     * @return the wearable armor slot that associates with the armor in the slot specified.
     * */
    private int getReplacementSlot(Slot slot) {
        ItemArmor armor = (ItemArmor) slot.getStack().getItem();
        Slot wearingSlot = inventoryHelper.getWearingArmor(armor.armorType);
        if (wearingSlot.getHasStack()) {
            ItemArmor wearingArmor = (ItemArmor) wearingSlot.getStack().getItem();
            if (armor.getArmorMaterial().compareTo(wearingArmor.getArmorMaterial()) > 0) {
                return wearingSlot.slotNumber;
            }
        } else
            return wearingSlot.slotNumber;

        return -1;
    }

    /**
     * Finds all pieces of armor within the inventory.
     */
    private Integer[] getArmor() {
        List<Integer> tempList = new ArrayList<Integer>();
        for (int o = 9; o < 44; o++) {
            if (mc.thePlayer.inventoryContainer.getSlot(o).getHasStack()) {
                ItemStack item = mc.thePlayer.inventoryContainer.getSlot(o).getStack();
                if (item != null) {
                    if (item.getItem() instanceof ItemArmor) {
                        tempList.add(o);
                    }
                }
            }
        }
        return tempList.toArray(new Integer[tempList.size()]);
    }

}
