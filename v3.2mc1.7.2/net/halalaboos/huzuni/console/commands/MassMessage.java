/**
 *
 */
package net.halalaboos.huzuni.console.commands;

import net.halalaboos.huzuni.console.Command;
import net.minecraft.client.gui.GuiPlayerInfo;
import net.minecraft.util.StringUtils;

/**
 * @author Halalaboos
 * @since Aug 4, 2013
 */
public class MassMessage implements Command {

    /**
     * @see net.halalaboos.huzuni.console.Command#getAliases()
     */
    @Override
    public String[] getAliases() {
        return new String[] {"massmessage", "mass", "mm"};
    }

    /**
     * @see net.halalaboos.huzuni.console.Command#getHelp()
     */
    @Override
    public String[] getHelp() {
        return new String[] {".massmessage <message>"};
    }

    /**
     * @see net.halalaboos.huzuni.console.Command#getDescription()
     */
    @Override
    public String getDescription() {
        return "Attempts sending that message to all players.";
    }

    /**
     * @see net.halalaboos.huzuni.console.Command#run(java.lang.String, java.lang.String[])
     */
    @Override
    public void run(String input, String[] args) {
        String message = input.substring((input.split(" ")[0] + " ").length());
        sendMessage(message);
    }

    private void sendMessage(final String message) {
        new Thread() {
            @Override
            public void run() {
                for (int i = 0; i < mc.getNetHandler().playerInfoList.size(); i++) {
                    GuiPlayerInfo playerInfo = (GuiPlayerInfo) mc.getNetHandler().playerInfoList.get(i);
                    String name = StringUtils.stripControlCodes(playerInfo.name);
                    if (name.equals(mc.thePlayer.getCommandSenderName()))
                        continue;
                    mc.thePlayer.sendChatMessage("/msg " + name + " " + message);
                    try {
                        Thread.sleep(1000L);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (NullPointerException ex) { //In case the user gets disconnected from the server while mass messaging.
                        ex.printStackTrace();
                    }
                }

            }
        }.start();
    }
}
