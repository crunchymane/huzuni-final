package net.halalaboos.huzuni.command.commands;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.console.Command;
import net.halalaboos.huzuni.command.CommandManager;
import net.minecraft.util.text.TextFormatting;

/**
 * @author brudin
 * @version 1.0
 * @since 5/17/14
 */
public class Commands implements Command {
	@Override
	public String[] getAliases() {
		return new String[]{"commands", "cmds"};
	}

	@Override
	public String[] getHelp() {
		return new String[] {"commands"};
	}

	@Override
	public String getDescription() {
		return "Lists all of the commands.";
	}

	@Override
	public void run(String input, String[] args) {
		Huzuni.addChatMessage(TextFormatting.GOLD + "--- " + TextFormatting.GRAY + "Type .help or .help [command] for help." + TextFormatting.GOLD + " ---");
		Huzuni.addChatMessage(getAllCommands());
	}

	private String getAllCommands() {
		String out = "";
		for(Command command : CommandManager.getCommands()) {
			out += command.getAliases()[0] + ", ";
		}
		return out.substring(0, out.length() - 2);
	}
}
