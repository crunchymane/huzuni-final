/**
 * 
 */
package net.halalaboos.huzuni.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import net.halalaboos.huzuni.Huzuni;


/**
 * @author Halalaboos
 *
 * @since Sep 30, 2013
 */
public class ThreadDownloadDonators extends Thread {
	
	public static final String donorURL = "http://halalaboos.net/donations.txt";
		
	public ThreadDownloadDonators() {
		
	}
	
	@Override
	public void run() {
		BufferedReader reader = null;
		try {
			URL url = new URL(donorURL);
			reader = new BufferedReader(new InputStreamReader(url.openStream()));
			for (String s; (s = reader.readLine()) != null; ) {
				Huzuni.addDonator(s.toLowerCase());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (reader != null)
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
	}
	
}
