/**
 *
 */
package net.halalaboos.huzuni.gui.ingame.themes;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.gui.ingame.IngameTheme;
import net.halalaboos.huzuni.mod.DefaultMod;
import net.halalaboos.huzuni.mod.ModManager;
import net.halalaboos.huzuni.util.GLUtils;
import net.minecraft.client.Minecraft;

/**
 * @author Halalaboos
 * @since Aug 3, 2013
 */
public class Nyan implements IngameTheme {

	private final Minecraft mc = Minecraft.getMinecraft();

    /**
     * @see net.halalaboos.huzuni.ui.ingame.IngameTheme#render(net.minecraft.src.Minecraft, int, int)
     */
    @Override
    public void render(Minecraft mc, int screenWidth, int screenHeight) {
        mc.fontRenderer.drawStringWithShadow(Huzuni.TITLE + "\247r v" + Huzuni.VERSION, 2, 2, 0xFFFFFF);

        int yPos = 12;
        int widest = getWidest();
        for (DefaultMod mod : ModManager.getDefaultMods()) {
        	if (mod.isEnabled() && mod.getVisible()) {
        		GLUtils.drawRect(2, yPos, 2 + widest, yPos + 11, 0x80000000);
        		GLUtils.drawRect(2, yPos, 4, yPos + 11, mod.getColor().getRGB());
        		mc.fontRenderer.drawStringWithShadow(mod.getRenderName(), 6, yPos + 2, mod.getColor().getRGB());
        		yPos += 12;
        	}
        }

    }

    private int getWidest() {
        int width = 50;
        for (DefaultMod mod : ModManager.getDefaultMods()) {
            if (mod.isEnabled() && mod.getVisible()) {
                    if (width < mc.fontRenderer.getStringWidth(mod.getRenderName()) + 6)
                        width = mc.fontRenderer.getStringWidth(mod.getRenderName()) + 6;
                
            }
        }
        return width;
    }

    /**
     * @see net.halalaboos.huzuni.ui.ingame.IngameTheme#getName()
     */
    @Override
    public String getName() {
        return "Nyan";
    }

    /**
     * @see net.halalaboos.huzuni.ui.ingame.IngameTheme#onKeyTyped(int)
     */
    @Override
    public void onKeyTyped(int keyCode) {
    }

}
