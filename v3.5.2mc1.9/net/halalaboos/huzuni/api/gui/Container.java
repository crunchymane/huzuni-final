package net.halalaboos.huzuni.api.gui;

import java.util.List;

import net.halalaboos.huzuni.api.gui.layout.Layout;
import net.halalaboos.huzuni.api.gui.render.Theme;

public interface Container {
	
	/**
	 * Lays out all of the components.
	 * */
	void layout();
	
	/**
	 * An update method.
	 * */
	void update();
	
	/**
	 * Invoked when you click your mouse.
	 * @return If you clicked inside of the container.
	 * */
	boolean mouseClicked(int x, int y, int buttonId);
	
	/**
	 * Invoked when you release your mouse off the container.
	 * */
	void mouseReleased(int x, int y, int buttonId);
	
	/**
	 * Invoked when you select the container and type a key.
	 * */
	void keyTyped(int keyCode, char c);
	
	/**
	 * @return If the point is inside of the container.
	 * */
	boolean isPointInside(int x, int y);
	
	/**
	 * Renders the components inside of the container.
	 * */
	void renderComponents(Theme theme);
	
	void add(Component component);
	
	void remove(Component component);
	
	/* GETTERS / SETTERS */
	
	float getX();
	
	void setX(float x);
	
	float getY();
	
	void setY(float y);
	
	float getWidth();
	
	void setWidth(float width);
	
	float getHeight();
	
	void setHeight(float height);
		
	boolean isMouseOver();
	
	void setMouseOver(boolean mouseOver);
	
	boolean isMouseDown();
	
	void setMouseDown(boolean mouseDown);
	
	Layout getLayout();
	
	void setLayout(Layout layout);
	
	List<Component> getComponents();
	
	void setComponents(List<Component> components);
	
	boolean isActive();
	
	void setActive(boolean active);
	
	String getTooltip();
}
