/**
 *
 */
package net.halalaboos.lib.ui.listeners;

import net.halalaboos.lib.ui.Component;

/**
 * @author Halalaboos
 * @since Aug 16, 2013
 */
public interface ActionListener<T extends Component> {

    public void onAction(T component);

}
