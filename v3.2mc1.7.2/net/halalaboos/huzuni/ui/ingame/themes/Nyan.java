/**
 *
 */
package net.halalaboos.huzuni.ui.ingame.themes;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.mods.DefaultModule;
import net.halalaboos.huzuni.ui.ingame.IngameTheme;
import net.halalaboos.huzuni.utils.RenderUtils;
import net.minecraft.client.Minecraft;

/**
 * @author Halalaboos
 * @since Aug 3, 2013
 */
public class Nyan implements IngameTheme {

    /**
     * @see net.halalaboos.huzuni.ui.ingame.IngameTheme#render(net.minecraft.src.Minecraft, int, int)
     */
    @Override
    public void render(Minecraft mc, int screenWidth, int screenHeight) {
        mc.fontRenderer.drawStringWithShadow(Huzuni.TITLE + "\247r v" + Huzuni.VERSION, 2, 2, 0xFFFFFF);

        int yPos = 12;
        int widest = getWidest();
        for (DefaultModule module : Huzuni.modManager.getDefaultModules()) {
            if (module.isEnabled()) {
                if (module.shouldRenderWhenEnabled()) {
                    RenderUtils.drawRect(2, yPos, 2 + widest, yPos + 11, 0x80000000);
                    RenderUtils.drawRect(2, yPos, 4, yPos + 11, module.getColor().getRGB());
                    mc.fontRenderer.drawStringWithShadow(module.getRenderName(), 6, yPos + 2, module.getColor().getRGB());
                    yPos += 12;
                }
            }
        }

    }

    private int getWidest() {
        int width = 50;
        for (DefaultModule module : Huzuni.modManager.getDefaultModules()) {
            if (module.isEnabled()) {
                if (module.shouldRenderWhenEnabled()) {
                    if (width < mc.fontRenderer.getStringWidth(module.getRenderName()) + 6)
                        width = mc.fontRenderer.getStringWidth(module.getRenderName()) + 6;
                }
            }
        }
        return width;
    }

    /**
     * @see net.halalaboos.huzuni.ui.ingame.IngameTheme#getName()
     */
    @Override
    public String getName() {
        return "Nyan";
    }

    /**
     * @see net.halalaboos.huzuni.ui.ingame.IngameTheme#onKeyPressed(int)
     */
    @Override
    public void onKeyPressed(int keyCode) {
    }

}
