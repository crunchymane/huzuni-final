/**
 * 
 */
package net.halalaboos.huzuni.command.commands;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.console.Command;
import net.minecraft.client.Minecraft;
import net.minecraft.util.EnumHand;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.event.HoverEvent;
import pw.brudin.huzuni.util.ChatComponentHelper;

/**
 * @author Halalaboos
 *
 */
public class ItemEnhancer implements Command {
	
	@Override
	public String[] getAliases() {
		return new String[] { "enhance" };
	}

	@Override
	public String[] getHelp() {
		return new String[] { "enhance <length>" };
	}
	
	@Override
	public String getDescription() {
		return "Makes the name of your currently held item extremely large";
	}
	
	@Override
	public void run(String input, String[] args) {
		if (!Huzuni.isVip()) {
			Huzuni.addChatMessage(getErrorMessage());
			return;
		}
		if (Minecraft.getMinecraft().playerController.isInCreativeMode()) {
			int length = Integer.parseInt(args[0]);
			String name = "";
	        for(int i = 0; i < length; i++)
	        	name = name + "\247k\247lg";
	        
	        Minecraft.getMinecraft().thePlayer.getHeldItem(EnumHand.MAIN_HAND).setStackDisplayName(name);
	       	Huzuni.addChatMessage("Now, drop the item and pick it back up again.");
		} else
			Huzuni.addChatMessage("You must be in creative mode!");
	}


	private TextComponentString getErrorMessage() {
		TextComponentString output = new TextComponentString(TextFormatting.DARK_GREEN + "[H] " + TextFormatting.RESET + "You must be VIP to use this!");
		TextComponentString alreadyPurchased = new TextComponentString(TextFormatting.GREEN + " \247o(Already have VIP?)");
		output.getChatStyle().setChatHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new TextComponentString("\247oClick here to get VIP!")));
		TextComponentString hover = ChatComponentHelper.multiline("Our site may be down", "if your VIP isn't working.");
		alreadyPurchased.getChatStyle().setChatHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, hover));
		output.appendSibling(alreadyPurchased);
		return output;
	}
}
