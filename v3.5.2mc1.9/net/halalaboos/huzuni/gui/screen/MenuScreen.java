package net.halalaboos.huzuni.gui.screen;

import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glEnable;
import net.halalaboos.huzuni.gui.clickable.ContainerManager;
import net.halalaboos.huzuni.gui.clickable.menu.Menu;
import net.halalaboos.huzuni.gui.clickable.theme.HuzuniTheme;
import net.halalaboos.huzuni.util.GLUtils;
import net.minecraft.client.gui.GuiScreen;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import java.io.IOException;

public abstract class MenuScreen extends GuiScreen {

	private final ContainerManager<Menu> menuManager = new ContainerManager<Menu>(new HuzuniTheme());
	
	private final Menu menu;
	
	private final String menuName;
	
	public MenuScreen(String menuName) {
		super();
		this.menu = new Menu("");
		this.menuName = menuName;
		menuManager.add(menu);
	}
	
	@Override
    public void initGui() {
    	Keyboard.enableRepeatEvents(true);
    	int slotWidth = 300, slotHeight = (GLUtils.getScreenHeight() - 114);
        menu.setX((GLUtils.getScreenWidth() / 2 - slotWidth / 2));
        menu.setY(30);
        menu.setWidth(slotWidth);
        menu.setHeight(slotHeight);
        setup(menu);
        menu.layout();
	}
	
	protected abstract void setup(Menu menu);
	
	@Override
	public void onGuiClosed() {
    	Keyboard.enableRepeatEvents(false);
	}
	

    @Override
    public void drawScreen(int mouseX, int mouseY, float delta) {
        super.drawDefaultBackground();
        super.drawScreen(mouseX, mouseY, delta);
        GLUtils.setupOverlayDefaultScale();
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        menuManager.render();
        glDisable(GL_BLEND);
        mc.entityRenderer.setupOverlayRendering();
        drawCenteredString(this.fontRendererObj, menuName, width / 2, 16, 0xFFFFFF);
    }
    
    @Override
    protected void keyTyped(char c, int keyCode) throws IOException {
        super.keyTyped(c, keyCode);
        menuManager.keyTyped(keyCode, c);
    }

    @Override
    protected void mouseClicked(int x, int y, int buttonId) throws IOException {
        super.mouseClicked(x, y, buttonId);
        menuManager.mouseClicked(GLUtils.getMouseX(), GLUtils.getMouseY(), buttonId);
    }
}
