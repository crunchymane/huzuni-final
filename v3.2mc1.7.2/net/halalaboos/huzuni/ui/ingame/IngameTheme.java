/**
 *
 */
package net.halalaboos.huzuni.ui.ingame;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.helpers.Helper;
import net.halalaboos.huzuni.helpers.MinecraftHelper;
import net.halalaboos.huzuni.rendering.font.MinecraftFontRenderer;
import net.minecraft.client.Minecraft;

/**
 * @author Halalaboos
 * @since Aug 3, 2013
 */
public interface IngameTheme extends Helper, MinecraftHelper {
	MinecraftFontRenderer fontRenderer = Huzuni.display.fontRenderer;
	
    /**
     * Renders inside the in-game GUI.
     */
    public void render(Minecraft mc, int screenWidth, int screenHeight);

    /**
     * @return the display name for this theme.
     */
    public String getName();

    /**
     * Invoked when a key is pressed in game.
     *
     * @param keyCode
     */
    public void onKeyPressed(int keyCode);
}
