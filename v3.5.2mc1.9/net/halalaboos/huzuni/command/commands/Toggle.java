package net.halalaboos.huzuni.command.commands;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.console.Command;
import net.halalaboos.huzuni.api.mod.Mod;
import net.halalaboos.huzuni.command.CommandManager;
import net.halalaboos.huzuni.mod.ModManager;
import net.halalaboos.huzuni.util.StringUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.event.ClickEvent;
import net.minecraft.util.text.event.HoverEvent;

/**
 * @author brudin
 * @version 1.0
 * @since 3/26/14
 */
public class Toggle implements Command {
	@Override
	public String[] getAliases() {
		return new String[] { "toggle", "t"};
	}

	@Override
	public String[] getHelp() {
		return new String[] { "toggle <mod name>"};
	}

	@Override
	public String getDescription() {
		return "Toggles the specified Mod.";
	}

	@Override
	public void run(String input, String[] args) {
		String modName = StringUtils.getAfter(input, 1);
		Mod mod = get(modName);
		mod.toggle();
		Minecraft.getMinecraft().ingameGUI.getChatGUI().printChatMessage(getToggleMessage(mod, CommandManager.COMMAND_PREFIX, mod.getName()));
	}

	private TextComponentString getToggleMessage(Mod mod, String prefix, String name) {
		TextComponentString output = new TextComponentString(TextFormatting.DARK_GREEN + "[H] " + TextFormatting.RESET + mod.getName() + " toggled " + (mod.isEnabled() ? TextFormatting.DARK_GREEN + "on" : TextFormatting.RED + "off") + TextFormatting.RESET + ".");
		output.getChatStyle().setChatHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new TextComponentString("\247oClick to toggle again.")));
		output.getChatStyle().setChatClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, prefix + "toggle " + name));
		return output;
	}

	private Mod get(String name) {
		name = name.replaceAll(" ", "");
		for (Mod mod : ModManager.getMods()) {
			String modName = mod.getName().replaceAll(" ", "");
			if (modName.equalsIgnoreCase(name)) {
				return mod;
			}
			if (mod.getAliases() != null) {
				for (String s : mod.getAliases()) {
					if (s.toLowerCase().equals(name)) {
						return mod;
					}
				}
			}
		}
		return null;
	}
}
