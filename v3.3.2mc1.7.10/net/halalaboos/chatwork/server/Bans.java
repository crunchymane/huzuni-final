package net.halalaboos.chatwork.server;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.halalaboos.chatwork.utils.FileUtils;

public final class Bans {

	public static final File BAN_FILE = new File("./bans.txt");
	
	/**
	 * Key: Username, Value: Address
	 * */
	public static final Map<String, String> bans = new HashMap<String, String>();
	
	private Bans() {
		
	}
	
	public static boolean isBanned(String address) {
		Collection<String> addresses = bans.values();
		for (String address1 : addresses) {
			if (address.equals(address1))
				return true;
		}
		return false;
	}
	
	public static void load() {
		bans.clear();
		List<String> lines = FileUtils.readFile(BAN_FILE);
		for (String line : lines) {
			if (line.contains(":")) {
				String ip = line.substring(0, line.indexOf(":"));
				String username = line.substring(line.indexOf(":") + 1);
				bans.put(username, ip);
			}
		}
	}
	
	public static void save() {
		List<String> lines = new ArrayList<String>();
		Set<String> usernames = bans.keySet();
		for (String username : usernames)
			lines.add(bans.get(username) + ":" + username);
		FileUtils.writeFile(BAN_FILE, lines);
	}

	public static void ban(ChatworkClient client) {
		bans.put(client.getInfo().getUsername(), client.getAddress());
		save();
	}
	
}
