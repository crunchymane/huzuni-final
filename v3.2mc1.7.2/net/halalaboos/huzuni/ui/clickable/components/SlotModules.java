/**
 * 
 */
package net.halalaboos.huzuni.ui.clickable.components;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;

import net.halalaboos.huzuni.mods.Module;
import net.halalaboos.lib.ui.components.SlotComponent;
import net.halalaboos.lib.ui.utils.GLUtils;

/**
 * @author Halalaboos
 *
 * @since Oct 5, 2013
 */
public class SlotModules extends SlotComponent<Module> {
	private final Color enabledColor = new Color(0F, 0.5F, 1F, 0.75F),
	disabledColor = new Color(0.3F, 0.3F, 0.3F, 0.75F);
	
	/**
	 * @param dimensions
	 */
	public SlotModules(Dimension dimensions) {
		super(dimensions);
		
	}

	/**
	 * @see net.halalaboos.lib.ui.components.SlotComponent#render(int, java.lang.Object, java.awt.Rectangle, boolean, boolean)
	 */
	@Override
	public void render(int index, Module module, Rectangle boundaries,
			boolean mouseOver, boolean mouseDown) {
		String name = module.getName();
		GLUtils.setColor(GLUtils.getColorWithAffects(module.isEnabled() ? enabledColor : disabledColor, mouseOver, mouseDown));
		GLUtils.drawFilledRect(boundaries.x, boundaries.y, boundaries.x + boundaries.width, boundaries.y + boundaries.height);
		GLUtils.drawString(name, boundaries.x + boundaries.width / 2 - GLUtils.getStringWidth(name) / 2, boundaries.y + boundaries.height / 2 - GLUtils.getStringHeight(name) / 2 + 1, module.isEnabled() ? 0xFFFFAA : 0xFFFFFF);
		
	}
	
	private void renderToolTip(String tooltip, int x, int y) {
		int aboveMouse = 8;
		int width = GLUtils.getStringWidth(tooltip);
		GLUtils.setColor(enabledColor.brighter());
		GLUtils.drawRect(x - 1, y - aboveMouse - 1, x + width + 5, y + GLUtils.getStringHeight(tooltip) - aboveMouse + 3, 1);
		GLUtils.setColor(enabledColor);
		GLUtils.drawFilledRect(x, y - aboveMouse, x + width + 4, y + GLUtils.getStringHeight(tooltip) - aboveMouse + 2);
		GLUtils.drawStringWithShadow(tooltip, x + 2, y - aboveMouse + 2, 0xFFFFFF);
	}

	/**
	 * @see net.halalaboos.lib.ui.components.SlotComponent#onClicked(int, java.lang.Object, java.awt.Rectangle, java.awt.Point, int)
	 */
	@Override
	public void onClicked(int index, Module module, Rectangle boundaries,
			Point mouse, int buttonID) {
	}

	/**
	 * @see net.halalaboos.lib.ui.components.SlotComponent#onReleased(int, java.lang.Object, java.awt.Point)
	 */
	@Override
	public void onReleased(int index, Module module, Point mouse) {
		if (module != null) {
			module.toggle();
		}
	}

	/**
	 * @see net.halalaboos.lib.ui.components.SlotComponent#getElementHeight()
	 */
	@Override
	public int getElementHeight() {
		return 13;
	}

}
