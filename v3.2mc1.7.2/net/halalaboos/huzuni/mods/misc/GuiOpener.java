package net.halalaboos.huzuni.mods.misc;

import net.halalaboos.huzuni.mods.DefaultModule;
import net.halalaboos.huzuni.mods.Keybindable;
import net.halalaboos.huzuni.mods.Module;
import net.halalaboos.huzuni.ui.screen.HuzuniGui;

import org.lwjgl.input.Keyboard;

public class GuiOpener extends Module implements Keybindable {

	int keyCode = Keyboard.KEY_RSHIFT;
	
    public GuiOpener() {
        super("GUI", "Huzuni Dev Team");
    }

    @Override
    protected void onEnable() {
    	 if (mc.theWorld != null)
             mc.displayGuiScreen(new HuzuniGui());
        setEnabled(false);
    }

    @Override
    protected void onDisable() {

    }

	/**
	 * @see net.halalaboos.huzuni.mods.Keybindable#getKeyCode()
	 */
	@Override
	public int getKeyCode() {
		return keyCode;
	}

	/**
	 * @see net.halalaboos.huzuni.mods.Keybindable#setKeyCode(int)
	 */
	@Override
	public void setKeyCode(int keyCode) {
		this.keyCode = keyCode;
	}

	/**
	 * @see net.halalaboos.huzuni.mods.Keybindable#onKeyPressed()
	 */
	@Override
	public void onKeyPressed() {
		toggle();
	}

	/**
	 * @see net.halalaboos.huzuni.mods.Keybindable#getKeyName()
	 */
	@Override
	public String getKeyName() {
        return keyCode == -1 ? "-1" : Keyboard.getKeyName(keyCode);
	}

}
