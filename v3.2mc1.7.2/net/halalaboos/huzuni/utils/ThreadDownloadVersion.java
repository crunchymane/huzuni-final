/**
 * 
 */
package net.halalaboos.huzuni.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

import net.halalaboos.huzuni.helpers.Helper;

/**
 * @author Halalaboos
 *
 * @since Sep 30, 2013
 */
public class ThreadDownloadVersion extends Thread {

	public ThreadDownloadVersion() {
		
	}
	
	@Override
	public void run() {
		try {
        final URL updateURL = new URL("http://halalaboos.net/client/version");
        final BufferedReader reader = new BufferedReader(new InputStreamReader(updateURL.openStream()));
        String line = reader.readLine();
        /* Everything here is obvious.. */
        if (!line.equals(net.halalaboos.huzuni.Huzuni.VERSION)) {
            reader.close();
            Helper.huzuniHelper.setUpdateCode(1);
        } else
        	Helper.huzuniHelper.setUpdateCode(0);
        reader.close();
		} catch (Exception e) {
			/* We cannot connect to halalaboos.net :'( */
            Helper.huzuniHelper.setUpdateCode(-1);
		}
	}
	
}
