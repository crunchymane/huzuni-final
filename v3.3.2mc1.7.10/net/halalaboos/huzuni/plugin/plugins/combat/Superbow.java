package net.halalaboos.huzuni.plugin.plugins.combat;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.plugin.Category;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent;
import net.halalaboos.huzuni.event.tick.UpdateListener;
import net.halalaboos.huzuni.plugin.DefaultPlugin;
import net.halalaboos.huzuni.util.Timer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBow;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import net.minecraft.network.play.client.C09PacketHeldItemChange;

public class Superbow extends DefaultPlugin implements UpdateListener {

	private final Timer timer = new Timer();
	
	public Superbow() {
		super("Superbow");
		setAuthor("Halalaboos");
		setDescription("Shoots your bow super fast.");
		setCategory(Category.COMBAT);
	}

	@Override
	public void onPreUpdate(MotionUpdateEvent event) {
		if (mc.thePlayer.getCurrentEquippedItem() != null) {
			Item item = mc.thePlayer.getCurrentEquippedItem().getItem();
			if (item instanceof ItemBow) {
				if (mc.thePlayer.isUsingItem() && mc.thePlayer.onGround) {
					doShoot();
					mc.thePlayer.stopUsingItem();
					timer.reset();
				}
			}
		}
	}
	
	private void doShoot() {
		mc.thePlayer.sendQueue.addToSendQueue(new C08PacketPlayerBlockPlacement(-1, -1, -1, 255, mc.thePlayer.getCurrentEquippedItem(), 0.0F, 0.0F, 0.0F));
		mc.thePlayer.sendQueue.addToSendQueue(new C09PacketHeldItemChange(mc.thePlayer.inventory.currentItem));
		for (int i = 0; i < 20; i++) {
			mc.thePlayer.sendQueue.addToSendQueue(new C03PacketPlayer(true));
		}
		mc.thePlayer.sendQueue.addToSendQueue(new C07PacketPlayerDigging(5, 0, 0, 0, -1));
	}

	@Override
	public void onPostUpdate(MotionUpdateEvent event) {
	}

	@Override
	protected void onEnable() {
		Huzuni.registerUpdateListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterUpdateListener(this);
	}
}
