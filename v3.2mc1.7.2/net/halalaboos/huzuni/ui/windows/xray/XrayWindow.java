/**
 *
 */
package net.halalaboos.huzuni.ui.windows.xray;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.client.Settings;
import net.halalaboos.huzuni.helpers.Helper;
import net.halalaboos.huzuni.ui.clickable.containers.HuzuniWindow;
import net.halalaboos.lib.ui.Component;
import net.halalaboos.lib.ui.DefaultContainer;
import net.halalaboos.lib.ui.components.Slider;
import net.halalaboos.lib.ui.components.SlotComponent;
import net.halalaboos.lib.ui.components.TextField;
import net.halalaboos.lib.ui.listeners.ActionListener;
import net.halalaboos.lib.ui.listeners.KeyListener;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import java.awt.*;
import java.util.List;
import java.util.ArrayList;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

/**
 * @author Halalaboos
 * @since Aug 19, 2013
 */
public class XrayWindow extends HuzuniWindow implements ActionListener, KeyListener {
    private final int[] badBlocks = new int[] {
    		31, 32, 34, 36, 50, 54, 55, 59, 63, 64, 68, 71, 93, 94, 95, 104, 105, 118, 119, 127, 131, 132, 140, 141, 142, 146, 149, 150
    };
    private final SlotComponent <ItemStack[]> slotBlocks;
    private final TextField textField;
    
    /**
     * @param title
     * @param dimensions
     */
    public XrayWindow() {
        super("Xray Manager");
        textField = new TextField();
        textField.addKeyListener(this);
        textField.setTooltip("Search for blocks.");
        add(textField);
        slotBlocks = new SlotBlocks(new Dimension(136, 68));
        slotBlocks.setComponents(setupBlocks());
        add(slotBlocks);
        Slider slider = new Slider("Xray Opacity", 0F, (Huzuni.xrayManager.getOpacity() / 255F) * 100F, 100F, 1F);
        slider.setValueWatermark("%");
        slider.addActionListener(this);
        add(slider);
    }

    /**
     * Finds blocks and sets up a list with an array of itemstacks in it.
     * */
    private List<ItemStack[]> searchBlock(String blockName) {
        List<ItemStack[]> goodBlocks = new ArrayList<ItemStack[]>();
        List<ItemStack> tempBlocks = new ArrayList<ItemStack>();;
    	 for (Object o : Block.field_149771_c) {
    		 Block block = (Block) o;
    		 if (block != null && Item.func_150898_a(block) != null) {
    			 boolean bad = false;
         		if (!bad) {
         			ItemStack itemStack = new ItemStack(block);
         			if (itemStack.getDisplayName().toLowerCase().contains(blockName.toLowerCase())) {
         				tempBlocks.add(itemStack);
    	        		if (tempBlocks.size() >= 6) {
    	        			goodBlocks.add(tempBlocks.toArray(new ItemStack[tempBlocks.size()]));
    	        			tempBlocks.clear();
    	        		}	
         			}
         		}
    		 }
    	 }
    	 if (!tempBlocks.isEmpty())
    		 goodBlocks.add(tempBlocks.toArray(new ItemStack[tempBlocks.size()]));
    	 tempBlocks.clear();
    	 return goodBlocks;
    }
    
    /**
     * Sets up all blocks for the x-ray window.
	 * @return
	 */
	private List<ItemStack[]> setupBlocks() {
        List<ItemStack[]> goodBlocks = new ArrayList<ItemStack[]>();
        List<ItemStack> tempBlocks = new ArrayList<ItemStack>();;
        
        for (Object o : Block.field_149771_c) {
        	Block block = (Block) o;
        	if (block != null && Item.func_150898_a(block) != null) {
        		boolean bad = false;
        		if (!bad) {
	        		tempBlocks.add(new ItemStack(block));
	        		if (tempBlocks.size() >= 6) {
	        			goodBlocks.add(tempBlocks.toArray(new ItemStack[tempBlocks.size()]));
	        			tempBlocks.clear();
	        		}
        		}
        	}
        }
        if (!tempBlocks.isEmpty())
        	goodBlocks.add(tempBlocks.toArray(new ItemStack[tempBlocks.size()]));
        tempBlocks.clear();
        return goodBlocks;
	}

	/**
     * @see net.halalaboos.lib.ui.listeners.ActionListener#onAction(net.halalaboos.lib.ui.Component)
     */
    @Override
    public void onAction(Component component) {
    	if (component instanceof Slider) {
    		Slider slider = (Slider) component;
    		Huzuni.xrayManager.setOpacity(slider.getValue());
    	}
        Huzuni.xrayManager.save();
    }

	/**
	 * @see net.halalaboos.lib.ui.listeners.KeyListener#onKeyTyped(net.halalaboos.lib.ui.Component, char, int)
	 */
	@Override
	public void onKeyTyped(Component component, char c, int keyCode) {
		if (textField.getText().length() > 0) {
			slotBlocks.setComponents(searchBlock(textField.getText()));
		} else {
    		slotBlocks.setComponents(setupBlocks());
		}
	}

}