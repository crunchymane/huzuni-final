package net.halalaboos.huzuni.mod.mods.player;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.mod.Category;
import net.halalaboos.huzuni.api.mod.ValueOption;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent;
import net.halalaboos.huzuni.event.tick.UpdateListener;
import net.halalaboos.huzuni.mod.DefaultMod;

public class Fastplace extends DefaultMod implements UpdateListener {

	private ValueOption speed = new ValueOption("Fastplace speed", 0F, 2F, 4F, 1F, "Speed you will place blocks at");
	
    public Fastplace() {
        super("Fast Place", -1);
        setCategory(Category.PLAYER);
        setAliases("fp");
        setDescription("Place blocks faster.");
        this.setOptions(speed);
    }

    @Override
    protected void onEnable() {
        Huzuni.registerUpdateListener(this);
    }

    @Override
    protected void onDisable() {
    	Huzuni.unregisterUpdateListener(this);
    }


	@Override
	public void onPreUpdate(MotionUpdateEvent event) {
    	float speed = this.speed.getValue();
        if (mc.rightClickDelayTimer > (4 - (byte) speed))
            mc.rightClickDelayTimer = (4 - (byte) speed);
    }

	@Override
	public void onPostUpdate(MotionUpdateEvent event) {
	}

}
