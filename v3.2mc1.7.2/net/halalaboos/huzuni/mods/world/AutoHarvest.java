/**
 *
 */
package net.halalaboos.huzuni.mods.world;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;
import com.darkmagician6.morbidlib.utils.timer.AccurateTimer;
import com.darkmagician6.morbidlib.utils.timer.Timer;

import net.halalaboos.huzuni.console.Command;
import net.halalaboos.huzuni.events.game.EventPostMotionUpdate;
import net.halalaboos.huzuni.events.game.EventPreMotionUpdate;
import net.halalaboos.huzuni.helpers.Helper;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.DefaultModule;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.resources.I18n;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.StatCollector;
import net.minecraft.util.Vec3;

import org.lwjgl.input.Keyboard;
import org.lwjgl.util.vector.Vector3f;

/**
 * @author Halalaboos
 * @since Aug 19, 2013
 */
public class AutoHarvest extends DefaultModule implements Listener, Helper {

	private final Timer timer = new AccurateTimer();
	
    private Vector3f blockCoordinates;

    private int side;

    private int mode = 0;
    
    private int replantID = 392;
    
    public AutoHarvest() {
        super("Auto Harvest");
        setCategory(Category.WORLD);
        setDescription("Harvests / Plants crops for you.");
        registerCommand(new Command() {

			@Override
			public String[] getAliases() {
				return new String[] { "autoharvest", "ah" };
			}

			@Override
			public String[] getHelp() {
				return new String[] { "autoharvest mode <harvest / replant <replant seed>>" };
			}

			@Override
			public String getDescription() {
				return "Changes the mode for the autoharvest mod.";
			}

			@Override
			public void run(String input, String[] args) {
				if (args[0].startsWith("m")) {
					if (args[1].startsWith("h")) {
						mode = 0;
						huzuni.addChatMessage("Now harvesting.");
					} else if (args[1].startsWith("r")) {
						String blockName = stringHelper.getAfter(input, 3);
						mode = 1;
						replantID = inventoryHelper.getItemID(inventoryHelper.getItemFromInput(blockName));
						huzuni.addChatMessage("Now replanting '" + blockName + "'");
					}
				} else
					huzuni.addChatMessage(getHelp()[0]);
			}
        	
        });
    }

    /**
     * @see net.halalaboos.huzuni.mods.DefaultModule#onEnable()
     */
    @Override
    protected void onEnable() {
        registry.registerListener(this);
        huzuni.addChatMessage("Type '.autoharvest' to swap between harvest and replant.");
    }

    /**
     * @see net.halalaboos.huzuni.mods.DefaultModule#onDisable()
     */
    @Override
    protected void onDisable() {
        registry.unregisterListener(this);
    }

    /**
     * @see net.halalaboos.huzuni.mods.DefaultModule#onToggle()
     */
    @Override
    protected void onToggle() {
    }

    @EventTarget
    public void onPreMotionUpdate(EventPreMotionUpdate event) {
        blockCoordinates = find();
        if (blockCoordinates != null) {
        	if (timer.hasReach(100F)) {
	        	if (mode == 0) {
	            	blockHelper.faceBlock(blockCoordinates.x + 0.5F, blockCoordinates.y + 0.5F, blockCoordinates.z + 0.5F);        		
	        	} else if (mode == 1) {
	        		int newHotbarItem = inventoryHelper.findHotbarItem(replantID);
	        		if (newHotbarItem != -1) {
	    	        	blockHelper.faceBlock(blockCoordinates.x + 0.5F, blockCoordinates.y + 0.5F, blockCoordinates.z + 0.5F);
	    			}
	        	}
        	}
        }
    }

    @EventTarget
    public void onPostMotionUpdate(EventPostMotionUpdate event) {
    	if (blockCoordinates != null) {
    		if (timer.hasReach(100F)) {
	    		if (mode == 0) {
	        		mc.thePlayer.swingItem();
	    			sendBreakPacket(blockCoordinates, 0, side);
	    			mc.playerController.onPlayerDestroyBlock((int) blockCoordinates.x, (int) blockCoordinates.y, (int) blockCoordinates.z, side);
	    		} else if (mode == 1) {
	    			int newHotbarItem = inventoryHelper.findHotbarItem(replantID);
	    			if (newHotbarItem != -1) {
	    				mc.thePlayer.inventory.currentItem = newHotbarItem;
	    				mc.playerController.updateController();
	    				sendRightClick(blockCoordinates, side); 
	    				mc.thePlayer.getCurrentEquippedItem().tryPlaceItemIntoWorld(mc.thePlayer, mc.theWorld, (int) blockCoordinates.x, (int) blockCoordinates.y, (int) blockCoordinates.z, 1, 0, 0, 0);
	    	    		mc.thePlayer.swingItem();
	    			}
	    		}
				blockCoordinates = null;
	    		timer.reset();
    		}
    	}
    }

    /**
	 * @param vector
	 */
	private void sendRightClick(Vector3f vector, int side) {
        mc.getNetHandler().addToSendQueue(new C08PacketPlayerBlockPlacement((int) vector.x, (int) vector.y, (int) vector.z, side, mc.thePlayer.inventory.getCurrentItem(), 0, 0, 0));

	}

	@Override
    public String getRenderName() {
        return super.getRenderName() + (isEnabled() ? " (" + getMode() + ")" : "");
    }

    /**
     * @return Vector with the block we need to harvest.
     */
    private Vector3f find() {
    	Vector3f closestVector = null;
        int radius = (int) 4;
        // 3D Loop.
        for (int i = radius; i >= -radius; i--) {
            for (int j = -radius; j <= radius; j++) {
                for (int k = radius; k >= -radius; k--) {
                    int possibleX = (int) (mc.thePlayer.posX + i),
                            possibleY = (int) (mc.thePlayer.posY + j),
                            possibleZ = (int) (mc.thePlayer.posZ + k);
                    Block block = mc.theWorld.getBlock(possibleX, possibleY, possibleZ);
                    Vector3f blockVector = new Vector3f(possibleX, possibleY, possibleZ);
                    // If the block at the position is not air
                    if (!isAir(block) && canReach(blockVector, radius)) {
                    	int blockID = blockHelper.getBlockID(block);
                    	MovingObjectPosition result = rayTrace(blockVector);
                        if (mode == 0) {
	                        switch (blockID) {
	                            case 83: // Sugarcane
	                            	if (getBlockID(possibleX, possibleY - 1, possibleZ) == 83) {
	                            		side = result != null ? result.sideHit : 0;
	                            		if (closestVector == null) {
	                            			closestVector = blockVector;
	                            		} else {
	                            			if (mc.thePlayer.getDistance(blockVector.x, blockVector.y, blockVector.z) < mc.thePlayer.getDistance(closestVector.x, closestVector.y, closestVector.z)) {
	                            				closestVector = blockVector;                        			
	                            			}
	                            		}
	                            	}
	                                continue;
	                            case 127: // Cocoa
	                            case 115: // Netherwart
	                            case 141: // Carrots
	                            case 142: // Potatoes
	                            case 59: // Wheat
	                                if (getGrowthLevel(blockVector) >= getMaxGrowthLevel(blockID)) {
	                                    side = result != null ? result.sideHit : 0;
	                                    if (closestVector == null) {
	                                    	closestVector = blockVector;
	                                    } else {
	                                    	if (mc.thePlayer.getDistance(blockVector.x, blockVector.y, blockVector.z) < mc.thePlayer.getDistance(closestVector.x, closestVector.y, closestVector.z)) {
	                                    		closestVector = blockVector;                        			
	                                    	}
	                                    }
	                                }
	                            default:
	                        }
                        } else if (mode == 1) {
                        	if (replantID == 351) {
	                        	if (blockID == 17 && mc.theWorld.getBlockMetadata(possibleX, possibleY, possibleZ) == 3) {
	                        		if (getBlockID(possibleX, possibleY, possibleZ - 1) == 0) {
	                        			side = 2;
	                        		} else if (getBlockID(possibleX, possibleY, possibleZ + 1) == 0) {
	                        			side = 3;
	                        		} else if (getBlockID(possibleX - 1, possibleY, possibleZ) == 0) {
	                        			side = 4;
	                        		} else if (getBlockID(possibleX + 1, possibleY, possibleZ) == 0) {
	                        			side = 5;
	                        		} else {
	                        			continue;
	                        		}
	                        		if (closestVector == null) {
                            			closestVector = blockVector;
                            		} else {
    	                        		if (mc.thePlayer.getDistance(blockVector.x, blockVector.y, blockVector.z) < mc.thePlayer.getDistance(closestVector.x, closestVector.y, closestVector.z)) {
    	                            		closestVector = blockVector;                        			
    	                        		}
                            		}
	                        		// - z = 2
	                        		// + z = 3
	                        		// - x = 4
	                        		// + x = 5
	                        	}
                        	} else {
	                        	if (blockID == 60) {
	                        		if (getBlockID((int) blockVector.x, (int) blockVector.y + 1, (int) blockVector.z) == 0) {
	                            		if (closestVector == null) {
	                            			closestVector = blockVector;
	                            		} else {
	    	                        		if (mc.thePlayer.getDistance(blockVector.x, blockVector.y, blockVector.z) < mc.thePlayer.getDistance(closestVector.x, closestVector.y, closestVector.z)) {
	    	                            		closestVector = blockVector;                        			
	    	                        		}
	                            		}
	                            		side = 1;
	                        		}
	                        	}
                        	}
                        }
                    }
                }
            }
        }
        return closestVector;
    }

    private void sendBreakPacket(Vector3f position, int mode, int side) {
        mc.getNetHandler().addToSendQueue(new C07PacketPlayerDigging(mode, (int) position.getX(), (int) position.getY(), (int) position.getZ(), side));
    }

    private void placeBlock(int slot, Vector3f position, int side) {
        mc.getNetHandler().addToSendQueue(new C08PacketPlayerBlockPlacement((int) position.x, (int) position.y, (int) position.z, side, mc.thePlayer.inventory.getCurrentItem(), 0.5F, 0.5F, 0.5F));
    }

    private boolean canReach(Vector3f vector, float distance) {
        return blockHelper.canReach(vector.x, vector.y, vector.z, distance);
    }

    private MovingObjectPosition rayTrace(Vector3f vector) {
        Vec3 player = mc.thePlayer.worldObj.getWorldVec3Pool().getVecFromPool(mc.thePlayer.posX, mc.thePlayer.posY + mc.thePlayer.getEyeHeight(), mc.thePlayer.posZ),
                block = mc.thePlayer.worldObj.getWorldVec3Pool().getVecFromPool(vector.x + 0.5F, vector.y + 0.5F, vector.z + 0.5F);

        return mc.theWorld.clip(player, block);
    }

    private int getBlockID(Vector3f vector) {
        return getBlockID((int) vector.x, (int) vector.y, (int) vector.z);
    }

    private boolean isReplantable(int blockID) {
        switch (blockID) {
            case 83: // Sugarcane
                return false;
            case 115: // Netherwart
            case 141: // Carrots
            case 142: // Potatoes
            case 59: // Wheat
            case 127: // Cocoa
                return true;
        }
        return false;
    }

    private int getGrowthLevel(Vector3f block) {
        return mc.theWorld.getBlockMetadata((int) block.x, (int) block.y, (int) block.z);
    }

    private int getMaxGrowthLevel(int blockID) {
        switch (blockID) {
            case 115: // Netherwart
                return 3;
            case 141: // Carrot
            case 142: // Potatoes
            case 59: // Wheat
            case 127: // Cocoa
            	return 7;
            default:
                return 0;
        }
    }

    private int getBlockIDFromSeed(int seedID) {
        switch (seedID) {
            case 372: // Netherwart
                return 115;
            case 391: // Carrot
                return 141;
            case 392: // Potatoes
                return 142;
            case 295: // Wheat
                return 59;
            case 127: // Cocoa
            	return 351;
            default:
                return -1;
        }
    }

    private int getSeedType(int blockID) {
        switch (blockID) {
            case 115: // Netherwart
                return 372;
            case 141: // Carrot
                return 391;
            case 142: // Potatoes
                return 392;
            case 59: // Wheat
                return 295;
            default:
                return -1;
        }
    }
    
    private String getMode() {
    	switch (mode) {
    	case 0:
    		return "Harvesting";
    	case 1:
    		return "Replanting";
    	}
    	return "";
    }
    
    private boolean isCocoaBeans(int x, int y, int z) {
    	return getBlockID(x, y, z) == 351; // && mc.theWorld.getBlockMaterial(x, y, z) == Material.plants;
    }
    
    /**
     * TODO determine the side we need to place cocoa beans on.
     * */
    private int hasAdjacent(int desiredBlockID, int x, int y, int z) {
    	return 1;
    }
    
    private int getBlockID(int x, int y, int z) {
    	return blockHelper.getBlockID(mc.theWorld.getBlock(x, y, z));
    }
    
    private boolean isAir(Block block) {
    	return block == null ? true : block.func_149688_o() == Material.air;
    }
}
