package net.halalaboos.huzuni.command.commands;

import java.io.File;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.console.Command;
import net.halalaboos.huzuni.scripting.ScriptManager;
import net.halalaboos.huzuni.util.StringUtils;

public class RunScript implements Command {

	@Override
	public String[] getAliases() {
		return new String[] { "runscript", "rs" };
	}

	@Override
	public String[] getHelp() {
		return new String[] { "runscript <file name>" };
	}

	@Override
	public String getDescription() {
		return "Runs a javascript file inside of the script engine.";
	}

	@Override
	public void run(String input, String[] args) {
		File name = new File(Huzuni.SAVE_DIRECTORY, StringUtils.getAfter(input, 1));
		ScriptManager.runScript(name);
	}

}
