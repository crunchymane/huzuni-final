/**
 *
 */
package net.halalaboos.huzuni.util.io;

import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import java.io.File;
import java.io.IOException;

/**
 * @author Halalaboos
 * @since Jul 14, 2013
 */
public class XMLFileHandler {
    private final File file;
    private final DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
    private Document document;

    public XMLFileHandler(File xmlFile) {
        this.file = xmlFile;
    }

    /**
     * Creates the document object for file writing.
     */
    public void createDocument() {
        try {
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            document = docBuilder.newDocument();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * */
    public Element createElement(String element) {
        return document.createElement(element);
    }

    /**
     * Creates an element and adds a text node.
     */
    public Element createElement(String elementName, String value) {
        Element element = document.createElement(elementName);
        element.appendChild(document.createTextNode(value));
        return element;
    }

    /**
     * Creates a comment instance from the document.
     */
    public Comment createComment(String data) {
        return document.createComment(data);
    }

    /**
     * Adds the element to the document.
     */
    public void addElement(Element element) {
        document.appendChild(element);
    }

    /**
     * Writes out the data on the document.
     */
    public void write() throws TransformerException {
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
        DOMSource source = new DOMSource(document);

        StreamResult result = new StreamResult(file);
        transformer.transform(source, result);
    }

    /**
     * Attempts to read the xml file.
     */
    public Document read() throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilder dBuilder = docFactory.newDocumentBuilder();
        Document document = dBuilder.parse(file);
        document.normalize();

        return document;
    }

}
