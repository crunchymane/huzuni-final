package pw.brudin.huzuni.theme.ingame;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.gui.ingame.IngameTheme;
import net.halalaboos.huzuni.mod.DefaultMod;
import net.halalaboos.huzuni.mod.ModManager;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;

import net.minecraft.util.text.TextFormatting;
import org.lwjgl.opengl.GL11;

/**
 * @since 10:05 PM on 3/7/2015
 */
public class JClient implements IngameTheme {

	@Override
	public void render(Minecraft mc, int screenWidth, int screenHeight) {
		long ping = mc.getCurrentServerData() == null ? 0 : mc.getCurrentServerData().pingToServer;
		String display = String.format("%s %s %s%sms", Huzuni.TITLE, Huzuni.VERSION, TextFormatting.GOLD, ping);
		mc.fontRenderer.drawStringWithShadow(display, 2, 2, -1);
		int yPosition = 12;
		for (DefaultMod Mod : ModManager.getDefaultMods()) {
			if (Mod.isEnabled() && Mod.getVisible()) {
				mc.fontRenderer.drawStringWithShadow(Mod.getRenderName().toLowerCase(), 2, yPosition, -1);
				yPosition += 12;
			}
		}
		int width = mc.fontRenderer.getStringWidth(display) + 4;
		renderPingBar(ping, width);
		renderItem(new ItemStack(Items.compass), width + 35, 0);
		renderItem(new ItemStack(Items.clock), width + 55, 0);
	}

	@Override
	public String getName() {
		return "jClient";
	}

	@Override
	public void onKeyTyped(int keyCode) {

	}

	private void renderItem(ItemStack itemStack, int x, int y) {
		try {
			GL11.glPushMatrix();
				GL11.glScaled(0.8D, 0.8D, 1.0D);
				mc.getRenderItem().renderItemAndEffectIntoGUI(itemStack, x, y);
				mc.getRenderItem().renderItemOverlays(mc.fontRenderer, itemStack, x, y);
			GL11.glPopMatrix();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void renderPingBar(long ping, int x) {
		int index = ping < 150L ? 0 : ping < 300L ? 1 : ping < 600L ? 2 : ping < 1000L ? 3 : 4;
		GL11.glPushMatrix();
			mc.getTextureManager().bindTexture(Gui.icons);
			Gui.drawModalRectWithCustomSizedTexture(x, 2, 0, (float) (176 + index * 8), 10, 8, 256.0F, 256.0F);
		GL11.glPopMatrix();
	}
}
