package net.halalaboos.huzuni.rendering.font;

import java.awt.*;
import java.awt.image.BufferedImage;

import net.halalaboos.huzuni.rendering.TextureUtils;
import static org.lwjgl.opengl.GL11.*;

/**
 * @author TheObliterator
 *         <p/>
 *         A class to create and draw true type fonts onto the Minecraft game
 *         engine.
 */

public class CFont {
	protected static final int IMAGE_WIDTH = 512, 
    IMAGE_HEIGHT = 512, 
    DEFAULT_CHAR_WIDTH = 8,
    DEFAULT_CHAR_HEIGHT = 8;
    protected int texID;
    protected CharData[] charData = new CharData[256];
    protected Font font;
    protected boolean antiAlias;
    protected boolean fractionalMetrics;

    protected int fontHeight = -1;
    protected int kerning = 1;

    public CFont(Font font, boolean antiAlias, boolean fractionalMetrics) {
        this.font = font;
        this.antiAlias = antiAlias;
        this.fractionalMetrics = fractionalMetrics;
        texID = setupTexture(font, antiAlias, fractionalMetrics, charData);
    }

    protected int setupTexture(Font font, boolean antiAlias, boolean fractionalMetrics, CharData[] chars) {
    	BufferedImage img = generateFontImage(font, antiAlias, fractionalMetrics, chars);
        
        try {
            return TextureUtils.applyTexture(TextureUtils.genTexture(), img, antiAlias, true);
        } catch (final NullPointerException e) {
            e.printStackTrace();
            return 0;
        }
    }
    
    protected BufferedImage generateFontImage(Font font, boolean antiAlias, boolean fractionalMetrics, CharData[] chars) {
    	// Image we'll use to store our font onto for rendering.
        BufferedImage bufferedImage = new BufferedImage(IMAGE_WIDTH, IMAGE_HEIGHT, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = (Graphics2D) bufferedImage.getGraphics();
        g.setFont(font);

        // Give blank background
        g.setColor(new Color(255, 255, 255, 0));
        g.fillRect(0, 0, IMAGE_WIDTH, IMAGE_HEIGHT);
        
        // Set color to white for rendering the font onto the texture.
        g.setColor(Color.WHITE);
        g.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, fractionalMetrics ? RenderingHints.VALUE_FRACTIONALMETRICS_ON : RenderingHints.VALUE_FRACTIONALMETRICS_OFF);
        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, antiAlias ? RenderingHints.VALUE_TEXT_ANTIALIAS_ON : RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, antiAlias ? RenderingHints.VALUE_ANTIALIAS_ON : RenderingHints.VALUE_ANTIALIAS_OFF);
        FontMetrics fontMetrics = g.getFontMetrics();
        // Max height for the characters.
        int charHeight = 0;
        int positionX = 0;
        int positionY = 0;
        // Stolen from the Slick library, it loads the characters into the image used for font rendering.
        for (int i = 0; i < chars.length; i++) {
            char ch = (char) i;
            CharData charData = new CharData();

            int height = fontMetrics.getHeight();
            int width = fontMetrics.stringWidth(String.valueOf(ch));

            charData.width = width;
            
            charData.height = height;

            if (positionX + charData.width >= IMAGE_WIDTH) {
                positionX = 0;
                positionY += charHeight;
                charHeight = 0;
            }

            if (charData.height > charHeight) {
            	charHeight = charData.height;
            }
            
            charData.storedX = positionX;
            charData.storedY = positionY;

            if (charData.height / 2 > fontHeight) {
                fontHeight = charData.height / 2;
            }
            
            chars[i] = charData;
            // Draw the char onto the final image we'll be using to render this font in game.
            g.drawString(String.valueOf(ch), positionX, positionY + fontMetrics.getAscent());

            positionX += charData.width + 1; // Add an extra pixel, so the characters have some space.
        }
        return bufferedImage;
    }

    /**
     * Private drawing method used within other drawing methods.
     */
    public void drawChar(CharData[] chars, char c, float x, float y)
            throws ArrayIndexOutOfBoundsException {
        try {
            drawQuad(x, y, (float) chars[c].width, (float) chars[c].height, chars[c].storedX, chars[c].storedY, (float) chars[c].width, (float) chars[c].height);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Again, stolen from the Slick library. Renders at the given image coordinates.
     */
    protected void drawQuad(float x, float y, float width, float height, float srcX, float srcY, float srcWidth, float srcHeight) {
        float renderSRCX = srcX / IMAGE_WIDTH,
                renderSRCY = srcY / IMAGE_HEIGHT,
                renderSRCWidth = (srcWidth) / IMAGE_WIDTH,
                renderSRCHeight = (srcHeight) / IMAGE_HEIGHT;
        glTexCoord2f(renderSRCX + renderSRCWidth, renderSRCY);
        glVertex2d(x + width, y);
        glTexCoord2f(renderSRCX, renderSRCY);
        glVertex2d(x, y);
        glTexCoord2f(renderSRCX, renderSRCY + renderSRCHeight);
        glVertex2d(x, y + height);
        glTexCoord2f(renderSRCX, renderSRCY + renderSRCHeight);
        glVertex2d(x, y + height);
        glTexCoord2f(renderSRCX + renderSRCWidth, renderSRCY + renderSRCHeight);
        glVertex2d(x + width, y + height);
        glTexCoord2f(renderSRCX + renderSRCWidth, renderSRCY);
        glVertex2d(x + width, y);
    }

    /**
     * Renders the text.
     */
    public float drawString(String text, double x, double y,
                           Color color, boolean shadow) {
    	x *= 2;
		y = ((y - 3) * 2);
        glPushMatrix();
        glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);

        glScaled(0.5D, 0.5D, 0.5D);
        glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, texID);
        glColor(shadow ? color.darker().darker() : color);
        int size = text.length();
        glBegin(GL_TRIANGLES);
        for (int i = 0; i < size; i++) {
            char character = text.charAt(i);
            if (character < charData.length && character >= 0) {
                drawChar(charData, character, (float) x, (float) y);
                x += charData[character].width + kerning;
            }
        }
        glEnd();
        glPopMatrix();
        return (float) x / 2F;
    }

    /**
     * OpenGL coloring.
     */
    public void glColor(Color color) {
        float red = (float) color.getRed() / 255F, green = (float) color.getGreen() / 255F, blue = (float) color.getBlue() / 255F, alpha = (float) color.getAlpha() / 255F;
        glColor4f(red, green, blue, alpha);
    }

    /**
     * @return The height of the given string.
     */
    public int getStringHeight(String text) {
    	/*int height = 0;
        for (char c : text.toCharArray()) {
            if (c < charData.length && c >= 0) {
            	if ((charData[c].height - DEFAULT_CHAR_HEIGHT) / 2 > height)
            		height = (charData[c].height - DEFAULT_CHAR_HEIGHT) / 2;
            }
        }
        return height;*/
        return getHeight();
    }

    /**
     * @return Total height that the current font can take up.
     */
    public int getHeight() {
        return (fontHeight);
    }

    /**
     * @return The width of the given string.
     */
    public int getStringWidth(String text) {
        int width = 0;
        for (char c : text.toCharArray()) {
            if (c < charData.length && c >= 0)
                width += charData[c].width + kerning;
        }
        return width / 2;
    }

    public boolean isAntiAlias() {
        return antiAlias;
    }

    public void setAntiAlias(boolean antiAlias) {
        if (this.antiAlias != antiAlias) {
            this.antiAlias = antiAlias;
    		glDeleteTextures(texID);
            texID = setupTexture(font, antiAlias, fractionalMetrics, charData);
        }
    }

    public boolean isFractionalMetrics() {
		return fractionalMetrics;
	}

	public void setFractionalMetrics(boolean fractionalMetrics) {
		if (this.fractionalMetrics != fractionalMetrics) {
			this.fractionalMetrics = fractionalMetrics;
			glDeleteTextures(texID);
        	texID = setupTexture(font, antiAlias, fractionalMetrics, charData);
		}
	}

	public Font getFont() {
        return font;
    }
    
    public void setFont(Font font) {
    	this.font = font;
		glDeleteTextures(texID);
        texID = setupTexture(font, antiAlias, fractionalMetrics, charData);
    }

    protected class CharData {
        public int width;
        public int height;
        public int storedX;
        public int storedY;
    }
}
