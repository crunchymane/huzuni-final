package net.halalaboos.huzuni.gui.clickable.theme;

import static org.lwjgl.opengl.GL11.*;
import java.awt.Color;
import net.halalaboos.huzuni.gui.clickable.button.Button;
import net.halalaboos.huzuni.util.GLUtils;

public class BmcTheme extends DefaultTheme {

	private final Color inside = new Color(70, 70, 70, 151),
			border = new Color(93, 93, 93, 151),
			buttons = new Color(0.4F, 0.4F, 0.4F, 0.75F);

	@Override
	protected void renderButtonRect(float x, float y, float x1, float y1, boolean highlight, boolean mouseOver, boolean mouseDown) {
		Color inside = GLUtils.getColorWithAffects(highlight ? buttons : this.inside, mouseOver, mouseDown);
		drawRect(x, y, x1, y1, inside.getRGB());
		start2D();
		color(highlight ? inside.getRGB() : border.getRGB());
		glLineWidth(1.0F);
		glBegin(GL_LINES);
		glVertex2d(x, y);
		glVertex2d(x, y1);
		glVertex2d(x1, y1);
		glVertex2d(x1, y);
		glVertex2d(x, y);
		glVertex2d(x1, y);
		glVertex2d(x, y1);
		glVertex2d(x1, y1);
		glEnd();
		end2D();
	}
	
	@Override
	protected int getButtonTextColor(Button button) {
		return button.isHighlight() ? 0x00FF00 : 0xFF0000;
	}

	@Override
	protected int getWindowTextColor() {
		return 0xFFFFFF;
	}
	
	@Override
	protected void renderWindowRect(float x, float y, float x1, float y1, float tabHeight, float lineWidth) {
		drawBorderedRect(x, y, x1, y1, lineWidth, border.getRGB(), inside.getRGB());
	}
	
	private void drawBorderedRect(double x, double y, double x1, double y1, float lineWidth, int borderColor, int insideColor) {
		drawRect(x, y, x1, y1, insideColor);

		start2D();
		startSmooth();
		color(borderColor);
		glLineWidth(lineWidth);
		glBegin(GL_LINES);
		glVertex2d(x, y);
		glVertex2d(x, y1);
		glVertex2d(x1, y1);
		glVertex2d(x1, y);
		glVertex2d(x, y);
		glVertex2d(x1, y);
		glVertex2d(x, y1);
		glVertex2d(x1, y1);
		glEnd();
		end2D();
		endSmooth();
	}

	private void drawRect(double x, double y, double x1, double y1, int color) {
		start2D();
		startSmooth();
		glShadeModel(GL_SMOOTH);

		glBegin(GL_QUADS);
		color(color);
		glVertex2d(x1, y);
		glVertex2d(x, y);
		glVertex2d(x, y1);
		glVertex2d(x1, y1);
		glEnd();

		glShadeModel(GL_FLAT);
		endSmooth();
		end2D();
	}

	private void start2D() {
		glPushMatrix();
		glDisable(GL_DEPTH_TEST);
		glEnable(GL_BLEND);
		glDisable(GL_TEXTURE_2D);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}

	private void end2D() {
		glEnable(GL_TEXTURE_2D);
		glDisable(GL_BLEND);
		glEnable(GL_DEPTH_TEST);
		glPopMatrix();
	}

	private void startSmooth() {
		glEnable(GL_LINE_SMOOTH);
		glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	}

	private void endSmooth() {
		glDisable(GL_LINE_SMOOTH);
		glHint(GL_LINE_SMOOTH_HINT, GL_DONT_CARE);

	}
	
	private void color(int color) {
		float red = (float)(color >> 16 & 255) / 255.0F;
		float green = (float)(color >> 8 & 255) / 255.0F;
		float blue = (float)(color & 255) / 255.0F;
		float alpha = (float)(color >> 24 & 255) / 255.0F;
		glColor4f(red, green, blue, alpha);
	}

	@Override
	public String getName() {
		return "BMC";
	}

	@Override
	protected int getTooltipTextColor() {
		return 0xFFFFFF;
	}

}
