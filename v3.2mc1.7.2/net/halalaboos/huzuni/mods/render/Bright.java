package net.halalaboos.huzuni.mods.render;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;
import com.darkmagician6.morbidlib.utils.timer.AccurateTimer;
import com.darkmagician6.morbidlib.utils.timer.Timer;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.events.game.EventTick;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.DefaultModule;

public class Bright extends DefaultModule implements Runnable {
	
    private float oldGamma;

    private Timer timer = new AccurateTimer();
        
    public Bright() {
        super("Bright", -1);
        setCategory(Category.RENDER);
        setRenderWhenEnabled(true);
        setDescription("Brightens up the world.");
    }

    @Override
    protected void onEnable() {
    	if (mc.gameSettings.gammaSetting <= 1 || Huzuni.modManager.getMod("Xray").isEnabled())
    		oldGamma = mc.gameSettings.gammaSetting;
    	new Thread(this).start();
    	
    }

    @Override
    protected void onDisable() {
    	new Thread(this).start();
    }

    @Override
    protected void onToggle() {
    }

	/**
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		final float INCREMENT_AMMOUNT = 0.05F,
    	MAX_BRIGHTNESS = 7;
		try {
			while (mc.gameSettings.gammaSetting <= MAX_BRIGHTNESS && mc.gameSettings.gammaSetting >= oldGamma) {
				mc.gameSettings.gammaSetting += isEnabled() ? INCREMENT_AMMOUNT : -INCREMENT_AMMOUNT;
				Thread.sleep(5L);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (mc.gameSettings.gammaSetting > MAX_BRIGHTNESS) {
			mc.gameSettings.gammaSetting = MAX_BRIGHTNESS;
		}
		if (mc.gameSettings.gammaSetting < oldGamma) {
			mc.gameSettings.gammaSetting = oldGamma;
		}
	}

}
