package net.halalaboos.huzuni.api.gui.event;

import net.halalaboos.huzuni.api.gui.Component;

public interface KeyListener <T extends Component> {

	void keyTyped(T component, int keyCode, char c);

}
