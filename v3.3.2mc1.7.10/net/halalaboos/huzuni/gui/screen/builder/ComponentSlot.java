package net.halalaboos.huzuni.gui.screen.builder;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.gui.Component;
import net.halalaboos.huzuni.api.gui.render.Theme;
import net.halalaboos.huzuni.gui.clickable.button.PluginButton;
import net.halalaboos.huzuni.gui.clickable.slider.ValueSlider;
import net.halalaboos.huzuni.gui.clickable.slot.SlotComponent;

public class ComponentSlot extends SlotComponent <Component> {

	public ComponentSlot() {
		super(0, 0);
		
	}

	@Override
	protected void onReleased(int index, Component component,
			int mouseX, int mouseY) {
	}

	@Override
	protected void onClicked(int index, Component component,
			float[] area, int mouseX, int mouseY, int buttonId) {
	}
	
	@Override
	protected void render(Theme theme, int index, Component component, float[] area, boolean highlight, boolean mouseOver,
			boolean mouseDown) {
		theme.renderSlot(this.getOffsetX(), this.getOffsetY(), index, area, highlight, mouseOver, mouseDown);
		if (component instanceof PluginButton) {
			PluginButton button = (PluginButton) component;
			Huzuni.drawStringWithShadow(button.getPlugin().getName(), area[0] + 2, area[1] + 2, 0xFFFFFF);
		}
		if (component instanceof ValueSlider) {
			ValueSlider slider = (ValueSlider) component;
			Huzuni.drawStringWithShadow(slider.getValueObj().getName(), area[0] + 2, area[1] + 2, 0xFFFFFF);
		}
	}

	@Override
	public int getComponentHeight() {
		return 12;
	}

}
