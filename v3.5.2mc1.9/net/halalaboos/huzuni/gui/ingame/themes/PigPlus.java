/**
 *
 */
package net.halalaboos.huzuni.gui.ingame.themes;

import net.halalaboos.huzuni.gui.ingame.IngameTheme;
import net.halalaboos.huzuni.mod.DefaultMod;
import net.halalaboos.huzuni.mod.ModManager;
import net.halalaboos.huzuni.util.GLUtils;
import net.minecraft.client.Minecraft;

/**
 * @author Halalaboos
 * @since Aug 21, 2013
 */
public class PigPlus implements IngameTheme {

    private static final int ICON_SIZE = 8;
    private int width = 12;
    private int height = 12;

    /**
     * @see net.halalaboos.huzuni.ui.ingame.IngameTheme#render(net.minecraft.src.Minecraft, int, int)
     */
    @Override
    public void render(Minecraft mc, int screenWidth, int screenHeight) {
        int xPos = 2;
        GLUtils.drawBorderedRect(xPos, 2, xPos + width + 2, height + 2, 1F, 0x80000000, 0xFF000000);
        width = 12;
        height = 4;
        for (DefaultMod mod : ModManager.getDefaultMods()) {
            if (mod.isBound()) {
                renderIcon(xPos + 2, height + 2, mod);
                String renderString = mod.getKeyName() + "  " + mod.getRenderName();
                mc.fontRenderer.drawStringWithShadow(renderString, xPos + 14, height + 3, mod.isEnabled() ? 0xFFFFFF : 0x999999);
                height += 13;

                int stringWidth = mc.fontRenderer.getStringWidth(renderString) + ICON_SIZE + 8;
                if (stringWidth > width)
                    width = stringWidth;
            }
        }
    }

    /**
     * Renders pig plus enabled / disabled icon.
     */
    private void renderIcon(int xPos, int yPos, DefaultMod mod) {
    	GLUtils.drawRect(xPos + 1, yPos + 1, xPos + ICON_SIZE + 1, yPos + ICON_SIZE + 1, 0xFF000000);
    	GLUtils.drawBorderedRect(xPos, yPos, xPos + ICON_SIZE, yPos + ICON_SIZE, 1F,
        		mod.isEnabled() ? 0xFF83F52C : 0xFFE3170D, 0xFF4D4D4D);

    }

    /**
     * @see net.halalaboos.huzuni.ui.ingame.IngameTheme#getName()
     */
    @Override
    public String getName() {
        return "Pig Plus";
    }

    /**
     * @see net.halalaboos.huzuni.ui.ingame.IngameTheme#onKeyTyped(int)
     */
    @Override
    public void onKeyTyped(int keyCode) {
    }


}
