package net.halalaboos.huzuni.mod.mods.render;

import static org.lwjgl.opengl.GL11.*;

import net.halalaboos.huzuni.Huzuni;
import org.lwjgl.input.Keyboard;
import net.halalaboos.huzuni.api.mod.Category;
import net.halalaboos.huzuni.mod.DefaultMod;

/**
 * @author brudin
 * @version 1.0
 * @since 3/26/14
 */
public class Wireframe extends DefaultMod {
	
	public static final Wireframe instance = new Wireframe();
	
	private Wireframe() {
		super("Wireframe", Keyboard.KEY_G);
		setCategory(Category.RENDER);
		setDescription("Renders the world in a wire-frame mode.");
		setAuthor("Halalaboos");
	}

	@Override
	protected void onEnable() {
		
	}

	@Override
	protected void onDisable() {

	}

	public void enable() {
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}

	public void disable() {
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
}
