/**
 *
 */
package net.halalaboos.huzuni.ui.clickable.components;

import net.halalaboos.huzuni.client.Settings;
import net.halalaboos.lib.ui.DefaultContainer;
import net.halalaboos.lib.ui.components.Label;
import net.halalaboos.lib.ui.components.Slider;
import net.halalaboos.lib.ui.listeners.ActionListener;

import java.awt.*;

/**
 * @author Halalaboos
 * @since Aug 31, 2013
 */
public abstract class ColorPicker extends DefaultContainer implements ActionListener<Slider> {
    private final String title;
    protected final Slider red, green, blue;

    public ColorPicker(String title, float red, float green, float blue) {
        super(new Rectangle(0, 0));
        this.title = title;
        add(new Label(title));
        this.red = new Slider("Red", 0, red, 255, 1);
        this.green = new Slider("Green", 0, green, 255, 1);
        this.blue = new Slider("Blue", 0, blue, 255, 1);
        this.red.addActionListener(this);
        this.green.addActionListener(this);
        this.blue.addActionListener(this);
        add(this.red);
        add(this.green);
        add(this.blue);
        layout();
    }

    protected abstract void onChange(int r, int g, int b);

    public void setRGB(int r, int g, int b) {
        red.setValue(r);
        green.setValue(g);
        blue.setValue(b);
        onChange(r, g, b);
    }

    public Color getColor() {
    	return new Color((int) red.getValue(), (int) green.getValue(), (int) blue.getValue());
    }

    /**
     * @see net.halalaboos.lib.ui.listeners.ActionListener#onAction(net.halalaboos.lib.ui.Component)
     */
    @Override
    public void onAction(Slider component) {
        onChange((int) red.getValue(), (int) blue.getValue(), (int) green.getValue());
    }
}
