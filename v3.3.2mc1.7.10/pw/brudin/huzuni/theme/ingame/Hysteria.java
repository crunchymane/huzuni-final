package pw.brudin.huzuni.theme.ingame;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.gui.ingame.IngameTheme;
import net.halalaboos.huzuni.plugin.DefaultPlugin;
import net.halalaboos.huzuni.plugin.PluginManager;
import net.minecraft.block.Block;
import net.minecraft.block.BlockSkull;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntitySkull;
import net.minecraft.util.Direction;
import net.minecraft.util.MathHelper;
import org.lwjgl.opengl.GL11;

/**
 * @author brudin
 * @version 1.0
 * @since 4:46 PM on 7/17/2014
 */
public class Hysteria implements IngameTheme {

	/**
	 * Position
	 * Chunk
	 * Facing DIRECTIONNAME
	 */

	@Override
	public void render(Minecraft mc, int screenWidth, int screenHeight) {
		int posY = 2;
		mc.fontRenderer.drawStringWithShadow("\247oHuzuni \2477\247o" + Huzuni.VERSION, 2, 2, 0xDCFFFFFF);
		for(String s : renderInfo()) {
			mc.fontRenderer.drawStringWithShadow(s, 2, posY += mc.fontRenderer.FONT_HEIGHT + 1, 0xDCAAAAAA);
		}
		for(DefaultPlugin plugin : PluginManager.getDefaultPlugins()) {
			if(plugin.isEnabled() && plugin.getVisible()) {
				mc.fontRenderer.drawStringWithShadow(plugin.getRenderName(), 2, posY += mc.fontRenderer.FONT_HEIGHT + 1, 0xDC595959);
			}
		}
	}

	private String[] renderInfo() {
		return new String[]{
				"Position(" + getFormattedCoordinates() + ")",
				"Chunk(" + getFormattedChunk() + ")",
				"Facing " + getDirection() + " looking at " + getLookingAt()
		};
	}

	private String getBlockInfo(Block block, int x, int y, int z) {
		if (block instanceof BlockSkull) {
			TileEntitySkull blockSkull = (TileEntitySkull) mc.theWorld.getTileEntity(x, y, z);
			return "Skull(" + (blockSkull.func_152108_a().getName().length() > 0 ? blockSkull.func_152108_a().getName() : "\247oOther\247r") + ")";
		} else {
			return "Block(" + Block.getIdFromBlock(block) + ":" + block.getDamageValue(mc.thePlayer.getEntityWorld(), x, y, z) + ", " + "\'" +
			getBlockName(block) + "\')";
		}
	}

	private String getEntityInfo(Entity entity) {
		if(entity instanceof EntityPlayer) {
			return "Player(" + entity.getEntityId() + ", " + entity.getCommandSenderName() + ", " +
					((EntityPlayer) entity).getHealth() + ", " + (((EntityPlayer) entity).getHeldItem()
					!= null ? ((EntityPlayer) entity).getHeldItem().getDisplayName() : "Nothing") + ")";
		} else if(entity instanceof EntityLiving) {
			return "Living(" + entity.getEntityId() + ", " + entity.getCommandSenderName() + ", " +
					((EntityLiving) entity).getHealth() + ", " + (((EntityLiving) entity).getHeldItem()
					!= null ? ((EntityLiving) entity).getHeldItem().getDisplayName() : "Nothing") + ")";
		} else {
			return "Entity(" + entity.getEntityId() + ", " + entity.getCommandSenderName() + ")";
		}
	}

	private String getBlockName(Block block) {
		String name = block.getLocalizedName();
		if(name.contains("tile.") || name.contains(".name")) {
			name = name.replaceAll("tile.", "").replaceAll(".name", "");
		}
		return name.length() > 1 ? name.substring(0, 1).toUpperCase() + name.substring(1) : name;
	}

	private String getLookingAt() {
		if(mc.objectMouseOver != null) {
			switch (mc.objectMouseOver.typeOfHit) {
				case BLOCK:
					int x = mc.objectMouseOver.blockX;
					int y = mc.objectMouseOver.blockY;
					int z = mc.objectMouseOver.blockZ;
					return getBlockInfo(mc.theWorld.getBlock(x, y, z), x, y, z);
				case ENTITY:
					Entity entity = mc.objectMouseOver.entityHit;
					return getEntityInfo(entity);
				case MISS:
					return "nothing";
			}
		}
		return "nothing";
	}

	private String getDirection() {
		return Direction.directions[MathHelper.floor_double((mc.thePlayer.rotationYaw * 4 / 360) + 0.5) & 3].toLowerCase();
	}

	private String getFormattedChunk() {
		return (getX() >> 4) + ", " + (getZ() >> 4) + ")"  + " (" + (getX() & 15 + 1) + ", " + (getZ() & 15 + 1);
	}

	private String getFormattedCoordinates() {
		return getX() + ", " + getY() + ", " + getZ();
	}

	private int getX() {
		return (int)mc.thePlayer.posX;
	}

	private int getY() {
		return (int)mc.thePlayer.posY;
	}

	private int getZ() {
		return (int)mc.thePlayer.posZ;
	}

	@Override
	public String getName() {
		return "Hysteria";
	}

	@Override
	public void onKeyTyped(int keyCode) {}
}
