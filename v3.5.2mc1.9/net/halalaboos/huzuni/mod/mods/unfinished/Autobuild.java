/**
 * 
 */
package net.halalaboos.huzuni.mod.mods.unfinished;

import static org.lwjgl.opengl.GL11.*;
import java.util.ArrayList;
import java.util.List;
import net.halalaboos.huzuni.mod.VipMod;
import net.minecraft.network.play.client.CPacketPlayerBlockPlacement;
import net.minecraft.network.play.client.CPacketPlayerTryUseItem;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import org.w3c.dom.Element;
import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.gui.render.Theme;
import net.halalaboos.huzuni.api.mod.Category;
import net.halalaboos.huzuni.api.mod.Option;
import net.halalaboos.huzuni.api.mod.ValueOption;
import net.halalaboos.huzuni.event.render.RenderEvent;
import net.halalaboos.huzuni.event.render.Renderable;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent;
import net.halalaboos.huzuni.event.tick.UpdateListener;
import net.halalaboos.huzuni.rendering.Box;
import net.halalaboos.huzuni.util.GLUtils;
import net.halalaboos.huzuni.util.GameUtils;
import net.halalaboos.huzuni.util.MathUtils;
import net.halalaboos.huzuni.util.Timer;
import net.minecraft.block.material.Material;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;

/**
 * @author Halalaboos
 *
 */
public class Autobuild extends VipMod implements UpdateListener, Renderable {

	private boolean[] pattern = new boolean[25];
	
	private float lastYaw, lastPitch;
	
	private List<BlockPos> layout = new ArrayList<BlockPos>();
	
	private BlockPos position = null;
	
	private boolean running = false;
	
	private final Timer timer = new Timer(), placeTimer = new Timer();
	
	private EnumFacing facing;
	
	private ValueOption delay = new ValueOption("Delay", 10F, 500F, 1000F, 10F, "Block placement delay");
	
	private final Box box;
		
	public Autobuild() {
		super("Auto Build");
        setCategory(Category.UNFINISHED);
        setAuthor("Halalaboos");
        setDescription("Places blocks in the pattern made.");
		box = new Box(AxisAlignedBB.fromBounds(0, 0, 0, 1, 1, 1));
		this.setOptions(new BuildOption(), delay);
	}
	
	@Override
	public void toggle() {
		super.toggle();
	}
	
	@Override
	protected void onEnable() {
		Huzuni.registerUpdateListener(this);
		Huzuni.registerRenderable(this);
		Huzuni.addChatMessage("Press sneak and place a block to start building!");
	}
	
	@Override
	protected void onDisable() {
		Huzuni.unregisterUpdateListener(this);
		Huzuni.unregisterRenderable(this);
		if (running) {
			position = null;
			running = false;
			Huzuni.addChatMessage("Cancelling..");
		}
	}
	
	private void getSelectedBlock() {
		if (mc.objectMouseOver != null && mc.objectMouseOver.typeOfHit == RayTraceResult.Type.BLOCK && placeTimer.hasReach(500)) {
			if (mc.gameSettings.keyBindUseItem.isKeyDown() && mc.gameSettings.keyBindSneak.isKeyDown()) {
				setupLayout();
				position = new BlockPos(mc.objectMouseOver.getBlockPos().getX(), mc.objectMouseOver.getBlockPos().getY() + 3, mc.objectMouseOver.getBlockPos().getZ());
				genDirection();
				running = true;
				timer.reset();
				Huzuni.addChatMessage("Placing..");
				placeTimer.reset();
			}
		}
	}
	
	@Override
	public void onPreUpdate(MotionUpdateEvent event) {
		lastYaw = mc.thePlayer.rotationYaw;
		lastPitch = mc.thePlayer.rotationPitch;
		if (running) {
			int index = getNextPiece();
			
			if (index == -1) {
				running = false;
				position = null;
				Huzuni.addChatMessage("Done!");
				return;
			}
			
			BlockPos nextPiece = layout.get(index);
			
			int[] positions = getAdjustedPosition(nextPiece);
            int x = positions[0], y = positions[1], z = positions[2];
            float offset = 0.5F;
			int side = getSide(position.getX() + x, position.getY() + y, position.getZ() + z);
			//GameUtils.face(position.getX() + x + offset, position.getY() + y + offset, position.getZ() + z + offset);
			
			positions = getPositionsFromSide(positions, side);
			x = positions[0]; y = positions[1]; z = positions[2];
			
			if (GameUtils.getDistance(position.getX() + x, position.getY() + y, position.getZ() + z) < 4.5F && mc.thePlayer.getHeldItem(EnumHand.MAIN_HAND) != null && mc.thePlayer.getHeldItem(EnumHand.MAIN_HAND).getItem() instanceof ItemBlock) {
				ItemBlock itemBlock = (ItemBlock) mc.thePlayer.getHeldItem(EnumHand.MAIN_HAND).getItem();
				if (itemBlock.canPlaceBlockOnSide(mc.theWorld, new BlockPos(position.getX() + x, position.getY() + y, position.getZ() + z), EnumFacing.getFront(side), mc.thePlayer, mc.thePlayer.getHeldItem(EnumHand.MAIN_HAND)) && timer.hasReach((int) delay.getValue())) {
					placeBlock(position.getX() + x, position.getY() + y, position.getZ() + z, side);
					GameUtils.swingItem();
					removeAndContinue(index);
					timer.reset();
				}
			}
		} else
			getSelectedBlock();
	}

	@Override
	public void onPostUpdate(MotionUpdateEvent event) {
		mc.thePlayer.rotationYaw = lastYaw;
		mc.thePlayer.rotationPitch = lastPitch;
	}
	
	@Override
	public void render(RenderEvent event) {
		if (running) {
			for (int i = 0; i < layout.size(); i++) {
				BlockPos position = layout.get(i);
				int[] positions = getAdjustedPosition(position);
	            int x = positions[0], y = positions[1], z = positions[2];
				double renderX = (this.position.getX() + x) - mc.getRenderManager().renderPosX;
				double renderY = (this.position.getY() + y) - mc.getRenderManager().renderPosY;
				double renderZ = (this.position.getZ() + z) - mc.getRenderManager().renderPosZ;
				glPushMatrix();
				glTranslated(renderX, renderY, renderZ);
				GLUtils.glColor(0, 1, 0, 0.2F);
				box.render();
				glPopMatrix();
			}
		}
    }
	
	protected void placeBlock(double x, double y, double z, int side) {
		float offset = 0F;
		BlockPos blockPos = new BlockPos(x, y, z);
		mc.getNetHandler().addToSendQueue(new CPacketPlayerTryUseItem(blockPos, EnumFacing.getFront(side), EnumHand.MAIN_HAND, 0, 0, 0));
//        mc.getNetHandler().addToSendQueue(new C08PacketPlayerBlockPlacement(blockPos, side, mc.thePlayer.inventory.getCurrentItem(), offset, offset, offset));
//        mc.thePlayer.getCurrentEquippedItem().onItemUse(mc.thePlayer, mc.theWorld, new BlockPos((int) x, (int) y, (int) z), EnumFacing.getFront(side), offset, offset, offset);
	}
	
	protected boolean removeAndContinue(int index) {
		if (layout.size() > index) {
			layout.remove(index);
			return false;
		} else
			return true;
	}
	
	protected int getNextPiece() {
		if (layout.size() < 1)
			return -1;
		else {
			for (int i = 0; i < layout.size(); i++) {
				BlockPos piece = layout.get(i);
				int[] positions = getAdjustedPosition(piece);
				
	            int x = positions[0], y = positions[1], z = positions[2];
				if (getSide(position.getX() + x, position.getY() + y, position.getZ() + z) != -1) {
					return i;
				}
			}
			return -1;
		}
	}
	
	/**
	 * Adjusts the given vector according to our direction.
	 * */
	protected int[] getAdjustedPosition(BlockPos position) {
		int x = 0, y = (int) (position.getY()), z = 0;
        switch (facing) {
        case SOUTH:
        	x = (int) -position.getX();
        	z = (int) position.getZ();
        	break;
        case WEST:
        	x = (int) -position.getZ();
        	z = (int) -position.getX();
        	break;
        case NORTH:
        	x = (int) position.getX();
        	z = (int) -position.getZ();
        	break;
        case EAST:
        	x = (int) position.getZ();
        	z = (int) position.getX();
        	break;
        default:
        	x = (int) position.getX();
        	z = (int) position.getZ();
        }
        return new int[] { x, y, z };
	}
	
	/**
	 * Finds a block that is adjacent to the coordinates, if there is a block it will give the side corresponding.
	 * */
	protected int getSide(int x, int y, int z) {
		if (!isAir(x, y, z))
			return -1;
		if (!isAir(x, y - 1, z))
			return 1;
		else if (!isAir(x, y + 1, z))
			return 0;
		else if (!isAir(x - 1, y, z))
			return 5;
		else if (!isAir(x + 1, y, z))
			return 4;
		else if (!isAir(x, y, z - 1))
			return 3;
		else if (!isAir(x, y, z + 1))
			return 2;
		else
			return -1;
	}
	
	/**
	 * From constant testing, I figured out that when you place a block,
	 * the coordinates sent is the coordinates to the block corresponding to the side. 
	 * They go as follow.
	 * */
	protected int[] getPositionsFromSide(int[] positions, int side) {
		switch (side) {
		case 0:
			positions[1] += 1;
			return positions;
		case 1:
			positions[1] -= 1;
			return positions;
		case 2:
			positions[2] += 1;
			return positions;
		case 3:
			positions[2] -= 1;
			return positions;
		case 4:
			positions[0] += 1;
			return positions;
		case 5:
			positions[0] -= 1;
			return positions;
			default:
				return positions;
		}
	}

	/**
	 * @return If the coordinates are air.
	 * */
	protected boolean isAir(double x, double y, double z) {
		return GameUtils.getMaterial(new BlockPos((int) x, (int) y, (int) z)) == Material.air;
	}
	
	protected void genDirection() {
        facing = mc.getRenderViewEntity().getHorizontalFacing();
	}
	
	/**
	 * Sets up the layout from the pattern.
	 * */
	protected void setupLayout() {
		
		this.layout = genLayout();
		/*
		 * SWASTIKA
		layout.clear();

		// TOP ARM
		layout.add(new Vector3f(2, 2, 0));
		layout.add(new Vector3f(1, 2, 0));
		layout.add(new Vector3f(0, 2, 0));
		layout.add(new Vector3f(0, 1, 0));
		
		// CENTER
		layout.add(new Vector3f(0, 0, 0));
		
		// BOTTOM ARM
		layout.add(new Vector3f(0, -1, 0));
		layout.add(new Vector3f(0, -2, 0));
		layout.add(new Vector3f(-1, -2, 0));
		layout.add(new Vector3f(-2, -2, 0));
		
		// RIGHT ARM
		layout.add(new Vector3f(1, 0, 0));
		layout.add(new Vector3f(2, 0, 0));
		layout.add(new Vector3f(2, -1, 0));
		layout.add(new Vector3f(2, -2, 0));
		
		// LEFT ARM
		layout.add(new Vector3f(-1, 0, 0));
		layout.add(new Vector3f(-2, 0, 0));
		layout.add(new Vector3f(-2, 1, 0));
		layout.add(new Vector3f(-2, 2, 0));
		*/
	}
	/**
	 * LAYOUT {
	 *	1  2  3  4 	5
	 *	6  7  8  9 	10
	 *	11 12 13 14 15
	 *	16 17 18 19 20
	 *	21 22 23 24 25
	 * }
	 * Generates a list from the pattern set.
	 * @return
	 */
	private List<BlockPos> genLayout() {
		List<BlockPos> list = new ArrayList<BlockPos>();
		for (int i = 0; i < pattern.length; i++) {
			boolean enabled = pattern[i];
			if (enabled) {
				list.add(new BlockPos(getX(i), getY(i), 0));
			}
		}
		return list;
	}

	public boolean[] getPattern() {
		return pattern;
	}
	
	private int getX(int id) {
		id += 1;
		return ((id % 5) == 0 ? -2 : -((id % 5) - 3));
	}
	
	private int getY(int id) {
		id += 1;
		return (int) (5 - Math.ceil((float)id / 5F)) - 2;
	}
	
	public class BuildOption extends Option {
		
	    private final ItemStack dirt = new ItemStack(Blocks.dirt),
	    grass = new ItemStack(Blocks.grass);
	    
		public BuildOption() {
			super("Build Option", null);	
		}

		@Override
		public boolean mouseClicked(int x, int y, int buttonId) {
			return this.isPointInside(x, y);
		}

		@Override
		public void mouseReleased(int x, int y, int buttonId) {
			int size = (int) ((getWidth() - 2) / 5F);
			int count = 0, yPos = 0, xPos = 0;
			for (int i = 0; i < 25; i++) {
				if (count >= 5) {
					yPos += size + 1;
					xPos = 0;
					count = 0;
				}
				float[] boxArea = new float[] { area[0] + xPos, area[1] + yPos, size, size };
				if (MathUtils.inside(x, y, boxArea)) {
					getPattern()[i] = !getPattern()[i];
				}
				xPos += size + 1;
				count++;
			}
		}

		@Override
		public void render(Theme theme, int index, float[] area,
				boolean mouseOver, boolean mouseDown) {
			int size = (int) ((getWidth() - 2) / 5F);
			int count = 0, yPos = 0, xPos = 0;
			for (int i = 0; i < 25; i++) {
				if (count >= 5) {
					yPos += size + 1;
					xPos = 0;
					count = 0;
				}
				float[] boxArea = new float[] { area[0] + xPos, area[1] + yPos, size, size };
				theme.renderSlot(0, 0, index, boxArea, getPattern()[i], mouseOver && MathUtils.inside(GLUtils.getMouseX(), GLUtils.getMouseY(), boxArea), mouseDown);
				GLUtils.draw2dItem(getPattern()[i] ? grass : dirt, (int) boxArea[0] + 1, (int) boxArea[1] + 1, 0F);
				xPos += size + 1;
				count++;
			}
		}
		@Override
		public void setArea(float[] area) {
			super.setArea(area);
			area[3] = ((area[2] - 2) / 5F) * 5 + 2;
		}

		@Override
		public void load(Element element) {
		}

		@Override
		public void save(Element element) {
		}
	}
	
}
