package net.halalaboos.huzuni.gui.screen;

import static org.lwjgl.opengl.GL11.*;
import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.gui.GuiManager;
import net.halalaboos.huzuni.gui.clickable.ContainerManager;
import net.halalaboos.huzuni.gui.clickable.button.Button;
import net.halalaboos.huzuni.gui.clickable.button.ModButton;
import net.halalaboos.huzuni.gui.clickable.label.Label;
import net.halalaboos.huzuni.gui.clickable.menu.Menu;
import net.halalaboos.huzuni.gui.clickable.slot.SlotComponent;
import net.halalaboos.huzuni.gui.clickable.theme.HuzuniTheme;
import net.halalaboos.huzuni.gui.clickable.window.Window;
import net.halalaboos.huzuni.gui.screen.macro.EditMacro;
import net.halalaboos.huzuni.util.GLUtils;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiYesNo;

import org.lwjgl.input.Keyboard;

import java.io.IOException;

import net.halalaboos.huzuni.api.gui.Component;
import net.halalaboos.huzuni.api.gui.render.Theme;

public class WindowEditorMenu extends GuiScreen {

	private final ContainerManager<Menu> menuManager = new ContainerManager<Menu>(new HuzuniTheme());

	private final GuiScreen parentGui;
	
	private final Menu menu;
	
	private SlotWindowComponents windowComponents;
	
	private SlotWindows slotWindows;
	
	private Button add = new Button("Add");
	
	public WindowEditorMenu(GuiScreen parentGui) {
		super();
		this.menu = new Menu("");
		this.menuManager.add(menu);
		this.menuManager.setTheme(GuiManager.getWindowThemes()[GuiManager.getSelectedWindowThemeId()]);
		this.parentGui = parentGui;
	}
	
	@Override
    public void initGui() {
		this.menu.clear();
    	Keyboard.enableRepeatEvents(true);
    	float padding = width / 4, topPadding = height / 4, bottomPadding = 50, internalPadding = 2;
        menu.setX(padding);
        menu.setY(topPadding);
        menu.setWidth((width * GLUtils.getScaleFactor() / 2) - padding * 2);
        menu.setHeight((height * GLUtils.getScaleFactor() / 2) - topPadding - bottomPadding * 2);
        Label windows = new Label("Windows");
        windows.setX(internalPadding);
        windows.setY(internalPadding);
        menu.add(windows);
        slotWindows = new SlotWindows(200 - internalPadding * 2, menu.getHeight() - 12 - internalPadding - internalPadding * 2);
        for (Window window : GuiManager.getWindows()) {
        	if (!GuiManager.isCustomWindow(window)) {
        		slotWindows.add(window);
        	}
        }
        slotWindows.setX(internalPadding);
        slotWindows.setY(12 + internalPadding + internalPadding);
		menu.add(slotWindows);
		Label components = new Label("Components");
		components.setX(200 + internalPadding);
		components.setY(internalPadding);
        menu.add(components);
		windowComponents = new SlotWindowComponents(menu.getWidth() - 200 - internalPadding, menu.getHeight() - 12 - internalPadding - internalPadding * 2) {
			
		};
		windowComponents.setX(200);
		windowComponents.setY(12 + internalPadding + internalPadding);
		menu.add(windowComponents);
        buttonList.add(new GuiButton(6, this.width / 2 - 154, this.height - 28, 152 * 2, 20, "Done"));
	}
	
	@Override
	public void onGuiClosed() {
    	Keyboard.enableRepeatEvents(false);
	}

	@Override
	protected void actionPerformed(GuiButton par1GuiButton) {
		if (!par1GuiButton.enabled) {
			return;
		}

		switch (par1GuiButton.id) {
		case 6:
			mc.displayGuiScreen(parentGui);
			break;
		default:
			break;
		}
	}

    @Override
    public void drawScreen(int mouseX, int mouseY, float delta) {
        super.drawDefaultBackground();
        super.drawScreen(mouseX, mouseY, delta);
        GLUtils.setupOverlayDefaultScale();
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        menuManager.render();
        glDisable(GL_BLEND);
        mc.entityRenderer.setupOverlayRendering();
    }
    @Override
    protected void keyTyped(char c, int keyCode) throws IOException {
        super.keyTyped(c, keyCode);
        menuManager.keyTyped(keyCode, c);
    }

    @Override
    protected void mouseClicked(int x, int y, int buttonId) throws IOException {
        super.mouseClicked(x, y, buttonId);
        menuManager.mouseClicked(GLUtils.getMouseX(), GLUtils.getMouseY(), buttonId);
    }
    
    private class SlotWindows extends SlotComponent<Window> {

		public SlotWindows(float width, float height) {
			super(width, height);
			//this.setRenderBackground(false);
		}
		
		@Override
		protected void onReleased(int index, Window window, int mouseX, int mouseY) {
		}

		@Override
		protected void onClicked(int index, Window window, float[] area, int mouseX, int mouseY, int buttonId) {
			this.selectedComponent = index;
			loadWindowComponents(index);
		}
		
		@Override
		protected void render(Theme theme, int index, Window window, float[] area, boolean highlight, boolean mouseOver, boolean mouseDown) {
			if (highlight)
				theme.renderSlot(this.getOffsetX(), this.getOffsetY(), index, area, highlight, mouseOver, mouseDown);
			Huzuni.drawString(window.getTitle(), area[0] + 2, area[1] + 2, 0xFFFFFF);
		}
		
		@Override
		public void keyTyped(int keyCode, char c) {
			super.keyTyped(keyCode, c);
			if (this.components.size() > 0) {
				if (keyCode == Keyboard.KEY_DOWN) {
					this.selectedComponent++;
					if (this.selectedComponent >= this.components.size())
						this.selectedComponent = 0;
					loadWindowComponents(selectedComponent);
				}
				if (keyCode == Keyboard.KEY_UP) {
					this.selectedComponent--;
					if (this.selectedComponent < 0)
						this.selectedComponent = this.components.size() - 1;
					loadWindowComponents(selectedComponent);
				}
			}
		}
		
		protected void loadWindowComponents(int index) {
			Window window = this.getSelectedComponentO();
			windowComponents.setSelectedComponent(-1);
			windowComponents.setComponents(window.getComponents());
			this.setSelectedComponent(index);
		}
    	
    }
    
    private class SlotWindowComponents extends SlotComponent<Component> {

		public SlotWindowComponents(float width, float height) {
			super(width, height);
		}
		
		@Override
		protected void onReleased(int index, Component component, int mouseX, int mouseY) {
		}

		@Override
		protected void onClicked(int index, Component component, float[] area, int mouseX, int mouseY, int buttonId) {
			this.selectedComponent = index;
		}
		
		@Override
		protected void render(Theme theme, int index, Component component, float[] area, boolean highlight, boolean mouseOver, boolean mouseDown) {
			if (highlight)
				theme.renderSlot(this.getOffsetX(), this.getOffsetY(), index, area, highlight, mouseOver, mouseDown);
			if (component instanceof ModButton) {
				ModButton button = (ModButton) component;
				Huzuni.drawString(button.getMod().getName(), area[0] + 2, area[1] + 2, 0xFFFFFF);
			} else
				Huzuni.drawString(component.toString(), area[0] + 2, area[1] + 2, 0xFFFFFF);
		}
		
		@Override
		public void keyTyped(int keyCode, char c) {
			super.keyTyped(keyCode, c);
			if (this.components.size() > 0) {
				if (this.hasSelectedComponent())
					if (keyCode == Keyboard.KEY_DELETE)
						this.components.remove(this.selectedComponent);
			}
		}
    	
    }
}
