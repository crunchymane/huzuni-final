package net.halalaboos.huzuni.gui.ingame.themes.tabbed;

import net.halalaboos.huzuni.api.mod.Category;
import net.halalaboos.huzuni.api.mod.Mod;
import net.minecraft.client.Minecraft;

public class Tab {
	
	private Category category;
	
	private Mod[] mods = null;
	
	private int width, selection = 0;
	
	public Tab(Category category) {
		this.category = category;
	}
	
	public Mod getSelectedMod() {
		keepSafe();
		return mods[selection];
	}
	
	public Category getCategory() {
		return category;
	}
	
	public Mod[] getMods() {
		return mods;
	}

	public void setMods(Mod[] mods) {
		this.mods = mods;
		calculateWidth();
	}

	public void calculateWidth() {
		width = 0;
		for (Mod mod : mods) {
			String name = mod.getName();
			if (Minecraft.getMinecraft().fontRenderer.getStringWidth(name) + 8 > width)
				width = Minecraft.getMinecraft().fontRenderer.getStringWidth(name) + 8;
		}
	}

	public void keepSafe() {
		if (selection >= mods.length)
			selection = mods.length - 1;
		if (selection < 0)
			selection = 0;
	}
	
	public void up() {
		selection--;
		if (selection < 0)
			selection = mods.length - 1;
	}
	
	public void down() {
		selection++;
		if (selection >= mods.length)
			selection = 0;
	}

	public int getWidth() {
		return width;
	}
}
