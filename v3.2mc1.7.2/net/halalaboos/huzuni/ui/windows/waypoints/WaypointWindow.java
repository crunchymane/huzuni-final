/**
 * 
 */
package net.halalaboos.huzuni.ui.windows.waypoints;

import java.awt.Dimension;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.client.waypoints.Waypoint;
import net.halalaboos.huzuni.ui.clickable.containers.HuzuniWindow;
import net.halalaboos.huzuni.ui.windows.WindowManager;
import net.halalaboos.lib.ui.Component;
import net.halalaboos.lib.ui.components.Button;
import net.halalaboos.lib.ui.components.SlotComponent;
import net.halalaboos.lib.ui.listeners.ActionListener;

/**
 * @author Halalaboos
 *
 * @since Oct 6, 2013
 */
public class WaypointWindow extends HuzuniWindow implements ActionListener<Button> {
	private final WindowManager windowContainer;
	
	private final SlotWaypoints slotWaypoints;
	
	private final Button addButton, removeButton, modifyButton;
	/**
	 * @param title
	 */
	public WaypointWindow(WindowManager windowContainer) {
		super("Waypoints");
		this.windowContainer = windowContainer;
		slotWaypoints = new SlotWaypoints(this, new Dimension(100, 54));
		add(slotWaypoints);
		slotWaypoints.setComponents(Huzuni.waypointManager.getList());
		
		addButton = new Button("Add");
		addButton.addActionListener(this);
		addButton.setHighlight(true);
		add(addButton);
		
		removeButton = new Button("Remove");
		removeButton.addActionListener(this);
		add(removeButton);

		layout();
		
		modifyButton = new Button("Modify");
		modifyButton.addActionListener(this);
		add(modifyButton);

		modifyButton.getArea().setLocation(removeButton.getArea().getLocation());
		modifyButton.getArea().setSize(removeButton.getArea().getSize());
		
		// Set the components next to eachother..		
		addButton.getArea().width /= 2;
		removeButton.getArea().setSize(addButton.getArea().getSize());
		removeButton.getArea().setLocation(addButton.getArea().x + addButton.getArea().width + 1, addButton.getArea().y);
	}

	/**
	 * @see net.halalaboos.lib.ui.listeners.ActionListener#onAction(net.halalaboos.lib.ui.Component)
	 */
	@Override
	public void onAction(Button button) {
		if (button == removeButton && removeButton.isHighlight()) {
			Waypoint waypoint = slotWaypoints.getSelectedWaypoint();
			if (waypoint != null) {
				Huzuni.waypointManager.remove(waypoint);
				slotWaypoints.setSelectedIndex(-1);
			}
		} else if (button == modifyButton && modifyButton.isHighlight()) {
			Waypoint waypoint = slotWaypoints.getSelectedWaypoint();
			if (waypoint != null) {
				// MODIFY
				ModifyWaypointWindow window = new ModifyWaypointWindow(this, waypoint);
				window.center();
				windowContainer.add(window);
			}
		} else if (button == addButton) {
			ModifyWaypointWindow window = new ModifyWaypointWindow(this);
			window.center();
			windowContainer.add(window);
		}
		onIndexChanged();
	}
	
	public void onIndexChanged() {
		boolean selected = slotWaypoints.getSelectedIndex() != -1;
		removeButton.setHighlight(selected);
		modifyButton.setHighlight(selected);
	}
	
	public void finishModify(ModifyWaypointWindow modifyWaypointWindow, Waypoint waypoint, boolean isNew) {
		if (isNew) {
			Huzuni.waypointManager.add(waypoint);
		} else {
		}
		windowContainer.remove(modifyWaypointWindow);
	}

}
