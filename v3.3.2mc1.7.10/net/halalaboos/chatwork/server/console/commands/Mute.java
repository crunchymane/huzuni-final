package net.halalaboos.chatwork.server.console.commands;

import net.halalaboos.chatwork.server.ChatworkClient;
import net.halalaboos.chatwork.server.Server;
import net.halalaboos.chatwork.server.console.Command;

public class Mute implements Command {

	@Override
	public String getDescription() {
		return "Mutes another user.";
	}

	@Override
	public String[] getAliases() {
		return new String[] { "mute", "silence" };
	}

	@Override
	public boolean isValid(String input) {
		return input.contains(" ");
	}

	@Override
	public void execute(ChatworkClient sender, String input, String[] args) {
		ChatworkClient reciever = Server.getUser(args[0]);
		reciever.getInfo().setMuted(true);
		sender.sendMessage(reciever.getInfo().getUsername() + " is now muted.");
		reciever.sendMessage("You are muted!");
	}

	@Override
	public void help(ChatworkClient sender) {
		sender.sendMessage("/mute <username>");
	}

	@Override
	public int getPermission() {
		return 10;
	}

}
