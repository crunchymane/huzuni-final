/**
 *
 */
package net.halalaboos.huzuni.mods.player;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;

import net.halalaboos.huzuni.client.Settings;
import net.halalaboos.huzuni.events.game.EventMovement;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.DefaultModule;
import net.halalaboos.lib.io.config.DefaultConfigListener;

/**
 * @author Halalaboos
 * @since Aug 1, 2013
 */
public class Step extends DefaultModule implements Listener {

	private DefaultConfigListener<Float> height = new DefaultConfigListener<Float>("Step Height", 1F);

	
    /**
     * @param name
     * @param keyCode
     */
    public Step() {
        super("Step", -1);
        setCategory(Category.MOVEMENT);
        Settings.put("Step Height", 1F);
        Settings.addListener(height);
    }

    /**
     * @see net.halalaboos.huzuni.mods.DefaultModule#onEnable()
     */
    @Override
    protected void onEnable() {
        registry.registerListener(this);
    }

    /**
     * @see net.halalaboos.huzuni.mods.DefaultModule#onDisable()
     */
    @Override
    protected void onDisable() {
        registry.unregisterListener(this);
        if (mc.thePlayer != null)
            mc.thePlayer.stepHeight = 0.5F;
    }

    /**
     * @see net.halalaboos.huzuni.mods.DefaultModule#onToggle()
     */
    @Override
    protected void onToggle() {
    }

    @EventTarget
    public void onMovement(EventMovement event) {
        mc.thePlayer.stepHeight = height.getValue();
    }
}
