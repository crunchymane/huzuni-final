package net.halalaboos.huzuni.plugin.plugins.combat;

import static org.lwjgl.opengl.GL11.GL_LINES;
import static org.lwjgl.opengl.GL11.GL_LINE_STRIP;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glColor3f;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glLineWidth;
import static org.lwjgl.opengl.GL11.glPopMatrix;
import static org.lwjgl.opengl.GL11.glPushMatrix;
import static org.lwjgl.opengl.GL11.glRotatef;
import static org.lwjgl.opengl.GL11.glTranslated;
import static org.lwjgl.opengl.GL11.glVertex3d;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.util.glu.Cylinder;
import org.lwjgl.util.glu.GLU;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.plugin.Category;
import net.halalaboos.huzuni.event.render.RenderEvent;
import net.halalaboos.huzuni.event.render.Renderable;
import net.halalaboos.huzuni.plugin.DefaultPlugin;
import net.minecraft.block.Block;
import net.minecraft.block.BlockLiquid;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.command.IEntitySelector;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBow;
import net.minecraft.item.ItemEgg;
import net.minecraft.item.ItemEnderPearl;
import net.minecraft.item.ItemPotion;
import net.minecraft.item.ItemSnowball;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;

public class Projectiles extends DefaultPlugin implements Renderable {

	public Projectiles() {
		super("Projectiles");
		setCategory(Category.COMBAT);
		setAuthor("Halalaboos");
		setDescription("Shows the trajectory of throwable objects.");
	}

	@Override
	public void render(RenderEvent event) {
        int mode = 0;
        EntityPlayer player = mc.thePlayer;
        /*check for bow and if the item is throwable*/
        if (player.getCurrentEquippedItem() != null) {
            Item item = player.getCurrentEquippedItem().getItem();
            if (!(item instanceof ItemBow || item instanceof ItemSnowball || item instanceof ItemEnderPearl || item instanceof ItemEgg || (item instanceof ItemPotion && player.getCurrentEquippedItem().getItemDamage() != 0)))
                return;
            if ((item instanceof ItemBow)) {
                mode = 1;
            } else if (item instanceof ItemPotion && player.getCurrentEquippedItem().getItemDamage() != 0) {
            	mode = 2;
            }
        } else
            return;
		
		/*copypasta from EntityArrow.java and EntityThrowable.java*/
        double posX = RenderManager.renderPosX - (double) (MathHelper.cos(player.rotationYaw / 180.0F * (float) Math.PI) * 0.16F),
                posY = (RenderManager.renderPosY + (double) player.getEyeHeight()) - 0.10000000149011612D,
                posZ = RenderManager.renderPosZ - (double) (MathHelper.sin(player.rotationYaw / 180.0F * (float) Math.PI) * 0.16F),
                motionX = (double) (-MathHelper.sin(player.rotationYaw / 180.0F * (float) Math.PI) * MathHelper.cos(player.rotationPitch / 180.0F * (float) Math.PI)) * (mode == 1 ? 1.0 : 0.4),
                motionY = (double) (-MathHelper.sin(player.rotationPitch / 180.0F * (float) Math.PI)) * (mode == 1 ? 1.0 : 0.4),
                motionZ = (double) (MathHelper.cos(player.rotationYaw / 180.0F * (float) Math.PI) * MathHelper.cos(player.rotationPitch / 180.0F * (float) Math.PI)) * (mode == 1 ? 1.0 : 0.4);

        //POWER: ((power ranging from 0 ~ 1) * 2) * 1.5
		/*ItemBow.java*/
        if (player.getItemInUseCount() <= 0 && mode == 1) {
            return;
        }
        int var6 = 72000 - player.getItemInUseCount();
        float power = (float) var6 / 20.0F;
        power = (power * power + power * 2.0F) / 3.0F;
        if ((double) power < 0.1D)
            return;
        if (power > 1.0F)
            power = 1.0F;
        glColor3f((1 - power), power, 0);

		/*prep motion (EntityArrow.java)*/
        float distance = MathHelper.sqrt_double(motionX * motionX + motionY * motionY + motionZ * motionZ);
        motionX /= (double) distance;
        motionY /= (double) distance;
        motionZ /= (double) distance;
		/*power (more EntityArrow.java)*/
        motionX *= (mode == 1 ? (power * 2) : 1) * getMult(mode);
        motionY *= (mode == 1 ? (power * 2) : 1) * getMult(mode);
        motionZ *= (mode == 1 ? (power * 2) : 1) * getMult(mode);

        glLineWidth(2);
        glBegin(GL_LINE_STRIP);
        boolean hasLanded = false, isEntity = false;
        MovingObjectPosition landingPosition = null;
        float size = (float) (mode == 1 ? 0.3 : 0.25);
        float gravity = getGravity(mode);
        for (; !hasLanded && posY > 0;) {
			/*Check for landing on a block*/
            Vec3 present = Vec3.createVectorHelper(posX, posY, posZ);
            Vec3 future = Vec3.createVectorHelper(posX + motionX, posY + motionY, posZ + motionZ);
            MovingObjectPosition possibleLandingStrip = mc.theWorld.func_147447_a(present, future, false, true, false);
            present = Vec3.createVectorHelper(posX, posY, posZ);
            future = Vec3.createVectorHelper(posX + motionX, posY + motionY, posZ + motionZ);
            if (possibleLandingStrip != null) {
                hasLanded = true;
                landingPosition = possibleLandingStrip;
            }
	        /*Check for landing on an entity*/
            AxisAlignedBB arrowBox = AxisAlignedBB.getBoundingBox(posX - size, posY - size, posZ - size, posX + size, posY + size, posZ + size);
            List entities = getEntitiesWithinAABB(arrowBox.addCoord(motionX, motionY, motionZ).expand(1.0D, 1.0D, 1.0D));

            for (int index = 0; index < entities.size(); ++index) {
                Entity entity = (Entity) entities.get(index);

                if (entity.canBeCollidedWith() && (entity != player)) {
                    float var11 = 0.3F;
                    AxisAlignedBB var12 = entity.boundingBox.expand((double) var11, (double) var11, (double) var11);
                    MovingObjectPosition possibleEntityLanding = var12.calculateIntercept(present, future);
                    if (possibleEntityLanding != null) {
                        hasLanded = true;
                        isEntity = true;
                        landingPosition = possibleEntityLanding;
                    }
                }
            }
	        /*Arrow rendering and calculation math stuff*/
            posX += motionX;
            posY += motionY;
            posZ += motionZ;
            float motionAdjustment = 0.99F;
            AxisAlignedBB boundingBox = AxisAlignedBB.getBoundingBox(posX - size, posY - size, posZ - size, posX + size, posY + size, posZ + size);
            if (isInMaterial(boundingBox, Material.water))
                motionAdjustment = 0.8F;

            motionX *= motionAdjustment;
            motionY *= motionAdjustment;
            motionZ *= motionAdjustment;
            motionY -= gravity;
            glVertex3d(posX - RenderManager.renderPosX, posY - RenderManager.renderPosY, posZ - RenderManager.renderPosZ);
        }
        glEnd();
        glPushMatrix();
        glTranslated(posX - RenderManager.renderPosX, posY - RenderManager.renderPosY, posZ - RenderManager.renderPosZ);
        if (landingPosition != null) {
            switch (landingPosition.sideHit) {
                case 2://east
                    glRotatef(90, 1, 0, 0);
                    break;
                case 3://west
                    glRotatef(90, 1, 0, 0);
                    break;
                case 4://north
                    glRotatef(90, 0, 0, 1);
                    break;
                case 5://south
                    glRotatef(90, 0, 0, 1);
                    break;
                default:
                    break;
            }
            if (isEntity)
                glColor3f(1, 0, 0);
        }
        renderPoint();
        glPopMatrix();
    }
	

    private float getMult(int mode) {
    	if (mode == 2) {
    		return 0.5F;
    	} else
    		return 1.5F;
    }

    private float getGravity(int mode) {
    	if (mode == 1 || mode == 2) {
    		return 0.05F;
    	} else
    		return 0.03F;
    }
    
    private void renderPoint() {
        glBegin(GL_LINES);
        glVertex3d(-.5, 0, 0);
        glVertex3d(0, 0, 0);
        glVertex3d(0, 0, -.5);
        glVertex3d(0, 0, 0);

        glVertex3d(.5, 0, 0);
        glVertex3d(0, 0, 0);
        glVertex3d(0, 0, .5);
        glVertex3d(0, 0, 0);
        glEnd();

        Cylinder c = new Cylinder();
        glRotatef(-90, 1, 0, 0);
        c.setDrawStyle(GLU.GLU_LINE);
        c.draw(0.5f, 0.5f, 0.1f, 24, 1);
    }

    private boolean isInMaterial(AxisAlignedBB axisalignedBB, Material material) {
        int chunkMinX = MathHelper.floor_double(axisalignedBB.minX);
        int chunkMaxX = MathHelper.floor_double(axisalignedBB.maxX + 1.0D);
        int chunkMinY = MathHelper.floor_double(axisalignedBB.minY);
        int chunkMaxY = MathHelper.floor_double(axisalignedBB.maxY + 1.0D);
        int chunkMinZ = MathHelper.floor_double(axisalignedBB.minZ);
        int chunkMaxZ = MathHelper.floor_double(axisalignedBB.maxZ + 1.0D);

        if (!mc.theWorld.checkChunksExist(chunkMinX, chunkMinY, chunkMinZ, chunkMaxX, chunkMaxY, chunkMaxZ)) {
            return false;
        } else {
            boolean isWithin = false;
            Vec3 vector = Vec3.createVectorHelper(0.0D, 0.0D, 0.0D);

            for (int x = chunkMinX; x < chunkMaxX; ++x) {
                for (int y = chunkMinY; y < chunkMaxY; ++y) {
                    for (int z = chunkMinZ; z < chunkMaxZ; ++z) {
                        Block block = mc.theWorld.getBlock(x, y, z);

                        if (block != null && block.getMaterial() == material) {
                            double liquidHeight = (double) ((float) (y + 1) - BlockLiquid.func_149801_b(mc.theWorld.getBlockMetadata(x, y, z)));

                            if ((double) chunkMaxY >= liquidHeight) {
                                isWithin = true;
                            }
                        }
                    }
                }
            }
            return isWithin;
        }
    }

    private List getEntitiesWithinAABB(AxisAlignedBB axisalignedBB) {
        List list = new ArrayList();
        int chunkMinX = MathHelper.floor_double((axisalignedBB.minX - 2.0D) / 16.0D);
        int chunkMaxX = MathHelper.floor_double((axisalignedBB.maxX + 2.0D) / 16.0D);
        int chunkMinZ = MathHelper.floor_double((axisalignedBB.minZ - 2.0D) / 16.0D);
        int chunkMaxZ = MathHelper.floor_double((axisalignedBB.maxZ + 2.0D) / 16.0D);

        for (int x = chunkMinX; x <= chunkMaxX; ++x) {
            for (int z = chunkMinZ; z <= chunkMaxZ; ++z) {
                if (mc.theWorld.getChunkProvider().chunkExists(x, z)) {
                    mc.theWorld.getChunkFromChunkCoords(x, z).getEntitiesWithinAABBForEntity(mc.thePlayer, axisalignedBB, list, (IEntitySelector) null);
                }
            }
        }

        return list;
    }

	@Override
	protected void onEnable() {
		Huzuni.registerRenderable(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterRenderable(this);
	}

}
