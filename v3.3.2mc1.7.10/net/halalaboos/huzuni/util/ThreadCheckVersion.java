package net.halalaboos.huzuni.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

import net.halalaboos.huzuni.Huzuni;

public class ThreadCheckVersion extends Thread {

	public ThreadCheckVersion() {
		
	}

	@Override
	public void run() {
		try {
			final URL updateURL = new URL(
					"http://halalaboos.net/client/version");
			final BufferedReader reader = new BufferedReader(new InputStreamReader(updateURL.openStream()));
			String line = reader.readLine();
			/* Everything here is obvious.. */
			if (!line.equals(Huzuni.VERSION)) {
				Huzuni.setUpdateCode(1);
			} else
				Huzuni.setUpdateCode(0);
			reader.close();
		} catch (Exception e) {
			/* We cannot connect to halalaboos.net :'( */
			Huzuni.setUpdateCode(-1);
		}
	}

}
