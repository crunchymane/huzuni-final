package net.halalaboos.huzuni.command.commands;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.console.Command;
import net.minecraft.client.Minecraft;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.*;
import org.lwjgl.opengl.DisplayMode;

/**
 * @since 1:58 PM on 8/11/2015
 */
public class Resize implements Command {

	@Override
	public String[] getAliases() {
		return new String[] {"resize"};
	}

	@Override
	public String[] getHelp() {
		return new String[] {"resize <width> <height>"};
	}

	@Override
	public String getDescription() {
		return "Resizes the window to the given dimensions.";
	}

	@Override
	public void run(String input, String[] args) {
		int width = Integer.parseInt(args[0]), height = Integer.parseInt(args[1]);
		try {
			Display.setDisplayMode(new DisplayMode(width, height));
			Minecraft.getMinecraft().resize(width, height);
			Huzuni.addChatMessage(String.format("Set window size to %sx%s.", width, height));
		} catch (LWJGLException e) {
			e.printStackTrace();
		}
	}
}
