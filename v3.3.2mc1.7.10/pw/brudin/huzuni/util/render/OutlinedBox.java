package pw.brudin.huzuni.util.render;

import net.halalaboos.huzuni.rendering.Vbo;
import net.minecraft.util.AxisAlignedBB;

import static org.lwjgl.opengl.GL11.*;

/**
 * @author brudin
 * @version 1.0
 * @since 3/25/14
 */
public class OutlinedBox extends Vbo {

	public OutlinedBox(AxisAlignedBB boundingBox) {
		setup(boundingBox);
	}

	public void setup(AxisAlignedBB bb) {
		addVertex(bb.minX, bb.minY, bb.minZ);
		addVertex(bb.minX, bb.minY, bb.minZ);
		addVertex(bb.maxX, bb.minY, bb.minZ);
		addVertex(bb.maxX, bb.minY, bb.maxZ);
		addVertex(bb.minX, bb.minY, bb.maxZ);

		addVertex(bb.minX, bb.minY, bb.minZ);
		addVertex(bb.minX, bb.maxY, bb.minZ);

		addVertex(bb.maxX, bb.maxY, bb.minZ);
		addVertex(bb.maxX, bb.minY, bb.minZ);
		addVertex(bb.maxX, bb.maxY, bb.minZ);

		addVertex(bb.maxX, bb.maxY, bb.maxZ);
		addVertex(bb.maxX, bb.minY, bb.maxZ);
		addVertex(bb.maxX, bb.maxY, bb.maxZ);

		addVertex(bb.minX, bb.maxY, bb.maxZ);
		addVertex(bb.minX, bb.minY, bb.maxZ);
		addVertex(bb.minX, bb.maxY, bb.maxZ);
		addVertex(bb.minX, bb.maxY, bb.minZ);
		compile();
	}

	public void render() {
		this.render(GL_LINE_LOOP);
	}
}
