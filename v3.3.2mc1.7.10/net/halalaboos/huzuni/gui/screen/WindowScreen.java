package net.halalaboos.huzuni.gui.screen;


import static org.lwjgl.opengl.GL11.*;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import net.halalaboos.huzuni.gui.GuiManager;
import net.halalaboos.huzuni.plugin.PluginManager;
import net.minecraft.client.gui.GuiScreen;

public class WindowScreen extends GuiScreen {


	@Override
    public void initGui() {
    	Keyboard.enableRepeatEvents(true);
    }
	
	@Override
	public void onGuiClosed() {
		GuiManager.saveWindows();
		PluginManager.savePlugins();
    	Keyboard.enableRepeatEvents(false);
	}
	

    @Override
    public void drawScreen(int mouseX, int mouseY, float par3) {
        super.drawDefaultBackground();
        super.drawScreen(mouseX, mouseY, par3);
        setupOverlayDefaultScale();
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        GuiManager.getWindowManager().render();
        glDisable(GL_BLEND);
        mc.entityRenderer.setupOverlayRendering();
    }
    
    private void setupOverlayDefaultScale() {
        double scale = 2;
        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glLoadIdentity();
        GL11.glOrtho(0.0D, mc.displayWidth / scale, mc.displayHeight / scale, 0.0D, 1000.0D, 3000.0D);
        GL11.glMatrixMode(GL11.GL_MODELVIEW);
        GL11.glLoadIdentity();
        GL11.glTranslatef(0.0F, 0.0F, -2000.0F);
        GL11.glViewport(0, 0, mc.displayWidth, mc.displayHeight);
    }
    
    @Override
    protected void keyTyped(char c, int keyCode) {
        super.keyTyped(c, keyCode);
        GuiManager.getWindowManager().keyTyped(keyCode, c);
    }

    @Override
    protected void mouseClicked(int x, int y, int buttonId) {
        super.mouseClicked(x, y, buttonId);
        GuiManager.getWindowManager().mouseClicked((int) ((Mouse.getX() * (mc.displayWidth / 2) / mc.displayWidth)), (((mc.displayHeight / 2) - Mouse.getY() * (mc.displayHeight / 2) / mc.displayHeight)) - 1, buttonId);
    }
}
