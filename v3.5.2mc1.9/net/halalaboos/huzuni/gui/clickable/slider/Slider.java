package net.halalaboos.huzuni.gui.clickable.slider;

import net.halalaboos.huzuni.gui.clickable.DefaultComponent;
import net.halalaboos.huzuni.util.GLUtils;

public class Slider extends DefaultComponent {

	private float pointSize = 7F;

    private int pointPadding = 1;

    private String label;

    private String valueWatermark = "";

    private float minVal, maxVal, sliderPercentage, incrementVal;

    private boolean dragging = false;
    
	/**
     * @param dimensions
     */
    public Slider(String label, float minVal, float startVal, float maxVal) {
    	super();
    	this.label = label;
        this.minVal = minVal;
        this.maxVal = maxVal;
        this.sliderPercentage = (startVal - minVal) / (maxVal - minVal);
        this.incrementVal = -1F;
        this.setWidth(110F);
        this.setHeight(12F);
    }
    
    public Slider(String label, float minVal, float startVal, float maxVal, float incrementVal) {
        this(label, minVal, startVal, maxVal);
        this.incrementVal = incrementVal;
    }
    
    @Override
    public void update() {
    	super.update();
    	handleDragging();
        constrictSlider();
    }
    
    @Override
	public boolean mouseClicked(int x, int y, int buttonId) {
    	boolean mouseAround = super.mouseClicked(x, y, buttonId);
    	if (mouseAround && buttonId == 0)
    		dragging = true;
    	return mouseAround;
    }
    
    @Override
	public void mouseReleased(int x, int y, int buttonId) {
    	if (buttonId == 0) {
    		dragging = false;
    		this.invokeActionListeners();
    	}
    }
    
    private void handleDragging() {
        if (dragging) {
            //get the difference (in pixels) from the cursor and the beginning of the slider boundaries ( (mouse X) / (slider X) ) and subtract the width of the slider.
            float differenceWithMouseAndSliderBase = (float) (GLUtils.getMouseX() - (this.getX() + this.getOffsetX())) - (pointSize / 2F);
            //converted into 0.0F ~ 1.0F percentage
            sliderPercentage = differenceWithMouseAndSliderBase / getWidthForPoint();
        }
    }
    
    public float getValue() {
        float calculatedValue = (sliderPercentage * (maxVal - minVal));
        if (incrementVal == -1)
            return calculatedValue + minVal;
        return ((calculatedValue) - /* This is literally how much the calculated value is off from being incremented by the increment value. */((calculatedValue) % incrementVal)) + minVal;
    }
    
    private float getWidthForPoint() {
        float maxPointForRendering = (float) (this.getWidth() - pointSize - pointPadding),
                beginPoint = (pointPadding);
        return maxPointForRendering - beginPoint;
    }
    
    private void constrictSlider() {
        if (sliderPercentage < 0.0F)
            sliderPercentage = 0.0F;
        if (sliderPercentage > 1.0F)
            sliderPercentage = 1.0F;
    }
    
    private float getPositionForPoint() {
        return ((float) sliderPercentage * getWidthForPoint());
    }
    
    public float[] getSliderPoint() {
    	return new float[] {
    			getX() + getPositionForPoint() + pointPadding,
    			getY() + pointPadding,
    			pointSize,
    			getHeight() - pointPadding * 2
    	};
    }
    
    public void setValue(float value) {
        sliderPercentage = ((float) value - minVal) / (maxVal - minVal);
        constrictSlider();
    }

	public float getPointSize() {
		return pointSize;
	}

	public void setPointSize(float pointSize) {
		this.pointSize = pointSize;
	}

	public int getPointPadding() {
		return pointPadding;
	}

	public void setPointPadding(int pointPadding) {
		this.pointPadding = pointPadding;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getValueWatermark() {
		return valueWatermark;
	}

	public void setValueWatermark(String valueWatermark) {
		this.valueWatermark = valueWatermark;
	}

	public float getMinVal() {
		return minVal;
	}

	public void setMinVal(float minVal) {
		this.minVal = minVal;
	}

	public float getMaxVal() {
		return maxVal;
	}

	public void setMaxVal(float maxVal) {
		this.maxVal = maxVal;
	}

	public float getIncrementVal() {
		return incrementVal;
	}

	public void setIncrementVal(float incrementVal) {
		this.incrementVal = incrementVal;
	}

	public float getSliderPercentage() {
		return sliderPercentage;
	}

	public void setSliderPercentage(float sliderPercentage) {
		this.sliderPercentage = sliderPercentage;
	}
	
}
