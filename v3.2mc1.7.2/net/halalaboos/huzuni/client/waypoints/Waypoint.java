package net.halalaboos.huzuni.client.waypoints;

import net.halalaboos.huzuni.utils.RenderUtils;
import net.minecraft.client.Minecraft;

import java.awt.*;

public final class Waypoint {
    private final String server;
	private String name;
    private double x, y, z;
    private Color color;
    private boolean render = true;
    
    public Waypoint(String name, String server, double x, double y, double z, Color color) {
        this.name = name;
        this.x = x;
        this.y = y;
        this.z = z;
        this.color = color;
        this.server = server;
    }
    
    public Waypoint(String name, double x, double y, double z, Color color) {
    	this(name,  Minecraft.getMinecraft().isSingleplayer() ?  Minecraft.getMinecraft().getIntegratedServer().getWorldName() : Minecraft.getMinecraft().currentServerData.serverIP, x, y, z, color);
    }

    public Waypoint(String name, double x, double y, double z) {
    	this(name, x, y, z, RenderUtils.getRandomColor());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

	public boolean shouldRender() {
		return render;
	}

	public void setRender(boolean render) {
		this.render = render;
	}

	public String getServer() {
		return server;
	}

	public boolean isOnServer() {
		if (Minecraft.getMinecraft().isSingleplayer()) {
			return Minecraft.getMinecraft().getIntegratedServer().getWorldName().equals(server);
		} else 
			return Minecraft.getMinecraft().currentServerData.serverIP.equals(server);
	}
	
}
