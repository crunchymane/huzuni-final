package net.halalaboos.huzuni.mods.movement;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;

import net.halalaboos.huzuni.events.game.EventPreMotionUpdate;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.DefaultModule;
import net.minecraft.block.material.Material;

/**
 * Glide hack, works pretty well with NC+.
 *
 * @author Brendong
 * @since Jun 23, 2013
 */

public class Glide extends DefaultModule implements Listener {

    public Glide() {
        super("Glide", -1);
        setCategory(Category.MOVEMENT);
        setDescription("Glides you through the air.");
    }

    @Override
    protected void onEnable() {
        registry.registerListener(this);

    }

    @Override
    protected void onDisable() {
        registry.unregisterListener(this);
    }

    @Override
    protected void onToggle() {
    }

    @EventTarget
    public void onPreMotionUpdate(EventPreMotionUpdate event) {
        if (mc.thePlayer.motionY < 0 && mc.thePlayer.isAirBorne && !mc.thePlayer.isInWater() && !mc.thePlayer.isOnLadder()
                && !mc.thePlayer.isInsideOfMaterial(Material.lava)) {
            mc.thePlayer.motionY = -0.125f;
            mc.thePlayer.jumpMovementFactor *= 1.21337f;
        }
    }

}
