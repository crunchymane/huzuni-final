package net.halalaboos.huzuni.scripting;

import javax.script.ScriptException;

public class ScriptFunction {
	
	private final String functionName;
	
	public ScriptFunction(String functionName) {
		this.functionName = functionName;
	}
	
	public void invoke() {
		try {
			ScriptManager.invoke(functionName, (Object[]) null);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (ScriptException e) {
			e.printStackTrace();
		}
	}

	public String getFunctionName() {
		return functionName;
	}
	
}
