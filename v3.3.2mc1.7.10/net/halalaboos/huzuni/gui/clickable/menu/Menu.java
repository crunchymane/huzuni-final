package net.halalaboos.huzuni.gui.clickable.menu;

import net.halalaboos.huzuni.gui.clickable.DefaultContainer;
import net.halalaboos.huzuni.gui.clickable.FittedLayout;

public class Menu extends DefaultContainer {

	private String title;

	public Menu(String title) {
		super(new FittedLayout());
		this.title = title;
		if (!title.isEmpty())
			this.setOffset(new int[] { 0, 12 });
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
		if (title.isEmpty())
			this.setOffset(new int[] { 0, 0 });
		else
			this.setOffset(new int[] { 0, 12 });
	}

}
