package net.halalaboos.huzuni.util.threads;

import net.halalaboos.huzuni.Huzuni;
import net.minecraft.client.Minecraft;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import pw.brudin.huzuni.screen.MainMenu;

/**
 * @author brudin
 * @version 1.0
 * @since 12:38 PM on 8/20/2014
 */
public class ThreadReadChangelog extends Thread {

	private static List<String> localUpdateList;

	@Override
	public void run() {
		if (localUpdateList == null) {
			localUpdateList = new ArrayList<String>();
			try {
				final BufferedReader reader = new BufferedReader(new InputStreamReader(new URL("http://halalaboos.net/client/updates").openStream()));
				for (String s; (s = reader.readLine()) != null; )
					localUpdateList.add(s.trim());
				reader.close();
			} catch (IOException e) {
				localUpdateList.add("Could not connect to server.");
				localUpdateList.add(e.getMessage());
				e.printStackTrace();
			}
		}
		Huzuni.setUpdateList(localUpdateList);
		if (Minecraft.getMinecraft().currentScreen instanceof MainMenu) {
			((MainMenu) Minecraft.getMinecraft().currentScreen).updateChangelog();
		}
	}
}
