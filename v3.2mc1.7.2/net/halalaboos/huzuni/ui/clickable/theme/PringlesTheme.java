/**
 * 
 */
package net.halalaboos.huzuni.ui.clickable.theme;

import static org.lwjgl.opengl.GL11.*;
import java.awt.Color;
import java.awt.Point;
import java.awt.Rectangle;
import java.text.DecimalFormat;

import net.halalaboos.huzuni.utils.RenderUtils;
import net.halalaboos.lib.ui.Component;
import net.halalaboos.lib.ui.Container;
import net.halalaboos.lib.ui.components.Button;
import net.halalaboos.lib.ui.components.Dropdown;
import net.halalaboos.lib.ui.components.Slider;
import net.halalaboos.lib.ui.components.SlotComponent;
import net.halalaboos.lib.ui.components.TextField;
import net.halalaboos.lib.ui.containers.Window;
import net.halalaboos.lib.ui.theme.ComponentRenderer;
import net.halalaboos.lib.ui.theme.Theme;

/**
 * @author Halalaboos
 *
 * @since Oct 16, 2013
 */
public class PringlesTheme extends Theme {
	/*private final Color windowInside = new Color(17, 17, 17, 151),
	windowTop = new Color(15, 35, 120, 151),
	enabledColor = new Color(0, 86, 156, 48),
	disabledColor = new Color(0, 0, 0, 80),
	windowBorder = new Color(0, 0, 0, 255);
	
	public PringlesTheme() {
		super();
		registerRenderer(Button.class, new ComponentRenderer<Button>() {
			
			@Override
			public void render(Button button, Rectangle area,
					boolean mouseOver, boolean mouseDown) {
				renderButtonRect(area, button.isHighlight(), mouseOver, mouseDown);
				utils.getRenderer().drawStringWithShadow(button.getTitle(), area.x + 3, area.y + area.height / 2 -
                        utils.getRenderer().getStringHeight(button.getTitle()) / 2 + 1, button.isHighlight() ? 0xFF4292db : 0xFFFFFFFF);
			}
			
		});
		registerRenderer(TextField.class, new ComponentRenderer<TextField>() {

            @Override
            public void render(TextField textField, Rectangle area,
                               boolean mouseOver, boolean mouseDown) {
            	renderButtonRect(area, false, mouseOver || textField.isEnabled(), false);
                utils.getRenderer().drawStringWithShadow(textField.getTextForRender(area) + textField.getSelection(), area.x + 2, area.y + area.height / 2 - utils.getRenderer().getStringHeight(textField.getText()) / 2, textField.getTextColor().getRGB());
            }

        });
		registerRenderer(Slider.class, new ComponentRenderer<Slider>() {
			protected DecimalFormat formatter = new DecimalFormat("#.#");

			@Override
			public void render(Slider slider, Rectangle area,
					boolean mouseOver, boolean mouseDown) {
				Point mouse = new Point(utils.getMouseX(), utils.getMouseY());
				Rectangle sliderPoint = slider.getSliderPoint(area);
				
				// Inside
				renderButtonRect(area, false, mouseOver && !sliderPoint.contains(mouse), mouseDown);
				
				// Text
				utils.getRenderer().drawString(slider.getLabel(), area.x + 2, area.y + 2, 0xFFFFFF);
				String formattedValue = formatter.format(slider.getValue()) + slider.getValueWatermark();
				utils.getRenderer().drawString(formattedValue, area.x + area.width - utils.getRenderer().getStringWidth(formattedValue) - 2, area.y + 2, 0xFFFFFF);
				
				// Blue slider part
				renderButtonRect(sliderPoint, true, mouseOver && sliderPoint.contains(mouse), mouseDown);
			}
			
		});
		registerRenderer(Window.class, new ComponentRenderer<Window>() {

			@Override
			public void render(Window window, Rectangle area,
					boolean mouseOver, boolean mouseDown) {
				Point mouse = new Point(utils.getMouseX(), utils.getMouseY());
				Rectangle draggableDimensions = window.getDraggableArea(area);
				Rectangle dimensions = window.getArea();
				int barSize = window.getBarSize();
				int barPadding = window.getBorderPadding();
				
//        		drawBorderedRect(draggableDimensions, 2F, windowBorder.getRGB(), windowTop.getRGB());
                renderWindowRect((int)draggableDimensions.getX(), (int)draggableDimensions.getY(), (int)draggableDimensions.getMaxX(), (int)draggableDimensions.getMaxY(), 2F);
                drawGradientRect(draggableDimensions, 0x900000CC, 0x800066FF);
				if (!window.isMinimized()) {
	            	renderWindowRect(draggableDimensions.x, draggableDimensions.y + draggableDimensions.height, dimensions.x + dimensions.width, dimensions.y + dimensions.height, 2F);
				}
				utils.getRenderer().drawString(window.getTitle().toUpperCase(), draggableDimensions.x + 2, draggableDimensions.y +
                        (draggableDimensions.height / 2) - (utils.getRenderer().getStringHeight(window.getTitle().toUpperCase()) / 2), 0xFFFFFF);

				// Minimize button
				Rectangle minimizeDimensions = window.getMinimizeArea();
				renderButtonRect(minimizeDimensions, false, mouseOver && minimizeDimensions.contains(mouse), false);				
			}
			
		});
        registerRenderer(Dropdown.class, new ComponentRenderer<Dropdown>() {

            @Override
            public void render(Dropdown dropdown, Rectangle area,
                               boolean mouseOver, boolean mouseDown) {
            	renderButtonRect(area, false, mouseOver, mouseDown);
                utils.getRenderer().drawString(dropdown.getTitle() + " (" + dropdown.getSelectedComponentName() + ")", area.x + 2, area.y + area.height / 2 - utils.getRenderer().getStringHeight(dropdown.getTitle()) / 2 + 1, Color.WHITE.getRGB());
                if (dropdown.isDown()) {
                	String[] components = dropdown.getComponents();
        			int yPos = dropdown.getTextPadding() + 2;
                    for (int i = 0; i < components.length; i++) {
        				Rectangle componentBoundaries = new Rectangle(area.x + 1, area.y + yPos, area.width - 2, dropdown.getTextPadding());
        				boolean mouseOverComponent = componentBoundaries.contains(utils.getMouseX(), utils.getMouseY());
        				renderButtonRect(componentBoundaries, false,false,false);
        				utils.getRenderer().drawString(components[i], componentBoundaries.x + 2, componentBoundaries.y + 2, 0xFFFFFF);
        				yPos += dropdown.getTextPadding();
                	}
                }
            }

        });
        registerRenderer(SlotComponent.class, new ComponentRenderer<SlotComponent>() {

            @Override
            public void render(SlotComponent slotComponent, Rectangle area,
                               boolean mouseOver, boolean mouseDown) {
            	if (slotComponent.shouldRenderBackground()) {
            		renderButtonRect(area, false, false, false);	
            	}
                if (slotComponent.hasEnoughToScroll() && slotComponent.hasSlider()) {
                    Rectangle sliderPoint = slotComponent.getDraggableArea(area);
                	utils.getRenderer().setColor(slotComponent.getSliderColor());
                	renderButtonRect(sliderPoint, true, mouseOver, false);	
                }
                slotComponent.renderElements(area, mouseOver, mouseDown);
            }

        });
	}
	
	private void renderWindowRect(int x, int y, int x1, int y1, float lineWidth) {
		drawBorderedRect(x, y, x1, y1, lineWidth, 0xff0d0d0d, 0x802d2d2d);
	}
	
	private void renderWindowRect(Rectangle area, float lineWidth) {
		drawBorderedRect(area, lineWidth, windowBorder.getRGB(), windowInside.getRGB());
	}
	
	private void renderButtonRect(Rectangle area, boolean highlight, boolean mouseOver, boolean mouseDown) {
		renderButtonRect(area.x, area.y, area.x + area.width, area.y + area.height, highlight, mouseOver, mouseDown);
	}
	
	private void renderButtonRect(int x, int y, int x1, int y1, boolean highlight, boolean mouseOver, boolean mouseDown) {
		int inside = utils.getRenderer().getColorWithAffects(highlight ? enabledColor : disabledColor, mouseOver, mouseDown).getRGB();
		drawBorderedRect(x, y, x1, y1, 1F, 0xff0d0d0d, inside);
	}

	public static void drawRect(Rectangle rect, int color) {
		drawRect(rect.getX(), rect.getY(), rect.getMaxX(), rect.getMaxY(), color);
	}

	public static void drawBorderedRect(double x, double y, double x1, double y1, float lineWidth, int borderColor, int insideColor) {
		drawRect(x, y, x1, y1, insideColor);

		start2D();
		startSmooth();
		color(borderColor);
		glLineWidth(lineWidth);
		glBegin(GL_LINES);
		glVertex2d(x, y);
		glVertex2d(x, y1);
		glVertex2d(x1, y1);
		glVertex2d(x1, y);
		glVertex2d(x, y);
		glVertex2d(x1, y);
		glVertex2d(x, y1);
		glVertex2d(x1, y1);
		glEnd();
		end2D();
		endSmooth();
	}

    public static void drawGradientRect(Rectangle rect, int innerColor, int insideColor) {
        RenderUtils.drawGradientRect((float)rect.getX(), (float)rect.getY(), (float)rect.getMaxX(), (float)rect.getMaxY(), innerColor, insideColor);
    }

	public static void drawBorderedRect(Rectangle rect, float lineWidth, int borderColor, int insideColor) {
		drawBorderedRect(rect.getX(), rect.getY(), rect.getMaxX(), rect.getMaxY(), lineWidth, borderColor, insideColor);
	}

	public static void drawRect(double x, double y, double x1, double y1, int color) {
		start2D();
		startSmooth();
		glShadeModel(GL_SMOOTH);

		glBegin(GL_QUADS);
		color(color);
		glVertex2d(x1, y);
		glVertex2d(x, y);
		glVertex2d(x, y1);
		glVertex2d(x1, y1);
		glEnd();

		glShadeModel(GL_FLAT);
		endSmooth();
		end2D();
	}
	public static void start2D() {
		glPushMatrix();
		glDisable(GL_DEPTH_TEST);
		glEnable(GL_BLEND);
		glDisable(GL_TEXTURE_2D);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}

	public static void end2D() {
		glEnable(GL_TEXTURE_2D);
		glDisable(GL_BLEND);
		glEnable(GL_DEPTH_TEST);
		glPopMatrix();
	}

	public static void startSmooth() {
		glEnable(GL_LINE_SMOOTH);
		glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	}

	public static void endSmooth() {
		glDisable(GL_LINE_SMOOTH);
	}
	public static void color(int color) {
		float red = (float)(color >> 16 & 255) / 255.0F;
		float green = (float)(color >> 8 & 255) / 255.0F;
		float blue = (float)(color & 255) / 255.0F;
		float alpha = (float)(color >> 24 & 255) / 255.0F;
		glColor4f(red, green, blue, alpha);
	}*/
	
	/**
	 * @see net.halalaboos.lib.ui.theme.Theme#renderToolTip(net.halalaboos.lib.ui.Component, java.lang.String, java.awt.Point)
	 */
	@Override
	public void renderToolTip(String tooltip, Point mouse) {
	}

	/**
	 * @see net.halalaboos.lib.ui.theme.Theme#renderContainer(net.halalaboos.lib.ui.Container, java.awt.Point)
	 */
	@Override
	public void renderContainer(Container container, Point mouse) {
	}

}
