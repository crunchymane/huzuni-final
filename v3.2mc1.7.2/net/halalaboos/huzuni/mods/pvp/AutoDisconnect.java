/**
 * 
 */
package net.halalaboos.huzuni.mods.pvp;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;

import net.halalaboos.huzuni.events.game.EventHealthChange;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.DefaultModule;

/**
 * @author Halalaboos
 *
 * @since Sep 11, 2013
 */
public class AutoDisconnect extends DefaultModule implements Listener {

	/**
	 * @param name
	 */
	public AutoDisconnect() {
		super("Auto Disconnect");
		setDescription("Automatically disconnects at 3 hearts.");
		setCategory(Category.PVP);
	}

	/**
	 * @see net.halalaboos.huzuni.mods.DefaultModule#onToggle()
	 */
	@Override
	protected void onToggle() {
	}

	/**
	 * @see net.halalaboos.huzuni.mods.Module#onEnable()
	 */
	@Override
	protected void onEnable() {
		registry.registerListener(this);
	}

	/**
	 * @see net.halalaboos.huzuni.mods.Module#onDisable()
	 */
	@Override
	protected void onDisable() {
		registry.unregisterListener(this);
	}
	
	@EventTarget
	public void onHealthChange(EventHealthChange event) {
		if (event.health <= 6) {
			mc.thePlayer.sendChatMessage("\2473Bye");
            //mc.loadWorld((WorldClient)null);
            setEnabled(false);
		}
	}

}
