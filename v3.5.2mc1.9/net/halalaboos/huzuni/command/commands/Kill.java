package net.halalaboos.huzuni.command.commands;

import net.halalaboos.huzuni.api.console.Command;
import net.minecraft.client.Minecraft;

public class Kill implements Command {

	@Override
	public String[] getAliases() {
		return new String[] { "kill" };
	}

	@Override
	public String[] getHelp() {
		return new String[] { "Just jump! If it doesn't work, the server doesn't have nocheat." };
	}

	@Override
	public String getDescription() {
		return "Kills you once you jump. (Nocheat+ dependant)";
	}

	@Override
	public void run(String input, String[] args) {
		new Thread(){
            public void run(){
                try{
                    Minecraft.getMinecraft().thePlayer.jump();
                    Thread.sleep(150L);
					Minecraft.getMinecraft().thePlayer.motionY = -999;
					Minecraft.getMinecraft().thePlayer.noClip = true;
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }.start();
	}

}
