package net.halalaboos.huzuni.gui;

import java.awt.Color;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.gui.Component;
import net.halalaboos.huzuni.api.gui.event.ActionListener;
import net.halalaboos.huzuni.gui.clickable.button.Button;
import net.halalaboos.huzuni.gui.clickable.slider.Slider;
import net.halalaboos.huzuni.gui.clickable.window.Window;
import net.halalaboos.huzuni.util.GLUtils;

public class ColorWindow extends Window implements ActionListener <Component> {

	private final Slider red = new Slider("Red", 0, 255, 255, 1), green = new Slider("Green", 0, 167, 255, 1), blue = new Slider("Blue", 0, 0, 255, 1);
	
	public ColorWindow() {
		super("Color");
		setup();
		layout();
	}

	private void setup() {
		add(red);
		red.setValue(Huzuni.getColorTheme().getRed());
		red.addActionListener(this);
		add(green);
		green.setValue(Huzuni.getColorTheme().getGreen());
		green.addActionListener(this);
		add(blue);
		blue.setValue(Huzuni.getColorTheme().getBlue());
		blue.addActionListener(this);
		Button random = new Button("Random");
		random.addActionListener(this);
		add(random);
		Button reset = new Button("Reset");
		reset.addActionListener(this);
		add(reset);
	}

	@Override
	public void onAction(Component component) {
		if (component instanceof Slider)
			Huzuni.setColorTheme(new Color((int) red.getValue(), (int) green.getValue(), (int) blue.getValue(), 167));
		else if (component instanceof Button) {
			if (((Button) component).getTitle().equals("Reset")) {
				Huzuni.setDefaultColorTheme();
				red.setValue(Huzuni.getColorTheme().getRed());
				green.setValue(Huzuni.getColorTheme().getGreen());
				blue.setValue(Huzuni.getColorTheme().getBlue());
			} else {
				Color random = GLUtils.getRandomColor();
				red.setValue(random.getRed());
				green.setValue(random.getGreen());
				blue.setValue(random.getBlue());
				Huzuni.setColorTheme(new Color((int) red.getValue(), (int) green.getValue(), (int) blue.getValue(), 167));
			}
		}
		
	}

}
