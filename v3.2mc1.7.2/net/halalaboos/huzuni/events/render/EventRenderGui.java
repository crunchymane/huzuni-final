/**
 *
 */
package net.halalaboos.huzuni.events.render;

import com.darkmagician6.eventapi.events.Event;

/**
 * @author Halalaboos
 * @since Jul 17, 2013
 */
public class EventRenderGui implements Event {

    public final float partialTicks;

    /**
     * @param partialTicks
     */
    public EventRenderGui(float partialTicks) {
        this.partialTicks = partialTicks;
    }

}
