package net.halalaboos.huzuni.gui.ingame.themes;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.gui.ingame.IngameTheme;
import net.halalaboos.huzuni.mod.DefaultMod;
import net.halalaboos.huzuni.mod.ModManager;
import net.halalaboos.huzuni.util.GLUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.util.StringUtils;

public class Skidded implements IngameTheme {
    int width, height;

    @Override
    public void render(Minecraft mc, int screenWidth, int screenHeight) {
        mc.fontRenderer.drawStringWithShadow(Huzuni.TITLE + " v" + Huzuni.VERSION, 2, 2, 0xFFFF);
        GLUtils.drawRect(1, 1, width, height, 0xAF424242);
        height = 12;
        width = 0;

        for (DefaultMod mod : ModManager.getDefaultMods()) {
            if (mod.isBound()) {
                String renderString = mod.getKeyName() + " = \247" + (mod.isEnabled() ? "2" : "4") + mod.getName() + "\247f = " + (mod.isEnabled() ? "on" : "off");
                mc.fontRenderer.drawStringWithShadow(renderString, 2, height, 0xFFFF);
                height += renderString.length() > 20 ? 12 : 10; // for dat fancy uneven spacing mmmmm

                int stringWidth = mc.fontRenderer.getStringWidth(StringUtils.stripControlCodes(renderString)) + 4;
                if (stringWidth > width)
                    width = stringWidth;
            }
        }
    }

    @Override
    public String getName() {
        return "Skidded";
    }

    @Override
    public void onKeyTyped(int keyCode) {
    }

}
