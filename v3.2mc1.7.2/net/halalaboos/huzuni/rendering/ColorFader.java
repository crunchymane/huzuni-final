package net.halalaboos.huzuni.rendering;

import java.awt.*;
import java.util.Random;

import net.halalaboos.huzuni.utils.Timer;

/**
 * User: Stl
 * Date: 7/11/13
 * Time: 12:48 AM
 * Use:
 */
public class ColorFader {

    private boolean firstRun = true;
    private int thisColor = 0xffffff, lastColor = 0xffffff;
    private Random rng = new Random();
    private Timer timer = new Timer();
    private long delay = 1000L;
    private int cur;

    public ColorFader() {
    }

    public void update() {
        if (firstRun) {
            thisColor = generateHex();
            lastColor = generateHex();
            firstRun = false;
            timer.reset();
        }
        if (timer.hasReach((int) delay)) {
            lastColor = thisColor;
            thisColor = generateHex();
            timer.reset();
        }
        cur = interpolate(lastColor, thisColor, ((float) (timer.getTimePassed()) / (float) delay));
    }

    public Color toColor(int alpha) {
        return new Color((cur >> 16 & 0xFF), (cur >> 8 & 0xFF), (cur & 0xFF), alpha);
    }

    public float getCurRed() {
        return (cur >> 16 & 0xFF) / 255.0F;
    }

    public float getCurGreen() {
        return (cur >> 8 & 0xFF) / 255.0F;
    }

    public float getCurBlue() {
        return (cur & 0xFF) / 255.0F;
    }

    private int generateHex() {
        int r = rng.nextInt(0xFF);
        int g = rng.nextInt(0xFF);
        int b = rng.nextInt(0xFF);
        return r << 16 | g << 8 | b;
    }

    private int interpolate(int rgba1, int rgba2, float percent) {
        int a1 = (rgba1 >> 24) & 255, r1 = (rgba1 >> 16) & 255, g1 = (rgba1 >> 8) & 255, b1 = rgba1 & 255;
        int a2 = (rgba2 >> 24) & 255, r2 = (rgba2 >> 16) & 255, g2 = (rgba2 >> 8) & 255, b2 = rgba2 & 255;
        int r = (int) (r1 + (r2 - r1) * percent);
        int g = (int) (g1 + (g2 - g1) * percent);
        int b = (int) (b1 + (b2 - b1) * percent);
        int a = (int) (a1 + (a2 - a1) * percent);

        return a << 24 | r << 16 | g << 8 | b;
    }

}