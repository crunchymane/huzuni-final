/**
 * 
 */
package net.halalaboos.huzuni.mods;

import net.halalaboos.huzuni.client.Settings;

/**
 * @author Halalaboos
 *
 */
public abstract class VipModule extends Module {

	/**
	 * @param name
	 * @param author
	 */
	public VipModule(String name, String author) {
		super(name, author);
		
	}

	@Override
	public void setEnabled(boolean enabled) {
		if (Settings.isVip()) {
			super.setEnabled(enabled);
		} else {
			huzuni.addChatMessage("You must be VIP to use this!");
		}
	}

}
