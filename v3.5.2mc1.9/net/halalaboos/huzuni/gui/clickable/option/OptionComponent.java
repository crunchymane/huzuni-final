package net.halalaboos.huzuni.gui.clickable.option;

import net.halalaboos.huzuni.api.gui.render.Theme;
import net.halalaboos.huzuni.api.mod.Option;
import net.halalaboos.huzuni.gui.clickable.DefaultComponent;
import net.halalaboos.huzuni.util.GLUtils;
import net.halalaboos.huzuni.util.MathUtils;

public class OptionComponent extends DefaultComponent {
	
	private Option option;
	
	public OptionComponent(Option option) {
		this.option = option;
	}
	
	@Override
	public boolean mouseClicked(int x, int y, int buttonId) {
		float[] componentBoundaries = new float[] { this.getX() + this.getOffsetX(), this.getY() + this.getOffsetY(), this.getWidth(), option.getHeight() };
		if (MathUtils.inside(x, y, componentBoundaries)) {
			option.mouseClicked(x, y, buttonId);
			return true;
		} else
			return false;
	}
	
	@Override
	public void mouseReleased(int x, int y, int buttonId) {
		option.mouseReleased(x, y, buttonId);
		super.mouseReleased(x, y, buttonId);
	}
	
	public void renderComponents(Theme theme, float offsetX, float offsetY, boolean mouseOver, boolean mouseDown) {
		float[] mouse = new float[] { GLUtils.getMouseX(), GLUtils.getMouseY() };
		this.setHeight(option.getHeight());
		float[] componentArea = new float[] { getX() + offsetX, getY() + offsetY, getWidth(), getHeight() };
		option.setArea(componentArea);
		option.render(theme, 0, componentArea, (mouseOver && MathUtils.inside(mouse[0], mouse[1], componentArea)), mouseDown);
	}
	
	@Override
	public String getTooltip() {
		return option.getDescription();
	}
	
	@Override
	public float getHeight() {
		return option.getHeight();
	}
	
}
