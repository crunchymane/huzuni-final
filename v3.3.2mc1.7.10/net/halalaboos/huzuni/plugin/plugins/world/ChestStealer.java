package net.halalaboos.huzuni.plugin.plugins.world;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.plugin.Category;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent;
import net.halalaboos.huzuni.event.tick.UpdateListener;
import net.halalaboos.huzuni.plugin.VipPlugin;
import net.halalaboos.huzuni.util.Timer;
import net.minecraft.client.gui.inventory.GuiChest;
import net.minecraft.inventory.ContainerChest;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;

public class ChestStealer extends VipPlugin implements UpdateListener {
	
	private final Timer timer = new Timer();
	
	private GuiChest guiChest;
	
	private IInventory chest;
	
	private int windowId, index;

	public ChestStealer() {
		super("Chest Stealer");
		setCategory(Category.WORLD);
		setDescription("Automatically place down a sign with text.");
	}

	@Override
	protected void onEnable() {
		Huzuni.registerUpdateListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterUpdateListener(this);
	}

	@Override
	public void onPreUpdate(MotionUpdateEvent event) {
		if (guiChest != null && chest != null) {
			if (timer.hasReach(125)) {
				for (; index < chest.getSizeInventory(); index++) {
					ItemStack item = chest.getStackInSlot(index);
					if (item == null)
						continue;
					clickSlot(windowId, index, true);
					timer.reset();
					index++;
					return;
				}
				mc.displayGuiScreen(null);
				chest = null;
				guiChest = null;
			}
		} else {
			if (mc.currentScreen instanceof GuiChest) {
				guiChest = (GuiChest) mc.currentScreen;
				chest = ((ContainerChest) guiChest.field_147002_h).getLowerChestInventory();
				index = 0;
				windowId = ((ContainerChest) guiChest.field_147002_h).windowId;
			}
		}
	}
	
	private void clickSlot(int windowId, int slot, boolean shiftClick) {
        mc.playerController.windowClick(windowId, slot, 0, shiftClick ? 1 : 0, mc.thePlayer);
    }

	@Override
	public void onPostUpdate(MotionUpdateEvent event) {
	}
	
}
