/**
 *
 */
package net.halalaboos.huzuni.console.commands;

import net.halalaboos.huzuni.console.Command;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author Halalaboos
 * @since Aug 17, 2013
 */
public class SanjPakalou implements Command {
    private final Random random = new Random();
    private static final String PAKALOU = "https://twitter.com/sanjpakalou";
    private List<String> quotes = null;

    /**
     * @see net.halalaboos.huzuni.console.Command#getAliases()
     */
    @Override
    public String[] getAliases() {
        return new String[] {"pakalou"};
    }

    /**
     * @see net.halalaboos.huzuni.console.Command#getHelp()
     */
    @Override
    public String[] getHelp() {
        return new String[] {"pakalou"};
    }

    /**
     * @see net.halalaboos.huzuni.console.Command#getDescription()
     */
    @Override
    public String getDescription() {
        return "Quotes the god known as Sanj Pakalou directly from his twitter URL.";
    }

    private void downloadPakalou() {
        huzuni.addChatMessage("Downloading Pakalou quotes..");
        quotes = new ArrayList<String>();
        try {
            URL url = new URL(PAKALOU);
            BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
            for (String s; (s = reader.readLine()) != null; ) {
                if (s.contains("<p class=\"js-tweet-text tweet-text\">")) {
                    String quote = s.split("<p class=\"js-tweet-text tweet-text\">")[1].split("</p>")[0];
                    quotes.add(quote);
                }
            }
            huzuni.addChatMessage(quotes.size() + " quotes loaded.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @see net.halalaboos.huzuni.console.Command#run(java.lang.String, java.lang.String[])
     */
    @Override
    public void run(String input, String[] args) {
        if (quotes == null)
            downloadPakalou();
        if (!quotes.isEmpty()) {
            String quote = quotes.get((int) (random.nextFloat() * (quotes.size() - 1)));
            mc.thePlayer.sendChatMessage(quote);
        }
    }

}
