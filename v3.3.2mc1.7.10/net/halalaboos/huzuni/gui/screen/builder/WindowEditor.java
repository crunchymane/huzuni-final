package net.halalaboos.huzuni.gui.screen.builder;

import java.util.List;

import net.halalaboos.huzuni.api.gui.Component;
import net.halalaboos.huzuni.api.gui.event.ActionListener;
import net.halalaboos.huzuni.api.plugin.Plugin;
import net.halalaboos.huzuni.gui.clickable.DefaultLayout;
import net.halalaboos.huzuni.gui.clickable.button.Button;
import net.halalaboos.huzuni.gui.clickable.button.PluginButton;
import net.halalaboos.huzuni.gui.clickable.dropdown.Dropdown;
import net.halalaboos.huzuni.gui.clickable.label.Label;
import net.halalaboos.huzuni.gui.clickable.menu.Menu;
import net.halalaboos.huzuni.gui.clickable.slider.ValueSlider;
import net.halalaboos.huzuni.gui.clickable.textfield.TextField;
import net.halalaboos.huzuni.gui.clickable.window.Window;
import net.halalaboos.huzuni.gui.screen.MenuScreen;
import net.halalaboos.huzuni.plugin.PluginManager;
import net.halalaboos.huzuni.plugin.value.Value;
import net.halalaboos.huzuni.plugin.value.ValueManager;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;

public class WindowEditor extends MenuScreen implements ActionListener {

	private final Window window;
	
	private final GuiScreen lastScreen;
	
	private final ComponentSlot componentSlot = new ComponentSlot();
	
	private Button addMod, addValue;
	
	private Dropdown modDropdown, valueDropdown;
	
	public WindowEditor(Window window, GuiScreen lastScreen) {
		super(window.getTitle());
		this.window = window;
		this.lastScreen = lastScreen;
	}

	@Override
	protected void setup(Menu menu) {
		menu.clear();
		menu.setLayout(new DefaultLayout());
		menu.add(new Label("Name:"));
		TextField nameField = new TextField(window.getTitle());
		nameField.addActionListener(this);
		menu.add(nameField);
		
		menu.add(new Label(""));
		
		menu.add(new Label("Mods"));
		modDropdown = new Dropdown("");
		modDropdown.setSelectedComponent(0);
		modDropdown.setComponents(getPlugins());
		menu.add(modDropdown);
		addMod = new Button("Add");
		addMod.addActionListener(this);
		menu.add(addMod);
		
		menu.add(new Label(""));
		
		menu.add(new Label("Values"));
		valueDropdown = new Dropdown("");
		valueDropdown.setSelectedComponent(0);
		valueDropdown.setComponents(getValues());
		menu.add(valueDropdown);
		addValue = new Button("Add");
		addValue.addActionListener(this);
		menu.add(addValue);
		// menu.add(componentSlot);
		// componentSlot.setComponents(window.getComponents());
		buttonList.add(new GuiButton(0, this.width / 2 - 100, this.height - 28, "Done"));
	}
	
	@Override
    protected void actionPerformed(GuiButton button) {
        switch (button.id) {
            case 0:
                mc.displayGuiScreen(lastScreen);
                break;
        }
	}
	
	private String[] getPlugins() {
		List<Plugin> plugins = PluginManager.getPlugins();
		String[] names = new String[plugins.size()];
		for (int i = 0; i < plugins.size(); i++)
			names[i] = plugins.get(i).getName();
		return names;
	}
	
	private String[] getValues() {
		List<Value> values = ValueManager.getValues();
		String[] names = new String[values.size()];
		for (int i = 0; i < values.size(); i++)
			names[i] = values.get(i).getName();
		return names;
	}

	@Override
	public void onAction(Component component) {
		if (component == addMod) {
			window.add(new PluginButton(PluginManager.getPlugin(modDropdown.getSelectedComponentName())));
			window.layout();
		} else if (component == addValue) {
			window.add(new ValueSlider(ValueManager.getValue(valueDropdown.getSelectedComponentName())));
			window.layout();
		} else if (component instanceof TextField) {
			TextField textField = (TextField) component;
			if (!textField.getText().isEmpty())
				window.setTitle(textField.getText());
		}
	}
}
