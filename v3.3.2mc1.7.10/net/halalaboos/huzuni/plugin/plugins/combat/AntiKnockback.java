package net.halalaboos.huzuni.plugin.plugins.combat;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.plugin.Category;
import net.halalaboos.huzuni.event.packet.PacketEvent;
import net.halalaboos.huzuni.event.packet.PacketListener;
import net.halalaboos.huzuni.plugin.DefaultPlugin;
import net.minecraft.network.play.server.S12PacketEntityVelocity;

/**
 * @author brudin
 * @version 1.0
 * @since 3/25/14
 */
public class AntiKnockback extends DefaultPlugin implements PacketListener {

	public static final AntiKnockback instance = new AntiKnockback();
	
	private AntiKnockback() {
		super("Anti Knockback");
		setAuthor("brudin");
		setDescription("Stops you from receiving knockback velocity.");
		setCategory(Category.COMBAT);
		setVisible(false);
	}

	@Override
	public void onPacket(PacketEvent event, PacketEvent.PacketType type) {
		if(event.getPacket() instanceof S12PacketEntityVelocity) {
			event.setCancelled(true);
		}
	}

	@Override
	protected void onEnable() {
		Huzuni.registerPacketListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterPacketListener(this);
	}

	public boolean isPushedByWater() {
		return false;
	}
}
