package net.halalaboos.huzuni.events.game;

import com.darkmagician6.eventapi.events.Event;

public class EventRecieveVelocity implements Event {

	private double velocityX, velocityY, velocityZ;

	public EventRecieveVelocity(double velocityX, double velocityY,
			double velocityZ) {
		this.velocityX = velocityX;
		this.velocityY = velocityY;
		this.velocityZ = velocityZ;
	}

	public double getVelocityX() {
		return velocityX;
	}

	public void setVelocityX(double velocityX) {
		this.velocityX = velocityX;
	}

	public double getVelocityY() {
		return velocityY;
	}

	public void setVelocityY(double velocityY) {
		this.velocityY = velocityY;
	}

	public double getVelocityZ() {
		return velocityZ;
	}

	public void setVelocityZ(double velocityZ) {
		this.velocityZ = velocityZ;
	}
	
	
}
