package net.halalaboos.huzuni.gui.screen;

import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glEnable;
import net.halalaboos.huzuni.gui.clickable.ContainerManager;
import net.halalaboos.huzuni.gui.clickable.menu.Menu;
import net.halalaboos.huzuni.gui.clickable.theme.HuzuniTheme;
import net.minecraft.client.gui.GuiScreen;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

public abstract class MenuScreen extends GuiScreen {

	private final ContainerManager<Menu> menuManager = new ContainerManager<Menu>(new HuzuniTheme());
	
	private final Menu menu;
	
	private final String menuName;
	
	public MenuScreen(String menuName) {
		super();
		this.menu = new Menu("");
		this.menuName = menuName;
		menuManager.add(menu);
	}
	
	@Override
    public void initGui() {
    	Keyboard.enableRepeatEvents(true);
    	int slotWidth = 300, slotHeight = (height - 114);
        menu.setX(this.width / 2 - slotWidth / 2);
        menu.setY(30);
        menu.setWidth(slotWidth);
        menu.setHeight(slotHeight);
        setup(menu);
        menu.layout();
	}
	
	protected abstract void setup(Menu menu);
	
	@Override
	public void onGuiClosed() {
    	Keyboard.enableRepeatEvents(false);
	}
	

    @Override
    public void drawScreen(int mouseX, int mouseY, float delta) {
        super.drawDefaultBackground();
        super.drawScreen(mouseX, mouseY, delta);
        setupOverlayDefaultScale();
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        menuManager.render();
        glDisable(GL_BLEND);
        mc.entityRenderer.setupOverlayRendering();
        drawCenteredString(this.fontRendererObj, menuName, width / 2, 16, 0xFFFFFF);
    }
    
    private void setupOverlayDefaultScale() {
        double scale = 2;
        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glLoadIdentity();
        GL11.glOrtho(0.0D, mc.displayWidth / scale, mc.displayHeight / scale, 0.0D, 1000.0D, 3000.0D);
        GL11.glMatrixMode(GL11.GL_MODELVIEW);
        GL11.glLoadIdentity();
        GL11.glTranslatef(0.0F, 0.0F, -2000.0F);
        GL11.glViewport(0, 0, mc.displayWidth, mc.displayHeight);
    }
    
    @Override
    protected void keyTyped(char c, int keyCode) {
        super.keyTyped(c, keyCode);
        menuManager.keyTyped(keyCode, c);
    }

    @Override
    protected void mouseClicked(int x, int y, int buttonId) {
        super.mouseClicked(x, y, buttonId);
        menuManager.mouseClicked((int) ((Mouse.getX() * (mc.displayWidth / 2) / mc.displayWidth)), (((mc.displayHeight / 2) - Mouse.getY() * (mc.displayHeight / 2) / mc.displayHeight)) - 1, buttonId);
    }
}
