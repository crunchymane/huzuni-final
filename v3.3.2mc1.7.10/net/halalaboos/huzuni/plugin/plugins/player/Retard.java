package net.halalaboos.huzuni.plugin.plugins.player;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.plugin.ModeOption;
import net.halalaboos.huzuni.api.plugin.Category;
import net.halalaboos.huzuni.event.packet.PacketEvent;
import net.halalaboos.huzuni.event.packet.PacketListener;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent;
import net.halalaboos.huzuni.event.tick.UpdateListener;
import net.halalaboos.huzuni.plugin.DefaultPlugin;
import net.halalaboos.huzuni.plugin.value.ValueManager;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.network.Packet;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;

import java.util.Random;

/**
 * @author brudin
 * @version 1.0
 * @since 4/10/14
 */
public class Retard extends DefaultPlugin implements UpdateListener, PacketListener {

	private final Random random = new Random();

	/**
	 * oh my god go in twerk mode and go in third person
	 * the default skin is staring into your eyes as he furiously humps the air
	 */

	private ModeOption mode;
	
	public Retard() {
		super("Retard");
		setAuthor("Halalaboos");
		setDescription("Makes your player's head go crazy.");
		setCategory(Category.PLAYER);
		setOptions(mode = new ModeOption("Retard mode", "Normal", "Headless", "Twerk"));

	}

	@Override
	protected void onEnable() {
		Huzuni.registerPacketListener(this);
		Huzuni.registerUpdateListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterPacketListener(this);
		Huzuni.unregisterUpdateListener(this);
	}

	@Override
	public void onPreUpdate(MotionUpdateEvent event) {
		float yaw = mc.thePlayer.rotationYaw;
		float pitch = mc.thePlayer.rotationPitch;
		
		if (mode.getSelected() == 0) {
			yaw = random.nextBoolean() ? random.nextInt(180)  : -random.nextInt(180);
			pitch = random.nextBoolean() ? random.nextInt(90)  : -random.nextInt(90);
		}

		if(mode.getSelected() == 2) {
			if(mc.gameSettings.keyBindSneak.getIsKeyPressed()) {
				KeyBinding.setKeyBindState(mc.gameSettings.keyBindSneak.getKeyCode(), false);
			} else {
				KeyBinding.setKeyBindState(mc.gameSettings.keyBindSneak.getKeyCode(), true);
			}
		}

		mc.thePlayer.sendQueue.addToSendQueue(changeRotation(pitch, yaw));
		if ((!this.mc.thePlayer.isSprinting())  && (!this.mc.thePlayer.isUsingItem() || !this.mc.thePlayer.isSwingInProgress || mc.thePlayer.swingProgress < 0)) {
			if ((!this.mc.thePlayer.isSwingInProgress) && (random.nextBoolean())) {
				this.mc.thePlayer.swingItem();
			}
		}
	}

	@Override
	public void onPostUpdate(MotionUpdateEvent event) {}

	@Override
	public void onPacket(PacketEvent event, PacketEvent.PacketType type) {
		if(mode.getSelected() == 1) {
			if(type == PacketEvent.PacketType.SENT) {
				Packet packet = event.getPacket();
				if(packet instanceof C08PacketPlayerBlockPlacement || packet instanceof
						C07PacketPlayerDigging) {
					event.sendPacket(normalHead());
				} else {
					if(packet instanceof C03PacketPlayer.C05PacketPlayerLook) {
						event.setPacket(headInbody());
					} else if(packet instanceof C03PacketPlayer.C06PacketPlayerPosLook) {
						event.setPacket(headInbody());
					}
				}
			}
		}
	}

	private Packet headInbody() {
		return new C03PacketPlayer.C06PacketPlayerPosLook(mc
				.thePlayer.posX, mc.thePlayer.boundingBox.minY, mc.thePlayer.posY,
				mc.thePlayer.posZ, mc.thePlayer.rotationYaw, 180, mc.thePlayer.onGround);
	}

	private Packet normalHead() {
		return new C03PacketPlayer.C06PacketPlayerPosLook(mc
				.thePlayer.posX, mc.thePlayer.boundingBox.minY, mc.thePlayer.posY,
				mc.thePlayer.posZ, mc.thePlayer.rotationYaw, mc.thePlayer.rotationPitch,
				mc.thePlayer.onGround);
	}

	private Packet changeRotation(float pitch, float yaw) {
		return new C03PacketPlayer.C06PacketPlayerPosLook(mc
				.thePlayer.posX, mc.thePlayer.boundingBox.minY, mc.thePlayer.posY,
				mc.thePlayer.posZ, yaw, pitch,
				mc.thePlayer.onGround);
	}
}
