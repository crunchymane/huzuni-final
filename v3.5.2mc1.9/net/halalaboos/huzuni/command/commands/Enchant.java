package net.halalaboos.huzuni.command.commands;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.console.Command;
import net.halalaboos.huzuni.util.GameUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.I18n;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagList;

import java.util.ArrayList;

/**
 * @author brudin
 * @version 1.0
 * @since 4/14/14
 */
public class Enchant implements Command {

	@Override
	public String[] getAliases() {
		return new String[] { "enchant", "enc"};
	}

	@Override
	public String[] getHelp() {
		return new String[] { "enchant" };
	}

	@Override
	public String getDescription() {
		return "Applies every enchant to your held item.";
	}

	@Override
	public void run(String input, String[] args) {
		if(Minecraft.getMinecraft().playerController.isInCreativeMode()) {
			if(Minecraft.getMinecraft().thePlayer.getHeldItemMainhand() != null) {
				if(args == null) {
						GameUtils.addEnchants(Enchantment.enchantmentRegistry, (short)127, Minecraft.getMinecraft().thePlayer.getHeldItemMainhand());
						Minecraft.getMinecraft().playerController.updateController();
					Huzuni.addChatMessage("Enchantments added.");
				} else {
					String enchantName = args[0];
					int enchantLevel = Integer.parseInt(args[1]);
					for(Enchantment enchantment : Enchantment.enchantmentRegistry) {
						if(I18n.format(enchantment.getName()).equalsIgnoreCase(enchantName.replaceAll("_", " "))) {
							String translateID = "enchantment.level.";
							GameUtils.addEnchant(enchantment, (short)enchantLevel, Minecraft.getMinecraft().thePlayer.getHeldItemMainhand());
							Minecraft.getMinecraft().playerController.updateController();
							Huzuni.addChatMessage(String.format("Enchantment %s added.", enchantment.getTranslatedName(enchantLevel).replaceAll(translateID, "")));
						}
					}
				}
			}
		} else {
			Huzuni.addChatMessage("Must be in creative.");
		}
	}
}
