/**
 *
 */
package net.halalaboos.huzuni.util;

import static org.lwjgl.opengl.GL11.*;

import java.awt.*;
import java.util.Random;

import net.halalaboos.huzuni.Huzuni;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.VertexBuffer;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.item.ItemStack;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL32;

/**
 * @author Halalaboos
 * @since Aug 18, 2013
 */
public final class GLUtils {
	
	private static final Minecraft mc = Minecraft.getMinecraft();
	
	private static final Random random = new Random();
	
	public static final GLUtils instance = new GLUtils();

    private GLUtils() {
    	
    }

	/**
	 * Enables GL constants for 3D rendering.
	 */
	public static void enableGL3D() {
		glEnable(GL32.GL_DEPTH_CLAMP);
		glDisable(GL_ALPHA_TEST);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glDisable(GL_TEXTURE_2D);
		glDisable(GL_DEPTH_TEST);
		glDepthMask(false);
		if (Huzuni.isAntialias())
			glEnable(GL_LINE_SMOOTH);
		glLineWidth(Huzuni.getLineSize());
	}

	/**
	 * Disables GL constants for 3D rendering.
	 */
	public static void disableGL3D() {
		glEnable(GL_TEXTURE_2D);
		glEnable(GL_DEPTH_TEST);
		glDisable(GL_BLEND);
		glEnable(GL_ALPHA_TEST);
		glDepthMask(true);
		if (Huzuni.isAntialias())
			glDisable(GL_LINE_SMOOTH);
		glDisable(GL32.GL_DEPTH_CLAMP);
	}
	
	public static void enableLightmap() {
		Minecraft.getMinecraft().entityRenderer.enableLightmap();
	}
	
	public static void disableLightmap() {
		Minecraft.getMinecraft().entityRenderer.disableLightmap();
	}
	
	public void drawString(String text, int x, int y, int color) {
		Minecraft.getMinecraft().fontRenderer.drawStringWithShadow(text, x, y, color);
	}
	
	public int getStringWidth(String text) {
		return Minecraft.getMinecraft().fontRenderer.getStringWidth(text);
	}
	
	public int getStringHeight(String text) {
		return Minecraft.getMinecraft().fontRenderer.FONT_HEIGHT;
	}

    public static int getMouseX() {
    	return (int) ((Mouse.getX() * getScreenWidth() / mc.displayWidth));
    }

    public static int getMouseY() {
    	return (int) ((getScreenHeight() - Mouse.getY() * getScreenHeight() / mc.displayHeight - 1));
    }

    public static int getScreenWidth() {
    	return mc.displayWidth / 2;
    }

    public static int getScreenHeight() {
    	return mc.displayHeight / 2;
    }

    public static Color getColorWithAffects(Color color, boolean mouseOver, boolean mouseDown) {
    	return mouseOver ? (mouseDown ? color.darker() : color.brighter()) : color;
    }
    
    public static void glColor(Color color) {
    	GlStateManager.color((float) color.getRed() / 255F, (float) color.getGreen() / 255F, (float) color.getBlue() / 255F, (float) color.getAlpha() / 255F);
    }
    
    public static void glColor(int hex, float alpha) {
         float red = (float) (hex >> 16 & 255) / 255.0F;
         float green = (float) (hex >> 8 & 255) / 255.0F;
         float blue = (float) (hex & 255) / 255.0F;
    	GlStateManager.color(red, green, blue, alpha);
    }
    
    public static void glColor(Color color, float alpha) {
    	GlStateManager.color((float) color.getRed() / 255F, (float) color.getGreen() / 255F, (float) color.getBlue() / 255F, alpha);
    }

    public static Color getRandomColor() {
        return getRandomColor(1000, 0.6F);
    }
    
    public static int getColor(Color color) {
    	return color.getAlpha() << 24 | color.getRed() << 16 | color.getGreen() << 8 | color.getBlue();
    }
    
    public static void glColor(int hex) {
        float alpha = (float) (hex >> 24 & 255) / 255.0F;
        float red = (float) (hex >> 16 & 255) / 255.0F;
        float green = (float) (hex >> 8 & 255) / 255.0F;
        float blue = (float) (hex & 255) / 255.0F;
        GlStateManager.color(red, green, blue, alpha);
    }
    
    public static void glColor(float red, float green, float blue, float alpha) {
    	GlStateManager.color(red, green, blue, alpha);
    }
    
    public static void drawRect(double x, double y, double x1, double y1, int color) {
        glColor(color);
        drawRect((double) x, (double) y, (double) x1, (double) y1);
    }
    
    public static void drawRect(double x, double y, double x1, double y1) {
    	glDisable(GL_TEXTURE_2D);
    	glEnable(GL_BLEND);
    	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    	Tessellator tessellator = Tessellator.getInstance();
    	VertexBuffer renderer = tessellator.getBuffer();
		renderer.begin(7, DefaultVertexFormats.POSITION);
    	renderer.pos(x, y1, 0F).endVertex();
    	renderer.pos(x1, y1, 0F).endVertex();
    	renderer.pos(x1, y, 0F).endVertex();
    	renderer.pos(x, y, 0F).endVertex();
    	tessellator.draw();
    	glEnable(GL_TEXTURE_2D);
    }

    public static void drawRect(float[] area) {
    	drawRect(area[0], area[1], area[0] + area[2], area[1] + area[3]);
    }
    
	public static void drawRect(Rectangle rectangle, int color) {
		drawRect(rectangle.x, rectangle.y, rectangle.x + rectangle.width, rectangle.y + rectangle.height, color);
	}

	/**
     * This isn't mine, but it's the most beautiful random color generator I've seen. Reminds me of easter.
     * */
    public static Color getRandomColor(int saturationRandom, float luminance) {
        final float hue = random.nextFloat();
        final float saturation = (random.nextInt(saturationRandom) + 1000) / (float) saturationRandom + 1000F;
        return Color.getHSBColor(hue, saturation, luminance);
    }
	
	public static void drawTexturedRect(float x, float y, float width, float height, float u, float v, float t, float s) {
		glBegin(GL_TRIANGLES);
        glTexCoord2f(t, v);
        glVertex2d(x + width, y);
        glTexCoord2f(u, v);
        glVertex2d(x, y);
        glTexCoord2f(u, s);
        glVertex2d(x, y + height);
        glTexCoord2f(u, s);
        glVertex2d(x, y + height);
        glTexCoord2f(t, s);
        glVertex2d(x + width, y + height);
        glTexCoord2f(t, v);
        glVertex2d(x + width, y);
        glEnd();
	}

	/**
	 * Draws a bordered rectangle at the coordinates specified with the hexadecimal color.
	 */
	public static void drawBorderedRect(double x, double y, double x1, double y1, float width, int internalColor, int borderColor) {
		glColor(internalColor);
		drawRect(x + width, y + width, x1 - width, y1 - width);
		glColor(borderColor);
		drawRect(x + width, y, x1 - width, y + width);
		drawRect(x, y, x + width, y1);
		drawRect(x1 - width, y, x1, y1);
		drawRect(x + width, y1 - width, x1 - width, y1);
	}
	
	public static void drawBorder(double x, double y, double x1, double y1, float width, int borderColor) {
		glColor(borderColor);
		drawRect(x + width, y, x1 - width, y + width);
		drawRect(x, y, x + width, y1);
		drawRect(x1 - width, y, x1, y1);
		drawRect(x + width, y1 - width, x1 - width, y1);
	}
	
	public static void glScissor(float x, float y, float x1, float y1) {
		int factor = 2;
		GL11.glScissor((int) (x * factor), (int) (mc.displayHeight - (y1 * factor)), (int) ((x1 - x) * factor), (int) ((y1 - y) * factor));
	}
	
	public static void setupOverlayDefaultScale() {
        double scale = 2;
        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glLoadIdentity();
        GL11.glOrtho(0.0D, mc.displayWidth / scale, mc.displayHeight / scale, 0.0D, 1000.0D, 3000.0D);
        GL11.glMatrixMode(GL11.GL_MODELVIEW);
        GL11.glLoadIdentity();
        GL11.glTranslatef(0.0F, 0.0F, -2000.0F);
        GL11.glViewport(0, 0, mc.displayWidth, mc.displayHeight);
    }
    
	public static void draw3dItem(ItemStack itemStack, int x, int y,
			float delta) {
		if (itemStack == null)
			return;
		RenderHelper.enableGUIStandardItemLighting();
		try {
        	mc.getRenderItem().zLevel = -100F;
			mc.getRenderItem().renderItemAndEffectIntoGUI(itemStack, x, y);
//        	mc.getRenderItem().renderItemOverlayIntoGUI(mc.fontRenderer, itemStack, x, y, "");
        	mc.getRenderItem().zLevel = 0F;
        } catch (Exception e) {
        	e.printStackTrace();
        }
		RenderHelper.disableStandardItemLighting();
		GlStateManager.enableAlpha();
		GlStateManager.disableBlend();
		GlStateManager.disableLighting();
		GlStateManager.clear(256);
	}
	
	public static void draw2dItem(ItemStack itemStack, int x, int y,
			float delta) {
		if (itemStack == null)
			return;
		glPushMatrix();
        RenderHelper.enableGUIStandardItemLighting();
        glEnable(GL_DEPTH_TEST);
       	glEnable(GL_LIGHTING);
        try {
        	glTranslatef(0.0F, 0.0F, 32.0F);
        	mc.getRenderItem().zLevel = 200F;
			mc.getRenderItem().renderItemAndEffectIntoGUI(itemStack, x, y);
        	mc.getRenderItem().renderItemOverlayIntoGUI(mc.fontRenderer, itemStack, x, y, "");
        	mc.getRenderItem().zLevel = 0F;
        } catch (Exception e) {
        	e.printStackTrace();
        }
       	glDisable(GL_LIGHTING);
       	glDisable(GL_DEPTH_TEST);
        RenderHelper.disableStandardItemLighting();
        glPopMatrix();
	}
	
	public static int getScaleFactor() {
		int scaleFactor = 1;
		boolean isUnicode = mc.isUnicode();
		int scaleSetting = mc.gameSettings.guiScale;

		if (scaleSetting == 0) {
			scaleSetting = 1000;
		}
		while (scaleFactor < scaleSetting && mc.displayWidth / (scaleFactor + 1) >= 320 && mc.displayHeight / (scaleFactor + 1) >= 240) {
			scaleFactor++;
		}

		if (isUnicode && scaleFactor % 2 != 0 && scaleFactor != 1) {
			scaleFactor--;
		}
		return scaleFactor;
	}
}
