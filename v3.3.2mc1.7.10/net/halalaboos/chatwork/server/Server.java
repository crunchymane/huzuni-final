package net.halalaboos.chatwork.server;

import java.io.IOException;

public final class Server {

	private static ChatworkServer server;
	
	private Server() {
		
	}
	
	public static void main(String[] args) {
		try {
			server = new ChatworkServer(50);
			server.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static ChatworkClient getUser(String username) {
		for (ChatworkClient client : server.getAuthenticatedClients()) {
			if (client.getUsername().equalsIgnoreCase(username))
				return client;
		}
		return null;
	}
	
	public static void broadcast(String message) {
		server.broadcast(message);
	}
	
	public static ChatworkServer getServer() {
		return server;
	}
	
}
