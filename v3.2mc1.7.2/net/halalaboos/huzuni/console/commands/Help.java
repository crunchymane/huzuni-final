package net.halalaboos.huzuni.console.commands;

import net.halalaboos.huzuni.console.Command;
import net.halalaboos.huzuni.helpers.Helper;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.MathHelper;

import java.util.ArrayList;
import java.util.List;

public class Help implements Command {
	private final int COMMAND_PER_PAGE = 4;

	@Override
	public String[] getAliases() {
		return new String[] { "help" };
	}

	@Override
	public String[] getHelp() {
		return new String[] { "help <command|page>" };
	}

	@Override
	public String getDescription() {
		return "Gives you all of the help commands.";
	}

	@Override
	public void run(String originalString, String[] args) {
		if (args != null) {
			if (Helper.stringHelper.isInteger(args[0])) {
				listHelp(Integer.parseInt(args[0]));
			} else {
				Command command = findCommand(args[0]);
				if (command != null) {
					huzuni.addChatMessage("Listing help for '" + args[0] + "'");
					for (String help : command.getHelp()) {
						huzuni.addChatMessage(help);
					}
				} else
					huzuni.addChatMessage("Command not found!");
			}
		} else {
			listHelp(1);
		}
	}

	/**
	 * @param string
	 * @return 
	 */
	private Command findCommand(String string) {
		for (Command command : huzuni.commandManager.getList()) {
			for (String alias : command.getAliases()) {
				if (string.equalsIgnoreCase(alias)) {
					return command;
				}
			}
		}
		return null;
	}

	/**
	 * @return Lists all commands for the page.
	 * */
	public void listHelp(int wantedPage) {
		int pages = getPages();
		boolean hasMoreCommands = wantedPage < pages;
		List<Command> commandsOnPage = getCommandsOnPage(wantedPage);
		if (commandsOnPage.isEmpty() || wantedPage <= 0) {
			huzuni.addChatMessage("\247q'" + wantedPage + "' is an invalid page!");
			return;
		}
		huzuni.addChatMessage(EnumChatFormatting.GOLD + "-------------[" + EnumChatFormatting.GRAY + "Help (" + wantedPage + "/" + pages + ")" + EnumChatFormatting.GOLD + "]-------------");
		for (Command command : commandsOnPage)
			huzuni.addChatMessage("." + EnumChatFormatting.GOLD + command.getAliases()[0] + EnumChatFormatting.GRAY + " - " + command.getDescription());
	}

	/**
	 * @return All commands for the page.
	 * */
	private List<Command> getCommandsOnPage(int page) {
		List<Command> tempList = new ArrayList<Command>();
		int pages = getPages(),
		pageCount = 1, 
		commandCount = 0;

		for (int i = 0; i < huzuni.commandManager.length(); i++) {
			Command command = huzuni.commandManager.get(i);
			if (command != this) {
				if (commandCount >= COMMAND_PER_PAGE) {
					pageCount++;
					commandCount = 0;
				}
				if (pageCount == page) {
					tempList.add(command);
				}
				commandCount++;
			}
		}
		return tempList;
	}

	/**
	 * @return Amount of pages of commands we have.
	 * */
	private int getPages() {
		boolean isDividedEvenly = false;// (float) (huzuni.commandManager.length() - 1) % (float) COMMAND_PER_PAGE == 0;
		return MathHelper.ceiling_float_int((float) (huzuni.commandManager.length() - 1) / (float) COMMAND_PER_PAGE - (isDividedEvenly ? 1 : 0));
	}

}
