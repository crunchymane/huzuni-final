package net.halalaboos.huzuni.gui.clickable;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.input.Mouse;

import net.halalaboos.huzuni.api.gui.Container;
import net.halalaboos.huzuni.api.gui.render.Theme;
import net.halalaboos.huzuni.util.GLUtils;
import net.halalaboos.huzuni.util.Timer;

public class ContainerManager <T extends Container> {
	
	private List<T> containers = new ArrayList<T>();
	
	private T mousedContainer, downContainer, selectedContainer;
	
	private Theme theme;
	
	private int lastMouseButton = 0;
	
	private boolean layering = true;
	
	private final Timer tooltipTimer = new Timer();
	
	public ContainerManager(Theme theme) {
		this.theme = theme;
	}
	
	public void render() {
		calculateMousedContainer();
		calculateMousedown();
		theme.renderBackground(GLUtils.getScreenWidth(), GLUtils.getScreenHeight());
		for (int i = 0; i < containers.size(); i++) {
			T container = containers.get(i);
			if (container.isActive()) {
				container.setMouseOver(downContainer == null ? mousedContainer == container : downContainer == container && mousedContainer == container);
				container.setMouseDown(downContainer == container);
				container.update();
				theme.renderContainer(container);
				container.renderComponents(theme);
			}
		}
		theme.renderForeground(GLUtils.getScreenWidth(), GLUtils.getScreenHeight());
		if (mousedContainer != null) {
			if (mousedContainer.getTooltip() != null && !Mouse.isButtonDown(0)) {
				if (tooltipTimer.hasReach(500))
					theme.renderTooltip(mousedContainer.getTooltip());
			} else
				tooltipTimer.reset();
		}
	}
	
	private void calculateMousedContainer() {
		int mouseX = GLUtils.getMouseX(), mouseY = GLUtils.getMouseY();
		for (int i = containers.size() - 1; i >= 0; i--) {
			T container = containers.get(i);
			if (container.isActive()) {
				if (container.isPointInside(mouseX, mouseY)) {
					this.mousedContainer = container;
					return;
				}
			}
		}
		this.mousedContainer = null;
	}
	
	private void calculateMousedown() {
		if (downContainer != null) {
			if (!Mouse.isButtonDown(lastMouseButton)) {
				downContainer.mouseReleased(GLUtils.getMouseX(), GLUtils.getMouseY(), lastMouseButton);
				downContainer = null;
			}
		}
	}
	
	public void keyTyped(int keyCode, char c) {
		if (selectedContainer != null && downContainer == null) {
			if (selectedContainer.isActive()) {
				selectedContainer.keyTyped(keyCode, c);
			}
		}
	}
	
	public void mouseClicked(int x, int y, int buttonId) {
		lastMouseButton = buttonId;
		for (int i = containers.size() - 1; i >= 0; i--) {
			T container = containers.get(i);
			if (container.isActive()) {
				if (container.mouseClicked(x, y, buttonId)) {
					this.selectedContainer = container;
					if (downContainer == null) {
						if (layering) {
							containers.remove(container);
							containers.add(containers.size(), container);
						}
						this.downContainer = container;
					}
					return;
				}
			}
		}
	}

	public Theme getTheme() {
		return theme;
	}

	public void setTheme(Theme theme) {
		this.theme = theme;
	}

	public boolean isLayering() {
		return layering;
	}

	public void setLayering(boolean layering) {
		this.layering = layering;
	}
	
	public void add(T container) {
		this.containers.add(container);
	}

	public void remove(T container) {
		this.containers.remove(container);
	}
	
	public boolean contains(T container) {
		return this.containers.contains(container);
	}

	public List<T> getContainers() {
		return containers;
	}

	public void setContainers(List<T> containers) {
		this.containers = containers;
	}
	
}
