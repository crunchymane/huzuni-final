package net.halalaboos.huzuni.ui.screen.keybinds;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;

import javax.swing.SwingUtilities;

import com.darkmagician6.morbidlib.utils.timer.AccurateTimer;
import com.darkmagician6.morbidlib.utils.timer.Timer;

import net.halalaboos.huzuni.mods.Keybindable;
import net.halalaboos.huzuni.mods.Module;
import net.halalaboos.huzuni.utils.LoginUtils;
import net.halalaboos.lib.ui.components.SlotComponent;
import net.halalaboos.lib.ui.utils.GLUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.util.Session;

public class SlotKeybinds extends SlotComponent<Module> {
	private final Color highlightColor = new Color(0F, 0.5F, 1F, 0.75F),
	defaultColor = new Color(0.4F, 0.4F, 0.4F, 0.75F);
	private final KeybindsManager guiKeybinds;

	private int selectedItem = -1;
	private Timer timer = new AccurateTimer();
	
	public SlotKeybinds(KeybindsManager guiKeybinds, Dimension dimensions) {
		super(dimensions);
		this.guiKeybinds = guiKeybinds;
	}

	@Override
	public void render(int index, Module mod, Rectangle boundaries,
			boolean mouseOver, boolean mouseDown) {
		Keybindable keybind = (Keybindable) mod;
		String keybindName = keybind.getKeyCode() == -1 ? "None" : keybind.getKeyName();
		int centerX = boundaries.x + boundaries.width / 2 - GLUtils.getStringWidth(mod.getName()) / 2;
		int keybindX = boundaries.x + boundaries.width / 2 - GLUtils.getStringWidth(keybindName) / 2;

		GLUtils.setColor(GLUtils.getColorWithAffects(selectedItem == index ? highlightColor : defaultColor, mouseOver, mouseDown));
		GLUtils.drawFilledRect(boundaries.x, boundaries.y, boundaries.x + boundaries.width, boundaries.y + boundaries.height);
		GLUtils.drawStringWithShadow(mod.getName(), centerX + 2, boundaries.y + 2, 0xFFFFFF);
		GLUtils.drawStringWithShadow(keybindName, keybindX + 2, boundaries.y + 12, keybind.getKeyCode() != -1 ? 0xFF3399FF : 0xFFFFFF);

	}
	
	@Override
	public void onKeyTyped(int keyCode, char c) {
		super.onKeyTyped(keyCode, c);
		if (selectedItem != -1) {
			((Keybindable)components.get(selectedItem)).setKeyCode(keyCode);
			selectedItem = -1;
		}
	}

	@Override
	public void onClicked(int index, Module mod, Rectangle boundaries,
			Point mouse, int buttonID) {
		if (selectedItem == index) {
			selectedItem = -1;
		} else {
			selectedItem = index;
		}
		timer.reset();
	}

	@Override
	public void onReleased(int index, Module mod, Point mouse) {
	}

	@Override
	public int getElementHeight() {
		return 22;
	}
	
	public void clear() {
		if (selectedItem != -1) {
			((Keybindable)components.get(selectedItem)).setKeyCode(-1);
			selectedItem = -1;
		}
	}

	public int getSelectedItem() {
		return selectedItem;
	}

	public void setSelectedItem(int selectedItem) {
		this.selectedItem = selectedItem;
	}
	
}
