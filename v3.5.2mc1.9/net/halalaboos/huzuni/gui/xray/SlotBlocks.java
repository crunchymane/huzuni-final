package net.halalaboos.huzuni.gui.xray;

import java.awt.Rectangle;

import org.lwjgl.input.Mouse;

import net.halalaboos.huzuni.api.gui.render.Theme;
import net.halalaboos.huzuni.gui.clickable.slot.SlotComponent;
import net.halalaboos.huzuni.mod.mods.world.Xray;
import net.halalaboos.huzuni.util.GLUtils;
import net.halalaboos.huzuni.util.MathUtils;
import net.minecraft.client.Minecraft;

public class SlotBlocks extends SlotComponent<BlockInfo[]> {
	
	private BlockInfo mousedDownBlockInfo = null,
			mousedOverBlockInfo = null;
    
	public SlotBlocks(float width, float height) {
		super(width, height);
		
	}
	
	@Override
	public void update() {
		super.update();

		if (mousedOverBlockInfo != null)
			this.setTooltip(mousedOverBlockInfo.getName());
		else
			this.setTooltip(null);
		mousedOverBlockInfo = null;
	}
	
	@Override
	protected void onReleased(int index, BlockInfo[] itemStacks, int mouseX, int mouseY) {
		
	}

	@Override
	protected void onClicked(int index, BlockInfo[] blocks, float[] area, int mouseX, int mouseY, int buttonId) {
		int xPos = 0;
		for (BlockInfo blockInfo : blocks) {
			if (blockInfo == null)
				continue;
			Rectangle bounds = new Rectangle((int) area[0] + xPos, (int) area[1], 20, 20);
			if (bounds.contains(mouseX, mouseY)) {
				mousedDownBlockInfo = blockInfo;
				checkBlock(blockInfo);
				Xray.instance.save();
				break;
			}
			xPos += 21;
		}
	}
	
	private void checkBlock(BlockInfo blockInfo) {
		blockInfo.toggleBlocks();
		if (Xray.instance.isEnabled())
			Minecraft.getMinecraft().renderGlobal.loadRenderers();
	}
	
	@Override
	protected void render(Theme theme, int index, BlockInfo[] blocks, float[] area, boolean highlight, boolean mouseOver, boolean mouseDown) {
		int xPos = 0;
		for (BlockInfo blockInfo : blocks) {
			if (blockInfo == null)
				continue;
			float[] bounds = new float[] { area[0] + xPos, area[1], 20, 20 };
			boolean mouseOverItemStack = mouseOver && MathUtils.inside(GLUtils.getMouseX(), GLUtils.getMouseY(), bounds) && (mousedDownBlockInfo == null ? true : mousedDownBlockInfo == blockInfo),
			mouseDownItemStack = mouseDown && mousedDownBlockInfo == blockInfo;
			theme.renderSlot(this.getOffsetX(), this.getOffsetY(), index, bounds, blockInfo.isEnabled(), mouseOverItemStack, mouseDownItemStack);
			GLUtils.draw2dItem(blockInfo.getRenderItem(), (int) bounds[0] + 2, (int) bounds[1] + 2, 0);
			xPos += 21;
			if (mouseOver && mouseOverItemStack && mousedOverBlockInfo == null)
				mousedOverBlockInfo = blockInfo;
		}
		if (!Mouse.isButtonDown(0))
			mousedDownBlockInfo = null;
	}

	@Override
	protected int getComponentHeight(BlockInfo[] block) {
		return 20;
	}

}
