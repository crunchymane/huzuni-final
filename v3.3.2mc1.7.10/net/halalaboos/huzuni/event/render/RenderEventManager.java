package net.halalaboos.huzuni.event.render;

import net.halalaboos.huzuni.api.event.EventManager;

/**
 * @author brudin
 * @version 1.0
 * @since 4/10/14
 */
public class RenderEventManager extends EventManager<RenderEvent, Renderable> {

	@Override
	protected void invoke(Renderable listener, RenderEvent event) {
		listener.render(event);
	}

}