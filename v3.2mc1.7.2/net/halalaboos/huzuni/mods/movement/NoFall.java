package net.halalaboos.huzuni.mods.movement;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;

import net.halalaboos.huzuni.events.game.EventPostMotionUpdate;
import net.halalaboos.huzuni.events.game.EventPreMotionUpdate;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.DefaultModule;

import org.lwjgl.input.Keyboard;

public class NoFall extends DefaultModule implements Listener {

    public NoFall() {
        super("NoFall", Keyboard.KEY_N);
        setCategory(Category.MOVEMENT);
        setDescription("Stops all fall damage.");
    }

    @Override
    protected void onEnable() {
        registry.registerListener(this);
    }

    @Override
    protected void onDisable() {
        registry.unregisterListener(this);
    }

    @Override
    protected void onToggle() {
    }

    @EventTarget
    public void onPreMotionUpdate(EventPreMotionUpdate event) {
        if (mc.thePlayer.fallDistance > 3) {
            mc.thePlayer.onGround = true;
        }
    }

    @EventTarget
    public void onPostMotionUpdate(EventPostMotionUpdate event) {
        if (mc.thePlayer.fallDistance > 3)
            mc.thePlayer.onGround = false;
    }
}
