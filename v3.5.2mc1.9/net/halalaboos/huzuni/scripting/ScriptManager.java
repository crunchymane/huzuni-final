package net.halalaboos.huzuni.scripting;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import net.halalaboos.huzuni.scripting.wrapping.ScriptWrapper;
import net.halalaboos.huzuni.util.GLUtils;
import net.minecraft.client.Minecraft;

public class ScriptManager {

	private static final ScriptEngine engine = genScriptEngine();
	
	private static ScriptEngine genScriptEngine() {
		ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("JavaScript");
        return engine;
	}
	
	public static void setupEngine() {
		engine.put("huzuni", ScriptWrapper.instance);
		engine.put("gl", GLUtils.instance);
	}
	
	private static void setupVariables() {
		if (Minecraft.getMinecraft().thePlayer != null) {
			engine.put("playerName", Minecraft.getMinecraft().thePlayer.getName());
			engine.put("playerX", (int) Minecraft.getMinecraft().thePlayer.posX);
			engine.put("playerY", (int) Minecraft.getMinecraft().thePlayer.posY);
			engine.put("playerZ", (int) Minecraft.getMinecraft().thePlayer.posZ);
			engine.put("yaw", Minecraft.getMinecraft().thePlayer.rotationYaw);
			engine.put("pitch", Minecraft.getMinecraft().thePlayer.rotationPitch);
		}
		engine.put("screenWidth", GLUtils.getScreenWidth());
		engine.put("screenHeight", GLUtils.getScreenHeight());
	}
	
	public static Invocable runScript(String script) {
		setupVariables();
		try {
			engine.eval(script);
			return (Invocable) engine;
		} catch (ScriptException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static Invocable runScript(File file) {
		setupVariables();
		try {
			engine.eval(new FileReader(file));
			return (Invocable) engine;
		} catch (ScriptException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void invoke(String functionName, Object... params) throws NoSuchMethodException, ScriptException {
		setupVariables();
		((Invocable) engine).invokeFunction(functionName, params);
	}
	
}
