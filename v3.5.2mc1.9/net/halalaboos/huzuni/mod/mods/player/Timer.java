package net.halalaboos.huzuni.mod.mods.player;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.mod.Category;
import net.halalaboos.huzuni.api.mod.ValueOption;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent;
import net.halalaboos.huzuni.event.tick.UpdateListener;
import net.halalaboos.huzuni.mod.DefaultMod;

/**
 * @since 9:08 PM on 3/21/2015
 */
public class Timer extends DefaultMod implements UpdateListener {

	private ValueOption timerSpeed = new ValueOption("Multiplier", 0.1F, 1F, 5F, 0.1F, "Timer speed multiplier");

	public Timer() {
		super("Timer");
		setAuthor("brudin");
		setDescription("Changes the timer speed.");
		setCategory(Category.PLAYER); //idk wat category
		this.setOptions(timerSpeed);
	}

	@Override
	protected void onEnable() {
		Huzuni.registerUpdateListener(this);
		mc.timer.timerSpeed = timerSpeed.getValue();
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterUpdateListener(this);
		mc.timer.timerSpeed = 1;
	}

	@Override
	public void onPreUpdate(MotionUpdateEvent event) {
		if (mc.currentScreen == null) {
			mc.timer.timerSpeed = timerSpeed.getValue();
			setRenderName("Timer [" + (int)(timerSpeed.getValue() * 100) + "%]");
		} else {
			mc.timer.timerSpeed = 1;
		}
	}

	@Override
	public void onPostUpdate(MotionUpdateEvent event) {

	}
}
