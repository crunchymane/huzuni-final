package net.halalaboos.huzuni.plugin.plugins.world;

import net.halalaboos.huzuni.api.plugin.Category;
import net.halalaboos.huzuni.plugin.DefaultPlugin;
import net.minecraft.network.play.client.C12PacketUpdateSign;
import net.minecraft.tileentity.TileEntitySign;

public class AutoSign extends DefaultPlugin {
	
	public static final AutoSign instance = new AutoSign();
	
	private String[] lines = new String[] {
			"Halalaboos", "is", "awesome", ""
	};
	
	private AutoSign() {
		super("Auto Sign");
		setCategory(Category.WORLD);
		setDescription("Automatically place down a sign with text.");
	}
	
	public void placeSign(final TileEntitySign sign) {
		sign.field_145915_a = lines;
		sign.onInventoryChanged();
		sign.func_145913_a(true);
		mc.getNetHandler().addToSendQueue(new C12PacketUpdateSign(sign.field_145851_c, sign.field_145848_d, sign.field_145849_e, sign.field_145915_a)); 
	}

	@Override
	protected void onEnable() {
	}

	@Override
	protected void onDisable() {
	}

	public String[] getLines() {
		return lines;
	}

	public void setLines(String[] lines) {
		this.lines = lines;
	}
	
	
}
