/**
 * 
 */
package net.halalaboos.huzuni.ui.windows.xray;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.helpers.MinecraftHelper;
import net.halalaboos.lib.ui.components.SlotComponent;
import net.halalaboos.lib.ui.utils.GLUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

/**
 * @author Halalaboos
 *
 * @since Sep 23, 2013
 */
public class SlotBlocks extends SlotComponent<ItemStack[]> implements MinecraftHelper {
	final Color highlightColor = new Color(0F, 0.5F, 1F, 0.75F),
    	defaultColor = new Color(0.3F, 0.3F, 0.3F, 0.75F);
    final RenderItem itemRenderer = new RenderItem();
    ItemStack mousedDownItemStack = null;
	/**
	 * @param dimensions
	 */
	public SlotBlocks(Dimension dimensions) {
		super(dimensions);
		
	}
    
	@Override
	public void render(int index, ItemStack[] itemStacks, Rectangle boundaries,
			boolean mouseOver, boolean mouseDown) {
		int xPos = 0;
		Point mouse = new Point(GLUtils.getMouseX(), GLUtils.getMouseY());
		for (ItemStack itemStack : itemStacks) {
			if (itemStack == null)
				continue;
			Rectangle area = new Rectangle(boundaries.x + xPos, boundaries.y, 20, 20);
			boolean mouseOverItemStack = mouseOver && area.contains(mouse) && (mousedDownItemStack == null ? true : mousedDownItemStack == itemStack),
			mouseDownItemStack = mouseDown && mousedDownItemStack == itemStack;
			GLUtils.setColor(GLUtils.getColorWithAffects(Huzuni.xrayManager.contains(Item.func_150891_b(itemStack.getItem())) ? highlightColor : defaultColor, mouseOverItemStack, mouseDownItemStack));
            GLUtils.drawFilledRect(area.x, area.y, area.x + area.width, area.y + area.height);
            GL11.glPushMatrix();
            GL11.glDisable(GL11.GL_BLEND);
            GL11.glEnable(GL12.GL_RESCALE_NORMAL);
            RenderHelper.enableGUIStandardItemLighting();
            GL11.glDepthMask(true);
            GL11.glDisable(GL11.GL_DEPTH_TEST);
            try {
            	itemRenderer.renderItemIntoGUI(mc.fontRenderer, mc.getTextureManager(), itemStack, area.x + 2, area.y + 2);
            	itemRenderer.renderItemOverlayIntoGUI(mc.fontRenderer, mc.getTextureManager(), itemStack, area.x + 2, area.y + 2);
            } catch (Exception e) {
            	e.printStackTrace();
            }
            RenderHelper.disableStandardItemLighting();
            GL11.glDisable(GL12.GL_RESCALE_NORMAL);
            GL11.glEnable(GL11.GL_DEPTH_TEST);
            GL11.glDepthMask(true);
            GL11.glPopMatrix();
			xPos += 21;
		}
		if (!Mouse.isButtonDown(0))
			mousedDownItemStack = null;
	}

	@Override
	public void onClicked(int index, ItemStack[] itemStacks,
			Rectangle boundaries, Point mouse, int buttonID) {
		if (buttonID == 0) {
			int xPos = 0;
			for (ItemStack itemStack : itemStacks) {
				if (itemStack == null)
					continue;
				Rectangle area = new Rectangle(boundaries.x + xPos, boundaries.y, 20, 20);
				if (area.contains(mouse)) {
					mousedDownItemStack = itemStack;
					checkBlock(itemStack);
					break;
				}
				xPos += 21;
			}
		}
	}

	@Override
	public void onReleased(int index, ItemStack[] itemStacks, Point mouse) {
	}
	
	@Override
	public int getElementHeight() {
		return 20;
	}
	
	private void checkBlock(ItemStack itemStack) {
		int itemID = Item.func_150891_b(itemStack.getItem());
		if (Huzuni.xrayManager.contains(itemID)) {
			Huzuni.xrayManager.remove(itemID);
		} else {
			Huzuni.xrayManager.add(itemID);
		}
		if (Huzuni.xrayManager.getModule().isEnabled())
			Minecraft.getMinecraft().renderGlobal.loadRenderers();
	}
	
}
