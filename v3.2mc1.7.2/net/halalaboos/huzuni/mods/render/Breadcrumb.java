/**
 *
 */
package net.halalaboos.huzuni.mods.render;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;

import net.halalaboos.huzuni.console.Command;
import net.halalaboos.huzuni.events.game.EventLoadWorld;
import net.halalaboos.huzuni.events.game.EventTick;
import net.halalaboos.huzuni.events.render.EventRenderWorld;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.DefaultModule;
import net.minecraft.client.renderer.entity.RenderManager;

import org.lwjgl.util.vector.Vector3f;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import static org.lwjgl.opengl.GL11.*;

/**
 * @author Halalaboos
 * @since Aug 30, 2013
 */
public class Breadcrumb extends DefaultModule implements Listener {

	private final List<Vector3f> vertexes = new CopyOnWriteArrayList<Vector3f>();

    private int movementCount = 0;
        
    /**
     * @param name
     * @param keyCode
     */
    public Breadcrumb() {
        super("Breadcrumbs", -1);
        setCategory(Category.RENDER);
        setDescription("Draws a line behind your ass.");
        this.registerCommand(new Command() {

            @Override
            public String[] getAliases() {
                return new String[] {"breadclear", "breadcrumbclear", "bc"};
            }

            /**
             * @see net.halalaboos.huzuni.console.Command#getHelp()
             */
            @Override
            public String[] getHelp() {
                return new String[] {"You don't need help with this command."};
            }

            @Override
            public String getDescription() {
                return "Clears the breadcrumb lines.";
            }

            @Override
            public void run(String input, String[] args) {
                vertexes.clear();
                huzuni.addChatMessage("Cleared!");
            }

        });
    }

    /**
     * @see net.halalaboos.huzuni.mods.DefaultModule#onEnable()
     */
    @Override
    protected void onEnable() {
        registry.registerListener(this);
    }

    /**
     * @see net.halalaboos.huzuni.mods.DefaultModule#onDisable()
     */
    @Override
    protected void onDisable() {
        registry.unregisterListener(this);
    }

    /**
     * @see net.halalaboos.huzuni.mods.DefaultModule#onToggle()
     */
    @Override
    protected void onToggle() {
    }

    @EventTarget
    public void onTick(EventTick event) {
        Vector3f position = new Vector3f((float) mc.thePlayer.posX, (float) mc.thePlayer.posY, (float) mc.thePlayer.posZ);
        if (vertexes.isEmpty())
            vertexes.add(position);
        else {
            Vector3f lastPosition = vertexes.get(vertexes.size() - 1);
            double diffX = mc.thePlayer.prevPosX - mc.thePlayer.posX;
            double diffY = mc.thePlayer.prevPosY - mc.thePlayer.posY;
            double diffZ = mc.thePlayer.prevPosZ - mc.thePlayer.posZ;
            if (diffX != 0 || diffY != 0 || diffZ != 0) {
            	movementCount++;
            	if (movementCount >= 5) {
            		vertexes.add(position);
            		movementCount = 0;
            	}
            }
        }
    }

    @EventTarget
    public void render(EventRenderWorld event) {
        glBegin(GL_LINE_STRIP);
        glColor4f(1F, 0F, 0F, 1F);
        glVertex3d(0, -mc.thePlayer.height, 0);
        for (int i = vertexes.size() - 1; i >= 0; i--) {
            Vector3f vertex = vertexes.get(i);
            glVertex3d(vertex.x - RenderManager.renderPosX, vertex.y - RenderManager.renderPosY - mc.thePlayer.height, vertex.z - RenderManager.renderPosZ);
        }
        glEnd();
    }

    @EventTarget
    public void onLoadWorld(EventLoadWorld event) {
        this.vertexes.clear();
    }

}
