package com.darkmagician6.morbidlib.utils.timer;

/**
 * Used as a regular timer, only in seconds.
 * */
public final class SecondTimer extends Timer {
	
	@Override
	public long getCurrentTime() {
		return (long) /* MS converted to seconds */(System.nanoTime() / 1E6) / 1000L;
	}

}
