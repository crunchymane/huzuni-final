package net.halalaboos.huzuni.plugin;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.transform.TransformerException;

import net.halalaboos.huzuni.plugin.plugins.MiddleClickFriends;
import net.halalaboos.huzuni.plugin.plugins.render.StatusHUD;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.plugin.Plugin;
import net.halalaboos.huzuni.io.XMLFileHandler;
import net.halalaboos.huzuni.plugin.plugins.InventoryHelper;
import net.halalaboos.huzuni.plugin.plugins.chat.*;
import net.halalaboos.huzuni.plugin.plugins.combat.*;
import net.halalaboos.huzuni.plugin.plugins.movement.*;
import net.halalaboos.huzuni.plugin.plugins.player.*;
import net.halalaboos.huzuni.plugin.plugins.render.*;
import net.halalaboos.huzuni.plugin.plugins.world.*;

public final class PluginManager {

	private static final List<Plugin> plugins = new ArrayList<Plugin>();
	
	private static final List<DefaultPlugin> defaultPlugins = new ArrayList<DefaultPlugin>();

	public static final File PLUGIN_FILE = new File(Huzuni.SAVE_DIRECTORY, "Plugins.txt");

	private static final XMLFileHandler fileHandler = new XMLFileHandler(PLUGIN_FILE);
	
	private PluginManager() {
		
	}
	
	public static void setupPlugins() {
		registerPlugin(new AutoTool());
		registerPlugin(AntiKnockback.instance);
		registerPlugin(new AntiCensor());
		registerPlugin(new DolanSpeak());
		registerPlugin(new NoFall());
		registerPlugin(new Sprint());
		registerPlugin(new Respawn());
		registerPlugin(new Flight());
		registerPlugin(Xray.instance);
		registerPlugin(new Bright());
		registerPlugin(new KillAura());
		registerPlugin(new Speedmine());
		registerPlugin(new AutoFish());
		registerPlugin(new Dolphin());
		registerPlugin(new Glide());
		registerPlugin(new Retard());
		registerPlugin(new Sneak());
		registerPlugin(new AutoDisconnect());
		registerPlugin(new ChestESP());
		registerPlugin(new Tracer());
		registerPlugin(new AutoTPA());
		registerPlugin(new Spider());
		registerPlugin(Freecam.instance);
		registerPlugin(new Projectiles());
		registerPlugin(new Breadcrumb());
		registerPlugin(Nametags.instance);
		registerPlugin(new FastLadder());
		registerPlugin(new Step());
		registerPlugin(AutoSign.instance);
		registerPlugin(new Nuker());
		registerPlugin(new AutoFarm());
		registerPlugin(new Fastplace());
		registerPlugin(Wireframe.instance);
		registerPlugin(new ChestStealer());
		registerPlugin(new VehicleOneHit());
		registerPlugin(new AutoArmor());
		registerPlugin(new AutoSoup());
		registerPlugin(new ClickAimbot());
		registerPlugin(new AutoBuild());
		registerPlugin(new Regen());
		registerPlugin(new Criticals());
		registerPlugin(MiddleClickFriends.instance);
		registerPlugin(NoSlowdown.instance);
		registerPlugin(StatusHUD.instance);
		registerPlugin(InventoryHelper.instance);
		registerPlugin(new Superbow());
	}
	
	public static void loadPlugins() {
			try {
			// Read the document.
			Document doc = fileHandler.read();
			// Get a nodelist of all the plugin elements.
			NodeList elementList = doc.getElementsByTagName("Plugin");
			for (int i = 0; i < elementList.getLength(); i++) {
				Node node = elementList.item(i);
				// If it's an element.
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					Element element = (Element) node;
					try {
						String name = element.getAttribute("Name");
						Plugin plugin = getPlugin(name);                    		
						if (plugin != null) {
							boolean pluginState = Boolean.parseBoolean(element.getAttribute("Enabled").toUpperCase());
							for (int j = 0; j < plugin.length(); j++)
								plugin.get(j).load(element);
							boolean vipPlugin = plugin instanceof VipPlugin;
							boolean checkPluginState = vipPlugin ? Huzuni.isVip() : true;
							if (checkPluginState) {
								if (plugin.isEnabled() != pluginState) {
									plugin.setEnabled(pluginState);
								}
							}
						}
	
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void savePlugins() {
		// Setup the xml document.
        fileHandler.createDocument();
        Element pluginsRoot = fileHandler.createElement("Plugins");
        fileHandler.addElement(pluginsRoot);
        for (Plugin plugin : plugins) {
			Element pluginElement = fileHandler.createElement("Plugin");
			pluginsRoot.appendChild(pluginElement);
			pluginElement.setAttribute("Name", plugin.getName());
			pluginElement.setAttribute("Enabled", Boolean.toString(plugin.isEnabled()));
			for (int i = 0; i < plugin.length(); i++) {
				plugin.get(i).save(pluginElement);
			}
        }
        try {
            fileHandler.write();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
	}
	
	public static void registerPlugin(Plugin plugin) {
		plugins.add(plugin);
		if (plugin instanceof DefaultPlugin) {
			defaultPlugins.add((DefaultPlugin) plugin);
			Huzuni.addKeybind((DefaultPlugin) plugin);
		}
	}
	
	public static void unregisterPlugin(Plugin plugin) {
		plugins.remove(plugin);
		if (plugin instanceof DefaultPlugin) {
			defaultPlugins.remove((DefaultPlugin) plugin);
		}
	}
	
	public static List<Plugin> getPlugins() {
		return plugins;
	}
	
	public static List<DefaultPlugin> getDefaultPlugins() {
		return defaultPlugins;
	}

	public static Plugin getPlugin(String name) {
		for (int i = 0; i < plugins.size(); i++) {
			Plugin plugin = plugins.get(i);
			if (plugin.getName().replaceAll(" ", "").equalsIgnoreCase(name.replaceAll(" ", "")))
				return plugin;
		}
		return null;
	}
		
}
