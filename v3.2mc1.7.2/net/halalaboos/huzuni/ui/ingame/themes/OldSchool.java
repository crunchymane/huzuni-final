/**
 *
 */
package net.halalaboos.huzuni.ui.ingame.themes;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.mods.DefaultModule;
import net.halalaboos.huzuni.ui.ingame.IngameTheme;
import net.minecraft.client.Minecraft;

/**
 * @author Halalaboos
 * @since Aug 3, 2013
 */
public class OldSchool implements IngameTheme {

    /**
     * @see net.halalaboos.huzuni.ui.ingame.IngameTheme#render(net.minecraft.src.Minecraft, int, int)
     */
    @Override
    public void render(Minecraft mc, int screenWidth, int screenHeight) {
        mc.fontRenderer.drawStringWithShadow(Huzuni.TITLE + "\247r v" + Huzuni.VERSION, 2, 2, 0xFFFFFF);

        int yPos = 12;
        for (DefaultModule module : Huzuni.modManager.getDefaultModules()) {
            if (!module.getKeyName().equals("-1")) {
                mc.fontRenderer.drawStringWithShadow("[" + module.getKeyName() + "] " + module.getRenderName(), 2, yPos, module.isEnabled() ? 0x339900 : 0x990000);
                yPos += 11;
            }
        }
    }

    /**
     * @see net.halalaboos.huzuni.ui.ingame.IngameTheme#getName()
     */
    @Override
    public String getName() {
        return "Old School";
    }

    /**
     * @see net.halalaboos.huzuni.ui.ingame.IngameTheme#onKeyPressed(int)
     */
    @Override
    public void onKeyPressed(int keyCode) {
    }

}
