package net.halalaboos.huzuni.util.threads;

import net.halalaboos.huzuni.Huzuni;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;


public class ThreadReadSplash extends Thread {

	private static String splash;

	@Override
	public void run() {
		if (splash == null) {
			try {
				splash = "";
				final BufferedReader reader = new BufferedReader(new InputStreamReader(new URL("http://halalaboos.net/client/splash").openStream()));
				for (String s; (s = reader.readLine()) != null; )
					splash += s.trim();
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		Huzuni.setSplash(splash);
	}
}
