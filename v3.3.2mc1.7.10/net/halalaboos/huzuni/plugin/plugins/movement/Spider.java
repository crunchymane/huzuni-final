/**
 * 
 */
package net.halalaboos.huzuni.plugin.plugins.movement;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.plugin.Category;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent;
import net.halalaboos.huzuni.event.tick.UpdateListener;
import net.halalaboos.huzuni.plugin.DefaultPlugin;

/**
 * @author Halalaboos
 *
 * @since Oct 10, 2013
 */
public class Spider extends DefaultPlugin implements UpdateListener {

	public Spider() {
		super("Spider");
		setCategory(Category.MOVEMENT);
		setDescription("You climb walls like a spider.");
	}

	@Override
	protected void onEnable() {
		Huzuni.registerUpdateListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterUpdateListener(this);
	}

	@Override
	public void onPreUpdate(MotionUpdateEvent event) {
		if (mc.thePlayer.isCollidedHorizontally) {
			mc.thePlayer.motionY = 0.2F;
			mc.thePlayer.onGround = true;
		}
	}

	@Override
	public void onPostUpdate(MotionUpdateEvent event) {
	}

}
