package net.halalaboos.huzuni.gui.clickable.theme;

import java.awt.Color;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.gui.clickable.button.Button;
import net.halalaboos.huzuni.gui.clickable.theme.DefaultTheme;
import net.halalaboos.huzuni.util.GLUtils;
import net.halalaboos.huzuni.util.OldGLUtils;

public class BlastoffTheme extends DefaultTheme {

	private final Color gradientTop = new Color(76, 76, 76, 255), gradientBottom = new Color(53, 53, 53, 255);
	
	@Override
	public String getName() {
		return "Blastoff";
	}
	
	private void renderButton(float x, float y, float x1, float y1, boolean on, boolean hover, boolean down) {
		int gradientTop = GLUtils.getColorWithAffects(on ? Huzuni.getColorTheme() : this.gradientTop, hover, down).getRGB(),
			gradientBottom = GLUtils.getColorWithAffects(on ? Huzuni.getColorTheme().darker() : this.gradientBottom, hover, down).getRGB();
		GLUtils.drawBorderedRect(x, y, x1, y1, 0.5F, 0, 0xFF333333);
		OldGLUtils.drawGradientRect(x + 0.5F, y + 0.5F, x1 - 0.5F, y1 - 0.5F, gradientTop, gradientBottom);
		if (hover)
			OldGLUtils.drawRect(x + 0.5F, y + 0.5F, x1 - 0.5F, y1 - 0.5F, 0x1F000000);
	}

	@Override
	protected void renderButtonRect(float x, float y, float x1, float y1, boolean highlight, boolean mouseOver, boolean mouseDown) {
		renderButton(x, y, x1, y1, highlight, mouseOver, mouseDown);
	}

	@Override
	protected void renderWindowRect(float x, float y, float x1, float y1, float tabHeight, float lineWidth) {
		OldGLUtils.drawGradientRect(x, y, x1, y1, 0xFF303030, 0xFF222222);
		OldGLUtils.drawBorder(x, y, x1, y1, 0.5F, 0xFF555555);
	}

	@Override
	protected int getButtonTextColor(Button button) {
		return 0xFFFFFF;
	}

	@Override
	protected int getWindowTextColor() {
		return 0xFFFFFF;
	}
	
	@Override
	protected int getTooltipTextColor() {
		return 0xFFFFFF;
	}

	@Override
	protected void renderBackgroundRect(float x, float y, float x1, float y1,
			boolean highlight, boolean mouseOver, boolean mouseDown) {
		renderButton(x, y, x1, y1, highlight, mouseOver, mouseDown);
	}

	@Override
	protected void renderDropdownBackgroundRect(float x, float y, float x1,
			float y1, boolean highlight, boolean mouseOver, boolean mouseDown) {
		renderButton(x, y, x1, y1, highlight, mouseOver, mouseDown);
	}
}
